﻿using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Image.Contracts
{
    public interface IImageCdnService
    {
        CdnResult Upload(CdnImageUploadParameter uploadParameter);
        CdnImageUrlResult GetContentUrl(CdnImageUrlQueryParameter queryParameter);
        CdnResult Remove(CdnImageUrlQueryParameter queryParameter);

        IEnumerable<CdnResult> Upload(IEnumerable<CdnImageUploadParameter> uploadParameters);
        IEnumerable<CdnImageUrlResult> GetContentUrls(IEnumerable<CdnImageUrlQueryParameter> queryParameters);
        IEnumerable<CdnResult> Remove(IEnumerable<CdnImageUrlQueryParameter> queryParameters);
    }
}
