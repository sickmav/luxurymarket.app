﻿using LuxuryApp.Cdn.Contracts.Model;
using System.Collections.Generic;

namespace LuxuryApp.Cdn.Image.Contracts.Model
{
    public class CdnImageUrlResult: CdnResult<IEnumerable<CdnImageUrl>>
    {
        public CdnImageUrlResult(string key, IEnumerable<CdnImageUrl> result, CdnException exception = null)
            :base (key, result, exception)
        {

        }
    }
}
