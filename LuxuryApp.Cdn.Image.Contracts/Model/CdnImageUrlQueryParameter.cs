﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Image.Contracts.Model
{
    public class CdnImageUrlQueryParameter
    {
        public string Key { get; set; }
        public IEnumerable<CdnImageSize> ImageSizes { get; set; }
    }
}
