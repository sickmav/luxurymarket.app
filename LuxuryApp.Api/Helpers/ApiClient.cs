﻿using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Contracts.Helpers;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace LuxuryApp.Api.Helpers
{
    public static class ApiClient
    {
        public static async Task<T> PostContentAsync<T>(string token, Uri baseUri, string requestUri, object values)
        {
            T responseT = default(T);
            using (var client = new HttpClient())
            {
                client.BaseAddress = baseUri;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", token);  //with bearer
                var response = await client.PostAsJsonAsync(requestUri, values);
                if (response.IsSuccessStatusCode)
                    responseT = await response.Content.ReadAsAsync<T>();
                return responseT;
            }
        }

        public static async Task UploadFile(string filePath, string token)
        {
            var content = File.ReadAllBytes(filePath);
            var responseUpload = await PostContentAsync<CdnResult>(token, WebConfigs.LuxuryMarketCDNUrl, "api/filecdn/uploadsingle", new CdnUploadParameter { Key = Path.GetFileName(filePath), Content = content });
            if ((responseUpload as CdnResult)?.IsSuccessful != true)
            {
                Trace.WriteLine("Error upload");
            }
        }
    }
}