﻿using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using LuxuryApp.Core.Infrastructure.Handlers;
using System.Web.Http.ExceptionHandling;
using LuxuryApp.Core.Infrastructure.Api.Loggers;
using LuxuryApp.Core.Infrastructure.Localization;

namespace LuxuryApp.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes

            config.MapHttpAttributeRoutes();
            //config.MessageHandlers.Add(new LanguageMessageHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Formatting.None;
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            config.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;

            //exceptions handler and logger
            config.Services.Replace(typeof(IExceptionHandler), new LuxuryExceptionHandler());
            config.Services.Add(typeof(IExceptionLogger), new ApiExceptionLogger());

        }
    }
}
