﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/users")]
    public class UserController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IContextProvider _contextProvider;

        public UserController(ICustomerService customerService, IContextProvider contextProvider)
        {
            _customerService = customerService;
            _contextProvider = contextProvider;
        }

        /// <summary>
        ///Get customer for current user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("current")]
        //[ResponseType(typeof(ApiResult<Cart>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<Customer>))]
        public async Task<IHttpActionResult> GetCustomerForCurrentUser()
        {
            var userId = _contextProvider.GetLoggedInUserId();
            var result = await _customerService.GetCustomerByUserId(userId);
            return result.ToHttpActionResult(this);
        }

        /// <summary>
        /// Edit Account Info
        /// </summary>
        /// <param name="model"><see cref="AccountInfo"/></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateAccountInfo")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<bool>))]
        public async Task<IHttpActionResult> UpdateAccountInfo(AccountInfo model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input");
                // return BadRequest(new ApiResultBadRequest<bool>(false,"Invalid input").ToApiResult());
            }

            var userId = _contextProvider.GetLoggedInUserId();
            var result = await _customerService.UpdateAccountInfo(model, userId, DateTimeOffset.Now);
            return result.ToHttpActionResult(this);
        }
    }
}