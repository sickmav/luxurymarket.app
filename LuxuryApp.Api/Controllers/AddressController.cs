﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Extensions;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/address")]
    public class AddressController : ApiController
    {
        private Func<IAddressService> _addressServiceFactory;
        private readonly IContextProvider _contextProvider;

        public AddressController(IContextProvider contextProvider, Func<IAddressService> addressServiceFactory)
        {
            _addressServiceFactory = addressServiceFactory;
            _contextProvider = contextProvider;

        }

        /// <summary>
        /// Get Addresses by Company Id
        /// </summary>
        /// <param name="companyId">The ID of a Company</param>
        /// <returns>an object of <see cref="AddressListing"/></returns>
        [HttpGet]
        [Route("byCompanyId/{companyId:int}")]
        [ResponseType(typeof(AddressListing))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AddressListing))]
        public IHttpActionResult GetAllByCompanyId(int companyId)
        {
            if (companyId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var entities = _addressServiceFactory().GetAllByCompanyId(companyId);
            var response = _addressServiceFactory().GetAddressesByType(entities);
            return Ok(response);
        }

        /// <summary>
        /// Get Address by Id
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("byaddressId/{addressId:int}")]
        public IHttpActionResult GetByAddressId(int addressId)
        {
            if (addressId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var entities = _addressServiceFactory().GetByAddressId(addressId);
            return Ok(entities);
        }

        /// <summary>
        /// Set Main Address 
        /// </summary>
        /// <param name="addressId"><see cref="int"/></param>
        /// <returns></returns>
        [HttpPost]
        [Route("setMain")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public IHttpActionResult SetMainAddress(int addressId)
        {
            if (addressId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var result = _addressServiceFactory().SetMainAddress(addressId, _contextProvider.GetLoggedInUserId(), DateTimeOffset.Now);
            return Ok();
        }

        /// <summary>
        /// Delete Address
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("delete")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public IHttpActionResult DeleteAddress(int addressId)
        {
            if (addressId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var result = _addressServiceFactory().DeleteAddress(addressId, _contextProvider.GetLoggedInUserId(), DateTimeOffset.Now);
            return Ok();
        }

        /// <summary>
        /// Add / Update Address
        /// </summary>
        /// <param name="model"><see cref="Contracts.Models.Addresses.AddressInfo"/></param>
        /// <returns>address id</returns>
        [HttpPost]
        [Route("saveInfo")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(int))]
        public IHttpActionResult SaveAddress(AddressInfo model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input");
            }

            return _addressServiceFactory().SaveAddress(model, _contextProvider.GetLoggedInUserId(), DateTimeOffset.Now).ToHttpActionResult(this);
        }

        /// <summary>
        /// Get Address by Id
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{companyId:int}/main")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<AddressInfo>))]
        public async Task<IHttpActionResult> GetGetCompanyMainAddresses(int companyId)
        {
            if (companyId <= 0)
                return new ApiResultBadRequest<Address>(null).ToHttpActionResult(this);

            var result = await _addressServiceFactory().GetCompanyMainAddresses(companyId);
            return Ok(result);
        }
    }
}