﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Processors;
using LuxuryApp.Processors.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/hierarchy")]
    public class HierarchyController : ApiController
    {
        private HierarchyService _hierarchyService;

        public HierarchyController()
        {
            _hierarchyService = new HierarchyService();
        }

        [HttpGet]
        [Route("get")]
        [ResponseType(typeof(List<SearchBusinessModel>))]
        public IHttpActionResult Get()
        {
            var hierarchy = _hierarchyService.Get();

            var hierarchyTree = (from h in hierarchy
                                 group h by h.BusinessID into grpBusiness
                                 let grpBusinessKey = hierarchy.First(x => x.BusinessID == grpBusiness.Key)

                                 let business = new SearchBusinessModel
                                 {
                                     Id = grpBusinessKey.HierarchyID,
                                     BusinessID = grpBusinessKey.BusinessID,
                                     BusinessName = grpBusinessKey.BusinessName,
                                     Divisions = (from hd in grpBusiness
                                                  group hd by hd.DivisionID into grpDivision
                                                  let divKey = hierarchy.First(x => x.DivisionID == grpDivision.Key)

                                                  let division = new SearchDivisionModel
                                                  {
                                                      Id = divKey.HierarchyID,
                                                      BusinessID = divKey.BusinessID,
                                                      DivisionID = divKey.DivisionID,
                                                      DivisionName = divKey.DivisionName,
                                                      Departments = (from hdd in grpDivision
                                                                     group hdd by hdd.DepartmentID into grpDepartment
                                                                     let depKey = hierarchy.First(x => x.DepartmentID == grpDepartment.Key)

                                                                     let department = new SearchDepartmentModel
                                                                     {
                                                                         Id = depKey.HierarchyID,
                                                                         BusinessID = depKey.BusinessID,
                                                                         DivisionID = depKey.DivisionID,
                                                                         DepartmentID = depKey.DepartmentID,
                                                                         DepartmentName = depKey.DepartmentName,
                                                                         Categories = (from hddc in grpDepartment
                                                                                       select new SearchCategoryModel
                                                                                       {
                                                                                           Id = hddc.HierarchyID,
                                                                                           BusinessID = hddc.BusinessID,
                                                                                           DivisionID = hddc.DivisionID,
                                                                                           DepartmentID = hddc.DepartmentID,
                                                                                           CategoryID = hddc.CategoryID,
                                                                                           CategoryName = hddc.CategoryName
                                                                                       }).ToList()
                                                                     }
                                                                     select department
                                                                    ).ToList()
                                                  }
                                                  select division).ToList()
                                 }
                                 select business).ToList();

            return Ok(hierarchyTree);
        }

        [HttpGet]
        [Route("categories")]
        [ResponseType(typeof(List<SelectListModel>))]
        public IHttpActionResult GetCategories()
        {
            return Ok(_hierarchyService.GetCategories());
        }

        [HttpGet]
        [Route("seasons")]
        [ResponseType(typeof(List<SelectListModel>))]
        public IHttpActionResult GetSeasons()
        {
            return Ok(_hierarchyService.GetSeasons());
        }
    }
}
