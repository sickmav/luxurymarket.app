﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Shipment;
using LuxuryApp.Contracts.Models.Shipping;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Extensions;
using Swashbuckle.Swagger.Annotations;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("shipping")]
    public class ShippingController : ApiController
    {
        private readonly IShippingService _shippingService;

        public ShippingController(IShippingService shippingService)
        {
            _shippingService = shippingService;
        }

        [HttpGet]
        [Route("preview_shipping")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<Shipping>))]
        public IHttpActionResult PreviewShippingForCart([FromUri] int cartId)
        {
            return _shippingService.PreviewShippingForCart(cartId).ToHttpActionResult(this);
        }

        [HttpPost]
        [Route("cancel_shipment")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<ShippingSummary>))]
        public IHttpActionResult CancelShipment(CancelShipmentModel model)
        {
            return _shippingService.CancelShipment(model).ToHttpActionResult(this);
        }
    }
}
