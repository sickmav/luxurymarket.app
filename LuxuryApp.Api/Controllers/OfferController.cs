﻿using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Offer;
using LuxuryApp.Core.Infrastructure;
using LuxuryApp.Core.Infrastructure.Authorization;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Api.Controllers
{

    [RoutePrefix("api/offers")]
    public class OfferController : ApiController
    {
        private readonly Func<IOfferService> _offerService;
        private readonly IContextProvider _contextProvider;

        public OfferController(IContextProvider contextProvider, Func<IOfferService> offerServiceFactory)
        {
            _offerService = offerServiceFactory;
            _contextProvider = contextProvider;
        }

        [HttpPost]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(OfferSearchResponse))]
        public IHttpActionResult Get(OfferSearch model)
        {
            if (Request.Headers.Contains(RequestHeaders.CompanyID))
            {
                int? companyId = null;
                companyId = Convert.ToInt32(Request.Headers.GetValues(RequestHeaders.CompanyID).First());

                if (companyId != null)
                {
                    model.CompanyID = (int)companyId;
                }
                else
                {
                    return BadRequest("Invalid input");
                }
            }
            else
            {
                return BadRequest("Invalid input");
            }

            model.SellerID = _contextProvider.GetLoggedInUserId();
            model.PageSize = WebConfigs.ProductsPageSize;
            var response = _offerService().Search(model);
            response.TotalPages = (int)Math.Ceiling((double)response.TotalOffers / (int)model.PageSize);

            return Ok(response);
        }


        [HttpPost]
        [Route("sizes")]
        [SwaggerResponse(HttpStatusCode.OK, Type=typeof(List<ProductOfferSize>))]
        public async Task<IHttpActionResult> GetOfferProductSizes(OfferProductSizesRequest model)
        {
            var sizes = await _offerService().GetOfferProductSizes(model);
            return Ok(sizes);
        }

        [HttpGet]
        [Route("statuses")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<OfferStatusResponse>))]
        public async Task<IHttpActionResult> GetStatuses()
        {
            var statuses = await _offerService().GetStatuses();
            return Ok(statuses);
        }

        [HttpPost]
        [Route("livesummary")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(OfferLiveSummary))]
        public IHttpActionResult Get(OfferLiveSummaryRequest model)
        {
            model.SellerID = _contextProvider.GetLoggedInUserId();
            var response = _offerService().GetLiveSummary(model);

            return Ok(response);
        }

        [HttpGet]
        [Route("products/{offerNumber:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<OfferImport>))]
        public IHttpActionResult GetProducts(int offerNumber)
        {
            var response = _offerService().GetProducts(offerNumber);
            return Ok(response);
        }

        [HttpPost]
        [Route("update_end_date")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public IHttpActionResult UpdateEndDate(Offer model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input");
            }

            _offerService().UpdateOfferEndDate(model);
            return Ok();
        }

        [HttpPost]
        [Route("delete")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public IHttpActionResult UpdateEndDate(int offerId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input");
            }

            _offerService().DeleteOffer(offerId);
            return Ok();
        }

        [HttpGet]
        [Route("isLive")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<IHttpActionResult> IsOfferActive(int offerId)
        {
            if (offerId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var result = await _offerService().IsActiveOffer(offerId);
            return Ok(result);
        }

        [HttpPost]
        [Route("getSellerProducts")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(OfferSellerProducts))]
        public async Task<IHttpActionResult> GetSellerProducts([FromBody]SellerProductsSearch search)
        {
            var result = await _offerService().GetSellerProducts(search);
            return Ok(result);
        }

        [HttpGet]
        [Route("getSellerSizesForProduct")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(OfferSellerSizes))]
        public async Task<IHttpActionResult> GetSellerSizesForProduct(int sellerProductId)
        {
            var result = await _offerService().GetSellerSizesForProduct(sellerProductId);
            return Ok(result);
        }

        [HttpPost]
        [Route("createOffer")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImport>))]
        public async Task<IHttpActionResult> CreateOffer(OfferCreate model, int companyId)
        {
            var result = await _offerService().CreateOffer(model, companyId);
            return Ok(result);
        }

    }
}
