﻿using LuxuryApp.Api.ActionFilterAttributes;
using LuxuryApp.Core.Infrastructure.Providers;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using System;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using LuxuryApp.Core.Infrastructure;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Api.Helpers;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Core.Infrastructure.Api.Extensions;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    /// <summary>
    /// Api controller for managing the customer offer imports
    /// </summary>
    [RoutePrefix("api/offerImports")]
    public class OfferImportController : ApiController
    {
        #region Private variable.

        private readonly Func<IOfferImportService> _offerServiceFactory;
        private readonly Func<INotificationHandler<EmailModel>> _emailServiceFactory;
        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public OfferImportController(Func<IOfferImportService> offerServiceFactory
                                    , IContextProvider contextProvider
                                    , Func<INotificationHandler<EmailModel>> emailServiceFactory)
        {
            _offerServiceFactory = offerServiceFactory;
            _emailServiceFactory = emailServiceFactory;
        }

        #endregion

        #region OfferImportBatch

        /// <summary>
        /// Upload file data and return the inserted items (without sizes)
        /// </summary>
        /// <returns>The inserted offerImport <see cref="OfferImport"/></returns>
        [Route("upload")]
        [HttpPost]
        [ValidateMimeMultipartContentFilter]
        [ResponseType(typeof(OfferImport))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImport>))]
        public async Task<IHttpActionResult> UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
                return BadRequest("Cannot parse content type.");

            var provider = new ImportProvider(WebConfigs.OfferImportFolder);
            var task = await Request.Content.ReadAsMultipartAsync(provider);

            var fileupload = provider.FileData.FirstOrDefault();
            var temppath = fileupload.LocalFileName;
            string token = String.Empty;
            ApiResult<OfferImport> result = null;
            using (var file = File.Open(temppath, FileMode.Open))
            {
                var fileName = Path.GetFileName(temppath);
                int companyId = 0;
                if (Request.Headers.Contains(RequestHeaders.CompanyID))
                {
                    companyId = Convert.ToInt32(Request.Headers.GetValues(RequestHeaders.CompanyID).First());
                }

                result = await _offerServiceFactory().UploadFile(fileName, file, companyId);
            }

            token = Request.Headers.GetValues("Authorization").FirstOrDefault();

            await ApiClient.UploadFile(temppath, token);

            return Ok(result);
        }

        /// <summary>
        /// Update offer general details : start date, end date, take all, status, delete etc.
        /// </summary>
        /// <param name="model">object of <see cref="OfferImport"/></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> SaveOfferImport(OfferImport model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid input");
            }
            var result = await _offerServiceFactory().UpdateOfferImportGeneralData(model);
            return Ok();
        }

        /// <summary>
        /// Get Offer import summary
        /// </summary>
        /// <param name="offerImportBatchId"> id of an offer import</param>
        /// <returns>object of <see cref="OfferImportSummary"/> </returns>
        [HttpGet]
        [Route("{offerImportBatchId:int}/summary")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImportSummary>))]
        public async Task<IHttpActionResult> GetOfferImportSummary(int offerImportBatchId)
        {
            if (offerImportBatchId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var result = await _offerServiceFactory().GetOfferImportSummary(offerImportBatchId);
            return Ok(result);
        }


        #endregion

        #region OfferImportBatchItem

        /// <summary>
        /// Get offer import item with all details :excel data, sizes,and errors , product images, description
        /// </summary>
        /// <param name="offerImportBatchItemId">The ID of an OfferImportBatchItem</param>
        /// <returns>an object of <see cref="OfferImportBachItem"/></returns>
        [Route("item/{offerImportBatchItemId:int}")]
        [HttpGet]
        [ResponseType(typeof(OfferImportBachItem))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImportBachItem>))]
        public async Task<IHttpActionResult> GetOfferImportBatchItem(int offerImportBatchItemId)
        {
            if (offerImportBatchItemId <= 0)
            {
                return BadRequest("Invalid input");
            }
            var result = await _offerServiceFactory().GetOfferProductFullData(offerImportBatchItemId);
            return Ok(result);
        }

        /// <summary>
        /// Update the 
        /// </summary>
        /// <param name="model"> <see cref="OfferImportBachItemEditModel"/></param>
        /// <param name="returnUpdatedItem"> return item info or not</param>
        /// <returns>Http status</returns>
        [HttpPost]
        [Route("item")]
        [ResponseType(typeof(OfferImportBachItem))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImport>))]
        public async Task<IHttpActionResult> UpdateOfferImportItem(OfferImportBachItemEditModel model, [FromUri] bool returnUpdatedItem = true)
        {
            if (model?.Id <= 0 || !ModelState.IsValid)
            {
                return BadRequest("Invalid input");
            }
            var result = await _offerServiceFactory().UpdateOfferImportItem(model, DateTimeOffset.Now, false, returnUpdatedItem);
            return Ok(result);
        }

        /// <summary>
        /// Update linebuy for an offerImportBatchItem or for all the offer items
        /// </summary>
        /// <param name="model"><see cref="OfferImportBatchEditLineBuy"/></param>
        /// <returns></returns>
        [HttpPost]
        [Route("lineBuy")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> UpdateLineBuy(OfferImportBatchEditLineBuy model)
        {
            if (model?.OfferImportBatchId <= 0 || !ModelState.IsValid)
            {
                return BadRequest("Invalid input");
            }

            var result = await _offerServiceFactory().UpdateOfferItemsLineBuy(model);
            return Ok();
        }

        [HttpPost]
        [Route("batch_update_item")]
        [ResponseType(typeof(OfferImportBachItem))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImport>))]
        public async Task<IHttpActionResult> UpdateOfferImportItem(OfferItemsBatchUpdateModel model)
        {
            var result = await _offerServiceFactory().BatchUpdateOfferImportItems(model);
            return Ok(result);
        }

        [Route("getModelNumbers")]
        [HttpPost]
        [ResponseType(typeof(List<string>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<string>))]
        public async Task<IHttpActionResult> GetModelNumberByBrand(ProductSearchMappingCriteria searchCriteria)
        {
            if (searchCriteria?.BrandId <= 0)
            {
                return BadRequest("Invalid brand");
            }
            var result = await _offerServiceFactory().GetModelNumberByBrand(searchCriteria);
            return Ok(result);
        }
        #endregion

        #region OfferImportBatchItemSizes

        /// <summary>
        /// Get sizes for an item
        /// </summary>
        /// <param name="offerImportBatchItemId">The ID of an OfferImportBatchItem</param>
        /// <returns>Collection of <see cref="OfferImportBatchItemSize"/></returns>
        [HttpGet]
        [Route("item/{offerImportBatchItemId:int}/sizes")]
        [ResponseType(typeof(OfferImportBachItem))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImportBatchItemSizeCollection>))]
        public async Task<IHttpActionResult> GetOfferImportItemSizes(int offerImportBatchItemId)
        {
            if (offerImportBatchItemId <= 0)
            {
                return BadRequest("Invalid input");
            }
            var result = await _offerServiceFactory().GetOfferImportItemSizes(offerImportBatchItemId);
            return Ok(result);
        }

        /// <summary>
        /// Update size item
        /// </summary>
        /// <param name="offerImportBatchItemId">the id of the offerImportBarchItem</param>
        /// <param name="item">object of <see cref="OfferImportBachItemSizeModel"/></param>
        /// <returns>number of total quantity</returns>
        [HttpPost]
        [Route("item/update_sizes")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<int>))]
        public async Task<IHttpActionResult> AddUpdateOfferImportItemSizes(OfferImportBachItemSizeModel item)
        {
            var result = await _offerServiceFactory().AddUpdateOfferImportItemSize(item);
            return Ok(result);
        }


        #endregion

        #region Validation

        /// <summary>
        /// Get the validation for an offerImportBatchItem
        /// </summary>
        /// <param name="offerImportBatchItemId">the id of an offerImportBatchItem</param>
        /// <returns>Collection of <see cref="OfferImportBatchItemError"/></returns>
        [HttpGet]
        [Route("item/{offerImportBatchItemId:int}/validate")]
        [ResponseType(typeof(ApiResult<OfferImportBatchItemErrorCollection>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImportBatchItemErrorCollection>))]
        public async Task<IHttpActionResult> ValidateItem(int offerImportBatchItemId, bool onlyActive = true)
        {
            if (offerImportBatchItemId <= 0)
            {
                var error = new ApiResultBadRequest<OfferImportBatchItemErrorCollection>(null, "Invalid input").ToApiResult();
                return Ok(error);
            }
            var result = await _offerServiceFactory().Validate(null, offerImportBatchItemId, onlyActive);
            return Ok(result);
        }

        /// <summary>
        /// Get the validation for the entire offerImport
        /// </summary>
        /// <param name="offerImportBatchId">the id of an offerImportBatch</param>
        /// <returns>Collection of <see cref="OfferImportBatchItemError"/></returns>
        [HttpGet]
        [Route("{offerImportBatchId:int}/validate")]
        [ResponseType(typeof(ApiResult<OfferImportBatchItemErrorCollection>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImportBatchItemErrorCollection>))]
        public async Task<IHttpActionResult> ValidateOffer(int offerImportBatchId, bool onlyActive = true)
        {
            if (offerImportBatchId <= 0)
            {
                var error = new ApiResultBadRequest<OfferImportBatchItemErrorCollection>(null, "Invalid input").ToApiResult();
                return Ok(error);
            }

            var result = await _offerServiceFactory().Validate(offerImportBatchId, null, onlyActive);
            return Ok(result);
        }

        #endregion

        #region OfferFiles

        /// <summary>
        /// Get offer import files
        /// </summary>
        /// <param name="offerImportBatchId">Id of offerImport</param>
        /// <returns>Collection of <see cref="OfferImportFile"/> </returns>
        [HttpGet]
        [Route("{offerImportBatchId:int}/files")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<OfferImportFileCollection>))]
        public async Task<IHttpActionResult> GetOfferImportFiles(int offerImportBatchId)
        {
            if (offerImportBatchId <= 0)
            {
                return BadRequest("Invalid input");
            }

            var result = await _offerServiceFactory().GetFiles(offerImportBatchId);
            return Ok(result);
        }

        /// <summary>
        /// Upload offer files
        /// </summary>
        /// <param name="offerImportId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{offerImportId:int}/files/upload")]
        [ValidateMimeMultipartContentFilter]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<int>))]
        public async Task<IHttpActionResult> UploadFile(int offerImportId)
        {
            if (offerImportId <= 0)
            {
                return BadRequest("Invalid input");
            }

            if (!Request.Content.IsMimeMultipartContent())
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Request!");
                throw new HttpResponseException(response);
            }

            var streamProvider = new ImportProvider(WebConfigs.OfferDocumentsFolder);
            var task = await Request.Content.ReadAsMultipartAsync(streamProvider);
            var data = streamProvider.FileData.FirstOrDefault();

            var name = Path.GetFileNameWithoutExtension(data.LocalFileName);
            var file = new OfferImportFile
            {
                OfferImportBatchId = offerImportId,
                FileName = Path.GetFileName(data.LocalFileName),
                OriginalName = string.Concat(
                            name.Remove(name.LastIndexOf("_")),
                            Path.GetExtension(data.LocalFileName))
            };
            var result = _offerServiceFactory().InsertFile(file);

            var token = Request.Headers.GetValues("Authorization").FirstOrDefault();
            await ApiClient.UploadFile(data.LocalFileName, token);

            return Ok(result);
        }

        /// <summary>
        /// Update offer import file (delete or change original name)
        /// </summary>
        /// <param name="offerImportBatchId">Id of offerImportBatch</param>
        /// <param name="file">object of <see cref="OfferImportFile"/> </param>
        /// <returns></returns>
        [HttpPost]
        [Route("{offerImportBatchId:int}/files/{offerImportFileId:int}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> UpdateFile(int offerImportBatchId, [FromBody] OfferImportFile file)
        {
            if (offerImportBatchId <= 0 || file?.OfferImportFileId <= 0)
            {
                return BadRequest("Invalid input");
            }
            file.OfferImportBatchId = offerImportBatchId;
            var result = await _offerServiceFactory().UpdateFile(file);
            return Ok();
        }

        /// <summary>
        /// Upload offer files
        /// </summary>
        /// <param name="model">object of <see cref="OfferImportReportError"/></param>
        /// <param name="offerImportBatchId">Id of the offer import batch</param> 
        /// <returns>HttpStatusCode.OK, Type = typeof(void) </returns>
        [HttpPost]
        [Route("{offerImportBatchId:int}/reportError")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> InsertOfferImportError(int offerImportBatchId, OfferImportReportError model)
        {
            if ((model?.OfferImportBatchId <= 0 && model?.OfferImportBatchItemId <= 0) || string.IsNullOrWhiteSpace(model?.ErrorDescription))
            {
                return BadRequest("Invalid item identifier");
            }
            if (string.IsNullOrWhiteSpace(model?.ErrorDescription))
            {
                return BadRequest("Description required");
            }
            model.OfferImportBatchId = offerImportBatchId;
            var result = await _offerServiceFactory().InsertOfferImportError(model);
            return Ok();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}