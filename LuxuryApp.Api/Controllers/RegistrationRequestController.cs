﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using System.Linq;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using System.Threading.Tasks;
using LuxuryApp.Api.Helpers;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/registrationRequests")]
    [AllowAnonymous]
    public class RegistrationRequestController : ApiController
    {
        private Func<IRegistrationRequestService> _requestServiceFactory;

        public RegistrationRequestController(Func<IRegistrationRequestService> requestServiceFactory)
        {
            _requestServiceFactory = requestServiceFactory;
        }

        /// <summary>
        /// Save Registration Request
        /// </summary>
        /// <param name="model"><see cref="RegistrationRequest"/> </param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ApiResult<int>))]
        public async Task<IHttpActionResult> SaveRequestAsync(RegistrationRequest model)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Keys.SelectMany(key => this.ModelState[key].Errors).Select(x => x.ErrorMessage).ToList();
                return BadRequest($"Invalid input :" + string.Join(";", errors));
            }

            var result = await _requestServiceFactory().CreateRequestAsync(model);
            return Ok(result);
        }
    }
}