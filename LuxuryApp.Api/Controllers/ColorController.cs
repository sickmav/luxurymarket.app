﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/colors")]
    public class ColorController : ApiController
    {
        private ColorService _colorServer;

        public ColorController()
        {
            _colorServer = new ColorService();
        }


        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var entities = _colorServer.Search();
            return Ok(entities);
        }

        [HttpGet]
        [Route("{colorId:int}")]
        public IHttpActionResult GetById(int colorId)
        {
            var entities = _colorServer.Search(colorId: colorId);
            return Ok(entities);
        }

        [HttpGet]
        [Route("brand/{brandId:int}")]
        public IHttpActionResult GetByBrand(int brandId)
        {
            var entities = _colorServer.Search(brandId: brandId);
            return Ok(entities);
        }

        [HttpGet]
        [Route("colorCode/{colorCode}")]
        public IHttpActionResult GetByColorCode(string colorCode)
        {
            var entities = _colorServer.Search(colorCode: colorCode);
            return Ok(entities);
        }

        [HttpGet]
        [Route("standards")]
        public IHttpActionResult GetStandards()
        {
            var entities = _colorServer.GetStandards();
            return Ok(entities);
        }
    }
}