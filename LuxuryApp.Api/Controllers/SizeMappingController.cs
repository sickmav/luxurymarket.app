﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Extensions;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Contracts.Models.SizeMappings;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/sizeMappings")]
    public class SizeMappingController : ApiController
    {
        private Func<ISizeRegionService> _sizeRegionServiceFactory;

        public SizeMappingController(Func<ISizeRegionService> sizeRegionServiceFactory)
        {
            _sizeRegionServiceFactory = sizeRegionServiceFactory;
        }

        [HttpGet]
        [Route("getSizeRegions")]
        [ResponseType(typeof(SizeRegionCollection))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SizeRegionCollection))]
        public async Task<IHttpActionResult> GetSizeRegion()
        {
            var entities =await _sizeRegionServiceFactory().GetSizeRegions();
            return Ok(entities);
        }

        [HttpGet]
        [Route("search")]
        [ResponseType(typeof(SizeRegionCollection))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SizeRegionCollection))]
        public IHttpActionResult SearchSizeRegion(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return BadRequest("Invalid Input");
            var entities =_sizeRegionServiceFactory().SearchSizeRegions(name);
            return Ok(entities);
        }
    }
}