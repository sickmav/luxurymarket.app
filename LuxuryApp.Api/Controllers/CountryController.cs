﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Api.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/countries")]
    public class CountryController : ApiController
    {
        private CountryService _countryService;

        public CountryController()
        {
            _countryService = new CountryService();
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var countries = _countryService.Search(null);
            return Ok(countries);
        }
        [HttpGet]
        [Route("{countryId:int}")]
        public IHttpActionResult GetById(int countryId)
        {
            var countries = _countryService.Search(countryId);
            return Ok(countries);
        }
    }
}