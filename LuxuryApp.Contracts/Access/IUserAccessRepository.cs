﻿namespace LuxuryApp.Contracts.Repository
{
    public interface IUserAccessRepository
    {
        bool CanUserAddOffers(int companyId, int userId);

    }
}
