﻿using LuxuryApp.Contracts.Models;
using System;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface ICustomerRepository
    {
        Task<Customer> GetCustomerByUserId(int userId);

        Task<bool> UpdateAccountInfo(AccountInfo model, int userId, DateTimeOffset currentDate);

        Task<Customer> GetCustomerWithCompaniesAsync(int userId);
    }
}
