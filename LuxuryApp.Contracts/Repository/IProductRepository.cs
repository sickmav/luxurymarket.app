﻿using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Contracts.Repository
{
    public interface IProductRepository
    {
        ProductDivision[] GetProductsDivisions(int[] productIds);

        //Product GetProductBySku(string sku);

        bool IsvalidProductSKU(string sku);
    }
}
