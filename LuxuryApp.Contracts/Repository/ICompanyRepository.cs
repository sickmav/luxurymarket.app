﻿using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    /// <summary>
    /// Interface for ICompanyRepository
    /// </summary>
    public interface ICompanyRepository
    {
        Company[] SearchCompaniesByName(string name, out int totalRecords);
        Company[] SearchCompaniesByName(string name);
        bool IsUserAssociatedToCompany(int userId, int companyId);

        /// <summary>
        /// Create Registration Request for Company
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int CreateRequest(RegistrationRequest model, out string errorMessage);

        Task<IEnumerable<Company>> GetCompaniesByCustomerId(int customerId, int companyTypeId);

        Task<Company> GetCompanyById(int companyId);

        Task<bool> AcceptTermsAndConditions(int companyId, int userId, DateTimeOffset currentDate);
    }
}
