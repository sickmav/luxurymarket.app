﻿using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Contracts.Models.SizeMappings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface ISizeRegionRepository
    {
        Task<IEnumerable<SizeRegion>> GetSizeRegions();

        IEnumerable<SizeRegion> SearchSizeRegions(string name);
    }
}
