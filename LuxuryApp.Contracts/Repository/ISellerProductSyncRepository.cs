﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface ISellerProductSyncRepository
    {
        Task<int> InsertProductsAsync(DataTable productDt, DataTable imageDt, Guid mainBatchID, int sellerID, int commandTimeoutSeconds=6000);

        Task<DateTimeOffset?> GetLastSyncDateAsync(int sellerId);

        Task<int> SaveInventoryAsync(DataTable inventory, int sellerID, int commandTimeoutSeconds= 6000);
    }
}
