﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Interfaces
{
    public interface IConnectionStringProvider
    {
        string ReadWriteConnectionString { get; }
        string ReadOnlyConnectionString { get; }
    }
}
