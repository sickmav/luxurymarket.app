﻿using System.Threading;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.BackgroudServices
{
    public interface IWorker
    {
        void Start(CancellationToken cancellationToken);
    }
}
