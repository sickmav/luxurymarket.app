﻿using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface IAddressService
    {
        Task<IEnumerable<AddressInfo>> GetCompanyMainAddresses(int companyId);

        List<AddressInfo> GetAllByCompanyId(int companyId);

        AddressInfo GetByAddressId(int addressId);

        bool SetMainAddress(int addressId, int userId, DateTimeOffset currentDate);

        bool DeleteAddress(int addressId, int userId, DateTimeOffset currentDate);

        ApiResult<int> SaveAddress(AddressInfo address, int userId, DateTimeOffset currentDate);

        AddressListing GetAddressesByType(List<AddressInfo> addresses);
    }
}
