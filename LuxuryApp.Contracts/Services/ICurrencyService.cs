﻿using System.Collections.Generic;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Contracts.Services
{
    public interface ICurrencyService
    {
        List<Currency> GetById(int? id);
        List<Currency> GetByName(string name);
    }
}