﻿
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    /// <summary>
    /// OfferImportService interface
    /// </summary>
    public interface IOfferImportService
    {
        Task<ApiResult<OfferImport>> UploadFile(string fileName, Stream file, int companyId);

        Task<ApiResult<OfferImportBachItem>> GetOfferProductFullData(int offerImportBatchItemId);

        Task<ApiResult<OfferImportSummary>> GetOfferImportSummary(int offerImportBatchId);

        Task<ApiResult<int>> UpdateOfferImportGeneralData(OfferImport item);

        Task<ApiResult<OfferImport>> UpdateOfferImportItem(OfferImportBachItemEditModel item, DateTimeOffset date,bool mapsizes, bool returnUpdatedItem);

        Task<ApiResult<OfferImport>> BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model);

        Task<ApiResult<int>> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item);

        Task<ApiResult<OfferImportBatchItemSizeCollection>> GetOfferImportItemSizes(int offerImportBatchItemId);

        Task<ApiResult<int>> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item);

        Task<ApiResult<OfferImportBatchItemErrorCollection>> Validate(int? offerImportId, int? offerImportItemId, bool onlyActive);

        Task<ApiResult<OfferImportFileCollection>> GetFiles(int offerImportId);

        Task<ApiResult<int>> UpdateFile(OfferImportFile file);

        ApiResult<int> InsertFile(OfferImportFile file);

        Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item);

        Task<ApiResult<int>> InsertOfferImportError(OfferImportReportError model);
    }
}
