﻿using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Orders;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    /// <summary>
    /// OrderService interface
    /// </summary>
    public interface IOrderService
    {
        Task<ApiResult<string>> SubmitOrderAsync(OrderSubmitDataView model, DateTimeOffset date);

        Task<ApiResult<Order>> GetOrderDetailsForBuyerAsync(int orderId);
        Task<ApiResult<OrderSellerView>> GetOrderItemForSellerByIdAsync(int orderSellerId);

        Task<ApiResult<OrderSellerManageList>> FilterSellerOrdersAsync(RequestFilterOrders model);
        Task<ApiResult<OrderBuyerManageList>> FilterBuyerOrdersAsync(RequestFilterOrders model);

        Task<IEnumerable<PaymentMethod>> GetPaymentMethodAsync();
        Task<IEnumerable<OrderStatusType>> GetSuborderStatusTypesAsync();

        Task<ApiResult<bool>> UpdateStatusAsync(OrderStatusUpdateView model, DateTimeOffset date);

        Task<ApiResult<OrderProductTotalForSeller>> ConfirmUnitsAsync(OrderConfirmUnitView model, DateTimeOffset date);

        Task<ApiResult<OrderFileList>> GetFilesAsync(int? mainOrderId, int? orderId, int? companyId);

        Task<ApiResult<int>> UpdateFileAsync(OrderFile model, int? companyId);

        Task<ApiResult<int>> InsertFileAsync(OrderFile model, int? companyId);

        ApiResult<byte[]> GenerateOrderSellerPDF(int orderSellerId);

        Task<ApiResult<int>> InsertOrderSellerPackingInformationAsync(OrderSellerPackingInformationInsert model);

        Task<ApiResult<OrderSellerPackingInformationInsert>> GetOrderSellerPackingInformationAsync(int orderSellerId);

        Task<ApiResult<OrderSellerPackageCollection>> GetOrderSellerPackagesAsync(int ordersellerId);

        Task<ApiResult<bool>> IsSubmittedPackageInformation(int ordersellerId);

        Task<ApiResult<byte[]>> ExportOrderExcel(int orderId, bool isBuyer);
    }
}
