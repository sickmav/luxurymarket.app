﻿using LuxuryApp.Contracts.Models;
using System.Collections.Generic;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace LuxuryApp.Contracts.Services
{
    public interface IProductService
    {
        Product GetProductById(int productId, int companyID);

        Product GetProductBySku(string sku);

        ProductSearchResponse SearchProduct(ProductSearchItems model);

        IEnumerable<ProductSearchResult> SearchProduct(int featureEventID);

        ApiResult<ProductSearchResult[]> SearchProduct(ProductFilterModel model);

        IEnumerable<ProductSearchResult> GetBestSellers(int numberOfItems, int companyID);
    }


}
