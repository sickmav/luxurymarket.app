﻿using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Cart;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface ICartService
    {
        ApiResult<Cart> GetCartForBuyer(int buyerCompanyId);

        ApiResult<CartSummary> GetCartSummaryForBuyer(int buyerCompanyId);

        ApiResult<CartOfferProductSummary> AddOfferProductSizeToCart(SaveOfferProductSizesToCartModel model);

        ApiResult<CartOfferProductSummary> AddEntireOfferToCart(AddEntireOfferToCartModel model);

        ApiResult<CartOfferProductSummary> AddProductsToCart(AddProductsToCartModel model);

        ApiResult<CartOfferProductSummary> AddAllProductToCart(AddAllProductToCartModel model);

        ApiResult<SetCartItemActiveFlagResult> SetCartItemSellerUnitSizeActiveFlag(SetCartItemActiveModel model);

        ApiResult<UpdateCartItemsQuantitiesResult> UpdateCartItemsQuantities(SaveOfferProductSizesToCartModel model);

        ApiResult<CartSummary> EmptyCartForBuyer(int buyerCompanyId);

        ApiResult<RemoveProductFromCartResult> RemoveProductsFromCart(RemoveProductsFromCartModel model);

        ApiResult<RemoveCartItemSellerUnitResult> RemoveItemSellerUnitFromCart(RemoveItemSellerUnitFromCartModel model);

        ApiResult<CartTotalSummary> GetTotalSummary(int cartId, bool onlyValidItems);

        ApiResult<byte[]> ExportCartExcel(int buyerCompanyId);

        ApiResult<CartItemValidatelCollection> GetInvalidItems(int cartId);

        ApiResult<CartItemReservedCollection> RelockProduct(int cartId, int offerProductId);

        ApiResult<CartItemReservedCollection> GetCartTimes(int cartId);

        ApiResult<CartSplitPreview> GetCartSplitPreview(int cartId);
    }
}
