﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface IUserService
    {
        Task<ApiResult<Customer>> GetCustomerWithCompaniesAsync(int orderId);
    }
}
