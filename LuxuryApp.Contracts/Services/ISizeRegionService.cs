﻿using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Contracts.Models.SizeMappings;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface ISizeRegionService
    {
        Task<ApiResult<SizeRegionCollection>> GetSizeRegions();

        ApiResult<SizeRegionCollection> SearchSizeRegions(string name);
    }
}
