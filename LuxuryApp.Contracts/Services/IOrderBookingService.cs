﻿using System;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    public interface IOrderBookingService
    {
        ApiResult<byte[]> GetSellerOrderBookingDocument(int orderSellerId,out string poNumber, Bookin865Data data = null);
        ApiResult PushSellerOrderBookingDocument(int orderSellerId);
        Task<ApiResult> SendEmailAsync(int orderSellerId);
    }
}
