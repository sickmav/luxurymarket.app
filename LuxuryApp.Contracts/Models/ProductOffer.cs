﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class ProductOffer
    {
        public int OfferID { get; set; }

        public bool TakeAll { get; set; }

        public bool LineBuy { get; set; }

        public double RetailPrice { get; set; }

        public double LMPrice { get; set; }

        public double Discount { get; set; }

        public double TotalQuantity { get; set; }

        public int AvailableQuantity
        {
            get
            {
                return this.Sizes.Sum(x => x.Quantity);
            }
        }

        public string ShipsFrom { get; set; }

        public string CompanyName { get; set; }

        public string SellerAllias { get; set; }

        public List<ProductOfferSize> Sizes { get; set; }

        public int SellerCompanyID { get; set; }
    }

    public class ProductOfferSize
    {
        public int OfferProductSizeID { get; set; }

        public int Quantity { get; set; }

        public string SizeName { get; set; }

        public int ReservedQuantityByOthers { get; set; }

        public int OfferAvailableQuantity { get; set; }

        public int UserCartQuantity { get; set; }

        public int ReservedTimeSeconds { get; set; }

        public bool IsReserved { get; set; }
    }

    [DataContract]
    public class OfferProductSizesRequest
    {
        [DataMember]
        public int CompanyID { get; set; }

        public int UserID { get; set; }

        [DataMember]
        public int OfferProductID { get; set; }

    }

    public class ProductOfferModel
    {
        public int OfferID { get; set; }

        public string OfferName { get; set; }

        public bool TakeAll { get; set; }

        public double RetailPrice { get; set; }

        public double LMPrice { get; set; }

        public bool LineBuy { get; set; }

        public double TotalQuantity { get; set; }

        public double Discount { get; set; }

        public int OfferProductSizeID { get; set; }

        public int Quantity { get; set; }

        public string SizeName { get; set; }

        public string ShipsFrom { get; set; }

        public string CompanyName { get; set; }

        public string SellerAllias { get; set; }

        public int SellerCompanyID { get; set; }

        public int ReservedQuantityByOthers { get; set; }

        public int UserCartQuantity { get; set; }

        public int ReservedTimeSeconds { get; set; }

        public bool IsReserved { get; set; }
    }
}
