﻿using System.Collections.Generic;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class Company
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public CompanyType CompanyType { get; set; }

        public string RetailId { get; set; }

        public string Description { get; set; }

        public string Website { get; set; }

        public IEnumerable<Address> Addresses { get; set; }

        public IEnumerable<Customer> Customers { get; set; }
    }

    public class CompanyCustomers
    {
        public int CompanyId { get; set; }

        public Company Company { get; set; }

        public IEnumerable<Customer> Customers { get; set; }
    }
}
