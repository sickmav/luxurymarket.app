﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartProductRequest
    {
        public int CartId { get; set; }

        public int? OfferProductId { get; set; }
    }
}
