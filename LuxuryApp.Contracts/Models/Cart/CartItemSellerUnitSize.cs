namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItemSellerUnitSize
    {
        public int Id { get; set; }
        public int OfferProductSizeId { get; set; }

        public int Quantity { get; set; }
        public int AvailableQuantity { get; set; }
        public bool IsCartItem => Quantity > 0;

        public string SizeName { get; set; }

        public int SizeTypeId { get; set; }
        public string SizeTypeName { get; set; }

        public int ReservedTimeSeconds { get; set; }

        public bool IsReserved { get { return ReservedTimeSeconds > 0; } }
    }
}