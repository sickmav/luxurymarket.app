﻿namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartSummary
    {
        public int CartId { get; set; }

        public int BuyerCompanyId { get; set; }

        public int ItemsCount { get; set; }
        public int UnitsCount { get; set; }
        public decimal TotalCustomerCost { get; set; }

        public decimal TotalShippingCost { get; set; }

        public decimal TotalLandedCost { get { return TotalCustomerCost + TotalShippingCost; } }

        public Currency Currency { get; set; }
        public int CartReservedTime { get; set; }
    }
}