﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartSplitPreview
    {
        public int CartId { get; set; }

        public CartTotalSummary validItemSummary { get; set; }

        public CartTotalSummary InvalidItemSummary { get; set; }

        public CartItem[] ValidItems { get; set; }

        public CartItem[] InvalidItems { get; set; }

        public CartSplitPreview()
        {
            ValidItems = new CartItem[0];
            InvalidItems = new CartItem[0];
            validItemSummary = new CartTotalSummary();
            InvalidItemSummary = new CartTotalSummary();
        }
    }

}
