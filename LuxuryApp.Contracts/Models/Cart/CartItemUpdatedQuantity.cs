﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItemUpdatedQuantity
    {
        public int CartItemId { get; set; }

        public int OldQuantity { get; set; }

        public int UpdatedQuantity { get; set; }

        public bool IsActiveOffer { get; set; }

        public CartReservedStatusType ReservedStatusType
        {
            get
            {
                return !IsActiveOffer? CartReservedStatusType.Failed_OfferIsNotActive:
                       (OldQuantity == UpdatedQuantity && OldQuantity>0) ? CartReservedStatusType.Success :
                       (OldQuantity > UpdatedQuantity && UpdatedQuantity > 0) ? CartReservedStatusType.PartialReserved :
                       (OldQuantity==0)? CartReservedStatusType.Failed_InvalidQuantity:
                       (UpdatedQuantity == 0) ? CartReservedStatusType.ReservedByOthers :
                       CartReservedStatusType.None;
            }
        }
    }
}
