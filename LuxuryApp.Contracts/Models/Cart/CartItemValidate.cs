﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItemValidate
    {
        public int CartItemId { get; set; }

        public int OfferProductSizeId { get; set; }

        public bool IsReserved { get; set; }

        public bool IsActiveOffer { get; set; }

        public bool OfferHasAvailableQuantity { get; set; }
    }

    public class CartItemReserved
    {
        public int CartItemId { get; set; }

        public int OfferProductSizeId { get; set; }

        public int OfferProductId { get; set; }

        public int Quantity { get; set; }

        public bool IsReserved { get; set; }

        public int ReservedTimeSeconds { get; set; }

        public int QuantityReservedByOthers { get; set; }

        public int OfferTotalQuantity { get; set; }

        public int OfferAvailableQuantity { get; set; }

        public string ReservedMessage { get; set; }

        public CartReservedStatusType ReservedStatusType { get; set; }

        public bool IsActiveOffer { get; set; }
    }


    public class CartItemReservedCollection
    {
        public CartItemReservedCollection()
        {
            Items = new List<CartItemReserved>();
        }

        public string ReservedMessage { get; set; }

        public CartReservedStatusType ReservedStatusType { get; set; }

        public List<CartItemReserved> Items { get; set; }
    }

    public class CartItemReservedCollectionR
    {
        public CartItemReservedCollectionR()
        {
            Items = new List<CartItemReserved>();
        }

        public string ReservedMessage { get; set; }

        public CartReservedStatusType ReservedStatusType { get; set; }

        public List<CartItemReserved> Items { get; set; }
    }


    public class CartItemValidatelCollection
    {
        public CartItemValidatelCollection()
        {
            Items = new List<CartItemValidate>();
        }

        public List<CartItemValidate> Items { get; set; }
    }

    public class CartOfferProductSummary
    {
        public CartOfferProductSummary()
        {
            Items = new List<CartItemReserved>();
        }

        public List<CartItemReserved> Items { get; set; }

        public CartSummary CartSummaryItem { get; set; }
    }
}
