﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class SetCartItemActiveFlagResult
    {
        public CartSummary CartSummary { get; set; }

        public CartItem[] CartItems { get; set; }

        public SetCartItemActiveFlagResult()
        {
            CartItems = new CartItem[0];
        }
    }
}
