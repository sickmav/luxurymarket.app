﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class UpdateCartItemsQuantitiesResult
    {
        public CartSummary CartSummary { get; set; }
        public CartItemSummary[] CartItemSummaries { get; set; }

        public UpdateCartItemsQuantitiesResult()
        {
            CartItemSummaries = new CartItemSummary[0];
        }
    }

    public class CartItemSummary
    {
        public int ProductId { get; set; }
        public decimal ItemTotalOfferCost { get; set; }
        public CartItemSellerSummary[] CartItemSummaries { get; set; }

        public CartItemSummary()
        {
            CartItemSummaries = new CartItemSellerSummary[0];
        }
    }

    public class CartItemSellerSummary
    {
        public CartItemSellerUnitIdentifier CartItemSellerUnitIdentifier { get; set; }
        public int UnitsCount { get; set; }
        public decimal TotalOfferCost { get; set; }

        public int OfferId { get; set; }
    }
}
