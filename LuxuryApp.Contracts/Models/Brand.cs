﻿namespace LuxuryApp.Contracts.Models
{
    public class Brand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? DisplayOrder { get; set; }
        public int? ParentBrandId { get; set; }
        public string Description { get; set; }
        public int? StandardBrandId { get; set; }

        public StandardBrandModel StandardBrand { get; set; }
    }
}