﻿using System;

namespace LuxuryApp.Contracts.Models
{
    [Serializable]
    public class Currency
    {
        public int CurrencyId { get; set; }

        public string Name { get; set; }

        public decimal Rate { get; set; }
    }
}
