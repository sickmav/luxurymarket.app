﻿
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemSize
    {
        public int OfferImportBatchSizeItemId { get; set; }

        public int OfferImportBatchItemId { get; set; }

        public string SizeName { get; set; }

        public int? SizeTypeSizeId { get; set; }

        public int? SellerSizeMappingId { get; set; }

        public string Quantity { get; set; }

        public int? QuantityValue { get; set; }
    }

    public class OfferImportBatchItemSizeCollection
    {
        public OfferImportBatchItemSizeCollection()
        {
            Items = new List<OfferImportBatchItemSize>();
        }

        public IEnumerable<OfferImportBatchItemSize> Items { get; set; }
    }


}
