﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBachItem : OfferImportBatchItemBase
    {
        public int? BrandId { get; set; }

        //public int? BusinessId { get; set; }

        public int? ProductId { get; set; }

        public int? HierarchyId { get; set; }

        public CurrencyType Currency { get; set; }

        public int? ColorId { get; set; }

        public int? MaterialId { get; set; }

        public int? SizeTypeId { get; set; }

        public double? OfferCostValue { get; set; }

        /// <summary>
        /// from product catalog
        /// </summary>
        public double? ProductRetailPrice { get; set; }

        /// <summary>
        /// calculated based on the retail price from Product Catalog
        /// </summary>
        public double? CustomerDiscount { get; set; }

        public int? ActualTotalUnits { get; set; }

        public double? CustomerUnitPrice { get; set; }

        public double? CustomerTotalPrice { get; set; }

        public bool LineBuy { get; set; }

        public bool Active { get; set; }

        public string CategoryName { get; set; }

        public string ProductName { get; set; }

        public string MainImageName { get; set; }

        public string MainImageLink
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLink100"] + MainImageName;
            }
        }

        public string OriginName { get; set; }

        public string SeasonCode { get; set; }

        public string Dimensions { get; set; }

        public bool HasErrors
        {
            get
            {
                return Errors != null && Errors.Any();
            }
        }

        public List<EntityDescription> Descriptions { get; set; }

        public List<EntityImage> Images { get; set; }

        public List<OfferImportBatchItemError> Errors { get; set; }

        public double? OfferCostValueConverted { get; set; }

        public double? SellerDiscount { get; set; }

        public double? OfferTotalPrice { get; set; }
    }
}
