﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class Product
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string SKU { get; set; }

        public string BrandName { get; set; }

        public string MaterialName { get; set; }

        public string ColorName { get; set; }

        public string OriginName { get; set; }

        public string SeasonName { get; set; }

        public double? RetailPrice { get; set; }

        public string Description { get; set; }

        public string MainImageName { get; set; }

        public int SizeTypeId { get; set; }

        public bool Inherit { get; set; }

        public List<EntityImage> Images { get; set; }

        public List<ProductOffer> Offers { get; set; }
    }
}