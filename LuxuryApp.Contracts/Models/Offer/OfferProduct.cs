﻿using System.Linq;
using Newtonsoft.Json;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferProduct
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int ProductId { get; set; }

        public int TotalQuantity
        {
            get { return OfferProductSizes.Sum(x => x.Quantity); }
        }

        [JsonIgnore]
        public decimal OfferCost { get; set; }
        
        public decimal Discount { get; set; }
        public decimal CustomerUnitPrice { get; set; }

        public bool LineBuy { get; set; }

        public Currency Currency { get; set; }

        public OfferProductSize[] OfferProductSizes { get; set; }

        public OfferProduct()
        {
            OfferProductSizes = new OfferProductSize[0];
        }
    }
}