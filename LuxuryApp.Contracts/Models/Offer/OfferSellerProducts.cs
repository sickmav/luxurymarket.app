﻿using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Serialization;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferSellerProducts
    {
        public int PageNumber { get; set; }

        public int TotalPages { get; set; }

        public int TotalProducts { get; set; }

        public List<OfferSellerProduct> OfferSellerProductsList { get; set; }
    }

    public class OfferSellerProduct
    {
        public int SellerProductId { get; set; }

        public int ProductId { get; set; }

        public int ImageId { get; set; }

        public string ImageName { get; set; }

        public string ProductMainImageLink => ConfigurationManager.AppSettings["S3BucketLink100"] + ImageName;

        public string ProductName { get; set; }

        public string BrandName { get; set; }

        public string SellerSKU { get; set; }

        public double RetailPrice { get; set; }

        public string CategoryName { get; set; }

        public string SeasonName { get; set; }

        public double WholesaleCost { get; set; }

        public string StandardMaterialName { get; set; }

        public string StandardColorName { get; set; }
    }

    [DataContract]
    public class SellerProductsSearch
    {
        [DataMember(Name = "pageNumber")]
        public int PageNumber { get; set; }

        [DataMember(Name = "categories")]
        public List<int> CategoryIDs { get; set; }

        [DataMember(Name = "brands")]
        public List<int> BrandIDs { get; set; }

        [DataMember(Name = "seasons")]
        public List<int> SeasonsIDs { get; set; }

        [DataMember(Name = "keyword")]
        public string Keyword { get; set; }

        [DataMember(Name = "sellerID")]
        public int SellerID { get; set; }

        [DataMember(Name = "hasInventory")]
        public bool HasInventory { get; set; }
    }
}
