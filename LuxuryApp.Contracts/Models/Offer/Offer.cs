﻿using System;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class Offer
    {
        public int Id { get; set; }
        public OfferStatus OfferStatus { get; set; }
        public int OfferNumber { get; set; }
        public string OfferName { get; set; }
        public bool TakeAll { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }        public Currency Currency { get; set; }
        public int SellerCompanyId { get; set; }

        public OfferProduct[] OfferProducts { get; set; }

        // for listing
        public int TotalStyles { get; set; }

        public decimal TotalCostOnOffer { get; set; }

        public decimal TotalCostSold { get; set; }

        public decimal TotalCostSellThrough { get; set; }

        public int UnitsOnOffer { get; set; }

        public int UnitsSold { get; set; }

        public decimal UnitsSellThrough { get; set; }

        public string OfferStatusName { get; set; }


        public Offer()
        {
            OfferProducts = new OfferProduct[0];
        }
    }
}
