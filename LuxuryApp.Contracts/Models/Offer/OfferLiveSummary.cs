﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferLiveSummary
    {
        public int TotalProducts { get; set; }

        public int TotalOffers { get; set; }
    }
}
