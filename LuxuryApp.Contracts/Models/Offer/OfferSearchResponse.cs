﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferSearchResponse
    {
        public double TotalOffers { get; set; }

        public int TotalPages { get; set; }

        public List<Offer> Offers { get; set; }
    }
}
