﻿namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferProductSize
    {
        public int Id { get; set; }
        public int OfferProductId { get; set; }
        public int Quantity { get; set; }
        public string SizeName { get; set; }
    }
}