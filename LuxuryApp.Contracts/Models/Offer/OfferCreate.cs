﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferCreate
    {
        public List<OfferCreateProductTable> Products { get; set; }

        public bool AreAllSelected { get; set; }

        public List<OfferCreateProductTable> ExcludedProducts { get; set; }

        public SellerProductsSearch Search { get; set; }

        public PriceSettings PriceSettings { get; set; }

        //public List<OfferCreateSizeTable> Sizes { get; set; }
    }

    public class OfferCreateProductTable
    {
        public int ProductId { get; set; }

        public decimal OfferPrice { get; set; }
    }

    public class OfferCreateSizeTable
    {
        public int ProductIndex { get; set; }

        public int Size { get; set; }

        public int Quantity { get; set; }
    }

    public class PriceSettings
    {
        public double Percentage { get; set; }

        public PriceSettingType Type { get; set; }
    }

    public enum PriceSettingType
    {
        BelowCost = 1,
        BelowRetail = 2,
        AboveCost = 3
    }
}
