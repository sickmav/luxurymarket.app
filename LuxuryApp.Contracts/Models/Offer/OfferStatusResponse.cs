﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferStatusResponse
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string CssClass
        {
            get
            {
                return Name.ToLower().Replace(" ", "-");
            }
        }
    }
}
