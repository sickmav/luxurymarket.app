﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferLiveSummaryRequest
    {
        public int CompanyID { get; set; }

        public int SellerID { get; set; }
    }
}
