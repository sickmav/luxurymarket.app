﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Offer
{
    public class OfferSellerSizes
    {
        public int SellerProductId { get; set; }

        public string SellerSizeTypeName { get; set; }

        public List<OfferSellerSize> SellerSizesList { get; set; }
    }

    public class OfferSellerSize
    {
        public int SizeId { get; set; }

        public string Size { get; set; }

        public int Quantity { get; set; }

        public int SizeRegionId { get; set; }

        public int MappedSizeId { get; set; }

        public string MappedSize { get; set; }
    }
}
