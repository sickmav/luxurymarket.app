﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class OfferProductSizeIdAncestors
    {
        public int OfferProductSizeId { get; set; }
        public int OfferProductId { get; set; }
        public int OfferId { get; set; }
    }
}
