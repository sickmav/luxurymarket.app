﻿namespace LuxuryApp.Contracts.Models
{
    public class State
    {
        public int StateId { get; set; }

        public string StateName { get; set; }

        public int CountryId { get; set; }

        public string Abbreviation { get; set; }
    }
}
