﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class ProductSearchMappingCriteria
    {
        public int? BusinessId { get; set; }

        public int BrandId { get; set; }

        public int? ColorId { get; set; }

        public int? MaterialId { get; set; }

        public int? SizeTypeId { get; set; }
    }
}
