﻿
namespace LuxuryApp.Contracts.Models.SizeMappings
{
    public class SizeTypeSize
    {
        public int Id { get; set; }

        public string Size { get; set; }

        public int DisplayOrder { get; set; }
    }
}
