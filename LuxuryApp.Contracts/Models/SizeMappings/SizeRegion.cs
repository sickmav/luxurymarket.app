﻿using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.SizeMappings
{
    public class SizeRegion
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string KeyWords { get; set; }
    }

    public class SizeRegionCollection
    {
        public SizeRegionCollection()
        {
            Items = new List<SizeRegion>();
        }
        public IEnumerable<SizeRegion> Items { get; set; }
    }
}
