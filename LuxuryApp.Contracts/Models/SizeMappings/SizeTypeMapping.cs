﻿
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.SizeMappings
{
    public class SizeTypeMapping
    {
        public int Id { get; set; }

        public SizeType SizeType { get; set; }

        public SizeRegion SizeRegion { get; set; }

        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public bool? IsValid { get; set; }

        public IEnumerable<SizeTypeSize> Sizes { get; set; }
    }

    public class SizeTypeMappingCollection
    {
        public SizeTypeMappingCollection()
        {
            SizeTypeMappingItems = new List<SizeTypeMapping>();
            SellerSizeMappingItems = new List<SellerSizeMapping>();
        }
        public IEnumerable<SizeTypeMapping> SizeTypeMappingItems { get; set; }

        public IEnumerable<SellerSizeMapping> SellerSizeMappingItems { get; set; }

    }
}
