﻿namespace LuxuryApp.Contracts.Models.Orders
{
    public class PaymentMethod
    {
        public int PaymentMethodId { get; set; }

        public string PaymentMethodName { get; set; }
    }
}
