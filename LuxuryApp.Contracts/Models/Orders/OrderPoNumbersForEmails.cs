﻿
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderPoNumberForEmail
    {
        public OrderPoNumberForEmail()
        {
            SellerEmailAddressList = new List<SellerEmailAddress>();
        }
        public int OrderId { get; set; }

        public string BuyerEmailAddress { get; set; }

        public string OrderPoNumber { get; set; }

        public List<SellerEmailAddress> SellerEmailAddressList { get; set; }
    }

    public class SellerEmailAddress
    {
        public SellerEmailAddress()
        {
            EmailList = new List<string>();
        }
        public int OrderSellerId { get; set; }

        public string PoNumber { get; set; }

        public List<string> EmailList { get; set; }
    }
}
