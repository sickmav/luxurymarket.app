﻿
namespace LuxuryApp.Contracts.Models.Orders
{
    public class BuyerEmailDataAfterSellerConfirmation
    {
        public int OrderSellerId { get; set; }

        public int OrderId { get; set; }

        public string BuyerFullName { get; set; }

        public string BuyerEmail { get; set; }

        public string OrderSellerPoNumber { get; set; }

        public string SellerAlias { get; set; }

        public int StatusTypeID { get; set; }

        public string StatusTypeName { get; set; }

        public string SellerEmail { get; set; }
    }
}
