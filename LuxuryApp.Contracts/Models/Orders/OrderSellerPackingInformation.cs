﻿using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderSellerPackingInformationInsert
    {
        public int OrderSellerId { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactEmail { get; set; }

        public TimeSpan FromPickupHour { get; set; }

        public TimeSpan ToPickupHour { get; set; }

        public DateTimeOffset StartPickupDate { get; set; }

        public DateTimeOffset EndPickupDate { get; set; }

        public string Notes { get; set; }

        public IEnumerable<OrderSellerPackageInsert> PackageDimensionsList { get; set; }
    }
}
