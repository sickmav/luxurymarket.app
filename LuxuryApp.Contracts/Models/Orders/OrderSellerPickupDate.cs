﻿using System;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderSellerPickupDate
    {
        public int OrderSellerId { get; set; }

        public DateTimeOffset? StartPickupDate { get; set; }

        public DateTimeOffset? EndPickupDate { get; set; }
    }
}
