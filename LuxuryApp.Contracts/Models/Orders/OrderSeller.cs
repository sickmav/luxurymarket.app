﻿using LuxuryApp.Contracts.Models.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models.Orders
{
    public abstract class OrderSeller
    {
        public OrderSeller()
        {
            ProductItems = new List<OrderSellerProduct>();
        }

        public int OrderSellerId { get; set; }

        public int OrderId { get; set; }

        public string PONumber { get; set; }

        public int SellerCompanyId { get; set; }

        public string SellerAlias { get; set; }

        public string SellerCompanyName { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public int TotalStyles
        {
            get { return ProductItems.Where(x => x.TotalUnits > 0).Select(x => x.ProductDetails?.ProductId ?? 0).Distinct().Count(); }
        }

        public int TotalUnits
        {
            get { return ProductItems.Sum(x => x.TotalUnits); }
        }

        public int StatusTypeId { get; set; }

        public string StatusTypeName { get; set; }

        public virtual IEnumerable<OrderSellerProduct> ProductItems { get; set; }
    }

    public class OrderSellerBuyerView : OrderSeller
    {
        public OrderSellerBuyerView() : base()
        {
        }

        public double TotalLandedCost
        {
            get { return ProductItems.Sum(x => x.TotalProductCost + x.TotalShippingCost); }
        }

        public double TotalShippingCost
        {
            get { return ProductItems.Sum(x => x.TotalShippingCost); }
        }

        public double TotalProductCost
        {
            get
            {
                return ProductItems.Sum(x => x.TotalProductCost);
            }
        }

        public ShippingTax ShippingTax { get; set; }

        public IEnumerable<OrderProductItemBuyerView> ProductItems { get; set; }

    }

    public class OrderSellerView : OrderSeller
    {
        public OrderSellerView() : base()
        {
        }

        public int BuyerCompanyId { get; set; }

        public string BuyerAlias { get; set; }

        public double TotalProductCost
        {
            get { return ProductItems.Sum(x => x.TotalProductCost); }
        }

        public IEnumerable<OrderProductItemSellerView> ProductItems { get; set; }

    }
}
