﻿namespace LuxuryApp.Contracts.Models.Orders
{
    public class RequestFilterOrders
    {
        public int? CompanyId { get; set; }

        public int? StatusTypeId { get; set; }
    }

}
