﻿
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class BoxSizeType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class BoxSizeTypeCollection
    {
        public BoxSizeTypeCollection()
        {
            Items = new List<BoxSizeType>();
        }
        public IEnumerable<BoxSizeType> Items { get; set; }
    }
}
