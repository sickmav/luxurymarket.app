﻿
using System.Collections;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderFile
    {
        public int Id { get; set; }

        public int? MainOrderId { get; set; }

        public int? OrderSellerId { get; set; }

        public string FileName { get; set; }

        public string OriginalName { get; set; }

        public bool Deleted { get; set; }
    }

    public class OrderFileList
    {
        public OrderFileList()
        {
            Items = new List<OrderFile>();
        }

        public IEnumerable<OrderFile> Items { get; set; }
    }
}
