﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class ProductSyncItem
    {
        public string SellerSKU { get; set; }

        public string ModelNumber { get; set; }

        public string MaterialCode { get; set; }

        public string ColorCode { get; set; }

        public string Color { get; set; }

        public string Material { get; set; }

        public string Brand { get; set; }

        public string Season { get; set; }

        public string SizeType { get; set; }

        public string SizeTypeDescription { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double? SellerRetailPrice { get; set; }

        public string Business { get; set; }

        public string Division { get; set; }

        public string Department { get; set; }

        public string Category { get; set; }

        public string Origin { get; set; }
  }

    public class ProductSyncImageItem
    {
        public string SellerSKU { get; set; }

        public string ImageUrl { get; set; }
    }
}
