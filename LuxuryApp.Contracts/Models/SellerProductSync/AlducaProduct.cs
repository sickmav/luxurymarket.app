﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    [Serializable, XmlRoot("SkuStok")]
    public class AlducaProduct
    {
        [XmlElement("sku_id")]
        public string SkuID { get; set; }

        [XmlElement("product_id")]
        public string ProductID { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("item_url")]
        public string ItemUrl { get; set; }

        [XmlElement("category")]
        public string Category { get; set; }

        [XmlElement("brand")]
        public string Brand { get; set; }

        [XmlElement("country_size")]
        public string CountrySize { get; set; }

        [XmlElement("size")]
        public string Size { get; set; }

        [XmlElement("color")]
        public string Color { get; set; }

        [XmlElement("made")]
        public string Made { get; set; }

        [XmlElement("brand_nation")]
        public string BrandsNation { get; set; }

        [XmlElement("texture")]
        public string Texture { get; set; }

        [XmlElement("suitable")]
        public string Suitable { get; set; }

        [XmlElement("model")]
        public string Model { get; set; }

        [XmlElement("supply_price")]
        public string SupplyPrice { get; set; }

        [XmlElement("market_price")]
        public string MarketPrice { get; set; }

        [XmlElement("season")]
        public string Season { get; set; }

        [XmlElement("stock")]
        public string Stock { get; set; }

        [XmlElement("item_description")]
        public string ItemDescription { get; set; }

        [XmlElement("item_imgs")]
        public string ItemImages { get; set; }

        [XmlElement("measurement")]
        public string Measurement { get; set; }
    }
}
