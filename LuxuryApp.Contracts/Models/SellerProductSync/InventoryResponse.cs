﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class InventoryResponse
    {
        public string VendorSKU { get; set; }

        public List<InventorySize> Sizes { get; set; }
    }
}
