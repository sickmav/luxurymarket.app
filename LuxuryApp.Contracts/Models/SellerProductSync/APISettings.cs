﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class APISettings
    {
        public string BaseAddress { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int SellerID { get; set; }

        public bool NeedsPreAuthentication { get; set; }
    }
}
