﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.SellerProductSync
{
    public class FashionDealerProductsList
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "data")]
        public Data Data { get; set; }

        [JsonProperty(PropertyName = "next_step")]
        public int NextStep { get; set; }
    }

    public class Data
    {
        [JsonProperty(PropertyName = "inventory")]
        public List<FashionDealerProduct> Products { get; set; }
    }

    public class FashionDealerProduct
    {
        [JsonProperty(PropertyName = "_id")]
        public Id Id { get; set; }

        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "barcode")]
        public string Barcode { get; set; }

        [JsonProperty(PropertyName = "sex")]
        public string Sex { get; set; }

        [JsonProperty(PropertyName = "age")]
        public string Age { get; set; }

        [JsonProperty(PropertyName = "title_en")]
        public string TitleEn { get; set; }

        [JsonProperty(PropertyName = "color")]
        public string Color { get; set; }

        [JsonProperty(PropertyName = "color_en")]
        public string ColorEn { get; set; }

        [JsonProperty(PropertyName = "desc_en")]
        public string DescEn { get; set; }

        [JsonProperty(PropertyName = "material")]
        public string Material { get; set; }

        [JsonProperty(PropertyName = "material_en")]
        public string MaterialEn { get; set; }

        [JsonProperty(PropertyName = "care")]
        public string Care { get; set; }

        [JsonProperty(PropertyName = "care_en")]
        public string CareEn { get; set; }

        [JsonProperty(PropertyName = "desc")]
        public string Desc { get; set; }

        [JsonProperty(PropertyName = "cur")]
        public int Cur { get; set; }

        [JsonProperty(PropertyName = "mnf_code")]
        public string MnfCode { get; set; }

        [JsonProperty(PropertyName = "sku_parent")]
        public string SkuParent { get; set; }

        [JsonProperty(PropertyName = "size")]
        public string Size { get; set; }

        [JsonProperty(PropertyName = "qty")]
        public int Qty { get; set; }

        [JsonProperty(PropertyName = "stock_price")]
        public double StockPrice { get; set; }

        [JsonProperty(PropertyName = "images")]
        public List<string> Images { get; set; }

        [JsonProperty(PropertyName = "buy_price")]
        public double? BuyPrice { get; set; }

        [JsonProperty(PropertyName = "season")]
        public string Season { get; set; }

        [JsonProperty(PropertyName = "dim_h")]
        public string DimH { get; set; }

        [JsonProperty(PropertyName = "dim_w")]
        public string DimW { get; set; }

        [JsonProperty(PropertyName = "dim_d")]
        public string DimD { get; set; }

        [JsonProperty(PropertyName = "weight")]
        public string Weight { get; set; }

        [JsonProperty(PropertyName = "made")]
        public string Made { get; set; }

        [JsonProperty(PropertyName = "made_en")]
        public string MadeEn { get; set; }
    }

    public class Id
    {
        public string id { get; set; }
    }
}
