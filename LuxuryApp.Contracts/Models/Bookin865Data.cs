﻿using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Contracts.Models.Orders;

namespace LuxuryApp.Contracts.Models
{
    public class Bookin865Data
    {
        public OrderSellerView Order { get; set; }

        public AddressInfo SellerAddress { get; set; }

        public Company SellerCompany { get; set; }

        public AddressInfo BuyerAddress { get; set; }

        public Company BuyerCompany { get; set; }

        public Customer BuyerContactData { get; set; }

        public OrderSellerPackingInformationInsert PackingInformation { get; set; }

    }
}
