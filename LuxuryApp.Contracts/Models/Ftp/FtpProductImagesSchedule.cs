﻿
using System;

namespace LuxuryApp.Contracts.Models.Ftp
{
    public class FtpProductImagesSchedule
    {
        public DateTimeOffset StartDateTime { get; set; }

        public TimeSpan ExecutionTime { get; set; }

        public int NumberUploadedImages { get; set; }

        public int NumberErrors { get; set; }

        public DateTimeOffset NextRun { get; set; }
    }
}