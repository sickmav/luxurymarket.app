﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Shipment
{
    public class CancelShipmentModel
    {
        public int CartId { get; set; }
        public int ShippingRegionId { get; set; }
    }
}
