﻿namespace LuxuryApp.Contracts.Models
{
    public class Business
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayOrder { get; set; }

        public StandardColorsModel StandardColor { get; set; }
    }
}
