﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class EntityDescription
    {
        public int Id { get; set; }

        public int EntityId { get; set; }

        public EntityType EntityType { get; set; }

        public string Description { get; set; }

        public string Notes { get; set; }

        public string Dimensions { get; set; }
    }
}
