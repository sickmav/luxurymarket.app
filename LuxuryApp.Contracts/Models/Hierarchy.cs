﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class SearchBusinessModel : BaseHierarchyModel
    {
        public int BusinessID { get; set; }

        public string BusinessName { get; set; }

        public int SizeTypeId { get; set; }

        public List<SearchDivisionModel> Divisions = new List<SearchDivisionModel>();
    }

    public class SearchDivisionModel : BaseHierarchyModel
    {
        public int BusinessID { get; set; }
        public int DivisionID { get; set; }
        public string DivisionName { get; set; }
        public List<SearchDepartmentModel> Departments = new List<SearchDepartmentModel>();
    }

    public class SearchDepartmentModel : BaseHierarchyModel
    {
        public int BusinessID { get; set; }
        public int DivisionID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public List<SearchCategoryModel> Categories = new List<SearchCategoryModel>();
    }

    public class SearchCategoryModel : BaseHierarchyModel
    {
        public int BusinessID { get; set; }
        public int DivisionID { get; set; }
        public int DepartmentID { get; set; }
        public int HierarchyID { get; set; }
        public int? CategoryID { get; set; }
        public string CategoryName { get; set; }
    }

    public class BaseHierarchyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayOrder { get; set; }
    }

    public class Hierarchy
    {
        public int HierarchyID { get; set; }
        public int BusinessID { get; set; }
        public int DivisionID { get; set; }
        public int DepartmentID { get; set; }
        public int? CategoryID { get; set; }

        public string BusinessName { get; set; }
        public string DivisionName { get; set; }
        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }
    }

    public class SelectListModel
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }
}
