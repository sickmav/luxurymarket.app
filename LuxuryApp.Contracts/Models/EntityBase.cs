﻿using System;

namespace LuxuryApp.Contracts.Models
{
    /// <summary>
    /// The core entity base from witch all core data entities should inherit
    /// </summary>
    public abstract class EntityBase
    {
        /// <summary>
        /// Default entity base constructor
        /// </summary>
        protected EntityBase()
        {
            CreatedDate = DateTimeOffset.Now;
            Active = true;
            Deleted = false;
        }

        #region Properties

        public int EntityBatchId { get; set; }

        public int CreatedBy { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        #endregion
    }
}
