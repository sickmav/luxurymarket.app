﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class TokenValidationResponse
    {
        public TokenValidationResponseType Type { get; set; }

        public string Email { get; set; }
    }
}
