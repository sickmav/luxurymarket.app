﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ISellerAuthenticateAgent
    {
        Task<AuthenticationResponse> Authenticate(APISettings settings);
    }
}
