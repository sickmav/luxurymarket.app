﻿namespace LuxuryApp.Contracts.Agents
{
    public interface IFtpUploadAgent
    {
        void UploadFromQueue();
    }
}
