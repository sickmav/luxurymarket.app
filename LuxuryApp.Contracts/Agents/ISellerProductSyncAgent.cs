﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ISellerProductSyncAgent
    {
        //  Task<IEnumerable<ProductSyncItem>> GetProducts(APISettings settings, Guid mainBatchID);

        Task<int> ProcessProducts(APISettings settings, Guid mainBatchID);

        List<AlducaProduct> GetProducts(APISettings settings);

        Task<bool> InsertProductsAsync(List<AlducaProduct> productsList, Guid mainBatchID, APISettings settings);

        Task<int> InsertStock(List<AlducaProduct> productsList, int sellerId);
    }
}
