﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Agents
{
    public interface ISellerInventoryUpdateAgent
    {
        Task<AuthenticationResponse> Authenticate(APISettings settings);

        Task<List<InventoryResponse>> GetInventory(AuthenticationResponse authResponse, List<string> skus, APISettings settings);
    }
}
