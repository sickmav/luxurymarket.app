﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    // to be kept in sync with table dbo.OfferStatuses
    public enum OfferStatus
    {
        [Description("In progress")]
        InProgress = 1,

        [Description("Pending Approval")]
        SentToPublish = 2,

        [Description("Live")]
        Published = 3,

        [Description("Rejected")]
        Rejected = 4,

        [Description("Canceled")]
        Canceled = 5,

        [Description("Closed")]
        Closed = 6,

        [Description("Expired")]
        Expired = 7
    }
}
