﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    public enum CartReservedStatusType
    {
        None,

        Success = 1,

        PartialReserved = 2,

        ReservedByOthers = 3,

        Failed_InvalidQuantity = 4,

        Failed_OfferIsNotActive = 5
    }
}
