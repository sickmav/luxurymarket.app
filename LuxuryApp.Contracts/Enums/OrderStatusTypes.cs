﻿namespace LuxuryApp.Contracts.Enums
{
    public enum OrderStatusTypes
    {
        None = 0,

        PendingConfirmation = 1,

        ConfirmPendingPayment = 2,

        Reject = 3,

        Cancel = 4,

        PartialConfirmation = 5,

        StartShipping = 6,

        PickedUpFromSeller = 7,

        AtCustoms = 8,

        PickedUpFromCustoms = 9,

        Delivered = 10,

        ProvidedPackageInformation = 11
    }
}
