﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    public enum SellerAPISettingType
    {
        Coltorti = 1,
        AlDuca = 2,
    }
}
