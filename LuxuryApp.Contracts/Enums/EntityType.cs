﻿namespace LuxuryApp.Contracts.Enums
{
    public enum EntityType
    {
        None = 0,

        Image = 1,

        ExcelFile = 2
    }
}
