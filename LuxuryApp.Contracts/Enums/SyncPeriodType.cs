﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    public enum SyncPeriodType
    {
        All = 1,

        LastSync = 2
    }
}
