﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Enums
{
    public enum CurrencyType
    {
        None = 0,

        EUR = 1,

        USD = 2

    }
}
