﻿using System;
using System.Configuration;
using LuxuryApp.Contracts.BackgroudServices;

namespace LuxuryMarket.Services.Vendor.AlducaSync.Implementations
{ 
    public class WorkerConfigAppSettings : IWorkerConfig
    {
        public int DelaySeconds
        {
            get
            {
                int value = 0;
                if (!int.TryParse(ConfigurationManager.AppSettings["RunIntervalSeconds"] ?? "", out value))
                {
                    value = 10;
                };
                return value;
            }
        }

    }
}
