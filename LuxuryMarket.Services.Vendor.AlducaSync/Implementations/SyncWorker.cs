﻿using System;
using System.Threading;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Enums;
using NLog;

namespace LuxuryMarket.Services.Vendor.AlducaSync.Implementations
{
    public class SyncWorker : IWorker
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly ISellerProductSyncAgent _sellerProductSyncAgent;
        private readonly ISellerApiSettingService _sellerApiSettingService;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private Thread _workerThread = null;
        
        public SyncWorker(ISellerProductSyncAgent sellerProductSyncAgent,
                          ISellerApiSettingService sellerApiSettingService,
                          IWorkerConfig workerConfig)
        {
            _sellerProductSyncAgent = sellerProductSyncAgent;
            _sellerApiSettingService = sellerApiSettingService;
            _workerConfig = workerConfig;
        }

        public void Start(CancellationToken cancellationToken)
        {
            if (_workerThread != null && _workerThread.ThreadState == ThreadState.Running)
            {
                return;
            }
            ThreadStart starter = new ThreadStart(DoWork);
            _workerThread = new Thread(starter);
            _workerThread.Start();
        }

        public void DoWork()
        {
            var settings = _sellerApiSettingService.GetSellerApiSettings(SellerAPISettingType.AlDuca).Result;
             while (true)
            {
                try
                {
                    var mainBatchID = Guid.NewGuid();

                    _logger.Info($"Starting upload from queue batch:{Guid.NewGuid()}");
                    //var task=_sellerProductSyncAgent.ProcessProducts(settings, Guid.NewGuid());
                    //task.Wait(taskWaitMiliseconds);

                    var products = _sellerProductSyncAgent.GetProducts(settings);

                    _logger.Trace($"Mapped with success. Starting db insert {products.Count} items");

                    var resultProductsTask = _sellerProductSyncAgent.InsertProductsAsync(products, mainBatchID, settings);

                    resultProductsTask.Wait();

                    _logger.Trace("Products inserted into database");

                    var resultInventoryTask = _sellerProductSyncAgent.InsertStock(products, settings.SellerID);
                    resultInventoryTask.Wait();

                    _logger.Trace("Products Inventory inserted into database");

                   _logger.Info("Finished upload from queue");
                }
                catch (AggregateException ex)
                {
                    _logger.Error("AggregateException exception: " + ex);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }

                Thread.Sleep(new TimeSpan(0, 0, _workerConfig.DelaySeconds));
            }
        }

    }
}
