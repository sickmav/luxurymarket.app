﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace LuxuryMarket.Services.Vendor.AlducaSync
{
    [RunInstaller(true)]
    public class AlDucaServiceInstaller : Installer
    {
        public AlDucaServiceInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.DisplayName = "LuxuryMarket.AlducaProductSync";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = "LuxuryMarket.Services.Vendor.AlducaSync";
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
