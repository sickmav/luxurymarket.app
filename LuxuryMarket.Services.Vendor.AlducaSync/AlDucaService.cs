﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Enums;
using NLog;
using System;
using System.ServiceProcess;
using System.Threading;

namespace LuxuryMarket.Services.Vendor.AlducaSync
{
    public partial class AlDucaService : ServiceBase
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly ISellerProductSyncAgent _sellerProductSyncAgent;
        private readonly ISellerApiSettingService _sellerApiSettingService;
        private Thread _workerThread = null;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly int taskWaitMiliseconds = 216000;

        public AlDucaService(ISellerProductSyncAgent sellerProductSyncAgent,
                       ISellerApiSettingService sellerApiSettingService,
                       IWorkerConfig workerConfig)
        {
            InitializeComponent();

            _sellerProductSyncAgent = sellerProductSyncAgent;
            _sellerApiSettingService = sellerApiSettingService;
            _workerConfig = workerConfig;
            // settings
            this.ServiceName = "LuxuryMarket.AlducaProductSync";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;

        }

        protected override void OnStart(string[] args)
        {
            _logger.Info("Thread started");
            ThreadStart starter = new ThreadStart(DoWork);
            _workerThread = new Thread(starter);
            _workerThread.Start();
        }

        public void DoWork()
        {
            var settings = _sellerApiSettingService.GetSellerApiSettings(SellerAPISettingType.AlDuca).Result;

            while (true)
            {
                try
                {
                    //_logger.Info("Starting upload from queue");
                    //var task=_sellerProductSyncAgent.ProcessProducts(settings, Guid.NewGuid());
                    //task.Wait(taskWaitMiliseconds);
                    //_logger.Info("Finished upload from queue");

                    var mainBatchID = Guid.NewGuid();

                    _logger.Info($"Starting upload from queue batch:{Guid.NewGuid()}");
                   

                    var products = _sellerProductSyncAgent.GetProducts(settings);

                    _logger.Trace($"Mapped with success. Starting db insert {products.Count} items");

                    var resultProductsTask = _sellerProductSyncAgent.InsertProductsAsync(products, mainBatchID, settings);

                    resultProductsTask.Wait(taskWaitMiliseconds);

                    _logger.Trace("Products inserted into database");

                    var resultInventoryTask = _sellerProductSyncAgent.InsertStock(products, settings.SellerID);
                    resultInventoryTask.Wait(taskWaitMiliseconds);

                    _logger.Trace("Products Inventory inserted into database");

                    _logger.Info("Finished upload from queue");

                }
                catch (AggregateException ex)
                {
                    _logger.Error("AggregateException " + ex);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }

                Thread.Sleep(new TimeSpan(0, 0, _workerConfig.DelaySeconds));
            }
        }
    }
}
