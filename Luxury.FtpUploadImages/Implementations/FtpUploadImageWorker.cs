﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Luxury.FtpUploadImages.Implementations
{
    class FtpUploadImageWorker : IWorker
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly IFtpUploadImageAgent _ftpUploadAgent;

        private Task _workerTask = null;

        public FtpUploadImageWorker(IWorkerConfig workerConfig, IFtpUploadImageAgent ftpUploadAgent)
        {
            _workerConfig = workerConfig;
            _ftpUploadAgent = ftpUploadAgent;
        }

        public void Start(CancellationToken cancellationToken)
        {
            if (_workerTask != null && _workerTask.Status == TaskStatus.Running)
            {
                return;
            }
            _workerTask = Task.Run(() => Worker(cancellationToken));

        }

        private async Task Worker(CancellationToken cancellationToken)
        {
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                var timeElapsed = await _ftpUploadAgent.UploadImagesAsync();
                var delaySeconds = (int)Math.Round(_workerConfig.DelaySeconds - timeElapsed);
                if (delaySeconds > 0)
                    await Task.Delay(delaySeconds * 1000, cancellationToken);
            }

        }
    }
}
