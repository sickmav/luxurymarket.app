﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  LuxuryApp.Auth.Core.Entities
{
    public partial class UserClaim<TKey>
    {
        public UserClaim()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }
        public TKey UserId { get; set; }
        public string ClaimValue { get; set; }
        public string ClaimType { get; set; }
    }
}
