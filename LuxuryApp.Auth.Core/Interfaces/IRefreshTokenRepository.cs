﻿using LuxuryApp.Auth.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.Core.Interfaces
{
    public interface IRefreshTokenRepository 
    {
        Task<IEnumerable<RefreshToken>> GetAllAsync();

        Task<IEnumerable<RefreshToken>> FindBySubjectAsync(string subject);

        Task<bool> DeleteAsync(string refreshTokenId);

        Task<bool> RenewAsync(RefreshToken token);

        Task<IEnumerable<RefreshToken>> FindByIdAsync(string refreshTokenId);

        Task InsertTokenAsync(RefreshToken token);
    }
}
