'use strict';

import gulp from 'gulp';
import gutil from 'gulp-util';
import autoprefixer from 'gulp-autoprefixer';
import concat from 'gulp-concat';
import conf from './env_config.json';
import connect from 'gulp-connect';
import cleanCSS from 'gulp-clean-css';
import htmlmin from 'gulp-htmlmin';
import jshint from 'gulp-jshint';
import _ from 'lodash';
import ngConfig from 'gulp-ng-config';
import path from 'path';
import rev from 'gulp-rev';
import revReplace from 'gulp-rev-replace';
import rimraf from 'rimraf';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import templateCache from 'gulp-angular-templatecache';
import uglify from 'gulp-uglify';

const paths = {
  fonts: {
    src: [
        'node_modules/bootstrap/dist/fonts/*',
        'node_modules/font-awesome/fonts/*',
        'assets/fonts/luxurymarket/*'
    ],
    dest: 'dist/fonts'
  },
  i18n: {
    src: [ 'i18n/**/*' ],
    dest: 'dist/i18n'
  },
  images: {
    // src: [ 'assets/images/**/*.{jpg,jpeg,png,gif}' ],
    src: [ 'assets/images/**/*'],
    dest: 'dist/images'
  },
  sass: {
    src: [ 'assets/stylesheets/**/*.scss' ],
    dest: 'dist/css',
    name: 'app.css',
    watch: 'assets/stylesheets/**/*',
    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true
    }
  },
  css: {
    src: [
      'node_modules/bootstrap/dist/css/bootstrap.css',
      'node_modules/font-awesome/css/font-awesome.css',
      'node_modules/angular-bootstrap-datetimepicker/src/css/datetimepicker.css',
      'node_modules/angular-ui-notification/dist/angular-ui-notification.css',
      'node_modules/slick-carousel/slick/slick.css',
      'node_modules/slick-carousel/slick/slick-theme.css',
      'node_modules/ui-select/dist/select.css',
    ],
    dest: 'dist/css',
    name: 'vendor.css'
  },
  js: {
    vendor: [
      'node_modules/angular/angular.js',
      'node_modules/angular-cookies/angular-cookies.js',
      'node_modules/angular-messages/angular-messages.js',
      'node_modules/angular-sanitize/angular-sanitize.js',
      'node_modules/angular-animate/angular-animate.js',
      'node_modules/angular-ui-router/release/angular-ui-router.js',
      'node_modules/angular-translate/dist/angular-translate.js',
      'node_modules/angular-translate/dist/angular-translate-loader-partial/angular-translate-loader-partial.js',
      'node_modules/ng-file-upload/dist/ng-file-upload-all.js',
      'node_modules/angular-local-storage/dist/angular-local-storage.js',
      'node_modules/angular-filter/dist/angular-filter.js',
      'node_modules/jquery/dist/jquery.js',
      'node_modules/bootstrap/dist/js/bootstrap.js',
      'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
      'node_modules/angular-ui-notification/dist/angular-ui-notification.js',
      'node_modules/moment/moment.js',
      'node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
      'node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js',
      'node_modules/underscore/underscore.js',
      'node_modules/humanize-duration/humanize-duration.js',
      'node_modules/angular-timer/dist/angular-timer.js',
      'node_modules/slick-carousel/slick/slick.js',
      'node_modules/ui-select/dist/select.js'
    ],
    app: [
      'src/**/*.module.js',
      'src/**/*.route.js',
      'src/**/*.config.js',
      'src/**/*.interceptor.js',
      'src/**/*.service.js',
      'src/**/*.controller.js',
      'src/**/*.directive.js',
      'src/**/*.constants.js',
      'src/**/libs.*.js'
    ],
    appWatch: [ 'src/**/*.js' ],
    templates: 'src/templates/**/*.html',
    dest: 'dist/js',
    outTemp: 'dist/temp',
    appName: 'app.js',
    vendorName: 'vendor.js',
    watch: 'src/**/*.js'
  },
  dest: 'dist/'
};

const fonts = () => gulp.src(paths.fonts.src)
  .pipe(gulp.dest(paths.fonts.dest));

const i18n = () => gulp.src(paths.i18n.src)
  .pipe(gulp.dest(paths.i18n.dest));

const images = () => gulp.src(paths.images.src)
  .pipe(gulp.dest(paths.images.dest));

const statics = gulp.series(fonts, images, i18n);

// main-hash.css
const scss = () => gulp.src(paths.sass.src)
  .pipe(sass(paths.sass.sassOpts))
  .pipe(sourcemaps.init())
  .pipe(autoprefixer({
    browsers: [
      '>1%',
      'last 3 versions'
    ],
    add: true
  }))
  .pipe(cleanCSS())
  .pipe(rev())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(paths.sass.dest))
  .pipe(rev.manifest('dist/rev-manifest.json', {
    base: paths.dest,
    merge: true
  }))
  .pipe(gulp.dest(paths.dest));

// vendor-hash.css
const css = () => gulp.src(paths.css.src)
  .pipe(concat(paths.css.name))
  .pipe(cleanCSS())
  .pipe(rev())
  .pipe(gulp.dest(paths.css.dest))
  .pipe(rev.manifest('dist/rev-manifest.json', {
    base: paths.dest,
    merge: true
  }))
  .pipe(gulp.dest(paths.dest));

const styles = gulp.series(css, scss);

const vendorJs = () => gulp.src(paths.js.vendor)
  .pipe(jshint())
  .pipe(jshint.reporter('default'))
  .pipe(sourcemaps.init({
    largeFile: true
  })).pipe(concat({
    path: paths.js.vendorName,
    cwd: ''
  }))
  .pipe(uglify({
    mangle: false,
    output: {
      max_line_len: 50000
    }
  }))
  .pipe(rev())
  .pipe(gulp.dest(paths.js.dest))
  .pipe(rev.manifest('dist/rev-manifest.json', {
    base: paths.dest,
    merge: true
  }))
  .pipe(gulp.dest(paths.dest));


  const vendorJsDev = () => gulp.src(paths.js.vendor)
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(sourcemaps.init({
      largeFile: true
    })).pipe(concat({
      path: paths.js.vendorName,
      cwd: ''
    }))
    // .pipe(uglify({
    //   mangle: false,
    //   output: {
    //     max_line_len: 50000
    //   }
    // }))
    .pipe(rev())
    .pipe(gulp.dest(paths.js.dest))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      base: paths.dest,
      merge: true
    }))
    .pipe(gulp.dest(paths.dest));


const cacheTemplates = () => gulp.src(paths.js.templates)//, { since: gulp.lastRun(cacheTemplates) }
  .pipe(templateCache({
    module: 'luxurymarket',
    moduleSystem: 'IIFE'
  })).pipe(gulp.dest(paths.js.outTemp))
  .on('error', gutil.log);

const appConstants = () => {

  // console.log('env is ', gutil.env.envType);

  return gulp.src('env_config.json')
    .pipe(ngConfig('luxurymarket',{
      environment: gutil.env.envType,
      createModule: false,
      wrap: true
    }))
    .pipe(gulp.dest('dist/temp'));

};



const appJs = () => {

  const hasTemplates = _.findIndex(paths.js.app, (path) => path === 'dist/temp/templates.js');
  const hasConfig = _.findIndex(paths.js.app, (path) =>  path === 'dist/temp/env_config.js');

  if (hasConfig === -1) {
    paths.js.app.push(paths.js.outTemp + '/env_config.js');
  }

  if (hasTemplates === -1) {
     paths.js.app.push(paths.js.outTemp + '/templates.js');
  }

  console.log(paths.js.app);

  return gulp.src(paths.js.app)//, { since: gulp.lastRun(appJs) }
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(sourcemaps.init({
      largeFile: true
    })).pipe(concat({
      path: paths.js.appName,
      cwd: ''
    }))
    .pipe(uglify({
      mangle: false,
      output: {
        max_line_len: 50000
      }
    }))
    .pipe(rev())
    .pipe(gulp.dest(paths.js.dest))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      base: paths.dest,
      merge: true
    }))
    .pipe(gulp.dest(paths.dest));
    // .pipe(connect.reload());
};


const appJsDev = () => {
  const hasTemplates = _.findIndex(paths.js.app, (path) => path === 'dist/temp/templates.js');
  const hasConfig = _.findIndex(paths.js.app, (path) =>  path === 'dist/temp/env_config.js');

  if (hasConfig === -1) {
    paths.js.app.push(paths.js.outTemp + '/env_config.js');
  }

  if (hasTemplates === -1) {
     paths.js.app.push(paths.js.outTemp + '/templates.js');
  }

  console.log(paths.js.app);

  return gulp.src(paths.js.app)//, { since: gulp.lastRun(appJs) }
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(sourcemaps.init({
      largeFile: true
    })).pipe(concat({
      path: paths.js.appName,
      cwd: ''
    }))
    .pipe(rev())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.js.dest))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      base: paths.dest,
      merge: true
    }))
    .pipe(gulp.dest(paths.dest));
};

const html = () => gulp.src('index.html')
  .pipe(revReplace({
    manifest: gulp.src('dist/rev-manifest.json')
  }))
  .pipe(htmlmin({ collapseWhitespace: false }))
  .pipe(gulp.dest(paths.dest))
  .pipe(connect.reload());

const server = () => {
  connect.server({
    root: 'dist',
    livereload: true
  });
};

const build = gulp.series(
  statics,
  styles,
  vendorJs,
  gulp.series(cacheTemplates, appConstants,  appJs),
  html
);

const buildDev = gulp.series(
  statics,
  styles,
  vendorJsDev,
  gulp.series(cacheTemplates, appConstants,  appJsDev),
  html
);


const watchers = () => {
  // TODO: clean generated files before series
  gulp.watch(paths.js.templates, gulp.series(cacheTemplates, appJsDev, html));
  gulp.watch(paths.js.appWatch, gulp.series(appJsDev, html));
  gulp.watch(paths.sass.watch, gulp.series(scss, html));

  connect.server({
    root: 'dist',
    livereload: true,
    middleware: (connect, opt) => {
      return [(req, res, next) =>{
        // console.log('from middleware');
        // console.log('req ', req.headers);
        // console.log('res ', res);
        next();
      }];
    }
  });
};

const dev = gulp.series(
  buildDev,
  watchers
);

export {
  fonts,
  i18n,
  images,
  statics, // series of fonts i18n images
  scss,
  css,
  styles,
  vendorJs,
  vendorJsDev,
  cacheTemplates,
  appConstants,
  appJs,
  html,
  build,
  server,
  dev,
  watchers
};

export default build;
