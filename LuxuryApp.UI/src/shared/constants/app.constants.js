﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .constant('MESSAGE_TYPES', {
            RequestSent: 1,
            ForgotPassword: 2
        })
        .constant('COMPANY_TYPES', {
            Buyer: 1,
            Seller: 2,
            Both: 3
        })
        .constant('ORDER_CUSTOMER_TYPE', {
            None: 0,
            Seller: 1,
            Buyer: 2
        })
        .constant('OFFER_COLUMN_NAMES', {
            Brand: 'Brand',
            Business: 'Business',
            Color: 'ColorCode',
            Material: 'Material',
            OfferCost: 'OfferCost',
            ModelNumber: 'SKU',
            Discount: 'Discount'
        })
        .constant('OFFER_STATUS_TYPES', {
            InProgress: 1,
            PendingApproval: 2,
            Published: 3,
            Rejected: 4,
            Canceled: 5,
            Closed: 6,
            Expired: 7
        })
        .constant('FEATURED_EVENT_IMAGE_TYPES', {
            Hero_Image: 1,
            Small_Tiles: 2,
            Big_Tiles: 3
        })
        .constant('FEATURED_EVENT_THEME_TYPES', {
            Default: -1,
            Light: 1,
            Dark: 2
        })
        .constant('PRODUCT_LIST_TYPE', {
            WOMENS: 1,
            HANDBAGS: 2,
            ACCESSORIES: 3,
            SHOES: 4,
            MENS: 5,
            OTHER: 6
        })
        .constant('ADDRESS_TYPES', {
            Billing_Address: 1,
            Shipping_Address: 2,
            Company_Address: 3
        })
        .constant('CART_DELETE_TYPES', {
            SELLER: 1,
            PRODUCT: 2,
            ALL: 3
        })
        .constant('ORDER_STATUS_TYPES', {
            PENDING_CONFIRMATION: 1,
            CONFIRM_PENDING_PAYMENT: 2,
            REJECT: 3,
            CANCEL: 4,
            PARTIAL_CONFIRMATION: 5,
            START_SHIPPING: 6,
            PICKED_UP_FROM_SELLER: 7,
            AT_CUSTOMS: 8,
            PICKED_UP_FROM_CUSTOMS: 9,
            DELIVERED: 10,
            PROVIDED_PACKAGE_INFORMATION: 11
        })
        .constant('CURRENCY', {
            '2': {
                text: 'USD',
                simbol: '$',
                id: 2
            },
            '1': {
                text: 'EUR',
                simbol: '€',
                id: 1
            }
        })
        .constant('RESERVED_STATES', {
            GREEN: 1,
            ORANGE: 2,
            BLACK: 3
        })
        .constant('RESERVED_RELOCK_STATES', {
            ALL: 1,
            PARTIAL: 2,
            RESERVED_BY_OTHER: 3,
            INVALID_QUANTITY: 4,
            FAILED: 5
        })
        .constant('TokenValidationResponseType', {
            Valid: 1,
            InvalidOrExpiredToken: 2,
            InvalidUser: 3
        })
        .constant('TokenPurpose', {
            None: 0,
            ResetPassword: 1,
            Confirmation: 2
        });
})(window.angular);