﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.directives')
        .directive("compareTo", compareTo);

    compareTo.$inject = [];

    function compareTo() {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    }

})(window.angular);