﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.directives')
        .directive("imageonload", imageonload);

    imageonload.$inject = [];

    function imageonload() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('load', function () {
                    scope.$apply(attrs.imageonload);
                });
            }
        };
    }

})(window.angular);