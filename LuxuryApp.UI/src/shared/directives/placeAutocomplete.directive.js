(function () {
  'use strict';

  angular.module('luxurymarket.directives')
    .directive('placeAutocomplete', [
    '$window',
    '_',
    'LocationService',
    PlaceAutocomplete
  ]);

  function PlaceAutocomplete($window, _, LocationService) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function (scope, elem, attrs, ngModelCtrl) {

        var addressComponents;
        var countryRestrictions;

        LocationService.getCountries().then(function (response) {
          if (response.success) {
            scope.countryRestrictions =  _.map(response.data, function (c) {
                  if (c.isO2Code) {
                    return c.isO2Code.toLowerCase();
                  }
                });
          }
        });

        elem.on('keyup', function (evt) {

          if ($window.google && scope.countryRestrictions) {
            var autocomplete = new $window.google.maps.places.Autocomplete(evt.currentTarget, {
              componentRestrictions: {
                country: scope.countryRestrictions
              }
            });

            if (autocomplete) {

              autocomplete.addListener('place_changed', function () {

                var place = autocomplete.getPlace();
                if (place.address_components) {
                  var filtered = _.filter(place.address_components, function (component) {
                    switch (component.types[0]) {
                      case 'street_number':
                      case 'route':
                      case 'locality':
                      case 'administrative_area_level_1':
                      case 'postal_code':
                      case 'country':
                        return true;
                      default:
                        return false;
                    }
                  });
                  ngModelCtrl.$setViewValue(place.name);
                  ngModelCtrl.$render();
                  scope.$emit('gPlaceComponents', { addressComponents: filtered, place: place.name});
                }
              });
            }
          }
        });

      }
    };
  }
})(angular);
