﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.staticcontent')
        .controller('StaticContentController', StaticContentController);

    StaticContentController.$inject = ['$state', 'AuthenticationService'];

    function StaticContentController($state, AuthenticationService) {
        var vm = this;

        // variables
        vm.isLoggedIn = AuthenticationService.isLoggedIn();
        vm.goBackText = vm.isLoggedIn ? 'Go back to site' : 'Go back';

        // init
        // if we need to add more, find a nicer way to do it. We can add params in the routes
        if (($state.current.name === 'auth.static.terms-and-conditions' ||
            $state.current.name === 'auth.static.policies' ||
            $state.current.name === 'auth.static.authenticity') && !vm.isLoggedIn) {
            $state.go('auth.login');
        }

        // functions
        vm.goBack = goBack;

        function goBack() {
            if (vm.isLoggedIn) {
                $state.go('site.home');
            }
            else {
                $state.go('auth.login');
            }
        }
    }
})(window.angular);