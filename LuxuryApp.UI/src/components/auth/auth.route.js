﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('auth.login', {
                url: "/login",
                templateUrl: "auth/new-login.html",
                controller: "LoginController",
                controllerAs: "login"
            })
            .state('auth.register', {
                url: "/register",
                templateUrl: "auth/new-register.html",
                controller: "RegisterController",
                controllerAs: "register"
            })
            .state('auth.forgot-password', {
                url: "/forgot-password",
                templateUrl: "auth/new-forgot-password.html",
                controller: 'ForgotPasswordController',
                controllerAs: 'forgotpass'
            })
            .state('auth.reset-password', {
                url: '/resetpassword?id&code&new',
                templateUrl: 'auth/create-password.html',
                controller: 'ResetPasswordController',
                controllerAs: 'reset'
            })
            .state('auth.message', {
                url: "/message",
                templateUrl: "auth/message.html",
                controller: 'MessageController',
                controllerAs: 'msg',
                params: {
                    type: null
                }
            })
            .state('auth.static', {
                abstract: true,
                url: '/content',
                templateUrl: 'content/index.html',
                controller: 'StaticContentController',
                controllerAs: 'staticcontent'
            })
            .state('auth.static.about', {
                url: "/about",
                templateUrl: "content/about.html"
            })
            .state('auth.static.customer-service', {
                url: "/customer-service",
                templateUrl: "content/customer-service.html",
                controller: 'CustomerServiceController',
                controllerAs: 'customerService',
                translationPartId: 'customer-service'
            })
            .state('auth.static.policies', {
                url: "/policies",
                templateUrl: "content/policies.html"
            })
            .state('auth.static.authenticity', {
                url: "/authenticity",
                templateUrl: "content/authenticity.html"
            })
            .state('auth.static.terms-and-conditions', {
                url: "/terms-and-conditions",
                templateUrl: "content/terms-and-conditions.html"
            });
    }

})(window.angular);
