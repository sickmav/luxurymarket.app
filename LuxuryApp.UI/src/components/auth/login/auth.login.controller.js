﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.login')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['AuthenticationService', '$state', 'localStorageService', 'COMPANY_TYPES', '$rootScope', '$cookies'];

    function LoginController(AuthenticationService, $state, localStorageService, COMPANY_TYPES, $rootScope, $cookies) {
        var vm = this;

        // view model
        vm.loginModel = {
            "userName": '',
            "password": '',
            "rememberMe": true
        };

        // view variables
        vm.hasError = false;
        vm.error_message = null;
        vm.isLoading = false;
        vm.validateField = validateField;

        // functions
        vm.login = login;

        function validateField(fieldName) {
            if (vm.loginForm[fieldName].$invalid && vm.loginForm[fieldName].$dirty) {
                return true;
            }

            if (vm.loginForm.$submitted && vm.loginForm[fieldName].$invalid) {
                return true;
            }

            return false;
        }

        function login() {
            if (vm.loginForm.$invalid) {
                return;
            }

            vm.isLoading = true;
            localStorageService.clearAll();
            AuthenticationService.getToken(vm.loginModel).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    var prevState = $cookies.getObject('previousState');
                    if (prevState && prevState.stateName !== 'auth.login') {
                        $cookies.remove('previousState');
                        $state.go(prevState.stateName, prevState.stateParam);
                    } else if (COMPANY_TYPES.Buyer === response.data.Companies[0].CompanyType) {
                        $state.go('site.home');
                        $rootScope.$broadcast('updateCart', true);
                    } else {
                        // check if its integrated
                        if (response.data.Companies[0].IsIntegrated) {
                            $state.go('admin.create-offer');
                        }
                        else {
                            $state.go('admin.new-offer.upload');
                        }
                    }
                }
                else {
                    vm.hasError = true;
                    vm.error_message = response.error_message;
                }
            });
        }

    }
})(window.angular);
