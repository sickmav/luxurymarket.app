﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.forgotpassword', []);

})(window.angular);