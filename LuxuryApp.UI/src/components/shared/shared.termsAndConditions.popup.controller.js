﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.address')
        .controller('TermsAndConditionsPopupController', TermsAndConditionsPopupController);

    TermsAndConditionsPopupController.$inject = ['$stateParams', '$state', '$rootScope', '$uibModalInstance', '$translatePartialLoader', 'AuthenticationService', 'CompanyService'];

    function TermsAndConditionsPopupController($stateParams, $state, $rootScope, $uibModalInstance, $translatePartialLoader, AuthenticationService, CompanyService) {
        var vm = this;

        // view models

        // functions
        vm.reject = reject;
        vm.accept = accept;

        // view variables
        vm.isLoading = false;

        // init
        $translatePartialLoader.addPart('terms-and-conditions-popup');

        // functions      
        function reject() {
            AuthenticationService.logout();
            $state.go('auth.login');
            $uibModalInstance.dismiss('cancel');
        }

        function accept() {
            vm.isLoading = true;
            CompanyService.acceptTermsAndConditions(AuthenticationService.getCurrentUserID()).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    AuthenticationService.setHasAcceptedTermsAndConditions();
                    $uibModalInstance.dismiss('cancel');
                }
                else {
                    Notification('There was an error. Please try again.');
                }
            });
        }

        // private functions
    }
})(window.angular);