﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.featuredevent')
        .controller('FeaturedEventProductsController', FeaturedEventProductsController);

    FeaturedEventProductsController.$inject = ['$stateParams', 'ProductService', '$state', 'CdnService'];

    function FeaturedEventProductsController($stateParams, ProductService, $state, CdnService) {
        var vm = this;

        // view models
        vm.title = $stateParams.title || "Products";
        vm.subtitle = $stateParams.subtitle;
        vm.list = [];

        // functions
        vm.goToDetails = goToDetails;
        vm.imageLoaded = imageLoaded;

        // init
        _loadProducts($stateParams.featuredEventId);

        // functions
        function goToDetails($event, product) {
            $event.preventDefault();
            $state.go('site.product-details', {
                id: product.id,
                breadcrumbs: [{
                    title: vm.title,
                    state: 'site.featured-event',
                    params: {
                        featuredEventId: $stateParams.featuredEventId
                    }
                }]
            });
        }

        function imageLoaded(product) {
            product.showImage = true;
        }

        // private functions
        function _loadProducts(featureEventId) {
            ProductService.getFeaturedEventProducts(featureEventId)
                .then(function (response) {
                    if (response.success) {

                        var toteBag = _.where(response.data, { id: 1902 }) || [];
                        if (toteBag.length) {
                            response.data = _.difference(response.data, toteBag);
                        }

                        var cardHolders = _.where(response.data, { name: 'Card Holder' }) || [];
                        if (cardHolders.length) {
                            response.data = _.difference(response.data, cardHolders);
                        }

                        var others = _.union(response.data, toteBag);
                        vm.list = _.union(others, cardHolders);

                        _loadImages();
                    }
                    else {
                        // display error -> failed to load products
                    }
                });
        }


        function _loadImages() {
            var params = [];
            _.forEach(vm.list, function (item) {
                if (item.mainImageName.length > 0) {
                    params.push({
                        key: item.mainImageName,
                        imageSizes: [
                              {
                                  "dimension": 1,
                                  "size": 250,
                                  "subFolder": "250"
                              }
                        ]
                    });
                }
                else {
                    item.imageUrl = '';
                }
            });

            CdnService.getImages(params)
                .then(function (response) {
                    if (response.success) {
                        _.forEach(response.data, function (image) {
                            // find product for image
                            var product = _.findWhere(vm.list, {
                                mainImageName: image.key
                            });

                            if (product) {
                                product.imageUrl = image.result[0].url;
                            }
                            else {
                                product.imageUrl = '';
                            }
                        });
                    }
                    else {
                        // 
                    }
                });
        }
    }
})(window.angular);