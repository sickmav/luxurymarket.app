﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.product')
        .controller('ProductDetailsController', ProductDetailsController);

    ProductDetailsController.$inject = ['ProductService', '$stateParams', 'CartService', 'Notification',
        '$scope', '$state', 'AuthenticationService', 'COMPANY_TYPES', '_', 'PRODUCT_LIST_TYPE',
        '$translatePartialLoader', '$timeout'];

    function ProductDetailsController(ProductService, $stateParams, CartService, Notification,
        $scope, $state, AuthenticationService, COMPANY_TYPES, _, PRODUCT_LIST_TYPE,
        $translatePartialLoader, $timeout) {
        var vm = this;

        vm.product = null;
        vm.isLoading = false;

        // functions
        vm.imageLoaded = imageLoaded;
        vm.initSize = initSize;
        vm.addSize = addSize;
        vm.addToCart = addToCart;
        vm.navigateToBreadcrumb = navigateToBreadcrumb;
        vm.selectAllSizes = selectAllSizes;
        vm.continueShopping = continueShopping;
        vm.flipImage = flipImage;
        vm.viewTakeAllProducts = viewTakeAllProducts;
        vm.viewSellerProducts = viewSellerProducts;
        vm.isSeller = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Seller;

        // init
        $translatePartialLoader.addPart('product-details');
        _loadProduct($stateParams.id);
        if ($stateParams.breadcrumbs !== null && !_.isUndefined($stateParams.breadcrumb)) {
            vm.breadcrumbs = $stateParams.breadcrumbs;
        }

        function imageLoaded() {
            vm.product.loadingImages = false;
        }

        function initSize(offer, size) {
            if (offer.takeAll || offer.lineBuy) {
                size.currentQty = size.quantity;
            }
            else {
                size.currentQty = size.userCartQuantity;
            }
        }

        function addSize($event, offer, size, type) {
            $event.preventDefault();
            $event.stopPropagation();

            if (offer.takeAll || offer.lineBuy) {
                return;
            }

            if (type === 1) {
                if (size.currentQty + 1 > size.offerAvailableQuantity) {
                    return;
                }

                size.currentQty++;
            }
            else {
                if (size.currentQty - 1 < 0) {
                    return;
                }
                size.currentQty--;
            }
        }

        function addToCart($event, offer) {
            var itemQuantities = [];
            _.forEach(offer.sizes, function (size) {
                if (size.currentQty > 0) {
                    itemQuantities.push({
                        "offerProductSizeId": size.offerProductSizeID,
                        "quantity": size.currentQty
                    });
                }
            });

            CartService.addToCart({
                buyerCompanyId: AuthenticationService.getCurrentUserID(),
                itemQuantities: itemQuantities
            }).then(function (response) {
                if (response.success) {
                    $scope.$emit('updateCart', false, {
                        itemsCount: response.data.cartSummaryItem.itemsCount,
                        unitsCount: response.data.cartSummaryItem.unitsCount,
                        totalCustomerCost: response.data.cartSummaryItem.totalCustomerCost,
                        totalLandedCost: response.data.cartSummaryItem.totalLandedCost
                    });

                    // update reserved info
                    _.forEach(offer.sizes, function (s) {
                        var stoUpdate = _.findWhere(response.data.items, {
                            offerProductSizeId: s.offerProductSizeID
                        });
                        angular.merge(s, stoUpdate);
                    });

                    _setupReserved(offer, true);
                } else {
                    Notification(response.error_message);
                }
            });
        }

        function navigateToBreadcrumb($event, breadcrumb) {
            $event.preventDefault();

            $state.go(breadcrumb.state, breadcrumb.params);
        }

        function selectAllSizes($event, offer, type) {
            $event.preventDefault();
            $event.stopPropagation();
            _.forEach(offer.sizes, function (size) {
                size.currentQty = type === 1 ? size.offerAvailableQuantity : 0;
            });
        }

        function continueShopping($event) {
            $event.preventDefault();

            $state.go('site.products-wrapper.list', $stateParams.listParams);
        }

        function flipImage($event, image) {
            var mainImageUrl = vm.product.mainImageUrl;

            vm.product.mainImageUrl = image.url;
            image.url = mainImageUrl;
        }

        function viewTakeAllProducts($event, product, offer) {
            $event.preventDefault();
            $event.stopPropagation();

            $state.go('site.products-wrapper.list', {
                oId: offer.offerID,
                header: 'Take all Offer',
                subtitle: ' ',
                listType: PRODUCT_LIST_TYPE.OTHER
            });
        }

        function viewSellerProducts($event, product, offer) {
            $event.preventDefault();
            $event.stopPropagation();

            $state.go('site.products-wrapper.list', {
                sId: offer.sellerCompanyID,
                header: offer.companyName + ' Products',
                listType: PRODUCT_LIST_TYPE.OTHER
            });
        }

        // private functions
        ////////////////////////////////////////////
        function _loadProduct(id) {
            vm.isLoading = true;
            ProductService.getByID(id)
           .then(function (response) {
               vm.isLoading = false;
               if (response.success) {
                   // process offers
                   _.forEach(response.data.offers, function (o) {
                       _setupReserved(o);
                   });


                   var bestPriceOffer = response.data.offers[0];
                   response.data.offers = _.without(response.data.offers, bestPriceOffer);

                   vm.product = response.data;
                   vm.product.bestPriceOffer = bestPriceOffer;

                   // init images variables
                   vm.product.loadingImages = true;
                   vm.product.mainImageUrl = null;
                   vm.product.imagesUrls = [];

                   // load product images
                   _.forEach(vm.product.images, function (img) {
                       if (img.name === vm.product.mainImageName) {
                           vm.product.mainImageUrl = img.link;
                       }
                       else {
                           vm.product.imagesUrls.push({
                               url: img.link
                           });
                       }
                   });

               }
               else {
                   // display error
               }
           });
        }

        function _setupReserved(offer, forceResetTimer) {
            forceResetTimer = forceResetTimer || false;

            var reservedSize = _.find(offer.sizes, function (s) {
                return s.reservedTimeSeconds > 0;
            });

            offer.availableUnitsOnOffer = _.reduce(offer.sizes, function (memo, num) {
                return memo + num.offerAvailableQuantity;
            }, 0);

            if (forceResetTimer) {
                offer.reservedTimeSeconds = 0;
                $timeout(function () {
                    offer.reservedTimeSeconds = _.isUndefined(reservedSize) ? 0 : reservedSize.reservedTimeSeconds;
                }, 10);
            }
            else {
                offer.reservedTimeSeconds = _.isUndefined(reservedSize) ? 0 : reservedSize.reservedTimeSeconds;
            }
        }
    }
})(window.angular);