﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.menu')
        .controller('SiteMenuController', SiteMenuController);

    SiteMenuController.$inject = ['$state', '$rootScope', '_', 'PRODUCT_LIST_TYPE'];

    function SiteMenuController($state, $scope, _, PRODUCT_LIST_TYPE) {
        var vm = this;

        //view vars
        vm.isSearchOpen = false;
        vm.keyword = null;

        // menu items
        vm.items = [
            {
                label: 'Featured',
                state: 'site.home',
                active: true
            },
            {
                label: 'Women\'s Apparel',
                state: 'site.products-wrapper.list',
                params: {
                    header: 'Women\'s Shop',
                    businessId: 4,
                    divisionId: 1,
                    keyword: undefined,
                    featuredEventId: undefined,
                    brandId: undefined,
                    departmentId : undefined,
                    oId: undefined,
                    sId: undefined,
                    subtitle: undefined,
                    listType: PRODUCT_LIST_TYPE.WOMENS
                }
            },
            {
                label: 'Handbags',
                state: 'site.products-wrapper.list',
                params: {
                    header: 'Handbags',
                    businessId: 4,
                    divisionId: 3,
                    departmentId: 7,
                    keyword: undefined,
                    oId: undefined,
                    sId: undefined,
                    featuredEventId: undefined,
                    brandId: undefined,
                    subtitle: undefined,
                    listType: PRODUCT_LIST_TYPE.HANDBAGS
                }
            },
            {
                label: 'Accessories',
                state: 'site.products-wrapper.list',
                params: {
                    header: 'Accessories',
                    businessId: 4,
                    divisionId: 3,
                    departmentId: 1,
                    keyword: undefined,
                    featuredEventId: undefined,
                    brandId: undefined,
                    oId: undefined,
                    sId: undefined,
                    subtitle: undefined,
                    listType: PRODUCT_LIST_TYPE.ACCESSORIES
                }
            },
            {
                label: 'Shoes',
                state: 'site.products-wrapper.list',
                params: {
                    header: 'Shoes',
                    businessId: 4,
                    divisionId: 3,
                    departmentId: 15,
                    keyword: undefined,
                    featuredEventId: undefined,
                    brandId: undefined,
                    oId: undefined,
                    sId: undefined,
                    subtitle: undefined,
                    listType: PRODUCT_LIST_TYPE.SHOES
                }
            },
            {
                label: 'Men\'s Shop',
                state: 'site.products-wrapper.list',
                params: {
                    header: 'Men\'s Shop',
                    businessId: 3,
                    divisionId: undefined,
                    departmentId: undefined,
                    keyword: undefined,
                    featuredEventId: undefined,
                    brandId: undefined,
                    oId: undefined,
                    sId: undefined,
                    subtitle: undefined,
                    listType: PRODUCT_LIST_TYPE.MENS
                }
            },
            {
                label: 'Brands',
                state: 'site.brands'
            }
        ];

        _updateMenu(null, $state.current, $state.current.params);

        //event captured
        $scope.$on('updateMenu', _updateMenu);

        // functions
        vm.menuItemClick = menuItemClick;
        vm.openSearch = openSearch;
        vm.searchByKeyword = searchByKeyword;

        function menuItemClick($event, item) {
            $event.preventDefault();

            if (item.active) {
                return;
            }

            angular.forEach(vm.items, function (menuItem) {
                menuItem.active = false;
            });

            item.active = !item.active;
            $state.go(item.state, item.params, {
                location: 'replace'
            });
            vm.isSearchOpen = false;
        }

        function openSearch($event) {
            $event.preventDefault();
            $event.stopPropagation();

            vm.isSearchOpen = !vm.isSearchOpen;
        }

        function searchByKeyword($event) {
            if ($event.keyCode === 13) {
                vm.isSearchOpen = false;
                $state.go('site.products-wrapper.list', {
                    keyword: vm.keyword,
                    header: vm.keyword,
                    listType: PRODUCT_LIST_TYPE.OTHER,
                    businessId: undefined,
                    departmentId: undefined,
                    divisionId: undefined,
                    featuredEventId: undefined,
                    brandId: undefined,
                    oId: undefined,
                    sId: undefined
                });
                vm.keyword = null;
            }
        }

        function _updateMenu(event, toState, toParams, fromState, fromParams) {
            angular.forEach(vm.items, function (menuItem) {
                menuItem.active = false;
            });

            if (_.isUndefined(toParams) || _.isUndefined(toParams.listType)) {
                var menuItemToMakeActive = _.findWhere(vm.items, { state: toState.name });
                if (!_.isUndefined(menuItemToMakeActive)) {
                    menuItemToMakeActive.active = true;
                }
            }
            else {
                var menuItems = _.where(vm.items, { state: toState.name });
                _.forEach(menuItems, function (mi) {
                    if (mi.params.listType === parseInt(toParams.listType)) {
                        mi.active = true;
                        return false;
                    }
                });
            }
        }
    }
})(window.angular);