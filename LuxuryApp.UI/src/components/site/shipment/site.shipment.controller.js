﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.shipment')
        .controller('ShipmentController', ShipmentController);

    ShipmentController.$inject = ['$state', '$stateParams', 'CartService', 'CdnService', '$scope', '$uibModal'];

    function ShipmentController($state, $stateParams, CartService, CdnService, $scope, $uibModal) {
        var vm = this;

        if ($stateParams.cartId === null) {
            $state.go('site.cart');
        }

        // view models
        vm.details = null;
        vm.isLoading = true;
        vm.isCancelLoading = false;

        // init
        _loadShipments();

        // events
        $scope.$on('updateShipmentListAfterCancel', _updateShipmentListAfterCancel);

        // functions
        vm.continueCheckout = continueCheckout;
        vm.imageLoaded = imageLoaded;
        vm.viewProducts = viewProducts;
        vm.cancelShipment = cancelShipment;

        function continueCheckout() {
            $state.go('site.order-preview', $stateParams);
        }

        function imageLoaded(product) {
            product.showImage = true;
        }

        function viewProducts($event, shipment) {
            if (_.isUndefined(shipment.showProducts)) {
                shipment.showProducts = false;
            }

            shipment.showProducts = !shipment.showProducts;
        }

        function cancelShipment($event, shipment) {
            $event.preventDefault();
            $event.stopPropagation();

            $uibModal.open({
                templateUrl: 'site/shipment-confirm-cancel.html',
                controller: 'DeleteShipmentController',
                controllerAs: 'ds',
                resolve: {
                    cartId: function () {
                        return $stateParams.cartId;
                    },
                    shipment: function () {
                        return shipment;
                    }
                }
            });
        }

        //// Private functions
        ///////////////////////////////////////
        function _loadShipments() {
            CartService.getShipments($stateParams.cartId)
               .then(function (response) {
                   vm.isLoading = false;
                   if (response.success) {
                       vm.details = response.data;
                   }
                   else {
                       // show error
                   }
               });
        }

        function _updateShipmentListAfterCancel($event, data, shipment) {
            vm.details.shipments = _.without(vm.details.shipments, shipment);
            vm.details.shipmentsCount = data.shipmentsCount;
            vm.details.totalShippingCost = data.totalShippingCost;
        }
    }
})(window.angular);