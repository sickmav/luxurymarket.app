﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.cart', []);

})(window.angular);