﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.cart')
        .controller('CartController', CartController);

    CartController.$inject = ['_', 'CartService', 'CdnService', '$state', 'AuthenticationService', '$scope', '$uibModal',
        'CART_DELETE_TYPES', 'Notification', 'SERVICES_CONFIG', 'RESERVED_STATES', '$compile', '$timeout', 'RESERVED_RELOCK_STATES'];

    function CartController(_, CartService, CdnService, $state, AuthenticationService, $scope, $uibModal,
        CART_DELETE_TYPES, Notification, SERVICES_CONFIG, RESERVED_STATES, $compile, $timeout, RESERVED_RELOCK_STATES) {
        var vm = this;

        // view variables
        vm.isOpen = false;
        vm.groupByType = 'default';
        vm.groupingField = null;
        vm.details = null;
        vm.loading = true;
        vm.cartDeleteTypes = CART_DELETE_TYPES;
        vm.isExporting = false;
        vm.exportLink = SERVICES_CONFIG.apiUrl + '/api/cart/export_cart?buyerCompanyId=' + AuthenticationService.getCurrentUserID() + '&token=' + AuthenticationService.getCurrentUserInfo().token;
        vm.countdown = 100;
        vm.RESERVED_STATES = RESERVED_STATES;
        vm.RESERVED_RELOCK_STATES = RESERVED_RELOCK_STATES;

        // functions
        vm.open = open;
        vm.setSellerActive = setSellerActive;
        vm.groupBy = groupBy;
        vm.imageLoaded = imageLoaded;
        vm.continuePurchase = continuePurchase;
        vm.selectAllSizes = selectAllSizes;
        vm.addSize = addSize;
        vm.addToCart = addToCart;
        vm.updateCart = updateCart;
        vm.deleteFromCart = deleteFromCart;
        vm.exportCart = exportCart;
        vm.reserveOfferProduct = reserveOfferProduct;
        vm.closeRelock = closeRelock;

        // init cart
        _load();

        //events captured
        $scope.$on('updateCartAfterDelete', _onUpdateCartAfterDelete);
        $scope.$on('timer-tick', _onTimerTick);

        function continuePurchase() {
            $state.go('site.order-preview', { cartId: vm.details.id });
        }

        function open($event, product) {
            if (typeof product === 'undefined') {
                product.isOpen = false;
            }

            product.isOpen = !product.isOpen;
        }

        function setSellerActive($event, product, seller) {
            seller.active = !seller.active;

            if (!seller.active) {

                var activeSellers = _.where(product.sellerUnits, { active: true });
                var activeSellersCounts = (activeSellers || []).length;
                if (activeSellersCounts === 0) {
                    product.inactive = true;
                }
            }
            else {
                product.inactive = false;
            }


            var params = seller.cartItemSellerUnitIdentifier;
            params.activeFlag = seller.active;
            CartService.setSellerActive(params)
                .then(function (response) {
                    if (response.success) {
                        // merge cart with response
                        vm.details.itemsCount = response.data.cartSummary.itemsCount;
                        vm.details.unitsCount = response.data.cartSummary.unitsCount;
                        vm.details.totalLandedCost = response.data.cartSummary.totalLandedCost;

                        $scope.$emit('updateCart', false, response.data.cartSummary);

                        _.forEach(response.data.cartItems, function (cartItem) {
                            var productToUpdate = _.findWhere(vm.details.items, { productId: cartItem.productId });
                            productToUpdate.totalCustomerCost = cartItem.totalCustomerCost;
                            productToUpdate.totalLandedCost = cartItem.totalLandedCost;
                            productToUpdate.sellerUnits = cartItem.sellerUnits;
                            productToUpdate.averageUnitCost = cartItem.averageUnitCost;
                            productToUpdate.averageUnitDiscountPercentage = cartItem.averageUnitDiscountPercentage;

                            // update product inactive
                            var activeSellers = _.where(productToUpdate.sellerUnits, { active: true });
                            productToUpdate.inactive = (activeSellers || []).length === 0;

                            _.forEach(productToUpdate.sellerUnits, function (si) {
                                _setupReservedState(si);
                                si.resetTimer = true;
                            });
                        });
                    }
                    else {
                        // show error
                    }
                });

        }

        function groupBy($event, type) {
            $event.preventDefault();

            if (type === vm.groupByType) {
                return;
            }

            vm.groupByType = type;
            switch (type) {
                case 'default':
                    vm.groupingField = null;
                    break;

                case 'category':
                    vm.groupingField = 'categoryName';
                    break;

                case 'brand':
                    vm.groupingField = 'brandId';
                    break;
            }
        }

        function imageLoaded(product) {
            product.showImage = true;
        }

        function selectAllSizes($event, seller, type) {
            $event.preventDefault();
            $event.stopPropagation();
            _.forEach(seller.sellerUnitSizes, function (size) {
                size.quantity = type === 1 ? size.availableQuantity : 0;
            });
        }

        function addSize($event, seller, size, type) {
            $event.preventDefault();
            $event.stopPropagation();

            if (seller.isTakeOffer || seller.isLineBuy) {
                return;
            }

            if (type === 1) {
                if (size.quantity + 1 > size.availableQuantity) {
                    return;
                }

                size.quantity++;
            }
            else {
                if (size.quantity - 1 < 0) {
                    return;
                }
                size.quantity--;
            }
        }

        function addToCart($event, seller) {
            var itemQuantities = [];
            _.forEach(seller.sellerUnitSizes, function (size) {
                if (size.quantity > 0) {
                    itemQuantities.push({
                        "offerProductSizeId": size.offerProductSizeId,
                        "quantity": size.quantity
                    });
                }
            });

            CartService.addToCart({
                buyerCompanyId: AuthenticationService.getCurrentUserID(),
                itemQuantities: itemQuantities
            }).then(function (response) {
                if (response.success) {
                    $scope.$emit('updateCart', false, response.data);
                } else {
                    Notification(response.error_message);
                }
            });
        }

        function updateCart($event, product, seller, size) {
            $event.preventDefault();

            var itemQuantities = [], quantityZeroSizesCount = 0;
            _.forEach(seller.sellerUnitSizes, function (size) {
                if (size.quantity === 0) {
                    quantityZeroSizesCount++;
                }

                itemQuantities.push({
                    "offerProductSizeId": size.offerProductSizeId,
                    "quantity": size.quantity
                });
            });

            if (quantityZeroSizesCount === itemQuantities.length) {
                Notification("You can't update the cart with 0 units");
                return false;
            }

            CartService.updateCart({
                buyerCompanyId: AuthenticationService.getCurrentUserID(),
                itemQuantities: itemQuantities
            }).then(function (response) {
                if (response.success) {
                    $scope.$emit('updateCart', false, response.data.cartSummary);

                    // update cart header
                    vm.details.itemsCount = response.data.cartSummary.itemsCount;
                    vm.details.unitsCount = response.data.cartSummary.unitsCount;
                    vm.details.totalLandedCost = response.data.cartSummary.totalLandedCost;

                    // sync ui
                    _.forEach(response.data.cartItemSummaries, function (cartItemSummary) {
                        var productToUpdate = _.findWhere(vm.details.items, { productId: cartItemSummary.productId });
                        productToUpdate.totalLandedCost = cartItemSummary.itemTotalOfferCost;

                        _.forEach(cartItemSummary.cartItemSummaries, function (cis) {
                            var sellerToUpdate = _.findWhere(productToUpdate.sellerUnits, { offerId: cis.offerId });

                            sellerToUpdate.unitsCount = cis.unitsCount;
                            sellerToUpdate.totalLandedCost = cis.totalOfferCost;
                        });
                    });

                    // update reserved times
                    CartService.getReservedTime({
                        cartId: vm.details.id
                    }).then(function (response) {
                        if (response.success) {

                            // update entire cart
                            _.forEach(vm.details.items, function (item) {
                                _.forEach(item.sellerUnits, function (su) {
                                    var suInfo = _.findWhere(response.data.items, { offerProductId: su.cartItemSellerUnitIdentifier.offerProductId });
                                    if (!_.isUndefined(suInfo)) {
                                        su.reservedTimeSeconds = suInfo.reservedTimeSeconds;
                                        _setupReservedState(su);

                                        su.resetTimer = false;
                                        $timeout(function () {
                                            su.resetTimer = true;
                                        }, 10);
                                    }

                                    _.forEach(su.sellerUnitSizes, function (sus) {
                                        var susInfo = _.findWhere(response.data.items, { offerProductSizeId: sus.offerProductSizeId });
                                        if (!_.isUndefined(susInfo)) {
                                            sus.reservedTimeSeconds = susInfo.reservedTimeSeconds;
                                            sus.availableQuantity = susInfo.offerAvailableQuantity;
                                        }
                                    });
                                });
                            });
                        }
                        else {
                            // display error
                        }
                    });

                } else {
                    Notification.primary(response.error_message);
                }
            });
        }

        function deleteFromCart($event, type, product, seller) {
            $event.preventDefault();
            $event.stopPropagation();

            $uibModal.open({
                templateUrl: 'site/cart-confirm-delete.html',
                controller: 'DeleteCartController',
                controllerAs: 'deletecart',
                resolve: {
                    type: function () {
                        return type;
                    },
                    product: function () {
                        return product;
                    },
                    seller: function () {
                        return seller;
                    }
                }
            });
        }

        function exportCart($event) {
            vm.isExporting = true;
            CartService.exportCart({
                buyerCompanyId: AuthenticationService.getCurrentUserID()
            }).then(function (response) {
                vm.isExporting = false;
                if (response.success) {
                    var d = new Date().toISOString().slice(0, 19).replace(/-/g, "");
                    var aToDownload = document.createElement('a');

                    aToDownload.id = 'downloadxls' + d;
                    aToDownload.href = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data;
                    aToDownload.setAttribute('download', "cart-" + d + ".xlsx");

                    document.body.appendChild(aToDownload);
                    aToDownload.click();
                } else {
                    Notification(response.error_message);
                }
            });
        }

        function reserveOfferProduct($event, seller) {
            if (seller.reservedState === RESERVED_STATES.BLACK) {

                seller.relockStatus = -1;
                seller.displayRelockDetails = true;
                angular.element(document.querySelector('.lm-back-drop')).addClass('open').on('click', function () {
                    $scope.$apply(function () {
                        seller.displayRelockDetails = false;
                        angular.element(document.querySelector('.lm-back-drop')).removeClass('open');
                    });
                });

                CartService.relockProduct({
                    cartId: vm.details.id,
                    offerProductId: seller.cartItemSellerUnitIdentifier.offerProductId
                }).then(function (response) {
                    if (response.success) {
                        seller.relockStatus = response.data.reservedStatusType;
                        seller.reservedMessage = response.data.reservedMessage;

                        _.forEach(vm.details.items, function (item) {
                            _.forEach(item.sellerUnits, function (su) {
                                var info = _.find(response.data.items, function (i) {
                                    return i.offerProductId === su.cartItemSellerUnitIdentifier.offerProductId && i.reservedTimeSeconds > 0;
                                });

                                if (!_.isUndefined(info)) {
                                    su.reservedTimeSeconds = info.reservedTimeSeconds;
                                    if (info.reservedStatusType > 0 && su.cartItemSellerUnitIdentifier.offerProductId !== seller.cartItemSellerUnitIdentifier.offerProductId) {
                                        su.relockStatus = info.reservedStatusType;
                                        su.reservedMessage = info.reservedMessage;
                                    }

                                    _setupReservedState(su);

                                    su.resetTimer = false;
                                    $timeout(function () {
                                        su.resetTimer = true;
                                    }, 10);
                                }

                                _.forEach(su.sellerUnitSizes, function (sus) {
                                    var susInfo = _.findWhere(response.data.items, { offerProductSizeId: sus.offerProductSizeId });
                                    if (!_.isUndefined(susInfo)) {
                                        sus.reservedTimeSeconds = susInfo.reservedTimeSeconds;
                                        sus.quantity = susInfo.quantity;
                                        sus.availableQuantity = susInfo.offerAvailableQuantity;
                                    }
                                });

                            });
                        });
                    }
                    else {
                        seller.relockStatus = RESERVED_RELOCK_STATES.OFFER_UNAVAILABLE;
                    }
                });
            }
        }

        function closeRelock($event, seller) {
            $event.preventDefault();
            $event.stopPropagation();
            seller.displayRelockDetails = false;
            angular.element(document.querySelector('.lm-back-drop')).removeClass('open');
        }

        // private functions
        ////////////////////////////////

        function _load() {
            CartService.getCart(AuthenticationService.getCurrentUserID()).then(function (response) {
                vm.loading = false;
                if (response.success) {
                    vm.details = response.data;
                    vm.details.totalLandedCost = vm.details.totalCustomerCost;

                    _.forEach(vm.details.items, function (item) {
                        // update product inactive
                        var activeSellers = _.where(item.sellerUnits, { active: true });
                        item.inactive = (activeSellers || []).length === 0;

                        _.forEach(item.sellerUnits, function (si) {
                            _setupReservedState(si);
                            si.resetTimer = true;
                        });
                    });

                    $scope.$emit('updateCart', true, {
                        itemsCount: response.data.itemsCount,
                        totalCustomerCost: response.data.totalCustomerCost,
                        totalLandedCost: response.data.totalLandedCost,
                        unitsCount: response.data.unitsCount
                    });
                }
                else {
                    // show error
                }
            });
        }

        function _onUpdateCartAfterDelete($event, responseData) {
            if (_.isUndefined(responseData)) {
                vm.details.items = null;
                vm.details.itemsCount = 0;
                vm.details.unitsCount = 0;
                vm.details.totalCustomerCost = 0;
                vm.details.totalLandedCost = 0;
                $scope.$emit('updateCart', true);
            }
            else {
                if (responseData.removedProductIds.length > 0) {
                    vm.details.items = _.difference(vm.details.items, _.filter(vm.details.items, function (item) {
                        return _.contains(responseData.removedProductIds, item.productId);
                    }));
                }

                // update multiple offers for the same product
                if (responseData.affectedRemainingCartItems.length > 0) {
                    _.forEach(responseData.affectedRemainingCartItems, function (item) {
                        var cartItemToUpdate = _.findWhere(vm.details.items, { productId: item.productId });
                        cartItemToUpdate.sellerUnits = item.sellerUnits;
                        cartItemToUpdate.averageUnitDiscountPercentage = item.averageUnitDiscountPercentage;
                        cartItemToUpdate.averageUnitCost = item.averageUnitCost;
                        cartItemToUpdate.totalCustomerCost = item.totalCustomerCost;

                        _.forEach(cartItemToUpdate.sellerUnits, function (si) {
                            _setupReservedState(si);
                            si.resetTimer = true;
                        });
                    });
                }

                vm.details.itemsCount = responseData.cartSummary.itemsCount;
                vm.details.unitsCount = responseData.cartSummary.unitsCount;
                vm.details.totalCustomerCost = responseData.cartSummary.totalCustomerCost;
                vm.details.totalLandedCost = responseData.cartSummary.totalLandedCost;
                $scope.$emit('updateCart', true, responseData.cartSummary);
            }
        }

        function _setupReservedState(sellerOfferProduct) {
            var reservedTotalTimeSeconds = vm.details.cartReservedTime * 60;
            var warningTreshold = 3600;
            if (sellerOfferProduct.reservedTimeSeconds === 0) {
                // black
                sellerOfferProduct.reservedState = RESERVED_STATES.BLACK;
                return;
            }

            if (sellerOfferProduct.reservedTimeSeconds > warningTreshold) {
                // green
                sellerOfferProduct.reservedState = RESERVED_STATES.GREEN;
            }
            else {
                if (reservedTotalTimeSeconds <= warningTreshold) {
                    var newWarningTreshold = reservedTotalTimeSeconds / 2;
                    sellerOfferProduct.reservedState = sellerOfferProduct.reservedTimeSeconds > newWarningTreshold ? RESERVED_STATES.GREEN : RESERVED_STATES.ORANGE;
                }
                else {
                    // orange
                    sellerOfferProduct.reservedState = RESERVED_STATES.ORANGE;
                }
            }
        }

        function _onTimerTick(event, args) {
            //////////////////////////////////////////////////////
            /// NOTE!!! : The language attribute of the timer library was used because we didn't want to touch the source code
            /// If the language will be needed we will need to find other solution, but maybe in time the lib will grow.
            /// The language attribute has a default fallback for english
            /////////////////////////////////////////////////////
            _.forEach(vm.details.items, function (item) {
                var su = _.find(item.sellerUnits, function (suItem) {
                    return suItem.cartItemSellerUnitIdentifier.offerProductId == event.targetScope.language;
                });

                if (!_.isUndefined(su)) {
                    su.reservedTimeSeconds = event.targetScope.countdown;
                    _setupReservedState(su);
                }
            });
        }
    }
})(window.angular);
