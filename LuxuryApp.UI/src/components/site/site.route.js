﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('site.home', {
                url: "/home",
                templateUrl: "site/home.html",
                controller: "HomeController",
                controllerAs: "home",
                translationPartId: 'home'
            })
            .state('site.products-wrapper', {
                url: "/products",
                templateUrl: "site/products-wrapper.html"
            })
            .state('site.products-wrapper.list', {
                url: "/list/:listType?featuredEventId&brandId&businessId&divisionId&departmentId&sId&oId",
                sticky: true,
                views: {
                    'list': {
                        // templateUrl: "site/products-list.html",
                        templateUrl: "site/product-list-vertical.html",
                        controller: "ProductsListController",
                        controllerAs: "products"
                    }
                },
                params: {
                    header: null,
                    title: null,
                    subtitle: null,
                    keyword: null
                }
            })
            .state('site.products-wrapper.details', {
                url: "/details/:id",
                views: {
                    'details': {
                        templateUrl: "site/product-details.html",
                        controller: "ProductDetailsController",
                        controllerAs: "pd"
                    }
                },
                params: {
                    breadcrumbs: null,
                    listParams: null
                }
            })


            .state('site.products', {
                url: "/products/brand/:brandId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-women', {
                url: "/products/women/:businessId/:divisionId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-handbags', {
                url: "/products/handbags/:businessId/:divisionId/:departmentId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-accessories', {
                url: "/products/accessories/:businessId/:divisionId/:departmentId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-shoes', {
                url: "/products/shoes/:businessId/:divisionId/:departmentId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-men', {
                url: "/products/mens-shop/:businessId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.product-details', {
                url: "/products/details/:id",
                templateUrl: "site/product-details.html",
                controller: "ProductDetailsController",
                controllerAs: "pd",
                params: {
                    breadcrumbs: null
                }
            })
            .state('site.cart', {
                url: "/cart",
                templateUrl: "site/cart-items.html",
                controller: "CartController",
                controllerAs: "cart",
                translationPartId: 'cart'
            })
            .state('site.brands', {
                url: "/brands",
                templateUrl: "site/brands.html",
                controller: "BrandsController",
                controllerAs: "brands",
                translationPartId: 'brand'
            })
            .state('site.manage-shippings', {
                url: "/manage-shippings/:cartId",
                templateUrl: "site/manage-shipping.html",
                controller: "ShipmentController",
                controllerAs: "shipment",
                translationPartId: 'manage-shippings'
            })
            .state('site.order-preview', {
                url: "/order-preview/:cartId",
                templateUrl: "site/order-summary.html",
                controller: "OrderSummaryController",
                controllerAs: "os",
                translationPartId: 'order-summary'
            })
            .state('site.featured-event', {
                url: "/featured-event/:featuredEventId",
                templateUrl: "site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    title: null,
                    subtitle: null
                }
            });
    }

})(window.angular);
