﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.menu')
        .controller('AdminMenuController', AdminMenuController);

    AdminMenuController.$inject = ['COMPANY_TYPES', 'AuthenticationService', '$translatePartialLoader', '_'];

    function AdminMenuController(COMPANY_TYPES, AuthenticationService, $translatePartialLoader, _) {
        var vm = this;

        $translatePartialLoader.addPart('admin-left-menu');

        var menuItems = [
            //{
            //    label: 'DASHBOARD',
            //    iconCls: 'fa fa-tachometer',
            //    path: '#/admin/'
            //},
            {
                id: 1,
                label: 'MY_ACCOUNT',
                iconCls: 'fa fa-user',
                children: [
                    {
                        label: 'ACCOUNT_INFO',
                        stateName: 'admin.account-info'
                    },
                    {
                        label: 'MANAGE_ADDRESS',
                        stateName: 'admin.manage-addresses'
                    }
                ]
            }, {
                id: 2,
                label: 'OFFERS',
                iconCls: 'fa fa-tag',
                access: COMPANY_TYPES.Seller,
                children: [
                    {
                        id: 1,
                        label: 'CREATE_NEW_OFFER',
                        stateName: 'admin.new-offer.upload'
                    },
                    {
                        id: 2,
                        label: 'MANAGE_OFFERS',
                        stateName: 'admin.manage-offers'
                    },
                    {
                        id: 3,
                        label: 'PRODUCTS_AND_CREATE_OFFERS',
                        stateName: 'admin.create-offer'
                    }
                ]
            },
            {
                id: 3,
                label: 'ORDERS',
                iconCls: 'fa fa-shopping-cart',
                children: [
                    {
                        label: 'MY_ORDERS',
                        stateName: 'admin.orders'
                    }
                ]
            }
        ];

        vm.items = _.filter(menuItems, function (menuItem) {

            // offers
            if (menuItem.id === 2) {
                var info = AuthenticationService.getCurrentUserInfo().userInfo;
                if (!info.Companies[0].IsIntegrated) {
                    menuItem.children = _.filter(menuItem.children, function (mi) {
                        return mi.id != 3;
                    });
                }
            }

            return _.isUndefined(menuItem.access) || menuItem.access === AuthenticationService.getCurrentUserType() || menuItem.access === COMPANY_TYPES.Both;
        });

        // functions
        vm.menuItemClick = menuItemClick;

        function menuItemClick($event, item) {
            $event.preventDefault();

            if (item.open) {
                item.open = false;
                return;
            }

            angular.forEach(vm.items, function (menuItem) {
                menuItem.open = false;
            });

            item.open = !item.open;
        }
    }

})(window.angular);
