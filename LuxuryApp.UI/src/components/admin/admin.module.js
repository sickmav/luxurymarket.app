﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin', [
        'luxurymarket.components.admin.menu',
        'luxurymarket.components.admin.offer',
        'luxurymarket.components.admin.address',
        'luxurymarket.components.admin.account',
        'luxurymarket.components.admin.order',
        'ui.router'
    ]);

})(window.angular);