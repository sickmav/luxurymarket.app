﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.address')
        .controller('AddressListController', AddressListController);

    AddressListController.$inject = ['$stateParams', '_', '$scope', '$uibModal', 'AddressService', 'AuthenticationService', 'ADDRESS_TYPES', 'COMPANY_TYPES'];

    function AddressListController($stateParams, _, $scope, $uibModal, AddressService, AuthenticationService, ADDRESS_TYPES, COMPANY_TYPES) {
        var vm = this;

        // private vars
        vm.companyId = AuthenticationService.getCurrentUserID();

        // view models
        vm.isLoading = false;
        vm.billingAddresses = null;
        vm.shippingAddresses = null;
        vm.addressTypes = ADDRESS_TYPES;
        vm.isBuyer = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Buyer;

        // functions
        vm.openDetails = openDetails;
        vm.setMain = setMain;

        // init
        _loadAddresses(vm.companyId);

        //events captured
        $scope.$on('onUpdateAddress', _onUpdateAddress);
        $scope.$on('onUpdateMainAddress', _onUpdateMainAddress);

        // functions
        function openDetails($event, address, addressType) {
            var isEdit = true;
            if (address === null) {
                isEdit = false;
                address = { companyId: vm.companyId };
            }

            var modalInstance = $uibModal.open({
                templateUrl: 'admin/address/address-details.html',
                controller: "AddressDetailsController",
                controllerAs: "adDetails",
                resolve: {
                    address: [function () {
                        return address;
                    }],
                    addressType: [function () {
                        return addressType;
                    }],
                    isEdit: [function () {
                        return isEdit;
                    }]
                }
            });
        }

        function setMain($event, addressId) {
            var modalInstance = $uibModal.open({
                templateUrl: 'admin/address/address-setMain.html',
                controller: "AddressSetMainController",
                controllerAs: "adSetMain",
                resolve: {
                    addressId: [function () {
                        return addressId;
                    }]
                }
            });
        }

        // private functions
        function _loadAddresses(companyId) {
            vm.isLoading = true;
            AddressService.getByCompanyId(companyId)
               .then(function (response) {
                   vm.isLoading = false;
                   if (response.success) {
                       vm.billingAddresses = response.data.billingAddress;
                       vm.shippingAddresses = response.data.shippingAddress;
                   }
                   else {
                       // display error
                   }
               });
        }

        function _onUpdateAddress($event, address) {
            _loadAddresses(address.companyId);
            if (address.isEdit) {
                var addressToUpdate = null;
                if (address.addressTypeId === vm.addressTypes.Billing_Address) {
                    addressToUpdate = _.findWhere(vm.billingAddresses, { addressId: address.addressId });
                }
                else {
                    addressToUpdate = _.findWhere(vm.shippingAddresses, { addressId: address.addressId });
                }
                angular.merge(addressToUpdate, address);
            }
            else {
                if (address.addressTypeId === vm.addressTypes.Billing_Address) {
                    vm.billingAddresses.push(address);
                }
                else {
                    vm.shippingAddresses.push(address);
                }
            }
        }

        function _onUpdateMainAddress($event, addressId) {
            var addressToUpdate = null, updateBillingAddress = true;
            addressToUpdate = _.findWhere(vm.billingAddresses, { addressId: addressId });
            if (_.isUndefined(addressToUpdate)) {
                updateBillingAddress = false;
                addressToUpdate = _.findWhere(vm.shippingAddresses, { addressId: addressId });
            }
            if (updateBillingAddress) {
                _.forEach(vm.billingAddresses, function (address) {
                    address.isMain = false;
                });
            }
            else {
                _.forEach(vm.shippingAddresses, function (address) {
                    address.isMain = false;
                });
            }
            addressToUpdate.isMain = true;
        }
    }
})(window.angular);