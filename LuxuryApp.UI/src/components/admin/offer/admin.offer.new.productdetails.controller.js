﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferProductDetailsController', NewOfferProductDetailsController);

    NewOfferProductDetailsController.$inject = ['_', '$stateParams', '$state', 'OfferService', 'OFFER_COLUMN_NAMES', 'AttributesService', 'Notification', 'CdnService', '$timeout', 'OFFER_STATUS_TYPES', 'CURRENCY'];

    function NewOfferProductDetailsController(_, $stateParams, $state, OfferService, OFFER_COLUMN_NAMES, AttributesService, Notification, CdnService, $timeout, OFFER_STATUS_TYPES, CURRENCY) {
        var vm = this;

        if ($stateParams.uniqueId === null) {
            $state.go('admin.new-offer.upload');
        }

        // view model
        vm.item = null;
        vm.OFFER_COLUMN_NAMES = OFFER_COLUMN_NAMES;
        vm.allowChanges = $stateParams.details.offerStatus === OFFER_STATUS_TYPES.InProgress;

        /// view models for edit
        vm.brands = null;
        vm.colors = null;
        vm.materials = null;
        vm.modelnumbers = null;
        vm.canMap = {
            brand: false,
            color: false,
            material: false,
            modelnumber: false
        };

        // functions
        vm.reportError = reportError;
        vm.back = back;
        vm.changeBrand = changeBrand;
        vm.changeColor = changeColor;
        vm.changeMaterial = changeMaterial;
        vm.changeModelNumber = changeModelNumber;
        vm.updateItem = updateItem;
        vm.imageLoaded = imageLoaded;
        vm.changePrice = changePrice;
        vm.updatePrice = updatePrice;
        vm.changeSize = changeSize;
        vm.updateSize = updateSize;

        // init
        _load($stateParams.offerItemId);

        function reportError($event) {
            $state.go('admin.new-offer.product-details-error', {
                details: $stateParams.details,
                offerId: $stateParams.offerId,
                uniqueId: $stateParams.uniqueId,
                offerItemId: vm.item.offerImportBatchItemId,
                productDetails: vm.item
            });
        }

        function back($event) {
            $event.preventDefault();
            $state.go('admin.new-offer.details', $stateParams);
        }

        function changeBrand($event) {
            $event.preventDefault();
            if (vm.allowChanges) {
                if (!vm.canMap.brand) {
                    return;
                }

                vm.item.editBrand = true;
                AttributesService.getBrands().then(function (response) {
                    if (response.success) {
                        vm.brands = response.data;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
            else {
                $event.stopPropagation();
            }
        }

        function changeColor($event) {
            $event.preventDefault();
            if (vm.allowChanges) {
                if (!vm.canMap.color) {
                    return;
                }

                vm.item.editColor = true;
                AttributesService.getColors(vm.item.brand.id).then(function (response) {
                    if (response.success) {
                        vm.colors = response.data;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
            else {
                $event.stopPropagation();
            }
        }

        function changeMaterial($event) {
            $event.preventDefault();
            if (vm.allowChanges) {
                if (!vm.canMap.material) {
                    return;
                }

                vm.item.editMaterial = true;
                AttributesService.getMaterials(vm.item.brand.id).then(function (response) {
                    if (response.success) {
                        vm.materials = response.data;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
            else {
                $event.stopPropagation();
            }
        }

        function changeModelNumber($event) {
            $event.preventDefault();
            if (vm.allowChanges) {
                if (!vm.canMap.modelnumber) {
                    return;
                }

                var colorId = typeof vm.item.color !== 'undefined' ? vm.item.color.id : undefined;
                var materialId = typeof vm.item.material !== 'undefined' ? vm.item.material.id : undefined;

                vm.item.editModelNumber = true;
                AttributesService.getModelNumbers({
                    brandId: vm.item.brand.id,
                    colorId: colorId,
                    materialId: materialId
                }).then(function (response) {
                    if (response.success) {
                        vm.modelnumbers = response.data;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
            else {
                $event.stopPropagation();
            }
        }

        function updateItem($event, type) {
            var params = {
                offerItemId: vm.item.offerImportBatchItemId
            };

            switch (type) {
                case 'brand':
                    params.brandId = vm.item.brand.id;
                    break;
                case 'color':
                    params.colorId = vm.item.color.id;
                    break;
                case 'material':
                    params.materialId = vm.item.material.id;
                    break;
                case 'modelnumber':
                    params.productId = vm.item.modelnumber.id;
                    break;
            }

            OfferService.updateItem(params)
                .then(function (response) {
                    if (response.success) {
                        switch (type) {
                            case 'brand':
                                vm.item.brandName = vm.item.brand.name;
                                vm.item.editBrand = false;
                                vm.canMap.color = true;
                                vm.canMap.material = true;
                                vm.canMap.modelnumber = true;
                                break;
                            case 'color':
                                vm.item.colorCode = vm.item.color.code;
                                vm.item.editColor = false;
                                break;
                            case 'material':
                                vm.item.materialCode = vm.item.material.code;
                                vm.item.editMaterial = false;
                                break;
                            case 'modelnumber':
                                vm.item.modelNumber = vm.item.modelnumber.name;
                                vm.item.editModelNumber = false;
                                break;
                        }

                        var hadImages = vm.item.images.length > 0;
                        angular.merge(vm.item, response.data);

                        // update errors
                        vm.item.uierrors = [];
                        _.forEach(response.data.errors, function (error) {
                            vm.item.uierrors[error.columnName] = error;
                        });

                        if (!hadImages) {
                            _loadImages();
                        }

                        // update state params list
                        var offerItemToUpdate = _.findWhere($stateParams.details.items, {
                            offerImportBatchItemId: vm.item.offerImportBatchItemId
                        });

                        if (offerItemToUpdate) {
                            angular.merge(offerItemToUpdate, vm.item);
                        }
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
        }

        function imageLoaded(image) {
            image.showImage = true;
        }

        function changePrice($event) {
            if (vm.allowChanges) {
                vm.item.offerCostNewValue = vm.item.offerCostValue;
                vm.item.showPriceInput = true;
                $timeout(function () {
                    angular.element($event.target).parent().find('input[type=text]').focus();
                }, 100);
            }
            else {
                $event.preventDefault();
                $event.stopPropagation();
            }
        }

        function updatePrice($event) {
            if (vm.item.updateInProgress) {
                return false;
            }

            vm.item.showPriceInput = false;
            var form = vm['productPriceForm']; // jshint ignore:line
            if (form.$invalid) {
                return false;
            }

            if (vm.item.offerCostNewValue == vm.item.offerCostValue) {
                // same value do nothing
                return false;
            }
            else {
                vm.item.updateInProgress = true;
                //update product
                OfferService.updateItem({
                    offerItemId: vm.item.offerImportBatchItemId,
                    offerCost: parseFloat(vm.item.offerCostNewValue)
                }).then(function (response) {
                    vm.item.updateInProgress = false;
                    if (response.success) {
                        // update price ui
                        var productToUpdate = _.findWhere(response.data.items, { offerImportBatchItemId: vm.item.offerImportBatchItemId });
                        angular.merge(vm.item, productToUpdate);

                        // update errors
                        vm.item.uierrors = [];
                        _.forEach(response.data.errors, function (error) {
                            vm.item.uierrors[error.columnName] = error;
                        });

                        // update state params list
                        var offerItemToUpdate = _.findWhere($stateParams.details.items, {
                            offerImportBatchItemId: vm.item.offerImportBatchItemId
                        });

                        if (offerItemToUpdate) {
                            angular.merge(offerItemToUpdate, vm.item);
                        }

                        // update totals
                        $stateParams.details.totalCost = response.data.totalCost;
                        $stateParams.details.totalUnits = response.data.totalUnits;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
        }

        function changeSize($event, size) {
            if (vm.allowChanges) {
                size.quantityNewValue = size.quantityValue;
                size.showSizeInput = true;
                $timeout(function () {
                    angular.element($event.target).parent().find('input[type=text]').focus();
                }, 100);
            }
            else {
                $event.preventDefault();
                $event.stopPropagation();
            }
        }

        function updateSize($event, size) {
            if (size.updateInProgress) {
                return false;
            }

            size.showSizeInput = false;
            var form = vm['productSizeForm' + size.sizeTypeSizeId]; // jshint ignore:line
            if (form.$invalid) {
                return false;
            }

            if (size.quantityNewValue == size.quantityValue) {
                // same value do nothing
                return false;
            }
            else {
                size.updateInProgress = true;

                // update size
                var newQty = parseInt(size.quantityNewValue);
                OfferService.updateProductSize({
                    offerImportBatchItemId: vm.item.offerImportBatchItemId,
                    sizeId: size.sizeTypeSizeId,
                    quantity: newQty
                }).then(function (response) {
                    size.updateInProgress = false;
                    if (response.success) {
                        size.quantityValue = newQty;
                        vm.item.actualTotalUnits = response.data;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
        }

        // private functions
        ////////////////////////////////////////////////
        function _load(itemId) {
            OfferService.getItemDetails(itemId).then(function (response) {
                if (response.success) {
                    vm.item = response.data;
                    vm.item.priceCurrency = CURRENCY[vm.item.currency];
                    var canMap = _.isUndefined(vm.item.productId);
                    vm.canMap = {
                        brand: canMap,
                        color: false,
                        material: false,
                        modelnumber: false
                    };

                    if (vm.item.hasErrors) {
                        vm.item.uierrors = [];
                        _.forEach(vm.item.errors, function (error) {
                            vm.item.uierrors[error.columnName] = error;
                        });
                    }
                }
                else {
                    Notification.error({ message: response.error_message });
                }
            });
        }

        function _revalidate() {
            OfferService.validateItem(vm.item.offerImportBatchItemId).then(function (response) {
                if (response.success) {
                    vm.item.uierrors = [];
                    _.forEach(response.data, function (error) {
                        vm.item.uierrors[error.columnName] = error;
                    });
                }
                else {
                    Notification.error({ message: response.error_message });
                }
            });
        }
    }

})(window.angular);