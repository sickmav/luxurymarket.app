﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('ManageOffersController', ManageOffersController);

    ManageOffersController.$inject = ['OfferService', '$scope', '_', '$state', 'OFFER_STATUS_TYPES', '$uibModal'];

    function ManageOffersController(OfferService, $scope, _, $state, OFFER_STATUS_TYPES, $uibModal) {
        var vm = this;

        // view models
        vm.offers = [];
        vm.isNewPageLoading = false;
        vm.isFirstLoad = true;
        vm.filters = {
            page: 1,
            totalPages: 2,
            statuses: [{
                id: -1,
                name: 'All'
            }],
            selectedStatus: -1,
            keyword: null
        };
        vm.totals = {
            totalProducts: 0,
            totalOffers: 0
        };

        // functions
        vm.filterByStatus = filterByStatus;
        vm.filterByKeyword = filterByKeyword;
        vm.viewDetails = viewDetails;
        vm.deleteOffer = deleteOffer;

        // Load initial page (first page or from query param)
        _search(vm.filters.page);

        // Register event handler
        $scope.$on('endlessScroll:next', function () {
            _search(++vm.filters.page);
        });
        $scope.$on('updateOffersListAfterDelete', _updateOffersListAfterDelete);

        // functions
        function filterByStatus() {
            vm.filters.page = 1;
            vm.filters.totalPages = 2;
            _search();
        }

        function filterByKeyword($event) {
            if ($event.which === 13 || $event.type === 'click') {
                vm.filters.page = 1;
                vm.filters.totalPages = 2;
                _search();
            }
        }

        function viewDetails(offer) {
            $state.go('admin.offer-details', {
                offerNumber: offer.offerNumber
            });
        }

        function deleteOffer($event, offer) {
            $uibModal.open({
                templateUrl: 'admin/offer/offer-confirm-delete.html',
                controller: 'DeleteOfferController',
                controllerAs: 'deleteoffer',
                resolve: {
                    offer: [function () {
                        return offer;
                    }]
                }
            });
        }

        // private functions
        function _getStatuses() {
            OfferService.getStatuses()
                .then(function (response) {
                    if (response.success) {
                        vm.filters.statuses.push.apply(vm.filters.statuses, response.data);
                    }
                    else {
                        // show error
                    }
                });
        }

        function _loadLiveSummary() {
            OfferService.getLiveSummary()
                .then(function (response) {
                    if (response.success) {
                        vm.totals = response.data;
                    }
                    else {
                        // show error
                    }
                });
        }

        function _search() {
            var isTerminal = vm.filters.page > vm.filters.totalPages;

            if (!isTerminal) {
                vm.isNewPageLoading = true;

                var params = {
                    page: vm.filters.page
                };

                if (vm.filters.selectedStatus > 0) {
                    params.statusID = vm.filters.selectedStatus;
                }

                params.keyword = vm.filters.keyword;

                OfferService.getOffers(params)
                    .then(function (response) {
                        vm.isNewPageLoading = false;
                        if (vm.isFirstLoad) {
                            vm.isFirstLoad = false;
                            _getStatuses();
                            _loadLiveSummary();
                        }

                        if (response.success) {
                            if (vm.filters.page === 1) {
                                vm.offers = [];
                            }

                            _.forEach(response.data.offers, function (o) {
                                if (!_.isUndefined(o.startDate)) {
                                    o.uiStartDate = new Date(o.startDate);
                                }

                                if (!_.isUndefined(o.endDate)) {
                                    o.uiEndDate = new Date(o.endDate);
                                }

                                o.canBeDeleted = (o.offerStatus === OFFER_STATUS_TYPES.Rejected || o.offerStatus === OFFER_STATUS_TYPES.Expired || o.offerStatus === OFFER_STATUS_TYPES.InProgress);
                            });

                            vm.offers.push.apply(vm.offers, response.data.offers);
                            vm.filters.totalPages = response.data.totalPages;
                        }
                        else {

                        }
                    });
            }
        }

        function _updateOffersListAfterDelete($event, offer) {
            vm.offers = _.without(vm.offers, offer);
        }
    }

})(window.angular);