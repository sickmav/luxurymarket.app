﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferDocumentsController', NewOfferDocumentsController);

    NewOfferDocumentsController.$inject = ['$scope', '_', '$stateParams', '$state', 'OfferService', 'Notification'];

    function NewOfferDocumentsController($scope, _, $stateParams, $state, OfferService, Notification) {

        if ($stateParams.uniqueId === null) {
            $state.go('admin.new-offer.upload');
        }

        // update steps
        $scope.$emit('updateSteps', { number: 3, complete: false, active: true });

        // view models
        $scope.uploading = false;
        $scope.uploadLater = ($stateParams.documents || {}).uploadLater || false;

        // functions
        $scope.upload = upload;
        $scope.continue = continueProcess;
        $scope.back = back;
        $scope.uploadFiles = $stateParams.documents === null ? null : $stateParams.documents.files;
        $scope.cancelUpload = cancelUpload;
        $scope.checkDisabled = checkDisabled;

        $scope.$watch('files', function () {
            if ($scope.uploadFiles && $scope.uploadFiles.length > 0) {
                $scope.uploadFiles = _.union($scope.uploadFiles, $scope.files);
            } else {
                $scope.uploadFiles = $scope.files;
            }
            $scope.upload($scope.files);
        });

        function continueProcess($event) {
            $scope.$emit('updateSteps', { number: 3, complete: true, active: true });
            $stateParams.documents = {
                files: $scope.uploadFiles,
                uploadLater: $scope.uploadLater
            };
            $state.go('admin.new-offer.summary', $stateParams);
        }

        function back($event) {
            $event.preventDefault();
            $stateParams.documents = {
                files: $scope.uploadFiles,
                uploadLater: $scope.uploadLater
            };
            $state.go('admin.new-offer.details', $stateParams);
        }

        function upload(files) {
            if (files && files.length) {
                $scope.files = files;
                angular.forEach(files, function (file) {
                    if (!file.$error) {
                        var syncFile = _findSyncFile(file);
                        syncFile.progress = 5;
                        $scope.uploading = true;
                        file.isLoading = true;
                        OfferService.uploadSupportingDocument({
                            offerId: $stateParams.offerId,
                            file: file
                        }).then(function (response) {
                            file.isLoading = false;
                            $scope.uploading = false;
                            if (response.success) {
                                file.fileId = response.data.data;
                            }
                            else {
                                syncFile.error = response.error_message;
                            }
                        }, null, function (evt) {
                            syncFile.progress = parseInt(100.0 * evt.loaded / evt.total);
                        });
                    }
                });
            }
        }

        function cancelUpload($event, file) {
            if (file.progress < 100) {
                $event.preventDefault();
                return false;
            }

            file.isLoading = true;
            OfferService.updateSupportingDocument({
                offerImportBatchId: $stateParams.offerId,
                offerImportFileId: file.fileId,
                deleted: true
            }).then(function (response) {
                file.isLoading = false;
                if (response.success) {
                    $scope.uploadFiles = _.without($scope.uploadFiles, file);
                    Notification('Deleted with success');
                }
                else {
                    Notification('Unable to delete file. Please try again');
                }
            });
        }

        function checkDisabled() {
            var hasFiles = true;
            if (_.isUndefined($scope.uploadFiles)) {
                hasFiles = false;
            }
            else if ($scope.uploadFiles.length === 0) {
                hasFiles = false;
            }
            return $scope.uploading || (!$scope.uploadLater && !hasFiles);
        }

        // private functions
        //////////////////////////////////////
        function _findSyncFile(file) {
            return _.findWhere($scope.uploadFiles, {
                name: file.name
            });
        }
    }

})(window.angular);