﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('DeleteOfferController', DeleteOfferController);

    DeleteOfferController.$inject = ['$rootScope', '$uibModalInstance', 'Notification', 'offer', 'OfferService'];

    function DeleteOfferController($rootScope, $uibModalInstance, Notification, offer, OfferService) {
        var vm = this;

        // view models
        vm.isLoading = false;
        vm.question = null;

        // functions
        vm.close = close;
        vm.submit = submit;

        // init
        vm.title = 'Delete offer';
        vm.question = 'Are you sure you want to delete offer ' + offer.offerNumber + ' ?';

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function submit($event) {
            vm.isLoading = true;
            OfferService.deleteOffer(offer.offerNumber)
                .then(function (response) {
                    vm.isLoading = false;
                    if (response.success) {
                        close();
                        $rootScope.$broadcast('updateOffersListAfterDelete', offer);
                        Notification('Offer deleted with success');
                    }
                    else {
                        Notification('There was an error. Please try again.');
                    }
                });
        }
    }
})(window.angular);