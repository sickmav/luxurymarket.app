﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('CreateOfferController', CreateOfferController);

    CreateOfferController.$inject = [
      'OfferService',
      'localStorageService',
      '_',
      'CURRENCY',
      'APP_ENV',
      '$translatePartialLoader',
      'AuthenticationService',
      'AttributesService',
      '$timeout',
      '$state',
      'Notification'
    ];

    function CreateOfferController(OfferService, localStorageService, _, CURRENCY,
        APP_ENV, $translatePartialLoader, AuthenticationService,
        AttributesService, $timeout, $state, Notification) {

        var vm = this;

        vm.areAllSelected = false;
        vm.isLoading = true;
        vm.products = [];
        vm.excludedProducts = [];
        vm.selectedProducts = [];
        vm.brands = [{
            id: -1,
            name: 'All Brands'
        }];
        vm.categories = [{
            id: -1,
            name: 'All Categories'
        }];
        vm.seasons = [{
            id: -1,
            name: 'All Seasons'
        }];
        vm.totalPages = 0;
        vm.currencyId = 0;
        vm.currencySymbol = '';
        vm.viewMode = false;
        vm.maxSize = 5;
        vm.updatePercentName = null;
        vm.updatePercent = null;
        vm.updatePercentType = null;
        vm.updatePercentages = [
          { id: 1, name: 'Below Cost' },
          { id: 2, name: 'Below Retail' },
          { id: 3, name: 'Above Cost' }
        ];

        vm.filter = {
            page: 1,
            sellerID: AuthenticationService.getCurrentUserID(),
            categories: [],
            brands: [],
            seasons: [],
            keyword: '',
            hasInventory: false
        };

        vm.selCategories = [];
        vm.selBrands = [];
        vm.selSeasons = [];
        vm.isFiltering = false;

        // functions
        vm.sellerProducts = _sellerProducts;
        vm.changePage = changePage;
        vm.toggleProductSelection = toggleProductSelection;
        vm.toggleViewMode = toggleViewMode;
        vm.isViewMode = isViewMode;
        vm.productPriceChanged = productPriceChanged;
        vm.isProductSelected = isProductSelected;
        vm.getProductSizes = getProductSizes;

        vm.getBrands = getBrands;
        vm.getCategories = getCategories;
        vm.getSeasons = getSeasons;

        vm.handleKeywordSearch = handleKeywordSearch;
        vm.selectBrand = selectBrand;
        vm.removeBrand = removeBrand;
        vm.selectCategory = selectCategory;
        vm.removeCategory = removeCategory;
        vm.selectSeason = selectSeason;
        vm.removeSeason = removeSeason;
        vm.updatePageItemsPricing = updatePageItemsPricing;

        vm.createOffer = createOffer;
        vm.selectTerm = selectTerm;
        vm.selectAllProducts = selectAllProducts;
        vm.toggleHasInventoryFilter = toggleHasInventoryFilter;

        // load translation
        $translatePartialLoader.addPart('offer-create');

        // init
        _sellerProducts();

        // load filters
        getBrands();
        getCategories();
        getSeasons();

        // setup currency
        _currencyId();

        function _sellerProducts(page) {
            vm.isLoading = true;
            return OfferService.getSellerProducts(vm.filter).then(function (res) {
                vm.isLoading = false;
                if (res.success) {
                    vm.products = res.data.offerSellerProductsList;
                    // to do 
                    for (var i = 0; i < vm.products.length; i += 1) {
                        vm.products[i].viewPrice = 0;
                        if (vm.areAllSelected) {
                            vm.products[i].isSelected = true;
                        }
                    }

                    vm.filter.page = res.data.pageNumber;
                    vm.totalPages = res.data.totalPages;
                    vm.totalItems = res.data.totalProducts;

                    if (vm.selectedProducts.length === 0) {
                        vm.selectedProducts = new Array(vm.totalPages);
                        for (var j = 0; j < vm.totalPages; j += 1) {
                            vm.selectedProducts[j] = [];
                        }
                    }

                    if (vm.updatePercentType !== null) {
                        updatePageItemsPricing();
                    }
                } else {
                    // show error?
                }
            });
        }

        function selectTerm(term) {
            vm.updatePercentType = term.id;
            vm.updatePercentName = term.name;
        }

        function selectAllProducts($event) {
            $event.preventDefault();

            vm.areAllSelected = !vm.areAllSelected;
            if (vm.areAllSelected) {
                _.forEach(vm.products, function (product) {
                    product.isSelected = true;
                    toggleProductSelection(null, product, product.viewPrice);
                });
            }
            else {
                _.forEach(vm.products, function (product) {
                    product.isSelected = false;
                });

                _.forEach(vm.selectedProducts, function (page) {
                    page = [];
                });
            }
        }

        function getProductSizes(product) {
            product.isLoadingSizes = true;
            OfferService.getSellerSizesForProduct(product.sellerProductId)
              .then(function (res) {
                  product.isLoadingSizes = false;
                  if (res.success) {
                      var sizes = res.data;

                      for (var i = 0; i < sizes.sellerSizesList.length; i += 1) {
                          sizes.sellerSizesList[i].currentQty = 0;
                      }

                      for (var j = 0; j < vm.products.length; j += 1) {
                          if (vm.products[j].sellerProductId === product.sellerProductId) {
                              vm.products[j].sizes = sizes;
                          }
                      }

                  } else {
                      // show error?
                  }
              });
        }

        function _currencyId() {
            var info = localStorageService.get('authorizationData' + APP_ENV).userInfo;
            if (info.Companies[0].currencyId) {
                vm.currencySymbol = CURRENCY[info.Companies[0].currencyId];
            } else {
                vm.currencySymbol = CURRENCY[1].simbol;
            }
        }

        function getBrands() {
            AttributesService.getBrands()
              .then(function (res) {
                  if (res.success) {
                      vm.brands = vm.brands.concat(res.data);
                  } else {
                      // show error?
                  }
              });
        }

        function getCategories() {
            OfferService.getCategories()
              .then(function (res) {
                  if (res.success) {
                      vm.categories = vm.categories.concat(res.data);
                  } else {
                      // show error?
                  }
              });
        }

        function getSeasons() {
            OfferService.getSeasons()
              .then(function (res) {
                  if (res.success) {
                      vm.seasons = vm.seasons.concat(res.data);
                  } else {
                      // show error?
                  }
              });
        }

        function toggleViewMode() {
            vm.viewMode = !vm.viewMode;
        }

        function isViewMode(v) {
            if (vm.viewMode === true) {
                return true;
            }
            return false;
        }

        function changePage() {
            vm.products = [];
            _sellerProducts();
        }

        function isProductSelected(pid) {
            var found = _.find(vm.selectedProducts[vm.filter.page - 1], function (parr) {
                return (parr.productId === pid);
            });

            if (found !== undefined) {
                return true;
            }
            return false;
        }

        function toggleProductSelection($event, product, price) {
            if (vm.areAllSelected && !product.isSelected) {
                vm.excludedProducts.push({
                    productId: product.sellerProductId,
                    offerPrice: price
                });
            }

            var spPage = vm.selectedProducts[vm.filter.page - 1];

            if (!vm.isProductSelected(product.sellerProductId)) {
                spPage.push({
                    productId: product.sellerProductId,
                    offerPrice: price
                });
            } else {
                var filtered = _.filter(spPage, function (obj) {
                    return (obj.productId !== product.sellerProductId);
                });
                vm.selectedProducts[vm.filter.page - 1] = filtered;
            }
        }

        function productPriceChanged(price, pid) {
            if (vm.isProductSelected(pid)) {
                var found = _.find(vm.selectedProducts[vm.filter.page - 1], function (obj) {
                    return (obj.productId === pid);
                });

                var idx = _.indexOf(vm.selectedProducts[vm.filter.page - 1], found);

                vm.selectedProducts[vm.filter.page - 1][idx].price = price;
            }
        }

        function selectBrand(i, m) {
            vm.filter.brands.push(i.id);
            vm.isFiltering = true;
            _sellerProducts().then(function () {
                vm.isFiltering = false;
            });
        }

        function selectCategory(i, m) {
            vm.filter.categories.push(i.id);
            vm.isFiltering = true;
            _sellerProducts().then(function () {
                vm.isFiltering = false;
            });
        }

        function selectSeason(i, m) {
            vm.filter.seasons.push(i.id);
            vm.isFiltering = true;
            _sellerProducts().then(function () {
                vm.isFiltering = false;
            });
        }


        function removeBrand(i, m) {
            var idx = _.indexOf(vm.filter.brands, i.id);
            if (idx >= 0) {
                vm.filter.brands = vm.filter.brands.slice(0, idx).concat(vm.filter.brands.slice(idx + 1));
                vm.isFiltering = true;
                _sellerProducts().then(function () {
                    vm.isFiltering = false;
                });
            }
        }
        function removeCategory(i, m) {
            var idx = _.indexOf(vm.filter.categories, i.id);
            if (idx >= 0) {
                vm.filter.categories = vm.filter.categories.slice(0, idx).concat(vm.filter.categories.slice(idx + 1));
                vm.isFiltering = true;
                _sellerProducts().then(function () {
                    vm.isFiltering = false;
                });
            }
        }

        function removeSeason(i, m) {
            var idx = _.indexOf(vm.filter.seasons, i.id);
            if (idx >= 0) {
                vm.filter.seasons = vm.filter.seasons.slice(0, idx).concat(vm.filter.seasons.slice(idx + 1));
                vm.isFiltering = true;
                _sellerProducts().then(function () {
                    vm.isFiltering = false;
                });
            }

        }

        function handleKeywordSearch(e) {
            vm.filter.keyword = e.target.value;
            $timeout(function () {
                vm.isFiltering = true;
                _sellerProducts().then(function () {
                    vm.isFiltering = false;
                });
            }, 2000);
        }

        function updatePageItemsPricing() {
            vm.isUpdatingPricings = true;
            if (vm.updatePercentType !== null) {
                for (var i = 0; i < vm.products.length; i += 1) {

                    var newPrice = null;
                    if (vm.updatePercentType === 1) {
                        newPrice = vm.products[i].wholesaleCost - ((vm.updatePercent / 100) * vm.products[i].wholesaleCost);
                    } else if (vm.updatePercentType === 2) {
                        newPrice = vm.products[i].retailPrice - ((vm.updatePercent / 100) * vm.products[i].retailPrice);
                    } else if (vm.updatePercentType === 3) {
                        newPrice = vm.products[i].wholesaleCost + ((vm.updatePercent / 100) * vm.products[i].wholesaleCost);
                    }

                    vm.products[i].viewPrice = newPrice;
                }
            }
            else {
                Notification.primary('Please select terms');
            }

            $timeout(function () {
                vm.isUpdatingPricings = false;
            }, 250);
        }

        function createOffer($event) {
            var selectedProducts = _.flatten(vm.selectedProducts);
            if (selectedProducts.length === 0) {
                $event.preventDefault();
                Notification.primary('You have 0 selected products');
                return false;
            }

            vm.isCreatingOffer = true;
            OfferService.createOffer(selectedProducts,
                vm.excludedProducts,
                vm.areAllSelected,
                vm.filter,
                {
                    percentage: vm.updatePercent,
                    type: vm.updatePercentType
                }, AuthenticationService.getCurrentUserID()
                ).then(function (response) {
                    vm.isCreatingOffer = false;
                    if (response.success) {
                        // proceed to offer details
                        $state.go('admin.new-offer.details', {
                            details: response.data.data,
                            offerId: response.data.data.id,
                            uniqueId: response.data.data.id + (new Date().toString())
                        });
                    }
                    else {
                        Notification.primary('Something went wrong. Please contact support if the error persists.');
                    }
                });

        }

        function toggleHasInventoryFilter($event) {
            vm.isFiltering = true;
            _sellerProducts().then(function () {
                vm.isFiltering = false;
            });
        }

    }
})(window.angular);
