﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferController', NewOfferController);

    NewOfferController.$inject = ['$scope', '_', '$translatePartialLoader'];

    function NewOfferController($scope, _, $translatePartialLoader) {
        var vm = this;

        // init translation for wizzard
        $translatePartialLoader.addPart('offer-wizard');

        // view models
        // we need to keep the steps in this order
        vm.steps = [
            {
                name: 'STEP_ONE_WIZZARD_TITLE',
                number: 1,
                active: false,
                complete: false
            },
            {
                name: 'STEP_TWO_WIZZARD_TITLE',
                number: 2,
                active: false,
                complete: false
            },
            {
                name: 'STEP_THREE_WIZZARD_TITLE',
                number: 3,
                active: false,
                complete: false
            },
            {
                name: 'STEP_FOUR_WIZZARD_TITLE',
                number: 4,
                active: false,
                complete: false
            }
        ];

        // events
        $scope.$on('updateSteps', _updateSteps);

        // private functions
        function _updateSteps(event, data) {
            angular.forEach(vm.steps, function (step) {
                if (step.number > data.number) {
                    // disable future steps
                    step.complete = false;
                    step.active = false;
                }
                else if (step.number < data.number) {
                    // previous steps
                    step.complete = true;
                    step.active = true;
                }
                else {
                    // this is current step
                    step.complete = data.complete;
                    step.active = data.active;
                }
            });
        }
    }

})(window.angular);