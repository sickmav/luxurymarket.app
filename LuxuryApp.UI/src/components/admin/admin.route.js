﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('admin.dashboard', {
                url: "/",
                templateUrl: "admin/dashboard.html",
                authenticate: true
            })
          .state('admin.manage-offers', {
              url: "/manage-offers",
              templateUrl: "admin/offer/manage-offers.html",
              controller: "ManageOffersController",
              controllerAs: "mo",
              authenticate: true
          })
         .state('admin.offer-details', {
             url: "/offer/details/:offerNumber",
             templateUrl: "admin/offer/offer-details.html",
             controller: 'NewOfferDetailsController',
             controllerAs: 'offerdetails',
             translationPartId: 'offer-details',
             authenticate: true
         })
          .state('admin.account-info', {
              url: "/account-info",
              templateUrl: "admin/account-info.html",
              controller: "AccountInfoController",
              controllerAs: "account",
              translationPartId: 'account-info',
              authenticate: true
          })
          .state('admin.manage-addresses', {
              url: "/manage-addresses",
              templateUrl: "admin/address/manage-addresses.html",
              controller: "AddressListController",
              controllerAs: "addresses",
              translationPartId: 'manage-addresses',
              authenticate: true
          })
         .state('admin.orders', {
             url: "/orders",
             templateUrl: "admin/order/manage-orders.html",
             controller: "OrdersController",
             controllerAs: "orders",
             translationPartId: 'manage-orders',
             authenticate: true
         })
         .state('admin.order-details', {
             url: "/orders/:orderId",
             templateUrl: "admin/order/order-details.html",
             controller: "OrderDetailsController",
             controllerAs: "od",
             translationPartId: 'order-details',
             authenticate: true
         })
        .state('admin.seller-confirmed', {
            url: "/order-confirmed",
            templateUrl: "admin/order/order-confirmed.html",
            translationPartId: 'order-confirmed',
            authenticate: true
        })
         .state('admin.order-files', {
             url: "/orders/:orderId/files?oc",
             templateUrl: "admin/order/support-documents.html",
             controller: "OrderFilesController",
             translationPartId: 'order-support-documents',
             authenticate: true
         })
        .state('admin.order-pickup-information', {
            url: "/orders/:orderId/order-pickup",
            templateUrl: "admin/order/pickup-information.html",
            controller: "OrderPickupInformationController",
            controllerAs: "orderPickup",
            translationPartId: 'order-pickup-information',
            authenticate: true
        });

    }
})(window.angular);
