﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order', []);

})(window.angular);