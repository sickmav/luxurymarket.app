﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('UpdatePickupInformationBoxSizeController', UpdatePickupInformationBoxSizeController);

    UpdatePickupInformationBoxSizeController.$inject = ['$rootScope', '$uibModalInstance', 'boxSizeType', '$translatePartialLoader'];

    function UpdatePickupInformationBoxSizeController($rootScope, $uibModalInstance, boxSizeType, $translatePartialLoader) {
        var vm = this;

        // init
        $translatePartialLoader.addPart('order-pickup-information-confirm-update');

        // view models        
        vm.title = 'P_I_CONFIRM_UPDATE_TITLE';
        vm.question = 'P_I_CONFIRM_UPDATE_QUESTION';

        // functions
        vm.close = close;
        vm.submit = submit;

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function submit($event) {
            close();
            $rootScope.$broadcast('updateBoxSizeType', boxSizeType);
        }
    }
})(window.angular);