﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('DeletePickupInformationBoxSizeController', DeletePickupInformationBoxSizeController);

    DeletePickupInformationBoxSizeController.$inject = ['$rootScope', '$uibModalInstance', 'boxSizeType', '$translatePartialLoader'];

    function DeletePickupInformationBoxSizeController($rootScope, $uibModalInstance, boxSizeType, $translatePartialLoader) {
        var vm = this;

        // init
        $translatePartialLoader.addPart('order-pickup-information-confirm-delete');

        // view models        
        vm.title = 'P_I_CONFIRM_DELETE_TITLE';
        vm.question = 'P_I_CONFIRM_DELETE_QUESTION';
        vm.translationData = {
            boxLength: boxSizeType.length,
            boxWidth: boxSizeType.width,
            boxDepth: boxSizeType.depth
        };

        // functions
        vm.close = close;
        vm.submit = submit;

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function submit($event) {
            close();
            $rootScope.$broadcast('deleteBoxSizeType', boxSizeType.boxSizeId);
        }
    }
})(window.angular);