﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.order')
        .controller('OrderFilesController', OrderFilesController);

    OrderFilesController.$inject = ['OrderService', '$stateParams', '$state', '_', 'ORDER_STATUS_TYPES', 'Notification', '$scope', 'ORDER_CUSTOMER_TYPE', '$translatePartialLoader'];

    function OrderFilesController(OrderService, $stateParams, $state, _, ORDER_STATUS_TYPES, Notification, $scope, ORDER_CUSTOMER_TYPE, $translatePartialLoader) {
        var vm = this;

        // view models
        $scope.isLoading = false;
        $scope.alreadyUploaded = false;
        $scope.orderIsConfirmed = $stateParams.oc === 'true';

        // functions
        $scope.upload = upload;
        $scope.cancelUpload = cancelUpload;
        $scope.checkDisabled = checkDisabled;
        $scope.goToOrderDetails = goToOrderDetails;

        // load translations
        $translatePartialLoader.addPart('order-confirmed');

        // events
        $scope.$watch('files', function () {
            if ($scope.uploadFiles && $scope.uploadFiles.length > 0) {
                $scope.uploadFiles = _.union($scope.uploadFiles, $scope.files);
            } else {
                $scope.uploadFiles = $scope.files;
            }
            _.forEach($scope.files, function (file) {
                if (_.isUndefined(file.timeId)) {
                    file.timeId = file.name + (new Date().getTime());
                }
            });
            $scope.upload($scope.files);
        });

        // init
        _init();

        // functions implementation
        function upload(files) {
            if (files && files.length) {
                $scope.files = files;
                angular.forEach(files, function (file) {
                    if (!file.$error) {
                        var syncFile = _findSyncFile(file);
                        syncFile.progress = 5;
                        $scope.uploading = true;
                        file.isLoading = true;
                        OrderService.uploadSupportingDocument({
                            orderId: $stateParams.orderId,
                            file: file
                        }).then(function (response) {
                            file.isLoading = false;
                            $scope.uploading = false;
                            if (response.success) {
                                file.fileId = response.data.data;
                            }
                            else {
                                syncFile.error = response.error_message;
                            }
                        }, null, function (evt) {
                            syncFile.progress = parseInt(100.0 * evt.loaded / evt.total);
                        });
                    }
                });
            }
        }

        function checkDisabled() {
            var hasFiles = true;
            if (_.isUndefined($scope.uploadFiles)) {
                hasFiles = false;
            }
            else if ($scope.uploadFiles.length === 0) {
                hasFiles = false;
            }
            return $scope.uploading || (!$scope.alreadyUploaded && !hasFiles);
        }

        function cancelUpload($event, file) {
            if (file.progress < 100) {
                $event.preventDefault();
                return false;
            }

            file.isLoading = true;
            OrderService.updateSupportingDocument({
                orderId: $stateParams.orderId,
                fileId: file.fileId,
                deleted: true
            }).then(function (response) {
                file.isLoading = false;
                if (response.success) {
                    $scope.uploadFiles = _.without($scope.uploadFiles, file);
                    Notification('Deleted with success');
                }
                else {
                    Notification('Unable to delete file. Please try again');
                }
            });
        }

        function goToOrderDetails($event) {
            $state.go('admin.order-details', {
                orderId: $stateParams.orderId
            });
        }

        // private functions
        //////////////////////////////////////
        function _init() {
            // load already uploaded supporting documents
            $scope.isLoadingHistory = true;
            OrderService.getDocuments($stateParams.orderId)
                .then(function (response) {
                    $scope.isLoadingHistory = false;
                    if (response.success) {
                        _.forEach(response.data.items, function (file) {
                            file.name = file.originalName;
                            file.progress = 100;
                            file.fileId = file.id;
                            file.timeId = file.name + (new Date().getTime());
                        });
                        $scope.uploadFiles = ($scope.uploadFiles || []).concat(response.data.items);
                    }
                });
        }

        function _findSyncFile(file) {
            return _.findWhere($scope.uploadFiles, {
                timeId: file.timeId
            });
        }
    }
})(window.angular);