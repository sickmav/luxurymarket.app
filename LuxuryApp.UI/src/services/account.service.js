﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('AccountService', AccountService);

    AccountService.$inject = ['$http', 'SERVICES_CONFIG'];

    function AccountService($http, SERVICES_CONFIG) {

        var svc = {
            getCurrent: getCurrent,
            submitChanges: submitChanges,
            changePassword: changePassword
        };

        return svc;

        function getCurrent() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/v1/users/current')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on product load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function submitChanges(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/v1/users/updateAccountInfo', params)
               .then(function (response) {
                   if (response.status === 200) {
                       return { success: true, data: response.data.data };
                   }
                   else {
                       return { success: false, error_message: 'Please try again' };
                   }
               }, function (response) {
                   return { success: false, error_message: response.data.exceptionMessage };
               });
        }

        function changePassword(params) {
            return $http.post(SERVICES_CONFIG.authServiceUrl + '/api/accounts/ChangePassword', params)
               .then(function (response) {
                   if (response.status === 200) {
                       return { success: true, data: response.data.data };
                   }
                   else {
                       return { success: false, error_message: 'Please try again' };
                   }
               }, function (response) {
                   return { success: false, error_message: response.data.exceptionMessage };
               });
        }
    }
})(window.angular);