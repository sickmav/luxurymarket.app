﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('ProductService', ProductService);

    ProductService.$inject = ['$http', 'SERVICES_CONFIG', '$q', 'AuthenticationService'];

    function ProductService($http, SERVICES_CONFIG, $q, AuthenticationService) {

        var svc = {
            search: search,
            filter: filter,
            getByID: getByID,
            getFeaturedEventProducts: getFeaturedEventProducts,
            getBestSellers: getBestSellers,
            addAllFilteredProducts: addAllFilteredProducts,
            addSelectedProducts: addSelectedProducts
        };

        var searchPromise = null;
        return svc;

        function search(params) {
            if (searchPromise && searchPromise._httpTimeout && searchPromise._httpTimeout.resolve) {
                searchPromise._httpTimeout.resolve();
            }

            searchPromise = $http.post(SERVICES_CONFIG.apiUrl + '/api/products/Search', {
                BusinessID: params.businessId,
                DepartmentID: params.departmentId,
                divisionID: params.divisionId,
                CategoryIDs: params.categories,
                Page: params.page,
                brandIDs: params.brands || [],
                colorIDs: params.colors || [],
                sort: params.sort || 0,
                FeaturedEventID: params.featuredEventId,
                Keyword: params.keyword,
                SellerCompanyID: params.sellerId,
                CompanyID: AuthenticationService.getCurrentUserID(),
                OfferID: params.offerId
            }).then(function (response) {
                searchPromise = null;
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error on products load. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });

            return searchPromise;
        }

        function filter(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/products/Filter', {
                "companyId": params.companyId || -1,
                "businessIds": params.businessIds || [],
                "divisionIds": params.divisionIds || [],
                "departmentIds": params.departmentIds || [],
                "categoriesIds": params.categoriesIds || [],
                "sort": params.sort || 0,
                "pageNumber": params.pageNumber || 1,
                "pageSize": 0
            }).then(function (response) {
                if (response.status === 200) {
                    return {
                        success: true, data: {
                            products: response.data,
                            totalRecords: response.totalRecords
                        }
                    };
                }
                else {
                    return { success: false, error_message: 'There was an error on products load. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }

        function getFeaturedEventProducts(featureEventId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/products/feature_event/' + featureEventId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on products load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function getByID(offerItemId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/products/details?id=' + offerItemId + '&companyId=' + AuthenticationService.getCurrentUserID())
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on product load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function getBestSellers() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/products/get_best_sellers?companyID=' + AuthenticationService.getCurrentUserID())
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error on products load. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }

        function addSelectedProducts(params) {

          return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/add_products', {
            buyerCompanyId: params.buyerCompanyId,
            productId: params.productId
          }).then(function (res) {
            if (res.data && res.data.code && res.data.code === 200) {
                return {
                  success: true,
                  data: res.data
                };
            } else {
                return {
                  success: false,
                  error_message: 'There was an error with sending selected products. Please try again'
                };
            }
          }, function (err) {
            return { success: false, error_message: err.data.Message };
          });
        }

        function addAllFilteredProducts(params) {
            // console.log(params);
          return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/add_all_products', {
            buyerCompanyId: params.buyerCompanyId,
            search: params.search
          }).then(function (res) {
            if (res.data && res.data.code && res.data.code === 200) {
                return {
                  success: true,
                  data: res.data
                };
            } else {
                return {
                  success: false,
                  error_message: 'There was an error with sending selected products. Please try again'
                };
            }
          }, function (err) {
            return { success: false, error_message: err.data.Message };
          });
        }
    }
})(window.angular);
