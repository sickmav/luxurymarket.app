﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('AttributesService', AttributesService);

    AttributesService.$inject = ['$http', 'SERVICES_CONFIG', '$q', 'localStorageService'];

    function AttributesService($http, SERVICES_CONFIG, $q, localStorageService) {

        var svc = {
            getBrands: getBrands,
            getColors: getColors,
            getStandardColors : getStandardColors,
            getMaterials: getMaterials,
            getModelNumbers: getModelNumbers
        };

        var cacheKeys = {
            BRAND: 'BRAND_CACHE_KEY',
            COLOR: 'COLOR_CACHE_KEY',
            MATERIAL: 'MATERIAL_CACHE_KEY',
            MODELNUMBER: 'MODELNUMBER_CACHE_KEY'
        };

        return svc;

        function getBrands() {
            var brands = localStorageService.get(cacheKeys.BRAND);
            if (brands) {
                return $q.resolve({ success: true, data: brands });
            }
            else {
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/brands')
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set(cacheKeys.BRAND, response.data);
                            return { success: true, data: response.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load brands' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load brands' };
                    });
            }
        }

        function getColors(brandId) {
            var apiCall = null;
            if (typeof brandId === 'undefined') {
                brandId = 0;
                apiCall = '/api/colors';
            }
            else {
                apiCall = '/api/colors/brand/' + brandId;
            }

            var items = localStorageService.get(cacheKeys.COLOR + brandId);
            if (items) {
                return $q.resolve({ success: true, data: items });
            }
            else {
                return $http.get(SERVICES_CONFIG.apiUrl + apiCall)
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set(cacheKeys.COLOR + brandId, response.data);
                            return { success: true, data: response.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load colors' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load colors' };
                    });
            }
        }

        function getStandardColors() {
            var items = localStorageService.get('standardColors');
            if (items) {
                return $q.resolve({ success: true, data: items });
            }
            else {
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/colors/standards')
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set('standardColors', response.data);
                            return { success: true, data: response.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load colors' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load colors' };
                    });
            }
        }

        function getMaterials(brandId) {
            var items = localStorageService.get(cacheKeys.MATERIAL + brandId);
            if (items) {
                return $q.resolve({ success: true, data: items });
            }
            else {
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/materials/brand/' + brandId)
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set(cacheKeys.MATERIAL + brandId, response.data);
                            return { success: true, data: response.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load materials' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load materials' };
                    });
            }
        }

        function getModelNumbers(params) {
            var idsKey = params.brandId + '_' + (params.colorId || '') + '_' + (params.materialId || '');
            var items = localStorageService.get(cacheKeys.MODELNUMBER + idsKey);
            if (items) {
                return $q.resolve({ success: true, data: items });
            }
            else {
                return $http.post(SERVICES_CONFIG.apiUrl + '/api/offerImports/getModelNumbers', {
                    "brandId": params.brandId,
                    "colorId": params.colorId,
                    "materialId": params.materialId
                }).then(function (response) {
                    if (response.status === 200) {
                        localStorageService.set(cacheKeys.MODELNUMBER + idsKey, response.data);
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load product ids' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load product ids' };
                });
            }
        }
    }
})(window.angular);