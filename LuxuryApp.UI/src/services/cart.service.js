﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('CartService', CartService);

    CartService.$inject = ['$http', 'localStorageService', 'SERVICES_CONFIG', '_'];

    function CartService($http, localStorageService, SERVICES_CONFIG, _) {

        var svc = {
            getCart: getCart,
            addToCart: addToCart,
            getCartSummary: getCartSummary,
            setSellerActive: setSellerActive,
            updateCart: updateCart,
            getShipments: getShipments,
            cancelShipment: cancelShipment,
            emptyCart: emptyCart,
            removeProduct: removeProduct,
            removeItemSellerUnit: removeItemSellerUnit,
            addOffer: addOffer,
            exportCart: exportCart,
            relockProduct: relockProduct,
            getReservedTime: getReservedTime,
            getInvalidItems: getInvalidItems
        };

        return svc;

        function getCart(buyerCompanyId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/get_or_create_cart_for_buyer?buyerCompanyId=' + buyerCompanyId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load cart' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load cart' };
                });
        }

        function addToCart(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/add_offer_product_sizes', {
                buyerCompanyId: params.buyerCompanyId,
                itemQuantities: params.itemQuantities
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to add to cart. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to add to cart. Please try again' };
            });
        }


        function getCartSummary(buyerCompanyId, includeShipping, cartId, onlyValidItems) {
            if (_.isUndefined(includeShipping)) {
                includeShipping = false;
            }


            if (includeShipping) {
                onlyValidItems = onlyValidItems || false;
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/cart/' + cartId + '/getTotalSummary?onlyValidItems=' + onlyValidItems)
                 .then(function (response) {
                     if (response.status === 200) {
                         return { success: true, data: response.data.data };
                     }
                     else {
                         return { success: false, error_message: 'There was an error. Please try again' };
                     }
                 }, function (response) {
                     return { success: false, error_message: response.data.Message };
                 });
            }
            else {

                return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/get_or_create_cart_summay_for_buyer?buyerCompanyId=' + buyerCompanyId)
                    .then(function (response) {
                        if (response.status === 200) {
                            return { success: true, data: response.data.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load cart summary. Please try again' };
                        }
                    }, function (response) {
                        return { success: false, error_message: 'Unable to load cart summary. Please try again' };
                    });
            }
        }

        function setSellerActive(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/set_active_flag', {
                "cartItemSellerUnitIdentifier": {
                    "cartId": params.cartId,
                    "productId": params.productId,
                    "sellerId": params.sellerId,
                    "offerProductId": params.offerProductId
                },
                "activeFlag": params.activeFlag
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to perform action. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to perform action. Please try again' };
            });
        }

        function updateCart(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/update_items_quantities', {
                buyerCompanyId: params.buyerCompanyId,
                itemQuantities: params.itemQuantities
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to add to cart. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to add to cart. Please try again' };
            });
        }

        function getShipments(cartId) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/shipping/preview_shipping?cartId=' + cartId)
				.then(function (response) {
				    if (response.status === 200) {
				        return { success: true, data: response.data.data };
				    }
				    else {
				        return { success: false, error_message: 'There was an error. Please try again' };
				    }
				}, function (response) {
				    return { success: false, error_message: response.data.Message };
				});
        }

        function cancelShipment(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/shipping/cancel_shipment', {
                "cartId": params.cartId,
                "shippingRegionId": params.shippingRegionId
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to cancel shipment. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to cancel shipment. Please try again' };
            });
        }

        function emptyCart(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/empty_cart?buyerCompanyId=' + params.buyerCompanyId).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to empty cart. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to empty cart. Please try again' };
            });
        }

        function removeProduct(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/remove_product_from_cart', {
                buyerCompanyId: params.buyerCompanyId,
                productIds: params.productIds
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to add to cart. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to add to cart. Please try again' };
            });
        }

        function removeItemSellerUnit(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/remove_item_seller_unit_from_cart', {
                buyerCompanyId: params.buyerCompanyId,
                cartItemSellerUnitIdentifier: params.cartItemSellerUnitIdentifier
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to add to cart. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to add to cart. Please try again' };
            });
        }

        function addOffer(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/add_offer', {
                buyerCompanyId: params.buyerCompanyId,
                offerId: params.offerId
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to add to cart. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: 'Unable to add to cart. Please try again' };
            });
        }

        function exportCart(params) {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/cart/export_cart?buyerCompanyId=' + params.buyerCompanyId).
                then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to export cart. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable export cart. Please try again' };
                });
        }

        function relockProduct(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/relockProduct', {
                cartId: params.cartId,
                offerProductId: params.offerProductId
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data.data };
                }
                else {
                    return { success: false, error_message: 'Unable to relock product. Please try again.' };
                }
            }, function (response) {
                return { success: false, error_message: 'There was an error' };
            });
        }

        function getReservedTime(params) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/reservedTimes?cartId=' + params.cartId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'There was an error' };
                });
        }

        function getInvalidItems(buyerCompanyId) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/cart/get_cart_preview?buyerCompanyId=' + buyerCompanyId)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error. Please try again.' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'There was an error. Please try again.' };
                });
        }
    }

})(window.angular);