﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', 'localStorageService', 'SERVICES_CONFIG', '$rootScope', 'APP_ENV', '$window'];

    function AuthenticationService($http, localStorageService, SERVICES_CONFIG, $rootScope, APP_ENV, $window) {

        var svc = {
            requestNewAccount: requestNewAccount,
            getToken: getToken,
            requestPasswordChange: requestPasswordChange,
            fillAuthData: fillAuthData,
            logout: logout,
            isLoggedIn: isLoggedIn,
            getUserData: getUserData,
            getCurrentUserID: getCurrentUserID,
            getCurrentUserType: getCurrentUserType,
            getCurrentUserInfo: getCurrentUserInfo,
            resetPassword: resetPassword,
            refreshToken: refreshToken,
            setHasAcceptedTermsAndConditions: setHasAcceptedTermsAndConditions,
            validateToken: validateToken
        };

        // private vars
        var _authentication = {
            isAuth: false,
            userName: null
        };

        return svc;

        // functions
        function requestNewAccount(model) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/registrationRequests', model)
                .then(function (response) {
                    if (response.data.code === 200) {
                        return { success: true };
                    }
                    else {
                        return { success: false, error_message: response.data.businessRulesFailedMessages[0] };
                    }
                }, function (response) {
                    return { success: false, error_message: 'There was an error. Please try again' };
                });
        }

        function getToken(model) {
            var data = "grant_type=password&username=" + model.userName + "&password=" + model.password;

            return $http.post(SERVICES_CONFIG.authServiceUrl + '/token', data, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                if (response.status === 200) {

                    _processLoginResponse(response.data);

                    //_initRefreshToken();

                    _authentication.isAuth = true;
                    _authentication.userName = response.data.userName;

                    return { success: true, data: localStorageService.get('authorizationData' + APP_ENV).userInfo };
                }
                else {
                    return { success: false, error_message: 'There was an error. Please try again' };
                }
            }, function errorCallback(response) {
                return { success: false, error_message: (response.data || {}).error_description || 'There was an error. Please try again' };
            });
        }

        function requestPasswordChange(model) {
            return $http.post(SERVICES_CONFIG.authServiceUrl + '/api/accounts/forgotPassword', model)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true };
                    }
                    else {
                        return { success: false, error_message: 'Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.exceptionMessage };
                });
        }

        function getUserData() {
            var userData = localStorageService.get('authorizationUserData');
            if (userData && userData.success) {
                return $q.resolve({ success: true, data: userData });
            } else {
                return $http.get(SERVICES_CONFIG.apiUrl + '/api/v1/users/current')
                    .then(function (response) {
                        if (response.status === 200) {
                            localStorageService.set('authorizationUserData', response.data.data);
                            return { success: true, data: response.data.data };
                        }
                        else {
                            return { success: false, error_message: 'Unable to load user data' };
                        }
                    }, function (response) {
                        return { success: false, error_message: response.data.exceptionMessage };
                    });
            }
        }

        function resetPassword(model) {
            return $http.post(SERVICES_CONFIG.authServiceUrl + '/api/accounts/resetPassword', model)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true };
                    }
                    else {
                        return { success: false, error_message: 'Token is expired or accout is already setup. Please contact support.' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Token is expired or accout is already setup. Please contact support.' };
                });
        }

        function fillAuthData() {
            var authData = localStorageService.get('authorizationData' + APP_ENV);
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }
        }

        function logout() {
            _authentication = {
                isAuth: false,
                userName: null
            };
            localStorageService.clearAll();
        }

        function isLoggedIn() {
            return _authentication.isAuth;
        }

        function getCurrentUserID() {
            var data = localStorageService.get('authorizationData' + APP_ENV);
            return data.userInfo.Companies[0].CompanyId;
        }

        function getCurrentUserType() {
            var data = localStorageService.get('authorizationData' + APP_ENV);
            return data.userInfo.Companies[0].CompanyType;
        }

        function getCurrentUserInfo() {
            var data = localStorageService.get('authorizationData' + APP_ENV);
            if (data) {
                return data;
            }
            else {
                return null;
            }
        }

        function refreshToken() {
            var data = localStorageService.get('authorizationData' + APP_ENV);
            var requestData = "grant_type=refresh_token&refresh_token=" + data.refresh_token;
            return $http.post(SERVICES_CONFIG.authServiceUrl + '/token', requestData, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                noBearer: true
            }).then(function (response) {
                if (response.status === 200) {
                    _processLoginResponse(response.data);
                    _authentication.isAuth = true;

                    return true;
                }
                else {
                    // logout
                    return false;
                }
            }, function (response) {
                return { success: false, error_message: response.data.exceptionMessage };
            });
        }

        function setHasAcceptedTermsAndConditions() {
            var data = localStorageService.get('authorizationData' + APP_ENV);
            data.userInfo.Companies[0].HasAcceptedTermsAndConditions = true;
            localStorageService.set('authorizationData' + APP_ENV, data);
        }

        function validateToken(userGuid, token, purpose) {
            var encodedCode = encodeURIComponent(token);
            return $http.post(SERVICES_CONFIG.authServiceUrl + '/api/accounts/validateCode', {
                userID: userGuid,
                purpose: purpose,
                code: token
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, data: { type: 2 } }; //InvalidOrExpiredToken
                }
            }, function (response) {
                return { success: false, data: { type: parseInt(response.data.message) } };
            });
        }


        // this is no longer used. But is keep until live move
        function _initRefreshToken() {
            var data = localStorageService.get('authorizationData' + APP_ENV);
            setTimeout(function () {
                var requestData = "grant_type=refresh_token&refresh_token=" + data.refresh_token;
                $http.post(SERVICES_CONFIG.authServiceUrl + '/token', requestData, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    noBearer: true
                }).then(function (response) {
                    if (response.status === 200) {

                        _processLoginResponse(response.data);

                        _authentication.isAuth = true;

                        _initRefreshToken();
                    }
                    else {
                        // logout
                    }
                });
            }, data.expiresIn - 30000);
        }

        function _processLoginResponse(data) {
            var startDate = new Date(data[".issued"]);
            var endDate = new Date(data[".expires"]);
            // additional logic
            var customerInfo = JSON.parse(data.customer);
            localStorageService.set('authorizationData' + APP_ENV, {
                token: data.access_token,
                userInfo: customerInfo,
                refresh_token: data.refresh_token,
                expiresIn: endDate - startDate
            });

            if (!_.isUndefined(customerInfo)) {
                $window._mfq.push(["setVariable", "Email Address", customerInfo.UserName]);
            }

            $rootScope.$broadcast('updateUserData', customerInfo);
        }
    }

})(window.angular);
