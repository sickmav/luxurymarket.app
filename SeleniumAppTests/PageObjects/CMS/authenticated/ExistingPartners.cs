﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS.authenticated
{
    public class ExistingPartners
    {
        #region Web Element Identifiers

        private String _searchID = "search";
        private String _contactClass = "contact";
        private String _requestsHeaderClass = "page-header";
        private String _companyLine = "company";
       
        #endregion

        public void searchField(string value)
        {
            WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(value);
            WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(Keys.Enter);

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_companyLine), value));
        }

        public CmsHeader checkApprovedRequest(string value)
        {
            searchField(value);

            return new CmsHeader(WebDriverHelper.Driver);
        }
    }
}
