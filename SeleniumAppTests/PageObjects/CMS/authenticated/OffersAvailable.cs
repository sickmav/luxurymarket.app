﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeleniumAppTests.PageObjects.CMS.authenticated
{
    public class OffersAvailable
    {

        #region Web Element Identifiers

        private String _availableOffersBodyXpath = "/html/body/main/div/div[2]/div[1]/table";
        private String _filterDropdownClass = "caret";
        private String _searchID = "search";
        private String _cancelOfferID = "btnCancelOffer";  

        #endregion

        #region Web Elements

        private IWebElement body;
        private IWebElement dropdown;
        private IWebElement search;

        #endregion

        public  OffersAvailable ()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_availableOffersBodyXpath)));
            InitWebElements();
        }

        public void searchOffer(int searchofferid)
        {         
            WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(searchofferid.ToString());
            SendKeys.SendWait(@"{Enter}");


            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_availableOffersBodyXpath)));
        }

        public void checkOffer(int offeridpublished)
        {
            String xpath = String.Format("//td/p[text()={0}]/ancestor::tr/td/button[contains(@class,'view-products')]", offeridpublished);

            var element = WebDriverHelper.Driver.FindElementByXPath(xpath);
            element.Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_cancelOfferID)));
        }

        public void InitWebElements ()
        {
            body = WebDriverHelper.Driver.FindElementByXPath(_availableOffersBodyXpath);
            dropdown = WebDriverHelper.Driver.FindElementByClassName(_filterDropdownClass);
            search = WebDriverHelper.Driver.FindElementById(_searchID);
        }
    }
}
