﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class AccountSettingsLeftMenu
    {
        #region Web Element Identifiers

        private String _leftSideMenuClass = "nav navbar-nav nav-list";
        private String _leftMenuBarID = "myNavbar";
        private String _viewWebsiteClass = "website";
        private String _myAccountTxt = "MY ACCOUNT";
        private String _offersTxt = "OFFERS";
        private String _ordersTxt = "ORDERS";

        #endregion

        #region Web Elements

        private IWebElement leftMenu;
        private IWebElement backToWebsite;
        private IWebElement myAccount;
        private IWebElement offers;
        private IWebElement orders;
        private IWebElement reports; 

        #endregion

        public AccountSettingsLeftMenu(RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_leftSideMenuClass)));
            InitWebElements();
        }

        public void viewWebsite()
        {
            backToWebsite.Click();
        }

        public void clickMyAccount()
        {
            myAccount.Click();
        }

        public void clickOffers()
        {
            offers.Click();
        }

        public void clickOrders()
        {
            orders.Click();
        }

        public void clickReports()
        {
            reports.Click();
        }

        private void InitWebElements()
        {
            leftMenu = WebDriverHelper.Driver.FindElementByClassName(_leftSideMenuClass);
            backToWebsite = WebDriverHelper.Driver.FindElementByClassName(_viewWebsiteClass);
            myAccount = WebDriverHelper.Driver.FindElementByLinkText(_myAccountTxt);
            offers = WebDriverHelper.Driver.FindElementByLinkText(_offersTxt);
            orders = WebDriverHelper.Driver.FindElementByLinkText(_ordersTxt);
        }
    }
}
