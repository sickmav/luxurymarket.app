﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class SiteHeader
    {
        #region  Web Elements Identifiers

        private String _headerID = "header-luxury";

        #endregion

        public SiteMenu Menu { get; set; }
        public SiteUserMenu UserMenu { get; set; }
        
        // constructor
        public SiteHeader(RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_headerID)));
            initWebElements(driver);
        }
        //method
        public void initWebElements(RemoteWebDriver driver)
        {
            UserMenu = new SiteUserMenu(driver);
            Menu = new SiteMenu(driver);
        }

    }
}
