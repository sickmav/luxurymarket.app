﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class SiteUserMenu
    {
        #region  Web Elements Identifiers

        private String _userMenuID = "usermenu";
        private String _userMenuWrapperID = "usermenuwrapper";
        private String _loginFormID = "loginform";
        private String _singOutTxt = "Sign out";
        private String _accSettingsTxt = "Account Settings";
        private String _accInfoFormXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/div/div[2]";

        #endregion

        #region  Web Elements

        private IWebElement userMenu;

        #endregion
        /// <summary>
        /// Waits for the site user menu to load
        /// </summary>
        public SiteUserMenu(RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var appmenu = driver.FindElementsById(_userMenuWrapperID).ToList();

            try
            {
                if (appmenu.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_userMenuWrapperID)));
                    initWebElements();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Site user menu is not visible or failed to load: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Navigates to account settings page and waits for the account info form to be visible
        /// </summary>
        public void accountSettings(RemoteWebDriver driver)
        {

            openMenu(WebDriverHelper.Driver);

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var accsettings = WebDriverHelper.Driver.FindElementsByLinkText(_accSettingsTxt).ToList();
            accsettings.Count();

            try
            {
                if (accsettings.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText(_accSettingsTxt)));
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Account settings option is not visible or the menu failed to load: " + ex.Message, ex);
            }

            WebDriverHelper.Driver.FindElementByLinkText(_accSettingsTxt).Click();

            var accinfopage = WebDriverHelper.Driver.FindElementsByXPath(_accInfoFormXpath).ToList();
            accinfopage.Count();

            try
            {
                if (accinfopage.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_accInfoFormXpath)));
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Account info form is not visible or the page failed to load: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Sings out the user and waits for the login page form to be visible
        /// </summary>
        public void signOut(RemoteWebDriver driver)
        {
            openMenu(WebDriverHelper.Driver);
            WebDriverHelper.Driver.FindElementByLinkText(_singOutTxt).Click();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var wassignout = WebDriverHelper.Driver.FindElementsById(_loginFormID).ToList();
            wassignout.Count();

            try
            {
                if (wassignout.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_loginFormID)));
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Logout failed or the login page failed to load: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Opens the user menu and waits for a specific text to be visible
        /// </summary>
        public void openMenu(RemoteWebDriver driver)
        {
            var buyerusermenu = WebDriverHelper.Driver.FindElementsById(_userMenuID).ToList();
            buyerusermenu.Count();

            if (buyerusermenu.Count > 0)
            {
                WebDriverHelper.Driver.FindElementById(_userMenuID).Click();
            }
            else
            {
                Assert.IsTrue(buyerusermenu.Count > 0, "User menu couldn't be found or the main page failed to load");
            }

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var signuserout = WebDriverHelper.Driver.FindElementsByLinkText(_singOutTxt).ToList();
            signuserout.Count();

            try
            {
                if (signuserout.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText(_singOutTxt)));
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("User menu didn't open or the page failed to load: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Initializes the user menu web element
        /// </summary>
        private void initWebElements()
        {
            var buyerusermenu = WebDriverHelper.Driver.FindElementsById(_userMenuID).ToList();
            buyerusermenu.Count();

            if (buyerusermenu.Count > 0)
            {
                Assert.IsTrue(buyerusermenu.Count > 0, "User menu couldn't be found or the main page failed to load");
            }
            else
            {
                userMenu = WebDriverHelper.Driver.FindElementById(_userMenuID);
            }
        }
    }
}

