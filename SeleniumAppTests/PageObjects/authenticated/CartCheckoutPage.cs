﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class CartCheckoutPage
    {
        #region Web Element Identifiers 

        private String _paymentDropdownXpath = "//*[contains(text(), 'Select')]";
        private String _selectPaymentXpath = "//*[contains(text(), 'Prepayment')]";
        private String _selectNoPaymentXPath = "//*[contains(text(), 'Select Payment')]";
        private String _submitOrderBtnXpath = "/html/body/ui-view/ui-view/div/div/div[2]/form/div[4]/div/button";
        private String _expandOrderClass = "btn-lm-small";
        private String _productNameClass = "info";
        private String _productCostXpath = "/html/body/ui-view/ui-view/div/div/div[2]/form/div[1]/div/div[1]/div[1]/p[1]/span";
        private String _productBrandXpath = "/html/body/ui-view/ui-view/div/div/div[2]/form/div[1]/div/div[2]/div/div[1]/div[3]/div/div[1]/span[1]";
        private String _submittedOrderTxtXpath = ".//p[contains(@class, 'text-center')]";


        #endregion

        #region Web Elements

        IWebElement payment;

        #endregion

        public CartCheckoutPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_paymentDropdownXpath)));
            InitWebElements();
        }

        public void selectPayment()
        {
            WebDriverHelper.Driver.FindElementByXPath(_paymentDropdownXpath).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_selectPaymentXpath)));

            WebDriverHelper.Driver.FindElementByXPath(_selectPaymentXpath).Click();

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_selectPaymentXpath)));
        }

        public void expandSummary()
        {
            WebDriverHelper.Driver.FindElementByClassName(_expandOrderClass).Click();
        }

        public void checkProductDetails(string prodname, string prodbrand, double prodprice)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_productNameClass)));

            string productname = WebDriverHelper.Driver.FindElementByClassName(_productNameClass).Text.ToString().Replace(" ","").ToLower();
            string productbrand = WebDriverHelper.Driver.FindElementByXPath(_productBrandXpath).Text.ToString().Replace(" ","").ToLower();
            double productprice = double.Parse(WebDriverHelper.Driver.FindElementByXPath(_productCostXpath).Text.Replace("$", ""));
            prodname = prodname.Replace(" ", "").ToLower();
            prodbrand = prodbrand.Replace(" ", "").ToLower();

            if (prodname != productname || prodbrand != productbrand || prodprice != productprice)
            {
                WebDriverHelper.Driver.FindElementByLinkText("BRANDS").Click();
            }
        }

        public void submitOrder()
        {
            WebDriverHelper.Driver.FindElementByXPath(_submitOrderBtnXpath).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath(_submittedOrderTxtXpath), "Your order was submitted with success."));
        }

        public void InitWebElements()
        {
            payment = WebDriverHelper.Driver.FindElementByXPath(_paymentDropdownXpath);
        }
    }
}
