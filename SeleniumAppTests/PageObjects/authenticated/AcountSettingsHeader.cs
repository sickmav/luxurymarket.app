﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class AccountSettingsHeader :BasePage
    {
        #region Web Elements Identifiers

        private String _headerXpath = "/html/body/ui-view/div[1]/div/div/p";
        private String _logOutBtnClass = "logout";
        private String _loginFormID = "loginform";

        #endregion

        #region Web Elements

        private IWebElement logout;

        #endregion

        /// <summary>
        /// Waits for the site header to load and initializes the elements from the header
        /// </summary>
        public AccountSettingsHeader(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var header = WebDriverHelper.Driver.FindElementsByXPath(_headerXpath).ToList();
            header.Count();

            try
            {
                if(header.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_headerXpath)));
                }
            }
            catch(Exception ex)
            {
                throw new ApplicationException("Page header is not visible or the page failed to load: " + ex.Message, ex);
            }
          
            //initWebElements();
        }

        public void logoutClick(RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var logoutbtn = WebDriverHelper.Driver.FindElementsByClassName(_logOutBtnClass).ToList();
            logoutbtn.Count();

            try
            {
                if(logoutbtn.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName(_logOutBtnClass)));
                }
                else
                {
                    WebDriverHelper.Driver.FindElementByClassName(_logOutBtnClass).Click();
                }
            }
            catch(Exception ex)
            {
                throw new ApplicationException("Logout button isn't present or the page failed to load: " + ex.Message, ex);
            }

            var loginform = WebDriverHelper.Driver.FindElementsById(_loginFormID).ToList();
            loginform.Count();
            
            try
            {
                if(loginform.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_loginFormID)));
                }
            }
            catch(Exception ex)
            {
                throw new ApplicationException("Login page form is not present or the page failed to load" + ex.Message, ex);
            }  
        }

        //private void initWebElements()
        //{
        //    logout = WebDriverHelper.Driver.FindElementByClassName(_logOutBtnClass);
        //}
    }
}
