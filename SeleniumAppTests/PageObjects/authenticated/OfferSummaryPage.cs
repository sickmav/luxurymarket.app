﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class OfferSummaryPage
    {
        #region Web Element Identifiers

        private String _termsCheckboxXpath = "//*[contains(text(), 'I am in possession')]";
        private String _publishBtnXpath = "//*[contains(text(), 'Publish')]";
        private String _currencyXpath = "//*[contains(text(), '$')]";
        private String _totalUnitsXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div/div[2]/div[2]/div/p[3]/span";
        private String _totalCostOfferXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div/div[2]/div[2]/div/p[4]/span";
        private String _offerIdXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div/div[2]/div[2]/div/p[1]/span";
        private String _clickSupportingDocsXpath = "/html/body/ui-view/div[2]/div[2]/ui-view/ui-view/div/div[1]/div/div[1]/a[1]";
        private String _publishedTxtClass = "sent-for-publish";
        private String _logOutBtnClass = "logout";
        private String _loginFormID = "loginform";

        #endregion

        #region Web Elements

        #endregion


        public void checkUnitsAndCost(int units, double totalCost)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_currencyXpath)));

            int totalunits = int.Parse(WebDriverHelper.Driver.FindElementByXPath(_totalUnitsXpath).Text);
            double totalcostoffer = double.Parse(WebDriverHelper.Driver.FindElementByXPath(_totalCostOfferXpath).Text.Replace("$", ""));

            if (units != totalunits || totalCost != totalcostoffer)
            {
                WebDriverHelper.Driver.FindElementByXPath(_clickSupportingDocsXpath).Click();
            }

            else
            {
                WebDriverHelper.Driver.FindElementByXPath(_termsCheckboxXpath).Click();
            }
        }

        public void getOfferId(out int offeridsummary)
        {
            offeridsummary = int.Parse(WebDriverHelper.Driver.FindElementByXPath(_offerIdXpath).Text);
        }

        public void submitOffer()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(_publishBtnXpath)));

            WebDriverHelper.Driver.FindElementByXPath(_publishBtnXpath).Click();

            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_publishedTxtClass), "Your offer was sent for processing"));
        }

        public void signOut()
        {
            WebDriverHelper.Driver.FindElementByClassName(_logOutBtnClass).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_loginFormID)));
        }
    }
}
