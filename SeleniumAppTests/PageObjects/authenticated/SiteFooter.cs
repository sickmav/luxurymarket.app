﻿using OpenQA.Selenium;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class SiteFooter
    {
        #region Web Element Identifiers

        private String _aboutID = "";
        private String _customerServiceID = "";
        private String _policiesID = "";
        private String _contactID = "";

        #endregion

        #region

        private IWebElement about;
        private IWebElement customerService;
        private IWebElement policies;
        private IWebElement contact;

        #endregion

        private void InitWebElements()
        {
            about = WebDriverHelper.Driver.FindElementById(_aboutID);
            customerService = WebDriverHelper.Driver.FindElementById(_customerServiceID);
            policies = WebDriverHelper.Driver.FindElementById(_policiesID);
            contact = WebDriverHelper.Driver.FindElementById(_contactID);
        }
    }
}
