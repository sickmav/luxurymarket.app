﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authentication
{
    public class ConfirmAccountPage : BasePage
    {

        #region Web Elements Identifiers

        private String _pageTitleClass = "sub-title";
        private String _passwordID = "password";
        private String _confirmPasswordName = "confirmPassword";
        private String _submitID = "submitSetupAccount";

        #endregion

        #region Web Elements

        private IWebElement title;
        private IWebElement password;
        private IWebElement confirmPass;
        private IWebElement submit;

        #endregion

        public ConfirmAccountPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_pageTitleClass)));
            InitWebElements();
        }

        public void setPassword(string value)
        {
            password.SendKeys(value);
        }

        public void setConfirmPass(string value)
        {
            confirmPass.SendKeys(value);
        }

        public void submitClick()
        {
            submit.Click();
        }

        public UploadOfferAndDetails acceptAccount(string value)
        {
            setPassword("luxury");
            setConfirmPass("luxury");
            submitClick();

            return new UploadOfferAndDetails(WebDriverHelper.Driver);
        }

        private void InitWebElements()
        {
            title = WebDriver.FindElementByClassName(_pageTitleClass);
            password = WebDriver.FindElementById(_passwordID);
            confirmPass = WebDriver.FindElementByName(_confirmPasswordName);
            submit = WebDriver.FindElementById(_submitID);
        }
    }
}
