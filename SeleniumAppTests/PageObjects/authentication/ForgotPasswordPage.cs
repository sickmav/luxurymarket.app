﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SeleniumAppTests.PageObjects.authentication
{
    public class ForgotPasswordPage : BasePage
    {

        #region Web Elements Identifiers

        private String _forgotpasswordFormID = "resetpasswordform";
        private String _resetEmailInputID = "forgotpassemail";
        private String _changePasswordButtonID = "changepassword";
        private String _backtoLoginButtonID = "backtologin";
        private String _gotologinButtonID = "gotologin"; // the button present after the email was reset

        #endregion

        #region Web Elements

        private IWebElement resetPwdForm;
        private IWebElement resetEmail;
        private IWebElement changePwdButton;
        private IWebElement backtoLoginButton;
        private IWebElement gotoLoginButton;
        
        #endregion

        public ForgotPasswordPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_forgotpasswordFormID)));
            initWebElements();
        }

        public void setResetEmail(string value)
        {
            resetEmail.SendKeys(value);
        }

        public void changePasswordClick()
        {
            changePwdButton.Click();
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("messageform")));
            var checktext = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            checktext.Until(ExpectedConditions.TextToBePresentInElement(WebDriver.FindElementById("messageform"), "Instructions to reset your password have been sent to the provided email."));           
        }

        public void backToLoginClick()
        {
            backtoLoginButton.Click();
        }

        public LoginPage goToLoginClick()
        {
            gotoLoginButton = WebDriver.FindElementById(_gotologinButtonID);
            gotoLoginButton.Click();     
            return new LoginPage(WebDriverHelper.Driver);         
        }       
        
        private void initWebElements()
        {
            resetPwdForm = WebDriver.FindElementById(_forgotpasswordFormID);
            resetEmail = WebDriver.FindElementById(_resetEmailInputID);
            changePwdButton = WebDriver.FindElementById(_changePasswordButtonID);
            backtoLoginButton = WebDriver.FindElementById(_backtoLoginButtonID);
        }  
    }
}