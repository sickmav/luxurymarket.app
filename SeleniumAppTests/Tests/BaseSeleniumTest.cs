﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumAppTests.Helpers;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumAppTests.PageObjects.authentication;
using SeleniumAppTests.PageObjects.CMS;
using SeleniumAppTests.PageObjects.CMS.authentication;
using System.Linq;

namespace SeleniumAppTests.Tests
{
    /// <summary>
    /// Summary description for BaseSeleniumTest
    /// </summary>
    [TestClass]
    public class BaseSeleniumTest
    {
        #region Web Elements Identifiers

        private String _luxuriLoginFormID= "loginform";
        private String _cmsLoginFormClass = "login-wrapper";
        private String _elasticLoginFrameID = "eframe";

        #endregion

        public BaseSeleniumTest()
        {
            WebDriverHelper.Init();            
        }

        [TestCleanup()]
        public void Cleanup()
        {
            WebDriverHelper.Driver.Dispose();
        }

        /// <summary>
        /// Navigates to Luxury APP and waits for the login form to load 
        /// </summary>
        /// <returns></returns> Returns a new loginpage instance
        protected LoginPage NavigateToApplication()
        {
            WebDriverHelper.Driver.Navigate().GoToUrl(AppConfig.AppUrl);

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var luxryform = WebDriverHelper.Driver.FindElementsById(_luxuriLoginFormID).ToList();

            try
            {
                if (luxryform.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_luxuriLoginFormID)));
                }
            }
            catch(Exception ex)
            {
                throw new ApplicationException("Luxury login page form isn't visible or the page failed to load" + ex.Message, ex);
            }
                       
            return new LoginPage(WebDriverHelper.Driver);
        }

        /// <summary>
        /// Nvigates to CMS APP and waits for the login form to load
        /// </summary>
        /// <returns></returns> Returns a new cms login page instance
        protected CmsLoginPage NavigateToCMms()
        {           
            WebDriverHelper.Driver.Navigate().GoToUrl(AppConfig.CmsUrl);
          
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var cmsform = WebDriverHelper.Driver.FindElementsByClassName(_cmsLoginFormClass).ToList();

            try
            {
                if (cmsform.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_cmsLoginFormClass)));
                }
            }
            catch(Exception ex)
            {
                throw new ApplicationException("CMS loging page form isn't visible or the page failed to load" + ex.Message, ex);
            }

            return new CmsLoginPage(WebDriverHelper.Driver);
        }

        /// <summary>
        /// Navigtes to Elastic APP and waits for the login form to load
        /// </summary>
        /// <returns></returns> Returns a new elastic login page instance
        protected ElasticEmail NavigateToElastic()
        {
            WebDriverHelper.Driver.Navigate().GoToUrl(AppConfig.ElasticUrl);

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            var elasticform = WebDriverHelper.Driver.FindElementsById(_elasticLoginFrameID).ToList();

            try
            {
                if (elasticform.Count == 0)
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_elasticLoginFrameID)));
                }
            }
            catch(Exception ex)
            {
                throw new ApplicationException("Elastic login page form isn't visible or the page failed to load" + ex.Message, ex);
            }

            return new ElasticEmail(WebDriverHelper.Driver);
        }
    }
}