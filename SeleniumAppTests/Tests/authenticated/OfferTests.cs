﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Remote;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated;
using SeleniumAppTests.PageObjects.authenticated.home;
using SeleniumAppTests.PageObjects.authentication;
using SeleniumAppTests.PageObjects.CMS;
using SeleniumAppTests.PageObjects.CMS.authenticated;
using SeleniumAppTests.PageObjects.CMS.authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.Tests.authenticated
{
    [TestClass]

    public class OfferTests : BaseSeleniumTest
    {
        #region Positive Tests
        [TestMethod, TestCategory("OfferFlow_PositiveTests")]
        public void CreateOfferAndApprove()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appSellerUsername);
            loginpage.setPassword(AppConfig.appSellerPassword);

            var site = loginpage.loginClick(LoginAccountTypes.Seller);

            var newoffer = new UploadOfferAndDetails(WebDriverHelper.Driver);
            newoffer.uploadOffer();

            var units = 0;
            var totalCost = 0.0;
            var offerid = 0;

            newoffer.getUnitsAndCost(out units, out totalCost);
            newoffer.setStartDate();
            newoffer.setEndDate();
            newoffer.continueClick();

            var support = new SupportingDocsPage();
            support.supportUpload();

            newoffer.continueClick();

            var summary = new OfferSummaryPage();

            summary.checkUnitsAndCost(units, totalCost);
            summary.getOfferId(out offerid);
            summary.submitOffer();
            summary.signOut();

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Offers);
            var cmsoffers = new OffersRequest();
            cmsoffers.searchOffer(offerid);
            cmsoffers.checkOfferIdAndView(offerid);
            
            var offerstoapprove = new OfferImportProductsPage();
            offerstoapprove.approveOffer();
            header.Navigate(CmsHeader.CMSPages.OffersAvailable);

            var offerspublished = new OffersAvailable();
            offerspublished.searchOffer(offerid);
            offerspublished.checkOffer(offerid);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
        }

        [TestMethod, TestCategory("OfferFlow_PositiveTests")]
        public void CreateOfferAndDecline()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appSellerUsername);
            loginpage.setPassword(AppConfig.appSellerPassword);

            var site = loginpage.loginClick(LoginAccountTypes.Seller);

            var newoffer = new UploadOfferAndDetails(WebDriverHelper.Driver);
            newoffer.uploadOffer();

            var units = 0;
            var totalCost = 0.0;
            var offerid = 0;

            newoffer.getUnitsAndCost(out units, out totalCost);
            newoffer.setStartDate();
            newoffer.setEndDate();
            newoffer.continueClick();

            var support = new SupportingDocsPage();
            support.supportUpload();


            var summary = new OfferSummaryPage();

            summary.checkUnitsAndCost(units, totalCost);
            summary.getOfferId(out offerid);
            summary.submitOffer();
            summary.signOut();

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Offers);
            var cmsoffers = new OffersRequest();
            cmsoffers.searchOffer(offerid);
            cmsoffers.checkOfferIdAndView(offerid);

            var offerimport = new OfferImportProductsPage();
            offerimport.declineOffer();
            //header.Navigate(CmsHeader.CMSPages.OffersAvailable);

            //var offerspublished = new OffersAvailable();
            //offerspublished.searchOffer(offerid);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
        }

        [TestMethod, TestCategory("OfferFlow_PositiveTests")]
        public void PurchaseProducts()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword(AppConfig.appBuyerPassword);

            var site = loginpage.loginClick(LoginAccountTypes.Buyer);

            var sitemenu = new SiteMenu(WebDriverHelper.Driver);
            sitemenu. Navigate(SiteMenuPages.Brands);
            
            var choosebrand = new BrandsPage();
            choosebrand.clickSellerBrand();

            var clickproduct = new ProductBrandsPage();
            clickproduct.clickOnProduct();

            var prodname = "";
            var prodbrand = "";
            var prodprice = 0.0;

            var clickbuy = new ProductDetailsPage();
            clickbuy.getProductName(out prodname);           
            clickbuy.clickBuyBtn(out prodbrand);
            clickbuy.addSizeToCart(out prodprice);
            clickbuy.addToLineSheet();
            clickbuy.goToCart();

            var clickcart = new CartSelectionPage();
            clickcart.continuePurchaseBtn();

            var gotoshipment = new CartShipmentPage();
            gotoshipment.continueToCheckout();

            var submitorder = new CartCheckoutPage();
            submitorder.selectPayment();
            submitorder.expandSummary();
            submitorder.checkProductDetails(prodname, prodbrand, prodprice);
            submitorder.submitOrder();

            var siteusermenu = new SiteUserMenu(WebDriverHelper.Driver);
            siteusermenu.signOut(WebDriverHelper.Driver);
        }

        #endregion

        #region Negative Tests

        #endregion

    }

}

