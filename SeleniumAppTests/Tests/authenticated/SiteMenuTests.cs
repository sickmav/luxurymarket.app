﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Remote;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated;
using SeleniumAppTests.PageObjects.authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.Tests.authenticated
{
    [TestClass]

    public class SiteMenuTests : BaseSeleniumTest
    {
        #region Positive Tests
        /// <summary>
        /// Logs in, verifies that the main banner is present than logs out
        /// </summary>
        [TestMethod]
        public void NavigateToBrands()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword(AppConfig.appBuyerPassword);
            var site = loginpage.loginClick(LoginAccountTypes.Buyer);

            var sitemenu = new SiteMenu(WebDriverHelper.Driver);
            sitemenu.Navigate(SiteMenuPages.Brands);

            var siteheader = new SiteUserMenu(WebDriverHelper.Driver);
            siteheader.signOut(WebDriverHelper.Driver);
        }
        
        #endregion

        #region Negative Tests

        #endregion

    }
}
