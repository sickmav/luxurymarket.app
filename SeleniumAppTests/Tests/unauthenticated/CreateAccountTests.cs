﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumAppTests.PageObjects.authentication;
using System.Threading;
using SeleniumAppTests.PageObjects.CMS;
using SeleniumAppTests.Tests.CMS;
using SeleniumAppTests.PageObjects.CMS.authentication;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.CMS.authenticated;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.PageObjects.authenticated;

namespace SeleniumAppTests.Tests.unauthenticated
{
    [TestClass]
    public class CreateAccountTests : BaseSeleniumTest
    {
        #region Positive Tests
        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateBuyer()
        {   //init data
            var contactBuyerName  = AppConfig.buyerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companyBuyerName = AppConfig.buyerContactName+ DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailBuyer = AppConfig.buyerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginPage = NavigateToApplication();
            var createaccPage = loginPage.newAccountClick();
            createaccPage.createBuyerAcc(createaccPage, contactBuyerName, companyBuyerName, emailBuyer);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateSeller()
        {
            //init data
            var contactSellerName = AppConfig.sellerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companySellerName = AppConfig.sellerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailSeller = AppConfig.sellerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginPage = NavigateToApplication();
            var createaccPage = loginPage.newAccountClick();
            createaccPage.createSellerAccount(createaccPage, contactSellerName, companySellerName, emailSeller);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateBuyerAndApprove()
        {
            var contactBuyerName = AppConfig.buyerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companyBuyerName = AppConfig.buyerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailBuyer = AppConfig.buyerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginpage = NavigateToApplication();
            var createaccpage = loginpage.newAccountClick();
            createaccpage.createBuyerAcc(createaccpage, contactBuyerName, companyBuyerName, emailBuyer);

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Partners);
            requestsPage.approveRequest(companyBuyerName);
            header.Navigate(CmsHeader.CMSPages.ExistingPartners);

            var existingpartners = new ExistingPartners();
            existingpartners.checkApprovedRequest(companyBuyerName);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateBuyerAndDecline()
        {
            var contactBuyerName = AppConfig.buyerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companyBuyerName = AppConfig.buyerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailBuyer = AppConfig.buyerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginPage = NavigateToApplication();
            var createaccPage = loginPage.newAccountClick();
            createaccPage.createBuyerAcc(createaccPage, contactBuyerName, companyBuyerName, emailBuyer);

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Partners);
            requestsPage.declineRequest(companyBuyerName);
            requestsPage.checkDeclinedRequest(companyBuyerName);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateSellerAndApprove()
        {
            var contactSellerName = AppConfig.sellerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companySellerName = AppConfig.sellerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailSeller = AppConfig.sellerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginPage = NavigateToApplication();
            var createaccPage = loginPage.newAccountClick();
            createaccPage.createSellerAccount(createaccPage, contactSellerName, companySellerName, emailSeller);

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Partners);
            requestsPage.approveRequest(companySellerName);
            header.Navigate(CmsHeader.CMSPages.ExistingPartners);

            var existingpartners = new ExistingPartners();
            existingpartners.checkApprovedRequest(companySellerName);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateSellerAndDecline()
        {
            var contactSellerName = AppConfig.sellerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companySellerName = AppConfig.sellerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailSeller = AppConfig.sellerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginPage = NavigateToApplication();
            var createaccPage = loginPage.newAccountClick();
            createaccPage.createSellerAccount(createaccPage, contactSellerName, companySellerName, emailSeller);

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick(); ;

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Partners);
            requestsPage.declineRequest(companySellerName);
            requestsPage.checkDeclinedRequest(companySellerName);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateBuyerApproveAndSetPassword()
        {
            var contactBuyerName = AppConfig.buyerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companyBuyerName = AppConfig.buyerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailBuyer = AppConfig.buyerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginpage = NavigateToApplication();
            var createaccpage = loginpage.newAccountClick();
            createaccpage.createBuyerAcc(createaccpage, contactBuyerName, companyBuyerName, emailBuyer);

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Partners);
            requestsPage.approveRequest(companyBuyerName);
            header.Navigate(CmsHeader.CMSPages.ExistingPartners);

            var existingpartners = new ExistingPartners();
            existingpartners.checkApprovedRequest(companyBuyerName);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
            cmsutils.tabSwitch();

            var gotoelastic = NavigateToElastic();
            var elastic = new ElasticEmail(WebDriverHelper.Driver);
            elastic.loginElastic(WebDriverHelper.Driver);
            elastic.reportsClick(WebDriverHelper.Driver);
            elastic.emailLogsClick(WebDriverHelper.Driver);
            elastic.searchEmail(emailBuyer);
            elastic.openSetPasswordPage(emailBuyer);

            var setpass = new NewAccSetPasswordPage(WebDriverHelper.Driver);
            setpass.setNewPassword();
            setpass.setConfirmPass();
            setpass.submitNewAccount(LoginAccountTypes.Buyer);
        }

        [TestMethod, TestCategory("CreateAccountPage_PositiveTests")]
        public void CreateSellerApproveAndSetPassword()
        {
            var contactSellerName = AppConfig.sellerContactName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var companySellerName = AppConfig.sellerCompanyName + DateTime.Now.ToString("_ddMMyyyyHHmmss");
            var emailSeller = AppConfig.sellerEmail + DateTime.Now.ToString("_ddMMyyyyHHmmss") + "@mejix.com";

            var loginpage = NavigateToApplication();
            var createaccpage = loginpage.newAccountClick();
            createaccpage.createSellerAccount(createaccpage, contactSellerName, companySellerName, emailSeller);

            var cmsutils = new CmsUtils();
            cmsutils.newTab();

            var cmsLogin = new CmsLoginPage(WebDriverHelper.Driver);
            cmsLogin.setUsername(AppConfig.cmsUsername);
            cmsLogin.setPassword(AppConfig.cmsPassword);

            var header = cmsLogin.loginClick();

            var requestsPage = header.Navigate(CmsHeader.CMSPages.Partners);
            requestsPage.approveRequest(companySellerName);
            header.Navigate(CmsHeader.CMSPages.ExistingPartners);

            var existingpartners = new ExistingPartners();
            existingpartners.checkApprovedRequest(companySellerName);

            header.Navigate(CmsHeader.CMSPages.UserLogout);
            cmsutils.tabSwitch();

            var gotoelastic = NavigateToElastic();
            var elastic = new ElasticEmail(WebDriverHelper.Driver);
            elastic.loginElastic(WebDriverHelper.Driver);
            elastic.reportsClick(WebDriverHelper.Driver);
            elastic.emailLogsClick(WebDriverHelper.Driver);
            elastic.searchEmail(emailSeller);
            elastic.openSetPasswordPage(emailSeller);

            var setpass = new NewAccSetPasswordPage(WebDriverHelper.Driver);
            setpass.setNewPassword();
            setpass.setConfirmPass();
            setpass.submitNewAccount(LoginAccountTypes.Seller);
        }

        #endregion

        #region Negative Tests

        #endregion

        public void swtichWindow(RemoteWebDriver driver)
        {
            string currentHandle = driver.CurrentWindowHandle;

            PopupWindowFinder finder = new PopupWindowFinder(driver);
            string popupWindowHandle = finder.Click(WebDriverHelper.Driver.FindElementById("emodal-View in New Window"));

            driver.SwitchTo().Window(popupWindowHandle);
        }

    }
}
