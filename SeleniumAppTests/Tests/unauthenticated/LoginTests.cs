﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using OpenQA.Selenium;
using System.Threading;
using SeleniumAppTests.PageObjects.authenticated;
using SeleniumAppTests.PageObjects.authentication;
using SeleniumAppTests.PageObjects.authenticated.home;

namespace SeleniumAppTests.Tests.unauthenticated
{
    [TestClass]

    public class LoginTests : BaseSeleniumTest
    {
        #region Positive Tests
        /// <summary>
        /// Logs in, verifies that the main banner is present than logs out
        /// </summary>
        [TestMethod, TestCategory("LoginPage_PositiveTests")]
        public void BuyerLoginAndOut()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword(AppConfig.appBuyerPassword);

            var site = loginpage.loginClick(LoginAccountTypes.Buyer);

            var sitemenu = new SiteUserMenu(WebDriverHelper.Driver);
            sitemenu.signOut(WebDriverHelper.Driver);
        }

        [TestMethod, TestCategory("LoginPage_PositiveTests")]
        public void SellerLoginAndOut()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appSellerUsername);
            loginpage.setPassword(AppConfig.appSellerPassword);

            var site = loginpage.loginClick(LoginAccountTypes.Seller);

            var siteheader = new AccountSettingsHeader(WebDriverHelper.Driver);
            siteheader.logoutClick(WebDriverHelper.Driver);
        }

        #endregion

        #region Negative Tests

        [TestMethod, TestCategory("LoginPage_NegativeTests")]
        public void InvalidAccountInfo()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword("q");
            loginpage.loginClick(LoginAccountTypes.Buyer,  true, PageObjects.authentication.LoginErrorTypes.InvalidAccount);
        }

        [TestMethod, TestCategory("LoginPage_NegativeTests")]
        public void IncorrectPassword()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword("q");
            loginpage.setPassword("", true, PageObjects.authentication.LoginErrorTypes.NoPassword);
        }

        [TestMethod, TestCategory("LoginPage_NegativeTests")]
        public void NoPassword()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword("q");
            loginpage.setPassword("", true, PageObjects.authentication.LoginErrorTypes.NoPassword);
        }

        [TestMethod, TestCategory("LoginPage_NegativeTests")]
        public void NoEmail()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword("luxury");
            loginpage.setUsername("", true, PageObjects.authentication.LoginErrorTypes.NoEmail);
        }

        [TestMethod, TestCategory("LoginPage_NegativeTests")]
        public void InvalidEmail()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername(AppConfig.appBuyerUsername);
            loginpage.setPassword("luxury");
            loginpage.setUsername("asd", true, PageObjects.authentication.LoginErrorTypes.InvalidEmail);
        }

        #endregion
    }
}
