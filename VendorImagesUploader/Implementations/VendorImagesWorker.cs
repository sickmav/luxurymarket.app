﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Luxury.VendorImagesUploader.Implementations
{
    class VendorImagesWorker : IWorker
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly IVendorImageAgent _vendorImageAgent;

        private Task _workerTask = null;

        public VendorImagesWorker(IWorkerConfig workerConfig, IVendorImageAgent vendorImageAgent)
        {
            _workerConfig = workerConfig;
            _vendorImageAgent = vendorImageAgent;
        }

        public void Start(CancellationToken cancellationToken)
        {
            if (_workerTask != null && _workerTask.Status == TaskStatus.Running)
            {
                return;
            }
            _workerTask = Task.Run(() => Worker(cancellationToken));

        }

        private async Task Worker(CancellationToken cancellationToken)
        {
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                var timeElapsed = await _vendorImageAgent.UploadImagesAsync();
                var delaySeconds = (int)Math.Round(_workerConfig.DelaySeconds - timeElapsed);
                if (delaySeconds > 0)
                    await Task.Delay(delaySeconds * 1000, cancellationToken);
            }

        }
    }
}
