﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Luxury.VendorImagesUploader
{
    [RunInstaller(true)]
    public class VendorImagesUploaderInstaller : Installer
    {
        public VendorImagesUploaderInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.DisplayName = "Luxury Vendor Images Uploader";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = "VendorImagesUploader";
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
