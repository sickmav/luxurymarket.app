﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Auth.DataAccess.DapperExtensions;
using LuxuryApp.Auth.Core.Interfaces;
using System.Data.SqlClient;
using LuxuryApp.Auth.DataAccess.Repositories;

namespace LuxuryApp.Auth.DataAccess
{
    public class DapperRepository<T> : DapperRepository<T, object>, IRepository<T> where T : class
    {
        public DapperRepository(IConnectionStringProvider connectionStringProvider)
         : base(connectionStringProvider, null)
        {
        }

        public DapperRepository(IConnectionStringProvider connectionStringProvider, IDbTransaction transaction = null)
            : base(connectionStringProvider, transaction)
        {
        }
    }

    public class DapperRepository<T, TKey> : RepositoryBase, IRepository<T, TKey> where T : class
    {
        private IDbTransaction _transaction;

        protected DapperRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
            _transaction = null;
        }

        public DapperRepository(IConnectionStringProvider connectionStringProvider, IDbTransaction transaction) : base(connectionStringProvider)
        {
            _transaction = transaction;
        }

        public long Insert(T item)
        {
            long result = 0;
            using (var conn = GetConnection())
            {
                conn.Open();
                result = conn.Insert<T>(item, _transaction);
                conn.Close();
            }
            return result;
        }

        public async Task<int> InsertAsync(T item)
        {
            int result = 0;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.InsertAsync<T>(item, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }

        public bool Update(T item)
        {
            var result = false;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.Update<T>(item, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public async Task<bool> UpdateAsync(T item)
        {
            var result = false;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.UpdateAsync<T>(item, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }

        public bool Remove(T item)
        {
            var result = false;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.Delete<T>(item, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public async Task<bool> RemoveAsync(T item)
        {
            var result = false;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.DeleteAsync<T>(item, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
            return result;
        }


        public bool RemoveAll()
        {
            var result = false;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.DeleteAll<T>(_transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public async Task<bool> RemoveAllAsync()
        {
            var result = false;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.DeleteAllAsync<T>(_transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
            return result;
        }

        public T Get(TKey id)
        {
            T result = null;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.Get<T>(id, _transaction);
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public async Task<T> GetAsync(TKey id)
        {
            T result = null;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.GetAsync<T>(id, _transaction);
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex) { }
            return result;
        }

        public void Remove(Expression<Func<T, bool>> predicate)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    conn.Remove<T>(predicate, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
        }

        public async Task RemoveAsync(Expression<Func<T, bool>> predicate)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    await conn.RemoveAsync<T>(predicate, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> result = null;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.Find<T>(predicate, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }

        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> result = null;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.FindAsync<T>(predicate, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }

        public IEnumerable<TS> Query<TS>(string sql, object param)
        {
            IEnumerable<TS> result = null;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.Query<TS>(sql, param, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }

        public async Task<IEnumerable<TS>> QueryAsync<TS>(string sql, object param)
        {
            IEnumerable<TS> result = null;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = await conn.QueryAsync<TS>(sql, param, _transaction);
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }

        public string GetTableName<T>()
        {
            var result = string.Empty;
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();
                    result = conn.GetTableName<T>();
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            return result;
        }
    }
}
