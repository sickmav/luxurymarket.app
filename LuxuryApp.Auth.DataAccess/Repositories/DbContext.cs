﻿using LuxuryApp.Auth.Core.Entities;
using System;
using System.Data;
using LuxuryApp.Auth.Core.Interfaces;

namespace LuxuryApp.Auth.DataAccess
{

    public class DapperIdentityDbContext<TUser, TRole> : IdentityDbContext<TUser, TRole, int, string>
        where TUser : User<int>
        where TRole : Role<string>
    {
        public DapperIdentityDbContext(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider, null)
        {

        }
        public DapperIdentityDbContext(IConnectionStringProvider connectionStringProvider, IDbTransaction transaction) : base(connectionStringProvider, transaction)
        {
        }
    }

    public class IdentityDbContext<TUser, TRole, TKey, TRoleKey> : IDisposable
        where TUser : User<TKey>
        where TRole : Role<TRoleKey>
    {
        private IDbTransaction _transaction;
        private IConnectionStringProvider _connectionStringProvider;


        private IRepository<TUser, TKey> _userRepository;
        private IRepository<UserClaim<TKey>> _userClaimRepository;

        public IRepository<TUser, TKey> UserRepository
        {
            get
            {
                return _userRepository ??
                       (_userRepository = new DapperRepository<TUser, TKey>(_connectionStringProvider, DbTransaction));
            }
        }

        public IRepository<UserClaim<TKey>> UserClaimRepository
        {
            get
            {
                return _userClaimRepository ??
                       (_userClaimRepository = new DapperRepository<UserClaim<TKey>>(_connectionStringProvider, DbTransaction));
            }
        }

        public IDbTransaction DbTransaction
        {
            get { return _transaction; }
        }

        protected IdentityDbContext(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
            _transaction = null;
        }

        protected IdentityDbContext(IConnectionStringProvider connectionStringProvider, IDbTransaction transaction)
        {
            _connectionStringProvider = connectionStringProvider;
            _transaction = transaction;
        }

        public void Dispose()
        {

        }
    }
}
