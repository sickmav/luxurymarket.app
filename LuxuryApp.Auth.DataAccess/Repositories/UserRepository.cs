﻿using LuxuryApp.Auth.Core.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Auth.Core.Entities;
using System.Data;
using System.Collections.Generic;
using System;

namespace LuxuryApp.Auth.DataAccess.Repositories
{
    public class UserRepository : DapperRepository<User>, IUserRepository
    {
        public UserRepository(IConnectionStringProvider connectionStringProvider)
             : base(connectionStringProvider, null)
        {

        }

        // <summary>
        /// SELECT operation for finding a user by the username.
        /// </summary>
        /// <param name="userName">The username of the user object.</param>
        /// <returns>Returns the User object for the supplied username or null.</returns>
        public async Task<User> FindByUserNameAsync(string userName)
        {
            string query = "SELECT * FROM app.Users WHERE LOWER(UserName)=LOWER(@UserName) and deleted=0";
            var user = await QueryAsync<User>(query, new { @UserName = userName });
            return user.SingleOrDefault();
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            string query = "SELECT * FROM app.Users WHERE LOWER(Email)=LOWER(@Email) and deleted=0";
            var user = await QueryAsync<User>(query, new { @Email = email });
            return user.SingleOrDefault();
        }

        public async Task<User> FindUserAsync(string username, string password)
        {
            string query = @"SELECT * FROM app.Users 
                             WHERE (LOWER(Email)=LOWER(@Email) or LOWER(UserName)=LOWER(UserName)) 
                             and PasswordHash=@Password and deleted=0";
            var user = await QueryAsync<User>(query, new { @Email = username, @UserName = username, @Password = password });
            return user.SingleOrDefault();
        }

        public async Task<IEnumerable<Customer>> FindCustomerInfoAsync(int userId)
        {
            var query = @"SELECT   CustomerID
                                   ,FirstName
			                       ,LastName
                                   ,UserName
                                   ,CompanyID
                                   ,CompanyName
		                           ,CompanyTypeID as CompanyType
                                   ,HasAcceptedTermsAndConditions
                                   ,CurrencyID
                                   ,APISettingsID
                          from " + $" {SqlFunctionName.fn_CustomerWithCompaniesGetByUserID}(@UserId)";
            var companyTask = await QueryAsync<CustomerDenormalized>(query, new { @UserId = userId });
            var customers = companyTask.GroupBy(x => new { x.CustomerId, x.FirstName, x.LastName, x.UserName }).Select(x => new Customer
            {
                CustomerId = x.Key.CustomerId,
                FirstName = x.Key.FirstName,
                LastName = x.Key.LastName,
                UserName = x.Key.UserName,
                Companies = x.Select(y => new Company
                {
                    CompanyId = y.CompanyId,
                    CompanyName = y.CompanyName,
                    CompanyType = y.CompanyType,
                    HasAcceptedTermsAndConditions = y.HasAcceptedTermsAndConditions,
                    CurrencyID = y.CurrencyID,
                    IsIntegrated = y.APISettingsID != null
                }).ToList()
            }).ToList();

            return customers;
        }

        public async Task<User> FindByGuidAsync(Guid id)
        {
            string query = @"SELECT * FROM app.Users 
                             WHERE UserID like @id and deleted=0";
            var user = await QueryAsync<User>(query, new { @id = id });
            return user.SingleOrDefault();
        }
    }
}