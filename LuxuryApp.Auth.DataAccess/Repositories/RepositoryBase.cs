﻿using LuxuryApp.Auth.Core.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace LuxuryApp.Auth.DataAccess.Repositories
{
    public abstract class RepositoryBase
    {
        private IConnectionStringProvider _connectionStringProvider;

        protected RepositoryBase(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
        }

        protected IDbConnection GetConnection()
        {
            return new SqlConnection(_connectionStringProvider.ReadWriteConnectionString);
        }

    }
}
