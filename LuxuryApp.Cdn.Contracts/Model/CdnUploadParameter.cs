﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Contracts.Model
{
    public class CdnUploadParameter
    {
        public string Key { get; set; }
        public byte[] Content { get; set; }
    }
}
