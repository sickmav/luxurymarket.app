﻿using LuxuryApp.Cdn.CdnServers.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers
{
    public class IISFileCdnService: IFileCdnService<ICdnAuthor, LocalCdnSettings>
    {
        private string _localPath;
        private string _webPath;

        public ICdnAuthor Author { get; protected set; }
        public LocalCdnSettings Settings { get; protected set; }

        public IISFileCdnService(ICdnAuthor author, LocalCdnSettings settings)
            //: base(author, settings) 
        {
            Author = author;
            Settings = settings;
            _localPath = settings.LocalRootPath;
            _webPath = settings.WebRootPath;
        }

        public CdnResult<string> GetContentUrl(string key)
        {
            var keyUri = new Uri(CombineUri(_webPath, TrimKey(key ?? "")));
            var result = new CdnResult<string>(key, keyUri.AbsoluteUri);
            return result;
        }

        public CdnResult Upload(string key, byte[] content)
        {
            if (!string.IsNullOrEmpty(key) && content != null)
            {
                SaveContent(key, content);
                return new CdnResult(key);
            }
            return new CdnResult(key, new CdnExceptionBadRequest());
        }

        public CdnResult Remove(string key)
        {
            File.Delete(GetLocalPath(key));
            return new CdnResult(key);
        }

        public IEnumerable<CdnResult<string>> GetContentUrls(IEnumerable<string> keys)
        {
            return from key in keys
                   select GetContentUrl(key);
        }

        public IEnumerable<CdnResult> Upload(IEnumerable<CdnUploadParameter> parameters)
        {
            parameters = parameters.ToArray();

            var result = new List<CdnResult>(parameters.Count());
            result.AddRange(parameters
                .Where(x => string.IsNullOrEmpty(x.Key) || x.Content == null)
                .Select(y => new CdnResult(y.Key, new CdnExceptionBadRequest())));

            parameters = parameters.Where(x => !string.IsNullOrEmpty(x.Key) && x.Content != null).ToList();

            var denormData = 
                (from p in parameters
                    select new
                    {
                        p.Key,
                        FullPath = GetLocalPath(p.Key),
                        p.Content,
                    }).ToList();

            CheckAndCreateDirectories(denormData.Select(x => x.FullPath));

            foreach (var dd in denormData)
            {
                try
                {
                    WriteFile(dd.FullPath, dd.Content);
                    result.Add(new CdnResult(dd.Key));
                } 
                catch (Exception e)
                {
                    result.Add(new CdnResult(dd.Key, new CdnExceptionInternalError(e)));
                }
            }
            return result;
        }

        public IEnumerable<CdnResult> Remove(IEnumerable<string> keys)
        {
            return from k in keys
                   select Remove(k);
        }


        private void CheckAndCreateDirectories(IEnumerable<string> fileFullPaths)
        {
            var directories =
                from f in fileFullPaths
                let dir = Path.GetDirectoryName(f)
                group dir by dir into gDir
                select gDir.Key;
            var directoriesToCreate = directories.Where(d => !Directory.Exists(d));
            foreach (var d in directoriesToCreate)
            {
                Directory.CreateDirectory(d);
            }
        }

        private void SaveContent(string key, byte[] content)
        {
            var fileFullPath = GetLocalPath(key);
            try
            {
                WriteFile(fileFullPath, content);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fileFullPath));
                WriteFile(fileFullPath, content);
            }
        }

        private void WriteFile(string fileFullPath, byte[] content)
        {
            using (var fs = new FileStream(fileFullPath, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fs))
                {
                    writer.Write(content);
                }
            }
        }

        private string GetLocalPath(string key)
        {
            return Path.Combine(_localPath, TrimKey(key));
        }

        public static string CombineUri(string uri1, string uri2)
        {
            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }

        private string TrimKey(string key)
        {
            return key.TrimStart(new[] { '/', '\\' });
        }
    }
}
