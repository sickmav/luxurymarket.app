﻿using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.CdnServers.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers
{
    public class MultiCdnService : IFileCdnService<CdnAuthor, NullCdnSettings>
    {
        private IMultiCdnRepository _repository;
        private IEnumerable<IFileCdnService<ICdnAuthor, ICdnSettings>> _workerServices;

        public CdnAuthor Author { get; protected set; }
        public NullCdnSettings Settings { get; protected set; }

        public MultiCdnService(CdnAuthor author, NullCdnSettings settings, 
            IMultiCdnRepository repository,
            IEnumerable<IFileCdnService<ICdnAuthor, ICdnSettings>> workerServices)
        {
            _repository = repository;
            _workerServices = workerServices;
            Author = author;
            Settings = settings;
        }

        public CdnResult Upload(string key, byte[] content)
        {
            return Upload(new[] { new CdnUploadParameter { Key = key, Content = content }, }).Single();
        }

        public CdnResult<string> GetContentUrl(string key)
        {
            return GetContentUrls(new[] { key }).First();
        }

        public CdnResult Remove(string key)
        {
            return Remove(new[] { key }).First();
        }

        public IEnumerable<CdnResult> Upload(IEnumerable<CdnUploadParameter> parameters)
        {
            var uploaders = 
                (from ws in _workerServices
                    select Task.Factory.StartNew<IEnumerable<CdnResult>>(() =>
                    {
                        var r = ws.Upload(parameters);
                        var fileDatas = parameters.Select(p =>
                            new Model.FileData(p.Key)
                            {
                                FileSizeBytes = p.Content?.Length ?? 0,
                            });
                        fileDatas = _repository.SaveFileData(fileDatas).ToArray();
                        _repository.SetCdnFileStatuses(fileDatas
                            .Select(f =>
                                new Model.FileConfigStatus
                                {
                                    FileInfoId = f.FileInfoId,
                                    CdnConfigurationId = ws.Settings.ConfigurationId,
                                    CdnStatus = CdnStatusType.Uploaded,
                                }));
                        return r;
                    }))
                .ToArray();
            try
            {
                Task.WaitAll(uploaders);
            }
            catch (AggregateException ae)
            {
                ae.Handle(e =>
                {
                    throw e;
                });
            }
            var result = uploaders.Where(x => x.IsCompleted).Select(y => y.Result).FirstOrDefault();
            if (result == null)
            {
                return
                    parameters.Select(
                        p =>
                            new CdnResult(p.Key,
                                new CdnExceptionInternalError(new ApplicationException("Could not reach any cdn service")))
                        ).ToList();
            }
            return result;
        }

        public IEnumerable<CdnResult<string>> GetContentUrls(IEnumerable<string> keys)
        {
            try
            {
                var cancelationTokenSource = new CancellationTokenSource();
                //todo: prefilter the getters by looking in db if file exists at the ConfigurationId
                var getters =
                    (from ws in _workerServices
                     select Task.Factory.StartNew<IEnumerable<CdnResult<string>>>(
                     () =>
                         ws.GetContentUrls(keys), cancelationTokenSource.Token)
                     //.ContinueWith<IEnumerable<CdnResult<string>>>(t =>
                     //    {
                     //        if (t.Result.Any(x => x.IsSuccessful))
                     //        {
                     //            cancelationTokenSource.Cancel(true);
                     //        }
                     //        return t.Result;
                     //    },
                     //    TaskContinuationOptions.OnlyOnRanToCompletion)
                     //.ContinueWith<IEnumerable<CdnResult<string>>>(f =>
                     //    {
                     //        if (f.Exception is AggregateException)
                     //        {
                     //            var ae = f.Exception as AggregateException;
                     //            ae.Handle(e => { return true; });
                     //        }
                     //        return
                     //            keys.Select(k =>
                     //                new CdnResult<string>(
                     //                    k, "", new CdnExceptionInternalError(f.Exception)));
                     //    },
                     //    TaskContinuationOptions.OnlyOnFaulted)
                        ).ToArray();
                int i = Task.WaitAny(getters);
                var result = getters[i].Result;
                return result;
            }
            catch (Exception e)
            {
                return keys.Select(k => new CdnResult<string>(k, string.Empty, new CdnExceptionInternalError(e)));
            }
        }

        public IEnumerable<CdnResult> Remove(IEnumerable<string> keys)
        {
            keys = keys.ToArray();
            var removers = 
                (from ws in _workerServices
                 select Task.Factory.StartNew<IEnumerable<CdnResult>>(
                    () =>
                    {
                        var fileDatas = _repository.GetFileData(keys);
                        _repository.SetCdnFileStatuses(fileDatas
                                .Select(f =>
                                    new Model.FileConfigStatus
                                    {
                                        FileInfoId = f.FileInfoId,
                                        CdnConfigurationId = ws.Settings.ConfigurationId,
                                        CdnStatus = CdnStatusType.Deleted,
                                    }));
                        return ws.Remove(keys);
                    })).ToArray();
            Task.WaitAll(removers);
            return removers.First().Result;
        }
    }
}
