﻿namespace LuxuryApp.Cdn.CdnServers
{
    public enum CdnConfigurationType
    {
        None = 0,
        Local = 1,
        AmazonS3 = 2,
        AzureBlob = 3
    }

    public enum CdnStatusType
    {
        None = 0,
        Uploaded = 1,
        Updated = 2,
        Deleted = 3,
        Lost = 4,
    }
}