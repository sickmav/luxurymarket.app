﻿using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers.Model
{
    public class AmazonS3CdnSettings: ICdnSettings
    {
        public int ConfigurationId { get; set; }
        public string KeyId { get; set; }
        public string KeySecret { get; set; }
        public string Bucket { get; set; }
        public string Region { get; set; }
    }
}
