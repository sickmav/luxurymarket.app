﻿using System;
using System.IO;
namespace LuxuryApp.Cdn.CdnServers.Model
{
    public class FileData
    {
        public int FileInfoId { get; set; }

        public string RelativePath { get; set; }
        public string FileName { get; set; }
        public string Key
        {
            get
            {
                return Path.Combine(RelativePath, FileName);
            }
            set
            {
                RelativePath = GetRelativePath(value);
                FileName = GetFileName(value);
            }
        }
        public int FileSizeBytes { get; set; }

        public FileData()
        {

        }

        public FileData(string key)
        {
            Key = key;
        }

        public static string GetRelativePath(string key)
        {
            return Path.GetDirectoryName(key);
        }

        public static string GetFileName(string key)
        {
            return Path.GetFileName(key);
        }
    }

    public class FileConfigStatusRequest
    {
        public int FileInfoId { get; set; }
        public int CdnConfigurationId { get; set; }
    }

    public class FileConfigStatus : FileConfigStatusRequest
    {
        public int FileInfo_CdnConfigurationId { get; set; }
        public CdnStatusType CdnStatus { get; set; }
        public DateTime CdnStatusDate { get; set; }
    }
}