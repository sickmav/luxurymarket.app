﻿using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers.Model
{
    public sealed class NullCdnSettings: ICdnSettings
    {
        public int ConfigurationId { get; set; }
    }
}
