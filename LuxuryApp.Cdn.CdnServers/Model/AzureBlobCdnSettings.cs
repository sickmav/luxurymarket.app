﻿using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.CdnServers.Model
{
    public class AzureBlobCdnSettings: ICdnSettings
    {
        public int ConfigurationId { get; set; }
        public string StorageConnectionString { get; set; }
        public string Container { get; set; }
    }
}
