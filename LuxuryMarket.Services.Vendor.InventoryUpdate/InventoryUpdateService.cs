﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Processors.Agents;
using LuxuryApp.Processors.Agents.Sellers;
using LuxuryApp.Processors.Services;
using LuxuryMarket.Services.Vendor.InventoryUpdate.Helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LuxuryMarket.Services.Vendor.InventoryUpdate
{
    public partial class InventoryUpdateService : ServiceBase
    {
        #region private vars

        private static InventoryService _invService = new InventoryService();
        private Thread _workerThread = null;
        private static SellerInventoryUpdateAgent _agent = new ColtortiInventoryUpdateAgent();
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        #endregion

        public InventoryUpdateService()
        {
            InitializeComponent();

            // settings
            this.ServiceName = Configurations.ServiceName;
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            // only coltorti for now
            // will be expanded when others will come
            ThreadStart starter = new ThreadStart(DoWork);
            _workerThread = new Thread(starter);
            _workerThread.Start();
        }

        protected override void OnStop()
        {
            if (_workerThread != null)
            {
                _workerThread.Abort();
            }
        }

        public static async void DoWork()
        {
            // Main Loop
            while (true)
            {
                try
                {
                    // do call to vendorAPI
                    var apiSettings = _invService.GetSellerApiSettings(SellerAPISettingType.Coltorti);

                    // get list of vendor sku for 
                    var sellerSkusToGetInventory = _invService.GetSellerSKUsForInventoryUpdate(apiSettings.SellerID);

                    // authenticate using credentials from our db
                    var authResponse = await _agent.Authenticate(apiSettings);

                    // use auth token to make stocks call -> returns list of inventory
                    var inventory = await _agent.GetInventory(authResponse, sellerSkusToGetInventory, apiSettings);

                    // store inventory in our database
                    _invService.SaveInventory(inventory, apiSettings.SellerID);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }

                Thread.Sleep(new TimeSpan(0, 0, Configurations.RunInterval));
            }
        }
    }
}
