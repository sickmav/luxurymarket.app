﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Services;

namespace Test_ExportOrderX2
{
    public partial class OrderX2 : Form
    {
        private readonly IOrderTrackingService _orderTrackingService;
        private readonly IFtpUploadAgent _ftpUploadAgent;
        private readonly IOrderBookingService _orderBookingService;

        public OrderX2(IOrderTrackingService orderTrackingService, IFtpUploadAgent ftpUploadAgent)
        {
            _orderTrackingService = orderTrackingService;
            _ftpUploadAgent = ftpUploadAgent;

            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var orderId = GetOrderId();
            if (orderId <= 0)
            {
                return;
            }
            txtGeneratedX2.Text = GetX2Order(orderId);
        }

        private int GetOrderId()
        {
            var s = txtOrderId.Text;
            int orderId;
            if (!int.TryParse(s, out orderId))
            {
                MessageBox.Show("Invalid order id. must be int");
                return -1;
            }
            return orderId;
        }

        private string GetX2Order(int orderId)
        {
            var poNumber = string.Empty;
            var r = _orderTrackingService.GetSellerOrderX2Document(orderId,out poNumber);
            if (r.IsSuccesfull)
            {
                return Encoding.ASCII.GetString(r.Data);
            }
            MessageBox.Show(r.BusinessRulesFailedMessages.FirstOrDefault());
            return "";
        }

        private void btnFtpEnqueue_Click(object sender, EventArgs e)
        {
            var orderId = GetOrderId();
            if (orderId <= 0)
            {
                return;
            }

            var r = _orderTrackingService.PushSellerOrderX2Document(orderId);
            if (r.IsSuccesfull)
            {
                MessageBox.Show("Successfull", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(r.BusinessRulesFailedMessages.FirstOrDefault(), "Failed", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            _ftpUploadAgent.UploadFromQueue();
            MessageBox.Show("Successfull", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
