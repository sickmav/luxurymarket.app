﻿namespace Test_ExportOrderX2
{
    partial class Booking856
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtOrderId = new System.Windows.Forms.TextBox();
            this.txtGeneratedX2 = new System.Windows.Forms.TextBox();
            this.btnFtpEnqueue = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order ID";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(226, 50);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(111, 24);
            this.btnGenerate.TabIndex = 1;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtOrderId
            // 
            this.txtOrderId.Location = new System.Drawing.Point(89, 53);
            this.txtOrderId.Name = "txtOrderId";
            this.txtOrderId.Size = new System.Drawing.Size(122, 20);
            this.txtOrderId.TabIndex = 2;
            // 
            // txtGeneratedX2
            // 
            this.txtGeneratedX2.Location = new System.Drawing.Point(39, 90);
            this.txtGeneratedX2.Multiline = true;
            this.txtGeneratedX2.Name = "txtGeneratedX2";
            this.txtGeneratedX2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGeneratedX2.Size = new System.Drawing.Size(521, 414);
            this.txtGeneratedX2.TabIndex = 3;
            // 
            // btnFtpEnqueue
            // 
            this.btnFtpEnqueue.Location = new System.Drawing.Point(343, 50);
            this.btnFtpEnqueue.Name = "btnFtpEnqueue";
            this.btnFtpEnqueue.Size = new System.Drawing.Size(103, 23);
            this.btnFtpEnqueue.TabIndex = 4;
            this.btnFtpEnqueue.Text = "FTP Enqueue";
            this.btnFtpEnqueue.UseVisualStyleBackColor = true;
            this.btnFtpEnqueue.Click += new System.EventHandler(this.btnFtpEnqueue_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(452, 50);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(108, 23);
            this.btnUpload.TabIndex = 5;
            this.btnUpload.Text = "Ftp Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // Booking856
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 569);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.btnFtpEnqueue);
            this.Controls.Add(this.txtGeneratedX2);
            this.Controls.Add(this.txtOrderId);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.label1);
            this.Name = "Booking856";
            this.Text = "Booking856";
            this.Load += new System.EventHandler(this.Booking856_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtOrderId;
        private System.Windows.Forms.TextBox txtGeneratedX2;
        private System.Windows.Forms.Button btnFtpEnqueue;
        private System.Windows.Forms.Button btnUpload;
    }
}

