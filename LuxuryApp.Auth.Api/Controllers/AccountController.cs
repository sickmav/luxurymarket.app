﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using LuxuryApp.Auth.Api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using LuxuryApp.Auth.Core.Interfaces;
using System.Net;
using Swashbuckle.Swagger.Annotations;
using System.Web.Http.Description;
using System.Configuration;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using System.Web;
using LuxuryApp.Auth.Core;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Auth.Api.Controllers
{
    [RoutePrefix("api/accounts")]
    public class AccountController : BaseApiController
    {
        private readonly Func<IRefreshTokenRepository> _userRepoFactory;
        public Func<INotificationHandler<EmailModel>> _emailServiceFactory;
        public Func<IUserRepository> _userRepositoryFactory;


        public AccountController(Func<IRefreshTokenRepository> refreshTokenRepositoryFactory, Func<INotificationHandler<EmailModel>> emailServiceFactory, Func<IUserRepository> userRepositoryFactory)
        {
            _userRepoFactory = refreshTokenRepositoryFactory;
            _emailServiceFactory = emailServiceFactory;
            _userRepositoryFactory = userRepositoryFactory;
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <returns>The inserted offerImport <see cref="UserReturnModel"/></returns>
        [AllowAnonymous]
        [HttpGet]
        //[Authorize(Roles = "Admin")]
        [ResponseType(typeof(UserReturnModel))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(UserReturnModel))]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(Guid Id)
        {
            var userModel = await _userRepositoryFactory().FindByGuidAsync(Id);
            if (userModel == null)
                return NotFound();
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
            var user = await AppUserManager.FindByIdAsync(userModel.Id);

            if (user != null)
            {
                return Ok(TheModelFactory.Create(user));
            }

            return NotFound();

        }

        /// <summary>
        /// Confirm user email
        /// </summary>
        /// <param name="id"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> ConfirmEmail(Guid userId, string code = "")
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                ModelState.AddModelError("", "User Id and Code are required");
                return BadRequest(ModelState);
            }

            var userModel = await _userRepositoryFactory().FindByGuidAsync(userId);
            if (userModel == null)
                return BadRequest("Invalid id or code.");

            IdentityResult result = await this.AppUserManager.ConfirmEmailAsync(userModel.Id, code);

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return GetErrorResult(result);
            }
        }

        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="model"><see cref="ChangePasswordBindingModel"/></param>
        /// <returns></returns>
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await AppUserManager.FindByEmailAsync(model.Email);

            IdentityResult result = await AppUserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        /// <summary>
        /// Soft delete for user
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}")]
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> DeleteUser(Guid id)
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)

            var userModel = await _userRepositoryFactory().FindByGuidAsync(id);
            if (userModel == null)
                return BadRequest("Invalid id or code.");


            var appUser = await AppUserManager.FindByIdAsync(userModel.Id);

            if (appUser != null)
            {
                appUser.Deleted = true;
                IdentityResult result = await AppUserManager.UpdateAsync(appUser);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return Ok();
            }

            return NotFound();

        }

        // POST: /Account/ForgotPassword
        /// <summary>
        /// Forgot Password for user - send email with the link to reset the password
        /// </summary>
        /// <param name="model"><see cref="ForgotPasswordViewModel"/></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("forgotPassword")]
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> ForgotPasswordAsync(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await AppUserManager.FindByEmailAsync(model.Email);
            if (user == null)
                return BadRequest("User not found");

            var customer = await _userRepositoryFactory().FindCustomerInfoAsync(user.Id);
            if (customer == null || !customer.Any())
            {
                return BadRequest("Customer not found");
            }

            var code = await AppUserManager.GeneratePasswordResetTokenAsync(user.Id);
            var callbackUrl = new Uri($"{ConfigurationManager.AppSettings["LuxuryMarketResetPasswordUrl"]}?id={user.UserId}&code={Uri.EscapeDataString(code)}&new={!user.EmailConfirmed}");
            if (model.SendEmail)
            {
                await _emailServiceFactory().SendAsync(new EmailModel
                {
                    DataBag = new
                    {
                        FORGOTPASSWORDURL = callbackUrl,
                        LOGOURL = ConfigurationManager.AppSettings["AWSBucketLink"] + "logo.png",
                        CONTACTNAME = customer.FirstOrDefault().FirstName + " " + customer.FirstOrDefault().LastName
                    },
                    Subject = "Reset Password",
                    To = model.Email,
                    MasterTemplateName = "MasterEmailTemplate",
                    EmailTemplateName = "ForgotPasswordMessage"
                });
            }
            else return Ok(callbackUrl);
            return Ok();
        }

        // POST: /Account/ResetPassword
        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="model"><see cref="ResetPasswordViewModel"/> </param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("resetPassword", Name = "ResetPassword")]
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userModel = await _userRepositoryFactory().FindByGuidAsync(model.Id);
            if (userModel == null)
                return BadRequest("Invalid user id.");

            var user = await AppUserManager.FindByIdAsync(userModel.Id);
            if (user == null)
            {
                return BadRequest("User not found");
                //var error = new ApiResultNotFound<string>(model.Email).ToApiResult();
                //return OK(error);
            }
            if (!user.EmailConfirmed)
            {
                user.EmailConfirmed = true;
                await AppUserManager.UpdateAsync(user);
            }
            // var code = HttpUtility.UrlDecode(model.Code);
            var result = await AppUserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);

            return (result.Succeeded) ? Ok() : GetErrorResult(result);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("getEmailById", Name = "GetEmailById")]
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> ResetPassword(Guid id)
        {
            var userModel = await _userRepositoryFactory().FindByGuidAsync(id);
            if (userModel == null)
                return BadRequest();

            var user = await AppUserManager.FindByIdAsync(userModel.Id);
            if (user == null)
            {
                return BadRequest("Id not found");
            }
            return Ok(user.Email);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("validateCode")]
        [ResponseType(typeof(void))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(void))]
        public async Task<IHttpActionResult> ValidateCode(ValidateTokenModel model)
        {
            var userModel = await _userRepositoryFactory().FindByGuidAsync(model.UserID);
            if (userModel == null)
                return BadRequest(((int)TokenValidationResponseType.InvalidUser).ToString());

            var response = new TokenValidationResponse();
            response.Email = userModel.Email;

            bool isTokenValid = await AppUserManager.VerifyUserTokenAsync(userModel.Id, model.Purpose.ToString(), model.Code);
            response.Type = isTokenValid ? TokenValidationResponseType.Valid : TokenValidationResponseType.InvalidOrExpiredToken;

            return Ok(response);
        }


        #region Claims

        //[Authorize(Roles = "Admin")]
        //[Route("user/{id:guid}/assignclaims")]
        //[HttpPut]
        //public async Task<IHttpActionResult> AssignClaimsToUser([FromUri] int id, [FromBody] List<ClaimBindingModel> claimsToAssign)
        //{

        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var appUser = await this.AppUserManager.FindByIdAsync(id);

        //    if (appUser == null)
        //    {
        //        return NotFound();
        //    }

        //    foreach (ClaimBindingModel claimModel in claimsToAssign)
        //    {
        //        if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
        //        {

        //            await this.AppUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
        //        }

        //        await this.AppUserManager.AddClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
        //    }

        //    return Ok();
        //}

        //[Authorize(Roles = "Admin")]
        //[Route("user/{id:guid}/removeclaims")]
        //[HttpPut]
        //public async Task<IHttpActionResult> RemoveClaimsFromUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToRemove)
        //{

        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var appUser = await this.AppUserManager.FindByIdAsync(id);

        //    if (appUser == null)
        //    {
        //        return NotFound();
        //    }

        //    foreach (ClaimBindingModel claimModel in claimsToRemove)
        //    {
        //        if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
        //        {
        //            await this.AppUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
        //        }
        //    }

        //    return Ok();
        //}


        //// POST api/Account/Register
        //[AllowAnonymous]
        //[Route("login")]
        //public async Task<IHttpActionResult> Login(LoginViewModel model)
        //{
        //    model.UserName = model.UserName.Trim();
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var findUser = await _userManager.FindAsync(model.UserName, model.Password.Trim());
        //    if (findUser != null)
        //        return BadRequest("User not found!");

        //    //generate access token response
        //    var accessTokenResponse = GenerateLocalAccessTokenResponse(model.UserName);

        //    return Ok(accessTokenResponse);
        //}

        #endregion

        #region Helpers

        private string GetQueryString(HttpRequestMessage request, string key)
        {
            var queryStrings = request.GetQueryNameValuePairs();

            if (queryStrings == null) return null;

            var match = queryStrings.FirstOrDefault(keyValue => string.Compare(keyValue.Key, key, true) == 0);

            if (string.IsNullOrEmpty(match.Value)) return null;

            return match.Value;
        }

        private JObject GenerateLocalAccessTokenResponse(string userName)
        {

            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, userName));
            identity.AddClaim(new Claim("role", "user"));

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTimeOffset.Now,
                ExpiresUtc = DateTimeOffset.Now.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);

            JObject tokenResponse = new JObject(
                                        new JProperty("userName", userName),
                                        new JProperty("access_token", accessToken),
                                        new JProperty("token_type", "bearer"),
                                        new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
                                        new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                                        new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
        );

            return tokenResponse;
        }

        private string GetHashPassword(string password)
        {
            int saltSize = 16;
            int bytesRequired = 32;
            byte[] array = new byte[1 + saltSize + bytesRequired];
            int iterations = 2000;//1000-which is the min recommended for Rfc2898DeriveBytes
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, saltSize, iterations))
            {
                byte[] salt = pbkdf2.Salt;
                Buffer.BlockCopy(salt, 0, array, 1, saltSize);
                byte[] bytes = pbkdf2.GetBytes(bytesRequired);
                Buffer.BlockCopy(bytes, 0, array, saltSize + 1, bytesRequired);
            }
            return Convert.ToBase64String(array);
        }
        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }
            public string ExternalAccessToken { get; set; }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer) || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name),
                    ExternalAccessToken = identity.FindFirstValue("ExternalAccessToken"),
                };
            }
        }

        private string GetErrors(IdentityResult result)
        {
            if (result.Errors.Any())
                return string.Join("; ", result.Errors);
            return string.Empty;
        }

        #endregion
    }
}