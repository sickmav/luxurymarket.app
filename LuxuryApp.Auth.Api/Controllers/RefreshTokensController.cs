﻿using System.Threading.Tasks;
using System.Web.Http;
using LuxuryApp.Auth.DataAccess.Repositories;
using System;
using LuxuryApp.Auth.Core.Interfaces;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Web.Http.Description;
using System.Collections.Generic;
using LuxuryApp.Auth.Core.Entities;

namespace LuxuryApp.Auth.Api.Controllers
{
    [RoutePrefix("api/RefreshTokens")]
    public class RefreshTokensController : ApiController
    {
        private readonly Func<IRefreshTokenRepository> _refreshTokenFactory;

        public RefreshTokensController(Func<IRefreshTokenRepository> refreshTokenFactory)
        {
            _refreshTokenFactory = refreshTokenFactory;
        }

        /// <summary>
        /// Get list of refresh tokens
        /// </summary>
        /// <returns><see cref="RefreshToken"/> </returns>
        [Route("")]
        [HttpGet]
        [ResponseType(typeof(IEnumerable<RefreshToken>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<RefreshToken>))]
        public IHttpActionResult Get()
        {
            return Ok(_refreshTokenFactory().GetAllAsync());
        }

        /// <summary>
        /// Delete token
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        [Authorize]
        [Route("")]
        [HttpDelete]
        [ResponseType(typeof(IEnumerable<RefreshToken>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<RefreshToken>))]
        public async Task<IHttpActionResult> Delete(string tokenId)
        {
            await _refreshTokenFactory().DeleteAsync(tokenId);
            return Ok();
        }

        //Refresh token?
    }
}
