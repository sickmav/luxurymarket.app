﻿using System;
using System.Web.Http;
using LuxuryApp.Auth.Api;
using LuxuryApp.Auth.Api.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using LuxuryApp.Auth.Api.Models;
using Autofac;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using Swashbuckle.Application;
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using LuxuryApp.Auth.Core.Interfaces;
using Autofac.Extras.CommonServiceLocator;
using NLog;
using System.Linq;
using NLog.Targets;
using LuxuryApp.Core.Infrastructure.Security;
using LuxuryApp.Auth.Api.Helpers;

[assembly: OwinStartup(typeof(Startup))]
namespace LuxuryApp.Auth.Api
{
    public class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            var dependencyContainer = (new DependencyBuilder()).GetDependencyContainer(config);

            var csl = new AutofacServiceLocator(dependencyContainer);
            ServiceLocator.SetLocatorProvider(() => csl);

            config
               .EnableSwagger(c =>
               {
                   c.SingleApiVersion("v1", "Luxury auth");
                   c.PrettyPrint();
               })
               .EnableSwaggerUi();


            app.UseAutofacMiddleware(dependencyContainer);

            app.UseAutofacWebApi(config);

            ConfigureMvc(app, dependencyContainer);

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            GlobalConfiguration.Configuration.Filters.Add(new CustomAuthorizationFilter());

            //Setup NLog
            GlobalDiagnosticsContext.Set("LogTable", ConfigurationManager.AppSettings["NLogTableName"]);
            GlobalDiagnosticsContext.Set("Environment", ConfigurationManager.AppSettings["Environment"]);
            var nlogConnectionString = ConfigurationManager.ConnectionStrings["NLogConnectionString"].ConnectionString;
            var databaseTargets = LogManager.Configuration.AllTargets.OfType<DatabaseTarget>();
            foreach (var databaseTarget in databaseTargets)
                databaseTarget.ConnectionString = nlogConnectionString;
        }

        protected void ConfigureMvc(IAppBuilder app, IContainer dependencyContainer)
        {

            DependencyResolver.SetResolver(new AutofacDependencyResolver(dependencyContainer));
            app.UseAutofacMvc();

            AreaRegistration.RegisterAllAreas();

        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            OAuthAuthorizationServerOptions oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {

                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(ConfigHelper.AccessTokenExpireTimeSpan),
                Provider = new AuthorizationServerProvider(ServiceLocator.Current.GetInstance<Func<IUserRepository>>()),
                RefreshTokenProvider = new RefreshTokenProvider(ServiceLocator.Current.GetInstance<Func<IRefreshTokenRepository>>())
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
        }
    }

}