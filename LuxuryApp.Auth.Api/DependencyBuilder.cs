﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using LuxuryApp.Auth.Core.Interfaces;
using LuxuryApp.Auth.DataAccess.Repositories;
using LuxuryApp.NotificationService;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.Core.Infrastructure.Logging;

namespace LuxuryApp.Auth.Api
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(configuration).AsSelf(); // needed for help area configuration

            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<AuthConnectionStringProvider>()
             .As<IConnectionStringProvider>();
            builder.RegisterType<UserRepository>()
           .As<IUserRepository>();
            builder.RegisterType<RefreshTokenRepository>()
         .As<IRefreshTokenRepository>();

            const string authCompKey = "authComp";

            builder.RegisterType<EmailService<EmailModel>>().As<INotificationHandler<EmailModel>>();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register your MVC controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            var container = builder.Build();
            return container;
        }
    }
}