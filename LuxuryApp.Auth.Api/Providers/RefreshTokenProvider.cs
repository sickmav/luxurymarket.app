﻿using System;
using System.Threading.Tasks;
using LuxuryApp.Auth.Api.Helpers;
using LuxuryApp.Auth.Core.Entities;
using Microsoft.Owin.Security.Infrastructure;
using System.Linq;
using System.Collections.Concurrent;
using Microsoft.Owin.Security;
using LuxuryApp.Auth.Core.Interfaces;
using System.Diagnostics;
using System.Configuration;

namespace LuxuryApp.Auth.Api.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public Func<IRefreshTokenRepository> _refreshTokenFactory;

        public RefreshTokenProvider(Func<IRefreshTokenRepository> refreshTokenRepositoryFactory)
        {
            _refreshTokenFactory = refreshTokenRepositoryFactory;
        }

        private static ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var refreshTokenId = Guid.NewGuid().ToString("n");

            var refreshTokenLifeTimeValue = ConfigHelper.RefreshTokenMinutesTime;

            int.TryParse(context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime"), out refreshTokenLifeTimeValue);

            var token = new RefreshToken
            {
                Id = EncryptHelper.GetHash(refreshTokenId),
                Subject = context.Ticket.Identity.Name,
                IssuedUtc = DateTimeOffset.Now,
                ExpiresUtc = DateTimeOffset.Now.AddMinutes(refreshTokenLifeTimeValue)
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

            token.ProtectedTicket = context.SerializeTicket();

            var resultInsert = await RenewToken(token);
            if (resultInsert)
                context.SetToken(refreshTokenId);
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin ?? "*" });

            var repository = _refreshTokenFactory();
            var hashedTokenId = EncryptHelper.GetHash(context.Token);

            var refreshTokens = await repository.FindByIdAsync(hashedTokenId);
            var item = refreshTokens.FirstOrDefault();
            if (item != null)
            {
                context.DeserializeTicket(item.ProtectedTicket);
                await repository.DeleteAsync(hashedTokenId);
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        private async Task<bool> RenewToken(RefreshToken token)
        {
            try
            {
                var repository = _refreshTokenFactory();
                var existingToken = await repository.FindBySubjectAsync(token.Subject);

                if (existingToken != null)
                {
                    var result = await repository.DeleteAsync(token.Subject);
                    Trace.WriteLine("Delete token =" + result);
                }

                await repository.InsertTokenAsync(token);
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError("Error renewing the token " + ex.InnerException);
                throw;
            }
        }
    }
}