﻿using LuxuryApp.Cdn.CdnServers.Model;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using System.Data;

namespace LuxuryApp.Cdn.SqlRepository
{
    public class AmazonS3CdnSettingsRepository :
        CdnSettingsRepositoryBase<AmazonS3CdnSettings>
    {
        public AmazonS3CdnSettingsRepository(string connectionString): base(connectionString, CdnServers.CdnConfigurationType.AmazonS3)
        {
        }

        public override AmazonS3CdnSettings GetCdnSettings(int configurationId)
        {
            AmazonS3CdnSettings result;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                result = conn.Query<AmazonS3CdnSettings>
                    (@"SELECT ConfigurationId = CdnConfigurationId
			        ,KeyId
			        ,KeySecret
					,Bucket
					,Region
					FROM cdn.GetConfigurationAmazonS3(@cdn_configuration_id)", new { cdn_configuration_id = configurationId }).SingleOrDefault();
                conn.Close();
            }
            return result;
        }

        public override int SaveCdnSettings(AmazonS3CdnSettings settings)
        {
            int result;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                result = conn.ExecuteScalar<int>("cdn.SaveConfigurationAmazonS3",
                        new
                        {
                            key_id = settings.KeyId,
                            key_secret = settings.KeySecret,
							bucket = settings.Bucket,
							region = settings.Region
                        }, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
            settings.ConfigurationId = result;
            return result;
        }
    }
}
