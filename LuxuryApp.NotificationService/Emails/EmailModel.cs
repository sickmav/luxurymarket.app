﻿using LuxuryApp.NotificationService.Configurations;
using System.Collections.Generic;
using System.IO;

namespace LuxuryApp.NotificationService
{
    public class EmailModel : ChannelDistribution
    {
        public EmailModel()
        {
            IsHtmlBody = true;
            IsActive = true;
            From = ConfigurationKey.EmailSendingFrom;
            FromName = ConfigurationKey.EmailSendingFromName;
            MasterTemplateName = "Master";
        }
        public string From { get; set; }

        public string FromName { get; set; }

        public string To { get; set; }

        public string Cc { get; set; }

        public string Subject { get; set; }

        public string EmailTemplateName { get; set; }

        public string EmailTemplateContent { get; set; }

        public object DataBag { get; set; }

        public bool IsHtmlBody { get; set; }

        public bool UseRazor { get; set; }
        /// <summary>
        /// If this is set to true then the default master will not be used for email templates
        /// </summary>
        public bool HasOwnMasterTemplate { get; set; }

        public IEnumerable<AttachmentModel> Attachments { get; set; }

        public string MasterTemplateName { get; set; }
    }

    public class AttachmentModel
    {
        public string FileName { get; set; }

        public string FilePath { get; set; }
    }

}
