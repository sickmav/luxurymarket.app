﻿using System.Threading.Tasks;

namespace LuxuryApp.NotificationService.Emails
{
    public interface INotificationHandler<T> where T : ChannelDistribution
    {
        Task SendAsync(T context);
    }
}