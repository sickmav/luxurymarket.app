﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.NotificationService.Configurations
{
    public class ConfigurationKey
    {
        #region Email

        public static string ElasticServer
        {
            get { return ConfigurationManager.AppSettings["ElasticServer"]; }
        }

        public static string ElasticUser
        {
            get { return ConfigurationManager.AppSettings["ElasticUser"]; }
        }

        public static string ElasticPassword
        {
            get { return ConfigurationManager.AppSettings["ElasticPassword"]; }
        }

        public static string EmailSendingFrom
        {
            get { return ConfigurationManager.AppSettings["EmailSendingFrom"]; }
        }

        public static string EmailSendingFromName
        {
            get { return ConfigurationManager.AppSettings["EmailSendingFromName"]; }
        }

        public static string EmailSendingTo
        {
            get { return ConfigurationManager.AppSettings["EmailSendingTo"]; }
        }

        public static bool EnableEmailSending
        {
            get
            {
                bool enable = false;
                Boolean.TryParse(ConfigurationManager.AppSettings["EnableEmailSending"], out enable);

                return enable;
            }
        }

        #endregion
    }
}
