﻿namespace LuxuryApp.NotificationService
{
    public class ElasticSettings : IChannelSettings
    {
        public string Username { get; set; }

        public string ApiKey { get; set; }

        public string From { get; set; }

        public string FromDisplayName { get; set; }

        public string ServerUrl { get; set; }
    }
}
