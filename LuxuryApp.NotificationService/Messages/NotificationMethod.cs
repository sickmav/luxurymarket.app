﻿
using System;

namespace LuxuryApp.NotificationService
{
    [Flags]
    public enum NotificationMethod
    {
        None = 0,
        Email = 1 << 0
    }
}
