﻿using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace LuxuryApp.NotificationService.Extensions
{
    public class MessageTemplate
    {
        public static string GetParsedContentFromTemplateContent(string templateContent, object tokens)
        {
            string content;
            var tokenList = BuildTokenValuePairs(tokens);

            try
            {
                content = ReplaceTokens(tokenList, templateContent);
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                   string.Format(
                       CultureInfo.InvariantCulture,
                       "Could not parse template content: {0} for {1}, reason: {2}, stack trace: {3}",
                       tokenList.FirstOrDefault(t => t.Key == "MessageType").Value,
                       tokenList.FirstOrDefault(t => t.Key == "Center").Value,
                       ex.Message,
                       ex.StackTrace));
                throw;
            }
            return content;
        }

        public static IEnumerable<KeyValuePair<string, string>> BuildTokenValuePairs(object tokens)
        {
            var type = tokens.GetType();
            var properties = type.GetProperties();

            return properties
                .Where(propertyInfo => propertyInfo.CanRead)
                .ToDictionary(
                    propertyInfo => string.Format(CultureInfo.InvariantCulture, "[@{0}]", propertyInfo.Name.ToUpperInvariant()),
                    propertyInfo => Convert.ToString(propertyInfo.GetValue(tokens, null)));
        }

        public static string ReplaceTokens(IEnumerable<KeyValuePair<string, string>> tokens, string content)
        {
            if (tokens == null)
            {
                return content;
            }

            var tokenFreeContent = new StringBuilder(content);
            foreach (var token in tokens)
            {
                tokenFreeContent.Replace(token.Key, token.Value);
            }

            return tokenFreeContent.ToString();
        }
    }
}
