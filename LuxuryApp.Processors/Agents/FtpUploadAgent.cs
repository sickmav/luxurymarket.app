﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Repository;
using System.Linq;
using EnterpriseDT.Net.Ftp;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Core.Infrastructure.Logging;
using System.Configuration;
using System.IO;
using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal;
using LuxuryApp.Core.Infrastructure.Api.Helpers;
using System.Threading.Tasks;
using LuxuryApp.Cdn.Image.Contracts.Model;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using LuxuryApp.Contracts.Helpers;

namespace LuxuryApp.Processors.Agents
{
    public class FtpUploadAgent : IFtpUploadAgent
    {
        private readonly IFtpRepository _ftpRepository;
        private readonly Func<INotificationHandler<EmailModel>> _emailServiceFactory;

        private ILogger _logger = new LuxuryLogger();

        public FtpUploadAgent(IFtpRepository ftpRepository, Func<INotificationHandler<EmailModel>> emailServiceFactory)
        {
            _ftpRepository = ftpRepository;
            _emailServiceFactory = emailServiceFactory;
        }

        public void UploadFromQueue()
        {
            var ftpConfigs = _ftpRepository.GetAllFtpConfigs();
            var ftpQueueItems = _ftpRepository.GetUploadableItems();

            var configItemsPair = from fc in ftpConfigs
                                  select new
                                  {
                                      FtpConfig = fc,
                                      FtpQueueItems = ftpQueueItems.Where(x => x.FtpConfigId == fc.Id).ToArray(),
                                  };

            foreach (var ci in configItemsPair)
            {
                UploadItems(ci.FtpConfig, ci.FtpQueueItems);
            }
        }

        private void UploadItems(FtpConfig ftpConfig, FtpQueueItem[] ftpQueueItems)
        {
            using (var ftpConnection = GetFtpConnection(ftpConfig))
            {
                try
                {
                    ftpConnection.Connect();

                    if (!ftpConnection.DirectoryExists(ftpConfig.UploadDirectory))
                    {
                        ftpConnection.CreateDirectory(ftpConfig.UploadDirectory);
                    }

                    ftpConnection.ChangeWorkingDirectory(ftpConfig.UploadDirectory);

                    foreach (var ftpQueueItem in ftpQueueItems)
                    {
                        try
                        {
                            _ftpRepository.SetItemUploadStatus(ftpQueueItem.Id, FtpUploadStatus.Uploading, null);

                            ftpConnection.UploadByteArray(ftpQueueItem.FileContentBytes, ftpQueueItem.FileName);

                            _ftpRepository.SetItemUploadStatus(ftpQueueItem.Id, FtpUploadStatus.Uploaded, null);
                        }
                        catch (FTPException e)
                        {
                            _ftpRepository.SetItemUploadStatus(ftpQueueItem.Id, FtpUploadStatus.Failed, e.ToString());
                            _emailServiceFactory().SendAsync(new EmailModel
                            {
                                DataBag = new
                                {
                                    LOGOURL = WebConfigs.LogoUrl,
                                    PoNumber = ftpQueueItem.PONumber,
                                    Link = $"{ConfigurationManager.AppSettings["CMSUrl"]}/Order?statusId=&search={ftpQueueItem.PONumber.Replace("#", "")}",
                                },
                                Subject = $"TRANSFER FAILED - EDI 850",
                                To = WebConfigs.FtpUploadFailedNotificationEmail,
                                EmailTemplateName = "EDI850FileFailed",
                                MasterTemplateName = "MasterGrayBackground"
                            });
                            _logger.Warning(
                                $"Could not upload file {ftpQueueItem.FileName} to {ftpConfig.Host}({ftpConfig.UploadDirectory}). Exception: {e.ToString()}");
                        }
                    }
                }
                catch (FTPException conex)
                {
                    _logger.Warning($"Failed to connect to host {ftpConfig.Host}. Exception: {conex.ToString()}");
                }
                finally
                {
                    if (ftpConnection.IsConnected)
                    {
                        ftpConnection.Close();
                    }
                }
            }
        }

        private FTPConnection GetFtpConnection(FtpConfig ftpConfig)
        {
            var ftpConnection = new FTPConnection();
            ftpConnection.ServerAddress = ftpConfig.Host;
            ftpConnection.UserName = ftpConfig.Username;
            ftpConnection.Password = ftpConfig.Password;


            return ftpConnection;
        }
    }
}
