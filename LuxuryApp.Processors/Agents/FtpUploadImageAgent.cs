﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Repository;
using System.Linq;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Core.Infrastructure.Logging;
using System.Configuration;
using System.IO;
using System;
using System.Collections.Generic;
using LuxuryApp.Core.Infrastructure.Api.Helpers;
using System.Threading.Tasks;
using LuxuryApp.Cdn.Image.Contracts.Model;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Cdn.Image.Contracts;
using System.Diagnostics;

namespace LuxuryApp.Processors.Agents
{
    public class FtpUploadImageAgent : IFtpUploadImageAgent
    {
        private readonly IFtpUploadImagesRepository _ftpRepository;
        private readonly IProductRepository _productRepository;
        private readonly IImageCdnService _imageCdnService;

        private ILogger _logger;

        private readonly string[] ImageExtensions = new string[] { ".jpg", ".jpeg", ".pjpeg", ".gif", ".png" };
        //private readonly string _UserEmail;
        //private readonly string _Password;
        private readonly int _UserId;

        private readonly string _DirectoryFrom;
        private readonly string _DirectoryTo;
        private readonly int _DelaySeconds;

        public FtpUploadImageAgent(IFtpUploadImagesRepository ftpRepository, IProductRepository productRepository, IImageCdnService imageCdnService, ILogger logger)
        {
            _ftpRepository = ftpRepository;
            _imageCdnService = imageCdnService;
            _productRepository = productRepository;
            _logger = logger;
            //_UserEmail = ConfigurationManager.AppSettings["userEmail"];
            //_Password = ConfigurationManager.AppSettings["password"];
            _DirectoryFrom = WebConfigs.FtpUploadImageFrom;
            _UserId = Convert.ToInt32(ConfigurationManager.AppSettings["userId"]);
            _DirectoryTo = WebConfigs.FtpUploadImageTo;
            _DelaySeconds = 10;
            int.TryParse(ConfigurationManager.AppSettings["WorkerPeriodSeconds"], out _DelaySeconds);
        }

        public async Task<double> UploadImagesAsync()
        {
            var scheduleItem = new FtpProductImagesSchedule();
            scheduleItem.StartDateTime = DateTimeOffset.UtcNow;
            var watch = new Stopwatch();
            watch.Start();
            try
            {
                _logger.Information($"Start Processing.");
                ValidateParameters();

                //var token = await GenerateTokenAsync();
                //if (token == null)
                //    throw new Exception("Token error.Stop service.");

                _logger.Information($"Start Processing.");
                var items = new List<FtpProductImageItem>();
                foreach (var skuDir in Directory.GetDirectories(_DirectoryFrom))
                {
                    var sku = Path.GetFileName(skuDir);

                    if (!IsValidProductSku(sku))
                    {
                        _logger.Warning($"{skuDir} not a valid product sku.");
                        continue;
                    }

                    var result = await ProcessDirectoryImagesAsync(skuDir, sku);
                    items.AddRange(result);

                }
                scheduleItem.NumberUploadedImages += items?.Where(x => x.IsUploaded).Count() ?? 0;
                scheduleItem.NumberErrors += items?.Where(x => !x.IsUploaded || !string.IsNullOrWhiteSpace(x.Error))?.Count() ?? 0;

            }
            catch (Exception ex)
            {
                _logger.Error($"UploadImages Exception: {ex.ToString()}");
                scheduleItem.NumberErrors += 1;
                if (ex.InnerException != null)
                {
                    string err = ex.InnerException.Message;
                    _logger.Error(err);
                }
            }

            DeleteEmptyDirs(_DirectoryFrom, true);

            watch.Stop();
            _logger.Information($"stopped!Time elapsed: {watch.Elapsed}");
            scheduleItem.NextRun = DateTimeOffset.UtcNow.AddSeconds(_DelaySeconds);
            scheduleItem.ExecutionTime = watch.Elapsed;

            _logger.Information($"Task Summary:ExecutionTime={scheduleItem.ExecutionTime};NumberErrors={ scheduleItem.NumberErrors};NumberUploadedImages={scheduleItem.NumberUploadedImages}.\n");
            await _ftpRepository.InsertScheduleAsync(scheduleItem);

            return watch.Elapsed.TotalSeconds;
        }

        #region Private Methods

        private async Task<List<FtpProductImageItem>> ProcessDirectoryImagesAsync(string skuDir, string sku)
        {
            _logger.Information($"Start Processing {skuDir}.");

            var listImages = new List<FtpProductImageItem>();
            foreach (var image in Directory.GetFiles(skuDir, "*.*", SearchOption.AllDirectories)
                  .Where(s => ImageExtensions.Contains(Path.GetExtension(s).ToLower())))
            {
                var ftpProductImageItem = new FtpProductImageItem
                {
                    OriginalFileName = Path.GetFileName(image),
                    FileName = $"{sku}_{Guid.NewGuid().ToString().Replace("-", "")}{Path.GetExtension(image)}",
                    Sku = sku,
                    FilePath = Path.GetFullPath(image),
                    FileContentBytes = File.ReadAllBytes(image),
                    IsUploaded = false
                };

                ftpProductImageItem.FileSizeBytes = ftpProductImageItem.FileContentBytes.Length;
                try
                {
                    var cdnItem = UploadToCdn(ftpProductImageItem);
                    listImages.Add(cdnItem);

                    await Task.Run(() => { if (cdnItem.IsUploaded) MoveFile(cdnItem, skuDir); });
                    await _ftpRepository.FtpUploadImagesAsync(new List<FtpProductImageItem> { cdnItem }, _UserId);
                }
                catch (Exception ex)
                {
                    _logger.Error($"---Method:ProcessDirectoryImagesAsync - ImageName:{ftpProductImageItem.FileName},ImagePath:{ftpProductImageItem.FilePath} not moved or inserted in database ! {ex.ToString()}");
                }
            }

            return listImages;
        }

        private FtpProductImageItem UploadToCdn(FtpProductImageItem item)
        {
            var cdnParameter = new CdnImageUploadParameter
            {
                Content = item.FileContentBytes,
                Key = item.FileName,
                ImageSizes = CdnImageSizeList.ImageSizes
            };

            var result = _imageCdnService.Upload(cdnParameter);

            if (result == null || !result.IsSuccessful)
            {
                _logger.Error($"Images {item.FilePath} not uploaded! cdnResults is null or not successful; { result?.Exception?.Message ?? string.Empty}");
                item.IsUploaded = false;
                item.Error = $"cdnResults is null or not successful; { result?.Exception?.Message ?? string.Empty}";
                item.IsUploaded = false;
                return item;
            }
            item.IsUploaded = true;
            return item;
        }

        private void MoveFile(FtpProductImageItem item, string fromDir)
        {
            if (item == null || !item.IsUploaded) return;
            var skuDir = Path.Combine(_DirectoryTo, item.Sku);
            if (!Directory.Exists(skuDir))
            {
                Directory.CreateDirectory(skuDir);
            }

            File.Move(item.FilePath, Path.Combine(skuDir, item.FileName));

            DeleteEmptyDirs(fromDir, false);
        }

        public void DeleteEmptyDirs(string dir, bool isRoot)
        {
            try
            {
                foreach (var d in Directory.EnumerateDirectories(dir))
                {
                    DeleteEmptyDirs(d, false);
                }

                var entries = Directory.EnumerateFiles(dir);

                if (!entries.Any() && !isRoot)
                {
                    try
                    {
                        Directory.Delete(dir);
                    }
                    catch (UnauthorizedAccessException ex) { _logger.Error($"Error deleting folder {dir} -{ex.ToString()}"); }
                    catch (DirectoryNotFoundException ex) { _logger.Error($"Error deleting folder {dir} -{ex.ToString()}"); }
                }
            }
            catch (UnauthorizedAccessException ex) { _logger.Error($"Error deleting folder {dir} -{ex.ToString()}"); }
        }

        private void ValidateParameters()
        {
            if (string.IsNullOrEmpty(_DirectoryFrom))
                throw new ArgumentException("FtpUploadImageFrom directory is a null reference or an empty string -check the config file", "FtpUploadImageFrom");
            if (string.IsNullOrEmpty(_DirectoryTo))
                throw new ArgumentException("DirectoryTo is a null reference or an empty string -check FtpUploadImageTo param in the config file", "FtpUploadImageTo");
            if (_UserId <= 0)
                throw new ArgumentException("UserId is invalid", "FtpUploadImageTo");
            //if (string.IsNullOrEmpty(_UserEmail))
            //    throw new ArgumentException("UserEmail is null or empty string.", "UserEmail");
            //if (string.IsNullOrEmpty(_Password))
            //    throw new ArgumentException("Password is null or empty string.", "_Password");
        }

        private bool IsValidProductSku(string sku)
        {
            return _productRepository.IsvalidProductSKU(sku);
        }

        #region WithToken - Not Used

        private async Task<string> GenerateTokenAsync(string userEmail, string password)
        {
            var token = await ClientHelper.GetTokenAsync(WebConfigs.AuthUrl, userEmail, password);
            if (token == null || !string.IsNullOrWhiteSpace(token.Error))
            {
                _logger.Error($"Could not get token  for user {userEmail}. Error: {token?.Error ?? string.Empty} - ErrorDescription: {token?.ErrorDescription ?? string.Empty}");
                return null;
            }
            return $"bearer {token.AccessToken}";
        }

        private async Task<FtpProductImageItem> UploadToCdnAsync(FtpProductImageItem item, string token)
        {
            var cdnParameter = new CdnImageUploadParameter
            {
                Content = item.FileContentBytes,
                Key = item.FileName,
                ImageSizes = CdnImageSizeList.ImageSizes
            };

            var result = await CdnClientHandler.UploadImage(WebConfigs.LuxuryMarketCDNUrl.ToString(), cdnParameter, token);

            if (result == null || !result.IsSuccessful)
            {
                _logger.Error($"Images {item.FilePath} not uploaded! cdnResults is null or not successful; { result?.Exception?.Message ?? string.Empty}");
                item.IsUploaded = false;
                item.Error = $"cdnResults is null or not successful; { result?.Exception?.Message ?? string.Empty}";
                item.IsUploaded = false;
                return item;
            }
            item.IsUploaded = result?.IsSuccessful ?? false;
            item.Error = $"cdnResults is not successful; { result?.Exception?.Message ?? string.Empty}";
            return item;
        }

        #endregion

        #endregion
    }
}
