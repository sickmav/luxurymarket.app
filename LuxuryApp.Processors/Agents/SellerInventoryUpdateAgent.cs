﻿using LuxuryApp.Contracts.Agents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models.SellerProductSync;
using System.Net.Http;
using LuxuryApp.Contracts.Models.SellerProductSync.Coltorti;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using LuxuryApp.Processors.Services;

namespace LuxuryApp.Processors.Agents
{
    public abstract class SellerInventoryUpdateAgent : ISellerInventoryUpdateAgent
    {
        public Task<AuthenticationResponse> Authenticate(APISettings settings)
        {
            return DoAuthentication(settings);
        }

        public virtual Task<List<InventoryResponse>> GetInventory(AuthenticationResponse authResponse, List<string> skus, APISettings settings)
        {
            throw new NotImplementedException();
        }

        protected async Task<AuthenticationResponse> DoAuthentication(APISettings settings)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(settings.BaseAddress);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", settings.Username, settings.Password))));

            HttpResponseMessage response = await client.PostAsync("/v1/user/token", null);
            return await ReadAuthenticationResponse(response);
        }

        protected async virtual Task<AuthenticationResponse> ReadAuthenticationResponse(HttpResponseMessage response)
        {
            throw new NotImplementedException();
        }

        protected async virtual Task<List<InventoryResponse>> ReadInventoryResponse(HttpResponseMessage response)
        {
            throw new NotImplementedException();
        }
    }
}
