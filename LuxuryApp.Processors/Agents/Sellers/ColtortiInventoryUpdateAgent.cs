﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Models.SellerProductSync.Coltorti;
using LuxuryApp.Processors.Helpers;
using LuxuryApp.Processors.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers
{
    public class ColtortiInventoryUpdateAgent : SellerInventoryUpdateAgent
    {
        protected override async Task<AuthenticationResponse> ReadAuthenticationResponse(HttpResponseMessage response)
        {
            AuthenticationResponse authResponse = null;
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                ColtortiAuthenticationResponse coltortiResponse = JsonConvert.DeserializeObject<ColtortiAuthenticationResponse>(json);
                authResponse = new AuthenticationResponse();
                authResponse.AccessToken = coltortiResponse.access_token;
            }

            return authResponse;
        }

        protected override async Task<List<InventoryResponse>> ReadInventoryResponse(HttpResponseMessage response)
        {
            var json = await response.Content.ReadAsStringAsync();
            Dictionary<string, List<ColtortiInventoryResponse>> coltortiResponse = new Dictionary<string, List<ColtortiInventoryResponse>>();
            try
            {
                coltortiResponse = JsonConvert.DeserializeObject<Dictionary<string, List<ColtortiInventoryResponse>>>(json);
            }
            catch (Exception ex)
            {
                // means no inventory
            }
            return coltortiResponse.ToInventoryResponse();
        }

        public override async Task<List<InventoryResponse>> GetInventory(AuthenticationResponse authResponse, List<string> skus, APISettings settings)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authResponse.AccessToken);

            var pageLimit = 200;
            var inventory = new List<InventoryResponse>();
            for (int i = 0; i < skus.Count; i += pageLimit)
            {
                var pagedSkus = skus.GetRange(i, Math.Min(pageLimit, skus.Count - i));
                var inventoryResponse = await client.GetAsync(settings.BaseAddress + String.Format("/v1/stocks?page=1&limit={0}&id={1}", pageLimit, String.Join(",", pagedSkus)));
                inventory.AddRange(await ReadInventoryResponse(inventoryResponse));
            }

            return inventory;
        }
    }
}
