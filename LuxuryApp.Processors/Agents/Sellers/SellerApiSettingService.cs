﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Agents.Sellers
{
    public class SellerApiSettingService: ISellerApiSettingService
    {
        private readonly Func<ISellerAPISettingRepository> _sellerAPISettingRepository;

        public SellerApiSettingService(Func<ISellerAPISettingRepository> sellerAPISettingRepository)
        {
            _sellerAPISettingRepository = sellerAPISettingRepository;
        }

        public async Task<APISettings> GetSellerApiSettings(SellerAPISettingType sellerAPIType)
        {
            return await _sellerAPISettingRepository().GetSellerApiSettings(sellerAPIType);
        }
    }
}
