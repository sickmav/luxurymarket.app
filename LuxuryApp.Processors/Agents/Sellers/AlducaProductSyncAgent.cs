﻿using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Processors.Helpers;
using LuxuryApp.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LuxuryApp.Processors.Agents.Sellers
{
    public class AlducaProductSyncAgent : ISellerProductSyncAgent
    {
        private readonly Func<ILogger> _logger;
        private readonly Func<ISellerProductSyncRepository> _sellerProductSyncRepository;
        private readonly Func<ISellerAPISettingRepository> _sellerAPISettingRepository;
        private readonly int _commandTimeoutSeconds = 0;
        private const int _stepSize = 500;

        #region Constructor         

        public AlducaProductSyncAgent(Func<ISellerAPISettingRepository> sellerAPISettingRepository,
                                     Func<ISellerProductSyncRepository> sellerProductSyncRepository, Func<ILogger> logger)
        {
            _logger = logger;
            _sellerAPISettingRepository = sellerAPISettingRepository;
            _sellerProductSyncRepository = sellerProductSyncRepository;
        }

        #endregion

        #region Public

        public async Task<int> ProcessProducts(APISettings settings, Guid mainBatchID)
        {
            var products = GetProducts(settings);

            _logger().Trace("Mapped with success. Starting db insert");

            var resultProducts = await InsertProductsAsync(products, mainBatchID, settings);

            _logger().Trace("Products inserted into database");

            var resultInventory = await InsertStock(products, settings.SellerID);

            _logger().Trace("Products Inventory inserted into database");

            return resultInventory;
        }

        #endregion

        #region Privates 

        public List<AlducaProduct> GetProducts(APISettings settings)
        {
            var _url = settings.BaseAddress + "/Services.asmx";
            var _action = settings.BaseAddress + "/GetSku4Platform";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(settings);
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                var productsList = ProcessWebResponse(webResponse);
                _logger().Trace("Done deserialiazation XML. Start mapping products.");
                return productsList;
            }
        }

        public async Task<bool> InsertProductsAsync(List<AlducaProduct> productsList, Guid mainBatchID, APISettings settings)
        {
            var listMapped = new List<ProductSyncItem>();
            var listImagesMapped = new List<ProductSyncImageItem>();
            productsList.ForEach(x =>
            {
                listMapped.Add(x.Map());
                listImagesMapped.AddRange(x.ImageMap());
            });
            listMapped = listMapped.Distinct().ToList();
            listImagesMapped = listImagesMapped.Distinct().ToList();
            var step = 0;
            var listToInsert = listMapped.ChunkBy(_stepSize);
           // List<Task> insertProductTasks = new List<Task>();
            foreach (var list in listToInsert)
            {
                _logger().Trace("Start insert step {0}", step);

                // process to break into user type data tables
                var productImages = (from product in list
                                     join image in listImagesMapped
                                     on product.SellerSKU equals image.SellerSKU
                                     select image).Distinct();
                var imageDt = productImages.ToList().ConvertToDataTable();

                var productsDt = listMapped.ConvertToDataTable();

                await _sellerProductSyncRepository().InsertProductsAsync(productsDt, imageDt, mainBatchID, settings.SellerID);

                //  insertProductTasks.Add(_sellerProductSyncRepository().InsertProductsAsync(productsDt, imageDt, mainBatchID, settings.SellerID, _commandTimeoutSeconds));
                _logger().Trace("Step {0} insertted with success.", step++);
            }
         //  await Task.WhenAll(insertProductTasks.ToArray());

            return true;
        }

        public async Task<int> InsertStock(List<AlducaProduct> productsList, int sellerId)
        {
            var stockItemDataTable = productsList.Select(x => new { SellerSKU = x.SkuID, SellerSize = x.Size, Quantity = x.Stock }).Distinct().ToList().ConvertToDataTable();

            var result = await _sellerProductSyncRepository().SaveInventoryAsync(stockItemDataTable, sellerId, _commandTimeoutSeconds);

            return result;
        }

        private static XmlDocument CreateSoapEnvelope(APISettings settings)
        {
            XmlDocument soapEnvelop = new XmlDocument();

            var xmlString = @"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                                                   xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" 
                                                   xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
                                    <soap12:Body>
                                        <GetSku4Platform xmlns=""" + settings.BaseAddress + @""">
                                            <Username>" + settings.Username + @"</Username>
                                            <Password>" + settings.Password + @"</Password>
                                        </GetSku4Platform>
                                    </soap12:Body>
                                </soap12:Envelope>";

            soapEnvelop.LoadXml(xmlString);
            return soapEnvelop;
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }

        private List<AlducaProduct> ProcessWebResponse(WebResponse webResponse)
        {
            List<AlducaProduct> itemList = null;
            using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
            {
                _logger().Trace(String.Format("Sync done. Starting XML Deserialization "));

                string soapResult = rd.ReadToEnd();

                XDocument doc = XDocument.Parse(soapResult);
                IEnumerable<XElement> responses = doc.Descendants("SkuStok");
                XmlSerializer serializer = new XmlSerializer(typeof(AlducaProduct));
                itemList = new List<AlducaProduct>();
                foreach (XElement response in responses)
                {
                    StringReader reader = new StringReader(response.ToString());
                    itemList.Add((AlducaProduct)serializer.Deserialize(reader));
                }
            }
            return itemList;
        }

        #endregion
    }
}
