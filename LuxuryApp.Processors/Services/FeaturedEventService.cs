﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Services
{
    public class FeaturedEventService
    {
        private readonly Repository _connection;

        public FeaturedEventService()
        {
            _connection = new Repository();
        }

        public List<FeaturedEvent> Get()
        {
            var reader = _connection.LoadData(StoredProcedureNames.FeaturedEvent_Get, new List<SqlParameter>().ToArray());
            return reader.ToFeaturedEventList();
        }
    }
}
