﻿using System;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Resources.Errors;
using LuxuryApp.Core.Infrastructure.Logging;

namespace LuxuryApp.Processors.Services
{
    public class RegistrationRequestService : BaseService, IRegistrationRequestService
    {
        private readonly Func<ICompanyRepository> _repositoryFactory;
        private Func<INotificationHandler<EmailModel>> _emailServiceFactory;

        public RegistrationRequestService(Func<ILogger> loggerFactory,
                                          Func<ICompanyRepository> repositoryFactory,
                                          Func<INotificationHandler<EmailModel>> emailServiceFactory)
            : base(loggerFactory)
        {
            _repositoryFactory = repositoryFactory;
            _emailServiceFactory = emailServiceFactory;

        }

        public async Task<ApiResult<int>> CreateRequestAsync(RegistrationRequest model)
        {
            var errorMessage = string.Empty;
            var result = _repositoryFactory().CreateRequest(model, out errorMessage);
            if (errorMessage != string.Empty)
            {
                return new ApiResultForbidden<int>(0, errorMessage.ProcessErrorMessage());
            }
            if (result > 0)
            {
                await _emailServiceFactory().SendAsync(new EmailModel
                {
                    DataBag = new
                    {
                        ContactName = model.ContactName,
                        RegistrationRequestID = result,
                        LOGOURL = WebConfigs.LogoUrl
                    },
                    MasterTemplateName = "MasterEmailTemplate",
                    EmailTemplateName = "RegistrationRequest",
                    Subject = $"We've received your request",
                    To = model.Email
                });
            }

            return result.ToApiResult();
        }
    }
}
