﻿using System.Collections.Generic;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;

namespace LuxuryApp.Processors.Services
{
    public class MaterialService
    {
        private readonly Repository _connection;

        public MaterialService()
        {
            _connection = new Repository();
        }

        public List<Material> Search(int? materialId = null, int? brandId = null, string materialCode = null, int? standardMaterialId = null)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@MaterialID", materialId),
                 new SqlParameter("@BrandID", brandId),
                 new SqlParameter("@MaterialCode", materialCode),
                 new SqlParameter("@StandardMaterialID", standardMaterialId)
             };
            var reader = _connection.LoadData(StoredProcedureNames.MaterialSearch, parameters.ToArray());
            return reader.ToMaterialList();
        }
    }
}
