﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly Func<ICompanyRepository> _companyRepositoryFactory;

        public CompanyService(Func<ICompanyRepository> companyRepositoryFactory)
        {
            _companyRepositoryFactory = companyRepositoryFactory;
        }

        public async Task<ApiResult<IEnumerable<Company>>> GetCompaniesByCustomerId(int customerId, int companyTypeId = 0)
        {
            var result = await _companyRepositoryFactory().GetCompaniesByCustomerId(customerId, companyTypeId);

            return result.ToApiResult();

        }

        public async Task<ApiResult<Company>> GetCompanyById(int companyId)
        {
            var result = await _companyRepositoryFactory().GetCompanyById(companyId);

            return result.ToApiResult();
        }

        public async Task<ApiResult<bool>> AcceptTermsAndConditions(int companyId, int userId, DateTimeOffset currentDate)
        {
            var result = await _companyRepositoryFactory().AcceptTermsAndConditions(companyId, userId, currentDate);
            return result.ToApiResult();
        }
    }
}
