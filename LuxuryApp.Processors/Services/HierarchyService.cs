﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Services
{
    public class HierarchyService
    {
        private readonly Repository _connection;

        public HierarchyService()
        {
            _connection = new Repository();
        }

        public List<Hierarchy> Get()
        {
            var reader = _connection.LoadData(StoredProcedureNames.HierarchiesGet, (new List<SqlParameter>()).ToArray());
            return reader.ToHierarchyTree();
        }

        public List<SelectListModel> GetCategories()
        {
            var reader = _connection.LoadData(StoredProcedureNames.CategoriesGet, (new List<SqlParameter>()).ToArray());
            return reader.ToSelectList();
        }

        public List<SelectListModel> GetSeasons()
        {
            var reader = _connection.LoadData(StoredProcedureNames.SeasonsGet, (new List<SqlParameter>()).ToArray());
            return reader.ToSelectList();
        }
    }
}
