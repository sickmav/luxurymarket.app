﻿using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using System.Threading.Tasks;
using System;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Processors.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IContextProvider _contextProvider;
        private readonly ICustomerRepository _customerRepository;
        private readonly ICompanyRepository _companyRepository;

        public CustomerService(IContextProvider contextProvider, ICustomerRepository customerRepository, ICompanyRepository companyRepository)
        {
            _contextProvider = contextProvider;
            _customerRepository = customerRepository;
            _companyRepository = companyRepository;
        }

        public async Task<ApiResult<Customer>> GetCustomerByUserId(int userId)
        {
            var customer = await _customerRepository.GetCustomerByUserId(userId);
            if (customer?.CustomerId > 0)
                customer.Companies = await _companyRepository.GetCompaniesByCustomerId(customer.CustomerId, 0);

            return customer.ToApiResult();
        }

        public async Task<ApiResult<bool>> UpdateAccountInfo(AccountInfo model, int userId, DateTimeOffset currentDate)
        {
            var result = await _customerRepository.UpdateAccountInfo(model, userId, currentDate);
            return result.ToApiResult();
        }

        public async Task<ApiResult<Customer>> GetCustomerWithCompaniesAsync(int userId)
        {
            var result = await _customerRepository.GetCustomerWithCompaniesAsync(userId);
            return result.ToApiResult();
        }

        public async Task<Customer> GetCurrentCustomerCompanies()
        {
            var result = await _customerRepository.GetCustomerWithCompaniesAsync(_contextProvider.GetLoggedInUserId());
            return result;
        }
    }
}
