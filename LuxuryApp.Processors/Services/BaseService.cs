﻿using LuxuryApp.Core.Infrastructure.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Services
{
    public class BaseService
    {
        protected readonly Func<ILogger> _loggerFactory;

        public BaseService(Func<ILogger> loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

    }
}
