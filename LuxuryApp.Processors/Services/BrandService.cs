﻿using System.Collections.Generic;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;

namespace LuxuryApp.Processors.Services
{
    public class BrandService
    {
        private readonly Repository _connection;

        public BrandService()
        {
            _connection = new Repository();
        }

        public List<Brand> GetById(int brandId)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@BrandID", brandId)
             };
            var reader = _connection.LoadData(StoredProcedureNames.BrandsGetByID, parameters.ToArray());
            return reader.ToBrandList();
        }

        public List<Brand> Search(int? brandId=null, string name="", int? parentId = null,int ? standardBrandId=null)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@BrandID", brandId),
                 new SqlParameter("@Name", name),
                 new SqlParameter("@ParentBrandID", parentId),
                 new SqlParameter("@StandardBrandID", standardBrandId)
              };
            var reader = _connection.LoadData(StoredProcedureNames.BrandsSearch, parameters.ToArray());
            return reader.ToBrandList();
        }


    }
}
