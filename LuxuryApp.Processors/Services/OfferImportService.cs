﻿using LuxuryApp.Contracts.Services;
using LuxuryApp.Utils.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LuxuryApp.Processors.Resources.Import;
using System.IO;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Enums;
using System;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Processors.Helpers.OfferImport;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Core.Infrastructure.Logging;
using OfficeOpenXml;
using LuxuryApp.Contracts.Models.SizeMappings;
using System.Threading;
using System.Collections;
using LuxuryApp.Resources.Errors;

namespace LuxuryApp.Processors.Services
{
    public class OfferImportService : IOfferImportService
    {
        private readonly Dictionary<string, string> _offerResourceColumns;
        private readonly List<string> _offerProductColumnsValues;
        private readonly Func<ISizeRegionRepository> _sizeRegionRepository;
        private readonly Func<IContextProvider> _contextProviderFactory;
        private readonly ILogger _logger;

        public readonly Func<IOfferImportRepository> _oiRepositoryFactory;
        public readonly Func<ICustomerService> _customerFactory;

        #region Public Methods

        public OfferImportService(Func<IContextProvider> contextProviderFactory,
                                  Func<IOfferImportRepository> offerImportRepositoryFactory,
                                  Func<ISizeRegionRepository> sizeRegionRepository,
                                  Func<ICustomerService> customerFactory, ILogger logger)
        {
            _contextProviderFactory = contextProviderFactory;
            _oiRepositoryFactory = offerImportRepositoryFactory;
            _customerFactory = customerFactory;
            _sizeRegionRepository = sizeRegionRepository;
            _offerResourceColumns = OfferImportHelper.GetOfferColumns();
            _offerProductColumnsValues = _offerResourceColumns.Select(x => x.Value).ToList();
            _logger = logger;
        }

        public async Task<ApiResult<OfferImport>> UploadFile(string fileName, Stream file, int companyId)
        {
            if (companyId <= 0)
                return new ApiResultForbidden<OfferImport>(null, "User's company could not be identified.");

            DataTable products = null;
            DataTable sizes = null;
            OfferImport offerImportItem = null;
            var userId = _contextProviderFactory().GetLoggedInUserId();
            SizeRegion region;

            using (var wsheet = ExcelParseHelper.GetWorksheet(file, 1))
            {
                try
                {
                    if (wsheet == null)
                        return new ApiResultUnprocessableEntity<OfferImport>(null, "The file could not be processed. Please contact Luxury Market support team.");

                    var regionName = wsheet.GetCellValue(1, 2);

                    region = SearchSizeRegion(regionName);
                    if (region?.Id == null)
                        return new ApiResultUnprocessableEntity<OfferImport>(null, "The provided region could not be identified. Please contact Luxury Market support team.");

                    var excelContent = wsheet.GetTable(2);
                    wsheet.Dispose();

                    if (excelContent == null || excelContent.Columns.Count == 0)
                        return new ApiResultUnprocessableEntity<OfferImport>(null, "The file could not be processed. Please contact Luxury Market support team.");

                    products = GetProductData(excelContent);
                    if (products == null || products.Columns.Count == 0 || products.Rows.Count == 0)
                        return new ApiResultUnprocessableEntity<OfferImport>(null, "No products could be found.");

                    var sizeColumns = GetProductSizeColumns(excelContent);
                    var sizeList = GetSizesData(excelContent, sizeColumns);
                    if (sizeList?.Count == 0 || sizeList.Select(x => x.ProductIndex).Distinct().Count() < products.Rows.Count)
                        return new ApiResultUnprocessableEntity<OfferImport>(null, "Some products don't have any sizes set. Please review the file.");
                    sizes = sizeList.ConvertToDataTable();

                }
                catch (Exception ex)
                {
                    _logger.Error($"companyId: {companyId},userid: {userId} fileName: {fileName}  - {ex.Message}");

                    return new ApiResultUnprocessableEntity<OfferImport>(null, "The excel file could not be processed.");
                }
                var offerImportBatchId = await _oiRepositoryFactory().InsertBatchItems(fileName, products, sizes, companyId, region.Id, userId);
                if (offerImportBatchId == 0)
                    return new ApiResultUnprocessableEntity<OfferImport>(null, "The file could not be processed. Please contact Luxury Market support team.");

                offerImportItem = new OfferImport();
                // offerImportItem.CompanyId = companyId;
                offerImportItem.Id = offerImportBatchId;
                offerImportItem.Items = await _oiRepositoryFactory().GetOfferImportProductItems(offerImportBatchId, null);

                var summary = await _oiRepositoryFactory().GetOfferImportSummary(offerImportBatchId);
                offerImportItem.OfferStatus = (OfferStatus?)summary.StatusId;
                offerImportItem.OfferStatusName = summary.StatusName;
                offerImportItem.TotalCost = summary.SellerTotalPrice;
                offerImportItem.TotalUnits = summary.TotalNumberOfUnits;

                var errorList = await GetValidateErrors(offerImportBatchId, null, true);

                if (errorList?.Any() == true)
                {
                    offerImportItem.Items.ToList().ForEach(x => x.Errors = errorList.Where(y => y.OfferImportBatchItemId == x.OfferImportBatchItemId).ToList());
                }

                return offerImportItem.ToApiResult();
            }
        }

        public async Task<ApiResult<OfferImportBachItem>> GetOfferProductFullData(int offerImportBatchItemId)
        {
            var result = await _oiRepositoryFactory().GetOfferProductFullData(offerImportBatchItemId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OfferImportSummary>> GetOfferImportSummary(int offerImportBatchId)
        {
            var result = await _oiRepositoryFactory().GetOfferImportSummary(offerImportBatchId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<int>> UpdateOfferImportGeneralData(OfferImport item)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            var result = await _oiRepositoryFactory().UpdateOfferImportGeneralData(item, userId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OfferImport>> UpdateOfferImportItem(OfferImportBachItemEditModel item, DateTimeOffset date, bool mapsizes, bool returnUpdatedItem)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            var errorMessage = string.Empty;
            var returnItem = new OfferImport();

            await _oiRepositoryFactory().UpdateOfferImportItem(item, userId, date, mapsizes);

            if (!string.IsNullOrWhiteSpace(errorMessage))
                return new ApiResultBadRequest<OfferImport>(null, errorMessage);

            if (returnUpdatedItem)
            {
                try
                {
                    var itemData = await _oiRepositoryFactory().GetOfferProductFullData(item.Id);

                    var summary = await _oiRepositoryFactory().GetOfferImportSummary(itemData.OfferImportBatchId);

                    var errors = await _oiRepositoryFactory().Validate(itemData.OfferImportBatchId, null, false);
                    var errorList = errors.ToList();
                    var hasErrors = errorList?.Any() == true;
                    var totalErrorItems = hasErrors ? errorList.Select(x => x.OfferImportBatchItemId).Distinct().Count() : 0;
                    var totalSuccessfullItems = summary.TotalProducts - totalErrorItems;

                    returnItem = new OfferImport
                    {
                        Id = summary.OfferImportBatchId,
                        SellerTotalPrice = summary.SellerTotalPrice,
                        OfferStatus = (OfferStatus?)summary.StatusId,
                        OfferStatusName = summary.StatusName,
                        TotalCost = summary.SellerTotalPrice,
                        TotalUnits = summary.TotalNumberOfUnits,
                        TakeAll = summary.TakeAll,
                        TotalProducts = summary.TotalProducts,
                        TotalErrorItems = totalErrorItems,
                        TotalSuccessfulItems = totalSuccessfullItems,
                        StartDate = summary.StartDate,
                        EndDate = summary.EndDate,
                        CompanyId = summary.CompanyId,
                        CompanyFee = summary.CompanyFee,
                        Items = new List<OfferImportBachItem> { itemData }
                    };
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    return new ApiResultInternalError<OfferImport>(null, errorMessage);
                }
            }
            return returnItem.ToApiResult();
        }

        public async Task<ApiResult<OfferImport>> BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            var errorMessage = String.Empty;
            var returnItem = new OfferImport();
            _oiRepositoryFactory().BatchUpdateOfferImportItems(model, userId, DateTimeOffset.Now, out errorMessage);

            try
            {
                var summary = await _oiRepositoryFactory().GetOfferImportSummary(model.OfferImportBatchId);

                returnItem = new OfferImport
                {
                    Id = summary.OfferImportBatchId,
                    SellerTotalPrice = summary.SellerTotalPrice,
                    TotalCost = summary.TotalCostOffer,
                    TotalUnits = summary.TotalNumberOfUnits,
                    TotalProducts = summary.TotalProducts
                };
            }
            catch (Exception ex)
            {
                return new ApiResultInternalError<OfferImport>(null, ex.Message);
            }


            return returnItem.ToApiResult();
        }

        public async Task<ApiResult<int>> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            var result = await _oiRepositoryFactory().AddUpdateOfferImportItemSize(item, userId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OfferImportBatchItemSizeCollection>> GetOfferImportItemSizes(int offerImportBatchItemId)
        {
            var result = await _oiRepositoryFactory().GetOfferImportItemSizes(offerImportBatchItemId);
            var collection = new OfferImportBatchItemSizeCollection { Items = result };
            return collection.ToApiResult();
        }

        public async Task<ApiResult<int>> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();

            var result = await _oiRepositoryFactory().UpdateOfferItemsLineBuy(item, userId);
            return result.ToApiResult();
        }

        public async Task<ApiResult<OfferImportBatchItemErrorCollection>> Validate(int? offerImportId, int? offerImportItemId, bool onlyActive = true)
        {
            var items = await GetValidateErrors(offerImportId, offerImportItemId, onlyActive);
            var collection = new OfferImportBatchItemErrorCollection { Items = items };
            return collection.ToApiResult();
        }

        public async Task<ApiResult<OfferImportFileCollection>> GetFiles(int offerImportId)
        {
            var result = await _oiRepositoryFactory().GetFiles(offerImportId);
            var collection = new OfferImportFileCollection { Items = result };
            return collection.ToApiResult();
        }

        public async Task<ApiResult<int>> UpdateFile(OfferImportFile file)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();

            var result = await _oiRepositoryFactory().UpdateFile(file, userId);
            return result.ToApiResult();
        }

        public ApiResult<int> InsertFile(OfferImportFile file)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            return _oiRepositoryFactory().InsertFiles(file, userId).ToApiResult();
        }

        public async Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item)
        {
            return await _oiRepositoryFactory().GetModelNumberByBrand(item);
        }

        public async Task<ApiResult<int>> InsertOfferImportError(OfferImportReportError model)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();

            var result = await _oiRepositoryFactory().InsertOfferImportError(model, userId);
            return result.ToApiResult();
        }

        #endregion

        #region Private Methods

        private SizeRegion SearchSizeRegion(string region)
        {
            if (string.IsNullOrWhiteSpace(region))
                return null;
            var item = _sizeRegionRepository().SearchSizeRegions(region.Trim());
            if (item.Any()) return item.FirstOrDefault();
            return null;
        }

        private DataTable GetProductData(DataTable dt)
        {
            return dt.AsEnumerable().Select((dr, index) => new OfferImportExcelProduct
            {
                ProductIndex = index,
                Brand = dr.Field<string>(OfferExcelColumnNames.Brand),
                Gender = dr.Field<string>(OfferExcelColumnNames.Gender),
                ModelNumber = dr.Field<string>(OfferExcelColumnNames.ModelNumber),
                ColorCode = dr.Field<string>(OfferExcelColumnNames.ColorCode),
                MaterialCode = dr.Field<string>(OfferExcelColumnNames.MaterialCode),
                VendorSku = dr.Field<string>(OfferExcelColumnNames.VendorSKU),
                CurrencyName = dr.Field<string>(OfferExcelColumnNames.Currency),
                OfferCost = dr.Field<string>(OfferExcelColumnNames.OfferCost),
                Discount = dr.Field<string>(OfferExcelColumnNames.Discount),
                TotalQuantity = dr.Field<string>(OfferExcelColumnNames.TotalQuantity),
                TotalPrice = dr.Field<string>(OfferExcelColumnNames.TotalPrice),
                RetailPrice = dr.Field<string>(OfferExcelColumnNames.RetailPrice),
            }).ToList().ConvertToDataTable();
        }

        private string[] GetProductSizeColumns(DataTable excelContent)
        {
            var columnsDictionary = OfferImportHelper.GetOfferColumns();
            var productColumns = columnsDictionary.Select(x => x.Value).ToList();
            var excelColumns = excelContent.Columns.Cast<DataColumn>().Select(x => x.ColumnName.ToLower().Replace(" ", "")).ToList();
            var sizesColumns = excelColumns.Except(productColumns).ToArray();
            return sizesColumns;
        }
        private List<ProductSizeImportModel> GetSizesData(DataTable excelContent, string[] sizesColumns)
        {
            var sizesList = new List<ProductSizeImportModel>();
            var dtSizes = new DataView(excelContent).ToTable(false, sizesColumns);
            for (var rowIndex = 0; rowIndex < dtSizes.Rows.Count; rowIndex++)
            {
                foreach (var columnName in sizesColumns)
                {
                    if (!string.IsNullOrWhiteSpace(dtSizes.Rows[rowIndex][columnName]?.ToString()))
                        sizesList.Add(new ProductSizeImportModel
                        {
                            ProductIndex = rowIndex,
                            Quantity = dtSizes.Rows[rowIndex][columnName]?.ToString().Trim(),
                            SizeName = columnName
                        });
                }
            }

            return sizesList;
        }

        public async Task<List<OfferImportBatchItemError>> GetValidateErrors(int? offerImportId, int? offerImportItemId, bool onlyActive = true)
        {
            var items = await _oiRepositoryFactory().Validate(offerImportId, offerImportItemId, onlyActive);
            var result = items.ToList();
            result.ForEach(x => x.Message = string.Format(x.ErrorType.ProcessErrorMessage() ?? string.Empty, x.ColumnName, x.ColumnValue));
            return result;
        }
        #endregion
    }
}
