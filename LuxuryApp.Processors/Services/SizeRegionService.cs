﻿using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;
using LuxuryApp.Contracts.Models.SizeMappings;

namespace LuxuryApp.Processors.Services
{
    public class SizeRegionService : ISizeRegionService
    {
        private readonly Func<ISizeRegionRepository> _sizeRegionRepository;

        public SizeRegionService(Func<ISizeRegionRepository> sizeRegionRepository)
        {
            _sizeRegionRepository = sizeRegionRepository;
        }

        public async Task<ApiResult<SizeRegionCollection>> GetSizeRegions()
        {
            var result = await _sizeRegionRepository().GetSizeRegions();
            var collection = new SizeRegionCollection { Items = result };
            return collection.ToApiResult();
        }

        public ApiResult<SizeRegionCollection> SearchSizeRegions(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return null;
            var result = _sizeRegionRepository().SearchSizeRegions(name);
            var collection = new SizeRegionCollection { Items = result };
            return collection.ToApiResult();
        }
    }
}
