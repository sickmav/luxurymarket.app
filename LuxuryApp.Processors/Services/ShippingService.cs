﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Contracts.Access;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Models.Shipment;
using LuxuryApp.Contracts.Models.Shipping;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Authorization;

namespace LuxuryApp.Processors.Services
{
    public class ShippingService: 
        IShippingService
    {
        private readonly IContextProvider _contextProvider;
        private readonly ICompanyAccess _companyAccess;
        private readonly ICartRepository _cartRepository;
        private readonly IShippingRepository _shippingRepository;
        private readonly IProductRepository _productRepository;

        public ShippingService(
            IContextProvider contextProvider,
            ICompanyAccess companyAccess,
            ICartRepository cartRepository,
            IShippingRepository shippingRepository,
            IProductRepository productRepository)
        {
            _contextProvider = contextProvider;
            _companyAccess = companyAccess;
            _cartRepository = cartRepository;
            _shippingRepository = shippingRepository;
            _productRepository = productRepository;
        }

        public ApiResult<Shipping> PreviewShippingForCart(int cartId)
        {
            var cart = _cartRepository.GetCart(cartId);
            if (!_companyAccess.CanAddToCartForCompany(cart?.BuyerCompanyId ?? 0))
            {
                return new ApiResultForbidden<Shipping>(null);
            }

            var shipping = ComputePreviewShippingFroCart(cart);

            return shipping.ToApiResult();
        }

        private Shipping ComputePreviewShippingFroCart(Cart cart)
        {
            if (cart == null)
            {
                return null;
            }
            var shippingTaxesIds = (from cisu in cart.Items.SelectMany(x => x.SellerUnits)
                group cisu by cisu.ShippingTaxId
                into gcisu
                select gcisu.Key).ToArray();

            var shippingTaxes = _shippingRepository.GeShippingTaxesByIds(shippingTaxesIds);
            var productDivisions = _productRepository.GetProductsDivisions(cart.Items.Select(x => x.ProductId).ToArray());

            var productApparelNonApparel =
                productDivisions.Select(x => new
                {
                    x.ProductId,
                    IsApparel = string.Compare(x.DivisionName, "Apparel", StringComparison.OrdinalIgnoreCase) == 0
                }).ToArray();

            var shippingItems =
            (from ci in cart.Items
                from cisu in ci.SellerUnits.Where(x => x.Active)
                let ssis = new
                {
                    cisu.ShippingTaxId,
                    ci.ProductId,
                    ShipmentItemBySeller = new ShipmentItemBySeller
                    {
                        Currency = cart.Currency,
                        SellerAlias = cisu.SellerAlias,
                        UnitOfferCost = cisu.CustomerUnitPrice,
                        UnitsCount = cisu.UnitsCount,
                        TotalOfferCost = cisu.TotalCustomerCost,
                    }
                }
                group ssis by new {ssis.ProductId, ssis.ShippingTaxId}
                into gssis
                join ci in cart.Items
                on gssis.Key.ProductId equals ci.ProductId
                join pana in productApparelNonApparel
                on gssis.Key.ProductId equals pana.ProductId
                join sht in shippingTaxes
                on gssis.Key.ShippingTaxId equals sht.Id
                let tu = gssis.Select(x => x.ShipmentItemBySeller.UnitsCount).Sum()
                let tc = gssis.Select(x => x.ShipmentItemBySeller.TotalOfferCost).Sum()
                //let tlc = tc + (decimal)(tc * (pana.IsApparel ? sht.ApparelTax : sht.NonApparelTax) / 100)
                let tlc = gssis.Select(x => x.ShipmentItemBySeller.UnitsCount*
                                            (Math.Ceiling(x.ShipmentItemBySeller.UnitOfferCost*
                                                          (pana.IsApparel ? sht.ApparelTax : sht.NonApparelTax)/100)))
                    .Sum()
                let shi = new ShipmentItem
                {
                    CartId = ci.CartId,
                    ProductId = ci.ProductId,
                    ProductName = ci.ProductName,
                    BrandName = ci.BrandName,
                    ProductMainImageName = ci.ProductMainImageName,
                    ShippingTaxId = gssis.Key.ShippingTaxId,
                    ShipmentItemByBySellers = gssis.Select(x => x.ShipmentItemBySeller).ToArray(),
                    TotalUnits = tu,
                    TotalCost = tc,
                    ShippingCost = tlc,
                    Currency = cart.Currency,
                    IsApparel = pana.IsApparel,
                }
                select shi).ToArray();

            var shipments =
                (from shi in shippingItems
                        group shi by shi.ShippingTaxId
                        into gshi
                        join sht in shippingTaxes on gshi.Key equals sht.Id
                        select new Shipment
                        {
                            ShippingRegionId = sht.Id,
                            ShipsFromRegion = sht.DisplayName,
                            ShipingIterval = sht.Days,
                            TotalStylesCount = gshi.Select(x => x.ProductId).Distinct().Count(),
                            Currency = cart.Currency,
                            ShipmentItems = gshi.ToArray(),
                            ApparelCost = gshi.Where(x => x.IsApparel).Sum(x => x.ShippingCost),
                            NonApparelCost = gshi.Where(x => !x.IsApparel).Sum(x => x.ShippingCost),
                            TotalShippingCost = gshi.Sum(x => x.ShippingCost),
                        }).
                    OrderBy(c => c.ShipsFromRegion)
                    .ThenBy(c => c.ShipingIterval).ToArray();
            var shipping = new Shipping
            {
                CartId = cart.Id,
                Currency = cart.Currency,
                Shipments = shipments,
                TotalShippingCost = shipments.Sum(x => x.TotalShippingCost),
            };
            return shipping;
        }

        public ApiResult<ShippingSummary> CancelShipment(CancelShipmentModel model)
        {
            var cartSummary = _cartRepository.GetCartSummary(model.CartId);
            if (!_companyAccess.CanAddToCartForCompany(cartSummary.BuyerCompanyId))
            {
                return new ApiResultForbidden<ShippingSummary>(null);
            }

            /*
             *  In current implementation each company has one shipping tax id. 
             *  For this reason there isn't any logic to verify whether the product is line buy or the offer is take all,
             *  since the offer is under a company, and a shipping tax includes company, consequently, taking a shipping tax out
             *  is taking a company out and therefore all offers under that company
             *  
             *  However, if a company can have multiple shipping taxes, the bellow logic should be revised.
             */ 

            _cartRepository.RemoveCartItemsWithShippingTaxId(model.CartId, model.ShippingRegionId);
            var cart = _cartRepository.GetCart(model.CartId);
            var shipping = ComputePreviewShippingFroCart(cart);
            var shippingSummary = new ShippingSummary
            {
                CartId = shipping.CartId,
                ShipmentsCount = shipping.ShipmentsCount,
                TotalShippingCost = shipping.TotalShippingCost,
                Currency = shipping.Currency,
            };
            return shippingSummary.ToApiResult();
        }
    }
}
