﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Processors.Helpers
{
    public static class AlducaMapping
    {
        public static List<ProductSyncImageItem> ImageMap(this AlducaProduct product)
        {
            //process image url
            var itemImagesList = new List<ProductSyncImageItem>();
            var listOfImagesSplitted = product.ItemImages.Split(',').ToList();
            int indexOfImage = 1;
            foreach (var image in listOfImagesSplitted)
            {
                var imageReplaced = image.Replace("pic" + indexOfImage++ + ":", "");
                if (!String.IsNullOrWhiteSpace(imageReplaced))
                {
                    itemImagesList.Add(new ProductSyncImageItem { SellerSKU = product.SkuID, ImageUrl=imageReplaced });
                }
            }
            return itemImagesList;
        }
        public static ProductSyncItem Map(this AlducaProduct product)
        {
            return new ProductSyncItem
            {
                SellerSKU = product.SkuID,
                ModelNumber = product.Model,
                MaterialCode = "",
                ColorCode = "",
                Color = product.Color,
                Material = product.Texture,
                Brand = product.Brand,
                Season = product.Season,
                SizeType = "",
                SizeTypeDescription = "",
                Name = product.Title,
                Description = product.ItemDescription,
                SellerRetailPrice = Convert.ToDouble(product.MarketPrice),
                Business = product.Suitable,
                Division = "",
                Department = "",
                Category = product.Category.Replace(product.Suitable.Trim(), "").Trim(),
                Origin = product.Made
            };
        }
    }
}
