﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.Orders;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LuxuryApp.Processors.Helpers.Export
{
    public class PDFHelper
    {
        public static byte[] GenereateOrderSellerPDF(List<SuborderDataForPDF> orderData)
        {
            // Create a Document object
            var document = new Document();

            // Set the page size
            document.SetPageSize(PageSize.A4.Rotate());

            using (var output = new MemoryStream())
            {
                var writer = PdfWriter.GetInstance(document, output);

                // Open the Document for writing
                document.Open();

                GenerateOrderSellerPDF(document, orderData);

                // Close the document
                document.Close();

                return output.ToArray();
            }
        }

        public static byte[] GenereateOrderConfirmedSellerPDF(List<SuborderDataForPDF> orderData)
        {
            // Create a Document object
            var document = new Document();

            // Set the page size
            document.SetPageSize(PageSize.A4.Rotate());

            using (var output = new MemoryStream())
            {
                var writer = PdfWriter.GetInstance(document, output);

                // Open the Document for writing
                document.Open();

                GenerateOrderConfirmedSellerPDF(document, orderData);

                // Close the document
                document.Close();

                return output.ToArray();
            }
        }

        public static byte[] GenereateOrderForSellerPDF(List<SuborderDataForPDF> orderData)
        {
            // Create a Document object
            var document = new Document();

            // Set the page size
            document.SetPageSize(PageSize.A4.Rotate());

            using (var output = new MemoryStream())
            {
                var writer = PdfWriter.GetInstance(document, output);

                // Open the Document for writing
                document.Open();

                GenerateOrderForSellerPDF(document, orderData);

                // Close the document
                document.Close();

                return output.ToArray();
            }
        }

        private static Document GenerateOrderSellerPDF(Document document, List<SuborderDataForPDF> orderData)
        {
            //variables
            var pdfTable = new PdfPTable(2);
            var pdfCellTable = new PdfPCell();
            var phrase = new Phrase();
            //fonts
            var fontBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9);
            var fontNormal = FontFactory.GetFont(FontFactory.HELVETICA, 9);
            var fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);

            var billingAddress = orderData.First().Addresses.Where(a => a.AddressType == AddressType.Billing).FirstOrDefault();
            var shippingAddress = orderData.First().Addresses.Where(a => a.AddressType == AddressType.Shipping).FirstOrDefault();

            phrase.Add(Chunk.NEWLINE);
            document.Add(phrase);

            #region Luxury Market Info
            //Luxury Market Info
            pdfTable = new PdfPTable(2);
            pdfTable.WidthPercentage = 100f;

            phrase = new Phrase();
            phrase.Add(new Chunk("Luxury Market", fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("230 West. 38th St, 5th FL", fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("New York, NY 10018", fontNormal));
            phrase.Add(Chunk.NEWLINE);
            pdfCellTable = new PdfPCell(phrase);
            pdfCellTable.BorderWidth = 0f;
            pdfTable.AddCell(pdfCellTable);

            phrase = new Phrase(new Chunk("INVOICE", fontTitle));
            pdfCellTable = new PdfPCell(phrase);
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCellTable.BorderWidth = 0f;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for Date and Invoice (PONumber)
            //Table for Date and Invoice (PONumber)
            pdfTable = new PdfPTable(2);
            pdfTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfTable.WidthPercentage = 30f;

            pdfCellTable = new PdfPCell(new Phrase("Date", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Invoice #", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().CreatedDate.DateTime.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().OrderSellerPONumber, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for Biill and Ship to
            //Table for Biill and Ship to
            pdfTable = new PdfPTable(2);
            pdfTable.WidthPercentage = 100f;

            pdfCellTable = new PdfPCell(new Phrase("Bill to", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Ship to", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            phrase = new Phrase();
            phrase.Add(new Chunk(orderData.FirstOrDefault().BuyerCompanyName, fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("City: ", fontBold));
            phrase.Add(new Chunk(billingAddress.City.ToUpper(), fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("Street: ", fontBold));
            phrase.Add(new Chunk(billingAddress.Street1 + " " + billingAddress.Street2, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("State: ", fontBold));
            phrase.Add(new Chunk(billingAddress.State.StateName + ", " + billingAddress.Country.CountryName, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("ZIP Code: ", fontBold));
            phrase.Add(new Chunk(billingAddress.ZipCode, fontNormal));
            pdfCellTable = new PdfPCell(phrase);
            pdfTable.AddCell(pdfCellTable);

            phrase = new Phrase();
            phrase.Add(new Chunk(orderData.FirstOrDefault().BuyerCompanyName, fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("City: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.City.ToUpper(), fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("Street: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.Street1 + " " + shippingAddress.Street2, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("State: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.State.StateName + ", " + shippingAddress.Country.CountryName, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("ZIP Code: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.ZipCode, fontNormal));
            pdfCellTable = new PdfPCell(phrase);
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for po number and terms            
            pdfTable = new PdfPTable(6);
            pdfTable.WidthPercentage = 100f;
            pdfTable.SetWidths(new float[] { 4, 2, 2, 1, 2, 2 });

            pdfCellTable = new PdfPCell(new Phrase("P.O. Number", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Terms", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Ship", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Via", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("F.O.B.", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Project", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().OrderSellerPONumber, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Products Details Table
            //Products Details Table
            pdfTable = new PdfPTable(9);
            pdfTable.WidthPercentage = 100f;
            pdfTable.SetWidths(new float[] { 2, 4, 2, 2, 1, 2, 1, 3, 3 });

            pdfCellTable = new PdfPCell(new Phrase("BRAND NAME", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("DESCRIPTION", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("VENDOR STYLE NUMBER", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("VENDOR COLOR", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("SIZE", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("NRF COLOR", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("QTY", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("UNIT USD COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL USD COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            var units = 0;
            decimal prodcutsCost = 0;
            foreach (var orderProduct in orderData)
            {
                pdfCellTable = new PdfPCell(new Phrase(orderProduct.BrandName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ProductName + " - [" + orderProduct.SKU + "]", fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ModelNumber, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ColorCode, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.SizeName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.StandardColorName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.Units.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.UnitLandedCost.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                decimal cost = orderProduct.UnitLandedCost * orderProduct.Units;

                pdfCellTable = new PdfPCell(new Phrase(cost.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                units += orderProduct.Units;
                prodcutsCost += orderProduct.CustomerUnitPrice * orderProduct.Units;
            }


            //Summary (total units and total columns)
            //First Row - totalu units and products cost
            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 4;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL UNITS", fontBold));
            pdfCellTable.Colspan = 2;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(units.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("PRODUCTS COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("$ " + prodcutsCost.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            //Second row - Shipping Cost
            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 7;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("FREIGHT & DUTIES", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("$ " + orderData.First().ShippingCost.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            //Third row - Total cost
            var totalCost = (prodcutsCost + orderData.First().ShippingCost).ToString();

            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 7;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("$ " + totalCost, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            #endregion

            return document;
        }

        private static Document GenerateOrderConfirmedSellerPDF(Document document, List<SuborderDataForPDF> orderData)
        {
            //variables
            var pdfTable = new PdfPTable(2);
            var pdfCellTable = new PdfPCell();
            var phrase = new Phrase();
            //fonts
            var fontBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9);
            var fontNormal = FontFactory.GetFont(FontFactory.HELVETICA, 9);
            var fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);

            var billingAddress = orderData.First().Addresses.Where(a => a.AddressType == AddressType.Billing).FirstOrDefault();
            var shippingAddress = orderData.First().Addresses.Where(a => a.AddressType == AddressType.Shipping).FirstOrDefault();

            phrase.Add(Chunk.NEWLINE);
            document.Add(phrase);

            #region Luxury Market Info
            //Luxury Market Info
            pdfTable = new PdfPTable(2);
            pdfTable.WidthPercentage = 100f;

            phrase = new Phrase();
            phrase.Add(new Chunk("Luxury Market", fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("230 West. 38th St, 5th FL", fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("New York, NY 10018", fontNormal));
            phrase.Add(Chunk.NEWLINE);
            pdfCellTable = new PdfPCell(phrase);
            pdfCellTable.BorderWidth = 0f;
            pdfTable.AddCell(pdfCellTable);

            phrase = new Phrase(new Chunk("INVOICE", fontTitle));
            pdfCellTable = new PdfPCell(phrase);
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCellTable.BorderWidth = 0f;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for Date and Invoice (PONumber)
            //Table for Date and Invoice (PONumber)
            pdfTable = new PdfPTable(2);
            pdfTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfTable.WidthPercentage = 30f;

            pdfCellTable = new PdfPCell(new Phrase("Date", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Invoice #", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().CreatedDate.DateTime.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().OrderSellerPONumber, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for Biill and Ship to
            //Table for Biill and Ship to
            pdfTable = new PdfPTable(2);
            pdfTable.WidthPercentage = 100f;

            pdfCellTable = new PdfPCell(new Phrase("Bill to", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Ship to", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            phrase = new Phrase();
            phrase.Add(new Chunk(orderData.FirstOrDefault().BuyerCompanyName, fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("City: ", fontBold));
            phrase.Add(new Chunk(billingAddress.City.ToUpper(), fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("Street: ", fontBold));
            phrase.Add(new Chunk(billingAddress.Street1 + " " + billingAddress.Street2, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("State: ", fontBold));
            phrase.Add(new Chunk(billingAddress.State.StateName + ", " + billingAddress.Country.CountryName, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("ZIP Code: ", fontBold));
            phrase.Add(new Chunk(billingAddress.ZipCode, fontNormal));
            pdfCellTable = new PdfPCell(phrase);
            pdfTable.AddCell(pdfCellTable);

            phrase = new Phrase();
            phrase.Add(new Chunk(orderData.FirstOrDefault().BuyerCompanyName, fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("City: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.City.ToUpper(), fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("Street: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.Street1 + " " + shippingAddress.Street2, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("State: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.State.StateName + ", " + shippingAddress.Country.CountryName, fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("ZIP Code: ", fontBold));
            phrase.Add(new Chunk(shippingAddress.ZipCode, fontNormal));
            pdfCellTable = new PdfPCell(phrase);
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for po number and terms            
            pdfTable = new PdfPTable(6);
            pdfTable.WidthPercentage = 100f;
            pdfTable.SetWidths(new float[] { 4, 2, 2, 1, 2, 2 });

            pdfCellTable = new PdfPCell(new Phrase("P.O. Number", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Terms", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Ship", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Via", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("F.O.B.", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("Project", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().OrderSellerPONumber, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Products Details Table
            //Products Details Table
            pdfTable = new PdfPTable(10);
            pdfTable.WidthPercentage = 100f;
            pdfTable.SetWidths(new float[] { 2, 3, 2, 2, 1, 2, 1, 2, 2, 3 });

            pdfCellTable = new PdfPCell(new Phrase("BRAND NAME", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("DESCRIPTION", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("VENDOR STYLE NUMBER", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("VENDOR COLOR", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("SIZE", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("NRF COLOR", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("QTY", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("QTY CONFIRMED", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("UNIT USD COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL USD COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            var units = 0;
            var unitsConfirmed = 0;
            decimal prodcutsCost = 0;
            foreach (var orderProduct in orderData)
            {
                pdfCellTable = new PdfPCell(new Phrase(orderProduct.BrandName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ProductName + " - [" + orderProduct.SKU + "]", fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ModelNumber, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ColorCode, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.SizeName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.StandardColorName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.Units.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.UnitsConfirmed.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.UnitLandedCost.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                decimal cost = orderProduct.UnitLandedCost * orderProduct.UnitsConfirmed;

                pdfCellTable = new PdfPCell(new Phrase(cost.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                units += orderProduct.Units;
                unitsConfirmed += orderProduct.UnitsConfirmed;
                prodcutsCost += orderProduct.CustomerUnitPrice * orderProduct.UnitsConfirmed;
            }


            //Summary (total units and total columns)
            //First Row - totalu units and products cost
            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 5;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL UNITS", fontBold));
            pdfCellTable.Colspan = 2;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(units.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("PRODUCTS COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("$ " + prodcutsCost.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            //Second row - Shipping Cost
            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 5;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL UNITS CONFIRMED", fontBold));
            pdfCellTable.Colspan = 2;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(unitsConfirmed.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("FREIGHT & DUTIES", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("$ " + orderData.First().ShippingCost.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            //Third row - Total cost
            var totalCost = (prodcutsCost + orderData.First().ShippingCost).ToString();

            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 8;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL COST", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("$ " + totalCost, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfCellTable.BorderWidthTop = 0f;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            #endregion

            return document;
        }


        private static Document GenerateOrderForSellerPDF(Document document, List<SuborderDataForPDF> orderData)
        {
            //variables
            var pdfTable = new PdfPTable(2);
            var pdfCellTable = new PdfPCell();
            var phrase = new Phrase();
            //fonts
            var fontBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9);
            var fontNormal = FontFactory.GetFont(FontFactory.HELVETICA, 9);
            var fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);

            phrase.Add(Chunk.NEWLINE);
            document.Add(phrase);

            #region Luxury Market Info
            //Luxury Market Info
            pdfTable = new PdfPTable(1);
            pdfTable.WidthPercentage = 100f;

            phrase = new Phrase();
            phrase.Add(new Chunk("Luxury Market", fontBold));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("230 West. 38th St, 5th FL", fontNormal));
            phrase.Add(Chunk.NEWLINE);
            phrase.Add(new Chunk("New York, NY 10018", fontNormal));
            phrase.Add(Chunk.NEWLINE);
            pdfCellTable = new PdfPCell(phrase);
            pdfCellTable.BorderWidth = 0f;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Table for Date and Invoice (PONumber)
            //Table for Date and Invoice (PONumber)
            pdfTable = new PdfPTable(2);
            pdfTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfTable.WidthPercentage = 30f;

            pdfCellTable = new PdfPCell(new Phrase("Date", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("PO#", fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().CreatedDate.DateTime.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(orderData.FirstOrDefault().OrderSellerPONumber, fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            document.Add(new Chunk(Chunk.NEWLINE));
            #endregion

            #region Products Details Table
            //Products Details Table
            pdfTable = new PdfPTable(8);
            pdfTable.WidthPercentage = 100f;
            pdfTable.SetWidths(new float[] { 2, 3, 2, 2, 1, 2, 1, 1 });

            pdfCellTable = new PdfPCell(new Phrase("BRAND NAME", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("DESCRIPTION", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("VENDOR STYLE NUMBER", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("VENDOR COLOR", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("SIZE", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("NRF COLOR", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("QTY", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("QTY CONFIRMED", fontBold));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfTable.AddCell(pdfCellTable);

            var units = 0;
            var unitsConfirmed = 0;
            foreach (var orderProduct in orderData)
            {
                pdfCellTable = new PdfPCell(new Phrase(orderProduct.BrandName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ProductName + " - [" + orderProduct.SKU + "]", fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ModelNumber, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.ColorCode, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.SizeName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.StandardColorName, fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.Units.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                pdfCellTable = new PdfPCell(new Phrase(orderProduct.UnitsConfirmed.ToString(), fontNormal));
                pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfCellTable.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfTable.AddCell(pdfCellTable);

                units += orderProduct.Units;
                unitsConfirmed += orderProduct.UnitsConfirmed;
            }


            //Summary (total units and total columns)
            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 5;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL UNITS", fontBold));
            pdfCellTable.Colspan = 2;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(units.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfTable.AddCell(pdfCellTable);

            //Summary (total units confirmed and total columns)
            pdfCellTable = new PdfPCell();
            pdfCellTable.BorderWidth = 0f;
            pdfCellTable.Colspan = 5;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase("TOTAL UNITS CONFIRMED", fontBold));
            pdfCellTable.Colspan = 2;
            pdfCellTable.BorderWidthRight = 0f;
            pdfCellTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.AddCell(pdfCellTable);

            pdfCellTable = new PdfPCell(new Phrase(unitsConfirmed.ToString(), fontNormal));
            pdfCellTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCellTable.BorderWidthLeft = 0f;
            pdfTable.AddCell(pdfCellTable);

            document.Add(pdfTable);
            #endregion

            return document;
        }
    }
}
