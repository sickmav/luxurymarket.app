﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Helpers
{
    public static class CompaniesHelper
    {
        public static int? CheckCompanyAccessAndType(IEnumerable<Company> companies, int? companyId, CompanyType type)
        {
            if (companies == null || type == CompanyType.None)
                return null;
            var companiesForType = companies.Where(x => x.CompanyType == type).ToList(); //|| x.CompanyType == CompanyType.Both
            var id = companiesForType.Where(x => ((companyId ?? 0) == 0) || x.CompanyId == companyId).FirstOrDefault()?.CompanyId;
            return id > 0 ? id : null;
        }
    }
}
