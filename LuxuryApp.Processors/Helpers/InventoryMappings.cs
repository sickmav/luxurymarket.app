﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Models.SellerProductSync.Coltorti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Helpers
{
    public static class InventoryMappings
    {
        public static List<InventoryResponse> ToInventoryResponse(this Dictionary<string, List<ColtortiInventoryResponse>> coltortiInventory)
        {
            var productsInventory = new List<InventoryResponse>();
            foreach (var product in coltortiInventory)
            {
                var newProduct = new InventoryResponse();
                newProduct.VendorSKU = product.Key;
                newProduct.Sizes = new List<InventorySize>();

                var inventoryInfoWrapper = product.Value.First();
                foreach (var size in inventoryInfoWrapper.scalars)
                {
                    var sizeInfo = size.Value.First();
                    newProduct.Sizes.Add(new InventorySize()
                    {
                        SellerSize = sizeInfo.Key,
                        Quantity = sizeInfo.Value
                    });
                }

                productsInventory.Add(newProduct);
            }

            return productsInventory;
        }
    }
}
