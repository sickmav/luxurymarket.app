﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Processors.Resources.Import;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace LuxuryApp.Processors.Helpers.OfferImport
{
    public static class OfferImportHelper
    {
        public static Dictionary<string, string> GetOfferColumns()
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            var resourceSet = OfferExcelColumnNames.ResourceManager.GetResourceSet(CultureInfo.CurrentCulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                columns.Add(entry.Key.ToString().Trim().ToLower(), entry.Value?.ToString().Trim().ToLower());
            }
            return columns;
        }

        //public static OfferImportModel ToModel(this DataRow row, int index)
        //{
        //    return new OfferImportModel
        //    {
        //        Discount = row.Field<string>(OfferExcelColumnNames.Discount),
        //        Brand = row.Field<string>(OfferExcelColumnNames.Brand),
        //        Gender = row.Field<string>(OfferExcelColumnNames.Gender),
        //        OfferCost = row.Field<string>(OfferExcelColumnNames.OfferCost),
        //        MaterialCode = row.Field<string>(OfferExcelColumnNames.MaterialCode),
        //        ColorCode = row.Field<string>(OfferExcelColumnNames.ColorCode),
        //        ModelNumber = row.Field<string>(OfferExcelColumnNames.ModelNumber),
        //         TotalPrice = row.Field<string>(OfferExcelColumnNames.TotalPrice),
        //        ProductIndex = index
        //    };
        //}
    }
}
