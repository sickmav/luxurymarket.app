﻿using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LuxuryApp.Processors.Helpers.OfferImport
{
    public static class OfferFileValidator
    {
        public static string ValidateOfferRequiredColumns(DataTable dtOffer, List<string> expectedColumns)
        {
            var columnNames = (from dc in dtOffer.Columns.Cast<DataColumn>()
                               select dc.ColumnName.ToLower().Trim()).ToList();

            var missingColumns = expectedColumns.Where(x => !columnNames.Contains(x)).Select(x => x).Distinct().ToList();

            if (missingColumns.Any())
                return $"Product Offer table - Missing columns: {string.Join(";", missingColumns)}";

            return string.Empty;
        }

    }
}
