﻿using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Readers
{
    public static class InventoryReader
    {
        public static APISettings ToAPISettings(this DataReader reader)
        {
            if (reader == null) return null;

            var model = new APISettings();

            while (reader.Read())
            {
                model.BaseAddress = reader.GetString("BaseAddress");
                model.SellerID = reader.GetInt("SellerID");
                model.Username = reader.GetString("Username");
                model.Password = reader.GetString("Password");
            }

            reader.Close();

            return model;
        }

        public static List<string> ToListOfSKUs(this DataReader reader)
        {
            if (reader == null) return null;

            var list = new List<string>();

            while (reader.Read())
            {
                list.Add(reader.GetString("SellerSKU"));
            }

            reader.Close();

            return list;
        }

    }
}
