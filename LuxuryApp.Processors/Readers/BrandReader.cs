﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Readers
{
    public static class BrandReader
    {
        public static List<Brand> ToBrandList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<Brand>();

            while (reader.Read())
            {
                var item = new Brand();
                item.Id = reader.GetInt("BrandID");
                item.ParentBrandId = reader.GetIntNullable("ParentBrandID");
                item.Description = reader.GetString("Description");
                item.DisplayOrder = reader.GetIntNullable("DisplayOrder");
                item.Name = reader.GetString("Name");
                item.StandardBrandId = reader.GetIntNullable("StandardBrandID");

                items.Add(item);
            }

            reader.Close();

            return items;
        }

    }
}
