﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Readers
{
    public static class FeaturedEventReader
    {
        public static List<FeaturedEvent> ToFeaturedEventList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<FeaturedEventGetModel>();

            while (reader.Read())
            {
                var item = new FeaturedEventGetModel();
                item.FeaturedEventID = reader.GetInt("FeaturedEventID");
                item.Title = reader.GetString("Title");
                item.Subtitle = reader.GetString("Subtitle");
                item.IsHero = reader.GetBool("IsHero");
                item.DisplayOrder = reader.GetIntNullable("DisplayOrder");
                item.ImageName = reader.GetString("ImageName");
                item.ImageTitle = reader.GetString("ImageTitle");
                item.ImageSubtitle = reader.GetString("ImageSubtitle");
                item.TypeID = reader.GetInt("TypeID");
                item.FeaturedEventThemeTypeID = reader.GetInt("ThemeTypeID");
                item.HasProductsAssigned = reader.GetBool("HasProducts");

                items.Add(item);
            }

            reader.Close();

            var featuredEvents = (from fe in items
                                  group fe by fe.FeaturedEventID into grpFeaturedEvent
                                  let grpFeKey = items.First(x => x.FeaturedEventID == grpFeaturedEvent.Key)

                                  let featuredEvent = new FeaturedEvent
                                  {
                                      FeaturedEventID = grpFeKey.FeaturedEventID,
                                      Title = grpFeKey.Title,
                                      Subtitle = grpFeKey.Subtitle,
                                      IsHero = grpFeKey.IsHero,
                                      DisplayOrder = grpFeKey.DisplayOrder,
                                      HasProductsAssigned = grpFeKey.HasProductsAssigned,
                                      Images = (from fei in grpFeaturedEvent
                                                select new FeaturedEventImage
                                                {
                                                    Type = (FeaturedEventImageTypes)fei.TypeID,
                                                    ImageName = fei.ImageName,
                                                    ImageTitle = fei.ImageTitle,
                                                    ImageSubtitle = fei.ImageSubtitle,
                                                    FeaturedEventThemeType = (FeaturedEventThemeType)fei.FeaturedEventThemeTypeID
                                                }).ToList()
                                  }
                                  select featuredEvent).ToList();

            return featuredEvents;
        }
    }
}
