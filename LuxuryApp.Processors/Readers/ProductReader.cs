﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Processors.Readers
{
    public static class ProductReader
    {
        public static Product ToProductDetails(this DataReader reader)
        {
            if (reader == null) return null;

            var product = new Product();

            while (reader.Read())
            {

                product.ID = reader.GetInt("ID");
                product.Name = reader.GetString("Name");
                product.SKU = reader.GetString("SKU");
                product.SizeTypeId = reader.GetInt("SizeTypeId");
                product.Inherit = reader.GetBool("Inherit");
                product.RetailPrice = reader.GetDoubleNullable("RetailPrice");

                product.BrandName = reader.GetString("BrandName");
                product.ColorName = reader.GetString("ColorName");
                product.MaterialName = reader.GetString("MaterialName");
                product.OriginName = reader.GetString("OriginName");
                product.SeasonName = reader.GetString("SeasonName");
                product.Description = reader.GetString("Description");
                product.MainImageName = reader.GetString("MainImageName");
            }

            if (reader.NextResult())
            {
                product.Images = GetProductImages(reader);
            }

            if (reader.NextResult())
            {
                var productOffers = new List<ProductOfferModel>();
                while (reader.Read())
                {
                    productOffers.Add(new ProductOfferModel()
                    {
                        OfferID = reader.GetInt("OfferID"),
                        OfferName = reader.GetString("OfferName"),
                        TakeAll = reader.GetBool("TakeAll"),
                        RetailPrice = reader.GetDouble("RetailPrice"),
                        LMPrice = reader.GetDouble("LMPrice"),
                        LineBuy = reader.GetBool("LineBuy"),
                        TotalQuantity = reader.GetInt("TotalQuantity"),
                        Discount = reader.GetDouble("Discount"),
                        OfferProductSizeID = reader.GetInt("OfferProductSizeID"),
                        Quantity = reader.GetInt("Quantity"),
                        SizeName = reader.GetString("SizeName"),
                        ShipsFrom = reader.GetString("ShipsFrom"),
                        CompanyName = reader.GetString("CompanyName"),
                        SellerCompanyID = reader.GetInt("SellerCompanyID"),
                        ReservedQuantityByOthers = reader.GetInt("ReservedQuantityByOthers"),
                        UserCartQuantity = reader.GetInt("UserCartQuantity"),
                        ReservedTimeSeconds = reader.GetInt("ReservedTimeSeconds"),
                        IsReserved = reader.GetBool("IsReserved")
                    });
                }

                product.Offers = (from o in productOffers
                                  group o by o.OfferID into grpOffers
                                  let grpOfferKey = productOffers.First(x => x.OfferID == grpOffers.Key)

                                  let offer = new ProductOffer
                                  {
                                      OfferID = grpOfferKey.OfferID,
                                      TakeAll = grpOfferKey.TakeAll,
                                      RetailPrice = grpOfferKey.RetailPrice,
                                      LMPrice = grpOfferKey.LMPrice,
                                      LineBuy = grpOfferKey.LineBuy,
                                      TotalQuantity = grpOfferKey.TotalQuantity,
                                      Discount = grpOfferKey.Discount,
                                      ShipsFrom = grpOfferKey.ShipsFrom,
                                      CompanyName = grpOfferKey.CompanyName,
                                      SellerCompanyID = grpOfferKey.SellerCompanyID,
                                      Sizes = (from os in grpOffers
                                               select new ProductOfferSize
                                               {
                                                   OfferProductSizeID = os.OfferProductSizeID,
                                                   Quantity = os.Quantity,
                                                   SizeName = os.SizeName,
                                                   IsReserved = os.IsReserved,
                                                   OfferAvailableQuantity = (os.Quantity - os.ReservedQuantityByOthers > 0) ? os.Quantity - os.ReservedQuantityByOthers : 0,
                                                   ReservedQuantityByOthers = os.ReservedQuantityByOthers,
                                                   ReservedTimeSeconds = os.ReservedTimeSeconds,
                                                   UserCartQuantity = os.UserCartQuantity
                                               }).ToList()
                                  }
                                  select offer).OrderBy(x => x.LMPrice).ToList();
            }
            reader.Close();

            return product;
        }

        public static List<EntityImage> GetEntityImages(DataReader reader)
        {
            var images = new List<EntityImage>();
            if (reader == null) return images;
            while (reader.Read())
            {
                images.Add(new EntityImage
                {
                    Id = reader.GetInt("ID"),
                    EntityId = reader.GetInt("EntityID"),
                    EntityType = (EntityType)reader.GetInt("EntityTypeID"),
                    Name = reader.GetString("Name"),
                    //Link = reader.GetString("Link"),
                    DisplayOrder = reader.GetInt("DisplayOrder")
                });
            }

            return images;
        }


        public static List<EntityImage> GetProductImages(DataReader reader)
        {
            var images = new List<EntityImage>();
            if (reader == null) return images;
            while (reader.Read())
            {
                images.Add(new EntityImage
                {
                    Id = reader.GetInt("ImageID"),
                    Name = reader.GetString("ImageName"),
                    DisplayOrder = reader.GetInt("DisplayOrder")
                });
            }

            return images;
        }

        public static List<EntityDescription> GetEntityDescriptions(DataReader reader)
        {
            if (reader == null) return null;

            var list = new List<EntityDescription>();

            while (reader.Read())
            {
                list.Add(new EntityDescription
                {
                    Id = reader.GetInt("ID"),
                    EntityId = reader.GetInt("EntityID"),
                    EntityType = (EntityType)reader.GetInt("EntityTypeID"),
                    Description = reader.GetString("Description"),
                    Notes = reader.GetString("Notes"),
                    Dimensions = reader.GetString("Dimensions")
                });
            }

            return list;
        }

        public static List<Size> GetSizes(DataReader reader)
        {
            if (reader == null) return null;

            var list = new List<Size>();

            while (reader.Read())
            {
                list.Add(new Size
                {
                    Id = reader.GetInt("ID"),
                    Name = reader.GetString("Name"),
                    SizeType = new SizeType
                    {
                        Id = reader.GetInt("SizeTypesID"),
                        Name = reader.GetString("SizeTypesName"),
                        Description = reader.GetString("SizeTypesDescription"),
                    }
                });
            }
            return list;
        }

        public static ProductSearchResponse ToProductSearchResult(this DataReader reader)
        {
            //  var reader = new DataReader { Reader = dataReader };

            if (reader == null) return null;

            var products = new List<ProductSearchResult>();
            var filters = new List<ProductSearchFilters>();
            var totalProducts = -1;

            while (reader.Read())
            {
                products.Add(
                    new ProductSearchResult
                    {
                        Id = reader.GetInt("ID"),
                        Name = reader.GetString("ProductName"),
                        SKU = reader.GetString("Sku"),
                        MainImageName = reader.GetString("MainImageName"),
                        BrandId = reader.GetInt("BrandID"),
                        BrandName = reader.GetString("BrandName"),
                        ColorId = reader.GetInt("ColorID"),
                        StandarColorId = reader.GetInt("StandardColorID"),
                        StandardColorName = reader.GetString("StandardColorName"),
                        MaterialId = reader.GetInt("MaterialID"),
                        StandarMaterialId = reader.GetInt("StandardMaterialID"),
                        StandardMaterialName = reader.GetString("StandardMaterialName"),
                        OriginId = reader.GetInt("OriginID"),
                        OriginName = reader.GetString("OriginName"),
                        SeasonId = reader.GetInt("SeasonID"),
                        SeasonName = reader.GetString("SeasonName"),
                        LMPrice = reader.GetDouble("LMPrice"),
                        RetailPrice = reader.GetDouble("RetailPrice"),
                        AvailableQuantity = reader.GetInt("AvailableQuantity"),
                        Discount = reader.GetDouble("Discount"),
                        OfferID = reader.GetInt("OfferID"),
                        OfferProductID = reader.GetInt("OfferProductID"),
                        TakeAll = reader.GetBool("TakeAll"),
                        LineBuy = reader.GetBool("LineBuy"),
                        AvailableOffers = reader.GetInt("AvailableOffers"),
                        HasTakeAll = reader.GetBool("HasTakeAll")
                    });
            }

            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    filters.Add(new ProductSearchFilters
                    {
                        HierarchyID = reader.GetInt("HierarchyID"),
                        BrandID = reader.GetInt("BrandID"),
                        ColorID = reader.GetInt("ColorID")
                    });
                }
            }

            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    totalProducts = reader.GetInt("TotalProducts");
                }
            }

            OfferSummary offerSummary = null;
            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    offerSummary = new OfferSummary();
                    offerSummary.OfferID = reader.GetInt("OfferID");
                    offerSummary.TotalUnits = reader.GetInt("TotalUnits");
                    offerSummary.TotalProducts = reader.GetInt("TotalProducts");
                    offerSummary.TotalPrice = reader.GetDouble("TotalPrice");
                }
            }

            reader.Close();

            var brands = filters.Select(e => e.BrandID).Distinct().ToList<int>();
            var colors = filters.Select(e => e.ColorID).Distinct().ToList<int>();
            var hierarchies = filters.Select(e => e.HierarchyID).Distinct().ToList<int>();

            ProductSearchResponse response = new ProductSearchResponse()
            {
                Products = products,
                Brands = brands,
                Colors = colors,
                Hierarchies = hierarchies,
                TotalProducts = totalProducts,
                OfferSummary = offerSummary
            };


            return response;
        }


        public static IEnumerable<ProductSearchResult> ToProductListResult(this DataReader reader)
        {
            var products = new List<ProductSearchResult>();
            while (reader.Read())
            {
                products.Add(
                    new ProductSearchResult
                    {
                        Id = reader.GetInt("ID"),
                        Name = reader.GetString("ProductName"),
                        SKU = reader.GetString("Sku"),
                        MainImageName = reader.GetString("MainImageName"),
                        BrandId = reader.GetInt("BrandID"),
                        BrandName = reader.GetString("BrandName"),
                        LMPrice = reader.GetDouble("LMPrice"),
                        BusinessID = reader.GetInt("BusinessID"),
                        DivisionID = reader.GetInt("DivisionID"),
                        DepartmentID = reader.GetInt("DepartmentID")
                    });
            }

            reader.Close();

            return products;
        }

        public static IEnumerable<ProductSearchResult> ToProductSearchResultPaginated(this DataReader reader, out int totalRows)
        {
            //  var reader = new DataReader { Reader = dataReader };
            totalRows = 0;
            if (reader == null) return null;

            var products = new List<ProductSearchResult>();

            bool isTotalRowsSet = false;
            while (reader.Read())
            {
                products.Add(
                    new ProductSearchResult
                    {
                        Id = reader.GetInt("ID"),
                        Name = reader.GetString("ProductName"),
                        SKU = reader.GetString("Sku"),
                        MainImageName = reader.GetString("MainImageName"),
                        BrandId = reader.GetInt("BrandID"),
                        BrandName = reader.GetString("BrandName"),
                        LMPrice = reader.GetDouble("LMPrice")
                    });

                if (isTotalRowsSet) continue;
                totalRows = reader.GetInt("TotalRows");
                isTotalRowsSet = true;
            }

            reader.Close();

            return products;
        }

        public static List<State> ToStateModel(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<State>();

            while (reader.Read())
            {
                var item = new State();
                item.StateId = reader.GetInt("StateID");
                item.StateName = reader.GetString("StateName");
                item.CountryId = reader.GetInt("CountryID");

                items.Add(item);
            }

            reader.Close();

            return items;
        }

    }
}
