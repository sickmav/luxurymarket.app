﻿using Gs1.Vics.Contracts.Models.Common;
using Gs1.Vics.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace Gs1.Vics.Contracts.Factories
{
    public interface IEdiSerializeServiceFactory
    {
        IEdiSerializeService<T> GetSerializeService<T>() where T : IEdiDocument;
        ApiResult<byte[]> Serialize<T>(T document) where T: IEdiDocument;
    }
}
