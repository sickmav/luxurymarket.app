﻿using Gs1.Vics.Contracts.Models.Common;
using System;

namespace Gs1.Vics.Contracts.Models.Booking
{
    public class BookingItemLevel
    {
        public int LineItemNumber { get; set; }
        public int LineParentItemNumber { get; set; }
        public string VendorStyleNumber { get; set; }

        public int PageNumber { get; set; }
        public int PageLineItemNumber { get; set; }
        public string ColorDescription { get; set; }
        public string SizeDescription { get; set; }
        public int Quantity { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}