﻿using Gs1.Vics.Contracts.Models.Common;

namespace Gs1.Vics.Contracts.Models.Booking
{
    public class OrderBooking : IEdiDocument
    {
        public OrderBookingHeader OrderHeader { get; set; }
        public BookingShipmentLevel[] ShipmentDetails { get; set; }
       }
}