﻿namespace Gs1.Vics.Contracts.Models.Common
{
    public interface IStTransactionSetHeader
    {
        string TransactionType { get; }
        string TransactionSetControlNumber { get; set; }
    }
}