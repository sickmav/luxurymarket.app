﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gs1.Vics.Contracts.Models.Common
{
    public enum LocationType
    {
        Origin,
        PoolPoint,
        PortOfLoading
    }
}
