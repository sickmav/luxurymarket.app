﻿using Gs1.Vics.Contracts.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gs1.Vics.Contracts.Models.Common
{
    public static class Extensions
    {
        public static string Text(this TestIndicator value)
        {
            switch (value)
            {
                case (TestIndicator.Production):
                    return "P";
                //case (TestIndicator.Test):
                default:
                    return "T";

            }
        }

        public static string Text(this TermsOfSale value)
        {
            switch (value)
            {
                case TermsOfSale.BasicTerm:
                    return "01";
                case TermsOfSale.DiscountNotApplicable:
                    return "05";
                case TermsOfSale.BasicDiscoutOffered:
                    return "08";
                case TermsOfSale.TenDaysEom:
                    return "12";
                default:
                    return "";
            }
        }

        public static string Text(this TransactionSetPurposeCode value)
        {
            switch (value)
            {
                case TransactionSetPurposeCode.Original:
                    return "00";
                case TransactionSetPurposeCode.Delete:
                    return "03";
                case TransactionSetPurposeCode.Change:
                    return "04";
                default:
                    return "";
            }
        }

        public static string Text(this LocationType value)
        {
            switch (value)
            {
                case LocationType.Origin:
                    return "OR";
                case LocationType.PoolPoint:
                    return "PP";
                case LocationType.PortOfLoading:
                    return "kl";
                default:
                    return "";
            }
        }


        public static string Text(this TransportationMethod value)
        {
            switch (value)
            {
                case TransportationMethod.Air:
                    return "00";
                case TransportationMethod.Motor:
                    return "03";
                case TransportationMethod.Ocean:
                    return "04";
                default:
                    return "";
            }
        }
    }
}
