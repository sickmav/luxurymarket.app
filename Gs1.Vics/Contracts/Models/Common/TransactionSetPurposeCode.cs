﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gs1.Vics.Contracts.Models.Common
{
    public enum TransactionSetPurposeCode 
    {
        Original,
        Delete,
        Change
    }
}
