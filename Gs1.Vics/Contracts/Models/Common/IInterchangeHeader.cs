﻿using System;
using Gs1.Vics.Contracts.Models.Order;

namespace Gs1.Vics.Contracts.Models.Common
{
    public interface IInterchangeHeader
    {
        string InterchangeSenderId { get; set; }
        string InterchangeReceiverId { get; set; }
        DateTimeOffset InterchangeDateTime { get; set; }
        string InterchangeControlVersionNumber { get; set; }
        int InterchangeControlNumber { get; set; }
        TestIndicator TestIndicator { get; set; }
    }
}