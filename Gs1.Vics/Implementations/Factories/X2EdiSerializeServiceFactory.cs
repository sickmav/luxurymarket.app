﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Contracts.Models.Common;
using Gs1.Vics.Contracts.Models.Order;
using Gs1.Vics.Contracts.Services;
using Gs1.Vics.Implementations.Services;
using LuxuryApp.Core.Infrastructure.Api.Models;
using Gs1.Vics.Contracts.Models.Booking;

namespace Gs1.Vics.Implementations.Factories
{
    public class X2EdiSerializeServiceFactory: IEdiSerializeServiceFactory
    {
        private readonly Dictionary<Type, object> _instances;

        public X2EdiSerializeServiceFactory()
        {
            _instances = new Dictionary<Type, object>()
            {
                { typeof(Order), new X2EdiOrderSerializeService()},
                { typeof(OrderBooking), new X2EdiBookingOrderSerializeService()},
            };
        }

        public IEdiSerializeService<T> GetSerializeService<T>() where T : IEdiDocument
        {
            return _instances[typeof(T)] as IEdiSerializeService<T>;
        }

        public ApiResult<byte[]> Serialize<T>(T document) where T : IEdiDocument
        {
            return GetSerializeService<T>().Serialize(document);
        }
    }
}
