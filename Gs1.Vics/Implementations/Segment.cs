﻿using Gs1.Vics.Contracts.Models.Common;
using Gs1.Vics.Contracts.Models.Order;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gs1.Vics.Implementations
{
    public class Segment
    {
        public string Name { get; }
        public int SegmentLength { get; }
        public List<SegmentData> SegmentDatas { get; set; }

        public SegmentedDocument Document { get; }

        public Segment(string name, int segmentLength, SegmentedDocument segmentedDocument)
        {
            Name = name;
            SegmentLength = segmentLength;
            SegmentDatas = new List<SegmentData>(segmentLength);
            Document = segmentedDocument;
        }

        public Segment(string name, int segmentLength)
        {
            Name = name;
            SegmentLength = segmentLength;
            SegmentDatas = new List<SegmentData>(segmentLength);
            Document = null;
        }

        public void ReplaceSegmentDatas(List<SegmentData> segmentDatas)
        {
            SegmentDatas = segmentDatas;
        }

        public void Serialize(StreamWriter writer)
        {
            writer.WriteLine(String.Join("*", Name, String.Join("*", SegmentDatas.Select(x => x.Value))));
        }

        public Segment Add(int position, string value)
        {
            SegmentDatas.Add(new SegmentData(position, value));
            return this;
        }

        public Segment ElementSeparator(int position)
        {
            SegmentDatas.Add(new SegmentData(position, ">"));
            return this;
        }

        public Segment EndSegment()
        {
            var sorted = SegmentDatas.OrderBy(x => x.Position).ToList();
            //
            var final = new List<SegmentData>();

            for (int idxFinal = 0, idxUnsorted = 0;
                idxFinal < SegmentLength;)
            {
                if (idxUnsorted < sorted.Count)
                {
                    var sortedElement = sorted[idxUnsorted];

                    if (idxFinal + 1 == sortedElement.Position)
                    {
                        final.Add(sortedElement);
                        idxUnsorted++;
                        idxFinal++;
                    }

                    while (idxFinal + 1 < sortedElement.Position && idxFinal < SegmentLength)
                    {
                        final.Add(EmptySegment(idxFinal + 1));
                        idxFinal++;
                    }
                }
                else
                {
                    break;
                }
            }
            //while (final.Count < SegmentLength)
            //{
            //    final.Add(EmptySegment(final.Count));
            //}
            ReplaceSegmentDatas(final);
            Document?.AddSegment(this);
            return this;
        }

        private static SegmentData EmptySegment(int position) => new SegmentData(position, "");
    }

    public class SegmentData
    {
        public int Position { get; }
        public string Value { get; }

        public SegmentData(int position, string value)
        {
            Position = position;
            Value = value;
        }
    }

    public static class SerializationExtensions
    {
        public static Segment BeginSegment(this SegmentedDocument streamWriter, string name, int length)
        {
            return new Segment(name, length, streamWriter);
        }
    }

    public class MessageSummary
    {
        public int CttNumberOfPo1SegmentsIncluded { get; set; }
        public int SeNumberOfSegmentsIncluded { get; set; }
        public int GeNumberOfTransactionsIncluded { get; set; }
        public string GeGroupControlNumber { get; set; }
        public int IeaNumberOfIncludedFunctionalGroups { get; set; }
        public string IeaInterchangeControlNumber { get; set; }
        public string SeTransactionSetControlNumber { get; set; }
    }

    public class SegmentedDocument
    {
        public List<Segment> Segments = new List<Segment>();

        public void AddSegment(Segment segment)
        {
            Segments.Add(segment);
        }

        public void Serialize(StreamWriter writer)
        {
            foreach (var segment in Segments)
            {
                segment.Serialize(writer);
            }
            var messageSummary = GetMessageSummary();
            var summarySegments = GetSummarySegments(messageSummary);
            foreach (var summarySegment in summarySegments)
            {
                summarySegment.Serialize(writer);
            }
        }

        public virtual Segment[] GetSummarySegments(MessageSummary messageSummary)
        {
            return new[]
            {
                new Segment("CTT", 1)
                    .Add(1, messageSummary.CttNumberOfPo1SegmentsIncluded.ToString())
                    .EndSegment(),
                new Segment("SE", 2)
                    .Add(1, messageSummary.SeNumberOfSegmentsIncluded.ToString())
                    .Add(2, messageSummary.SeTransactionSetControlNumber)
                    .EndSegment(),
                new Segment("GE", 2)
                    .Add(1, messageSummary.GeNumberOfTransactionsIncluded.ToString())
                    .Add(2, messageSummary.GeGroupControlNumber)
                    .EndSegment(),
                new Segment("IEA", 2)
                    .Add(1, messageSummary.IeaNumberOfIncludedFunctionalGroups.ToString())
                    .Add(2, messageSummary.IeaInterchangeControlNumber)
                    .EndSegment(),
            };
        }

        public virtual MessageSummary GetMessageSummary()
        {
            var messageSummary = new MessageSummary();
            messageSummary.CttNumberOfPo1SegmentsIncluded = Segments.Count(x => x.Name == "PO1");
            messageSummary.SeNumberOfSegmentsIncluded = Segments.Count;
            messageSummary.SeTransactionSetControlNumber =
                Segments
                .Where(x => x.Name == "ST")
                .SelectMany(y => y.SegmentDatas)
                .SingleOrDefault(z => z.Position == 2)?.Value ?? "";
            messageSummary.GeNumberOfTransactionsIncluded =
                Segments.Count(x => x.Name == "GS");
            messageSummary.GeGroupControlNumber =
                Segments.Where(x => x.Name == "GS")
                    .SelectMany(y => y.SegmentDatas)
                    .SingleOrDefault(z => z.Position == 6)?.Value ?? "";
            messageSummary.IeaNumberOfIncludedFunctionalGroups =
                Segments.Count(x => x.Name == "ISA");
            messageSummary.IeaInterchangeControlNumber =
                Segments.Where(x => x.Name == "ISA")
                    .SelectMany(y => y.SegmentDatas)
                    .SingleOrDefault(z => z.Position == 13)
                    ?.Value ?? "";
            return messageSummary;
        }
    }

}
