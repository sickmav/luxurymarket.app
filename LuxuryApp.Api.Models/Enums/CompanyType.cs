﻿
namespace LuxuryApp.Api.Models.Enums
{
    public enum CompanyType
    {
        None=0,

        Seller=1,

        Buyer=2,

        Both=3
    }
}
