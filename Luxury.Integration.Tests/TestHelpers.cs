﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using LuxuryApp.Auth.Api.Models;
using LuxuryApp.Auth.DataAccess;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Cart;
using Microsoft.AspNet.Identity;
using LuxuryApp.Auth.DataAccess.Repositories;

namespace Luxury.Integration.Tests
{
    public static class TestHelpers
    {
        public static void RegisterUserStore(ContainerBuilder builder)
        {
            var conString = new AuthConnectionStringProvider();
            builder.Register(
                    c => new DapperIdentityDbContext<ApplicationUser, ApplicationRole>(conString)).AsSelf();

            builder.Register(
                    c => new UserStore<ApplicationUser, ApplicationRole>(c.Resolve<DapperIdentityDbContext<ApplicationUser, ApplicationRole>>()))
                .As<IUserStore<ApplicationUser, int>>();
        }

        public static Cart ComputedTotalsCart(Cart cart)
        {
            foreach (var cartItem in cart.Items)
            {
                foreach (var cartItemSellerUnit in cartItem.SellerUnits)
                {
                    cartItemSellerUnit.UnitsCount = cartItemSellerUnit.SellerUnitSizes.Sum(x => x.Quantity);
                    cartItemSellerUnit.TotalCustomerCost = cartItemSellerUnit.UnitsCount*Math.Ceiling(cartItemSellerUnit.CustomerUnitPrice);
                    cartItemSellerUnit.TotalShippingCost =
                        Math.Ceiling(cartItemSellerUnit.UnitsCount*
                                     Math.Ceiling(cartItemSellerUnit.CustomerUnitPrice*
                                                  cartItemSellerUnit.UnitShippingTaxPercentage/100));
                }
                cartItem.AverageUnitCost = cartItem.SellerUnits.Where(x => x.Active).Average(x => x.CustomerUnitPrice);
                cartItem.AverageUnitDiscountPercentage = cartItem.SellerUnits.Where(x => x.Active).Average(x => x.DiscountPercentage);
                cartItem.TotalCustomerCost = cartItem.SellerUnits.Where(x => x.Active).Sum(x => x.TotalCustomerCost);
                cartItem.TotalShippingCost = cartItem.SellerUnits.Where(x => x.Active).Sum(x => x.TotalShippingCost);
                cartItem.TotalLandedCost = cartItem.TotalCustomerCost + cartItem.TotalShippingCost;
            }
            cart.ItemsCount = cart.Items.Count(x => x.SellerUnits.Any(y => y.Active));
            cart.UnitsCount = cart.Items.SelectMany(x => x.SellerUnits).Where(y => y.Active).Sum(y => y.UnitsCount);
            cart.TotalCustomerCost = cart.Items.Sum(x => x.TotalCustomerCost);
            if (cart.Currency == null)
            {
                cart.Currency = GetDefaultCurrency();
            }
            return cart;
        }

        public static Currency GetDefaultCurrency()
        {
            return new Currency()
            {
                CurrencyId = 2,
                Name = "USD",
                Rate = (decimal) 1.0,
            };
        }
    }
}
