﻿using LuxuryApp.Contracts.Models.Cart;

namespace Luxury.Integration.Tests
{
    public class RemoveItemSellerUnitScenario
    {
        public SellerOffer[] SellerOffers { get; set; }

        public RemoveProductData RemoveProduct { get; set; }

        public Cart ExpectedCart { get; set; }
        public ExpectedRemovedData ExpectedRemoved { get; set; }

        public class RemoveProductData
        {
            public string SellerCompanyName { get; set; }
            public string ProductSku { get; set; }
        }

        public class ExpectedRemovedData
        {
            public string[] RemovedProductsSkus { get; set; }
            public string[] AffectedItemsProductSkus { get; set; }

            public ExpectedRemovedData()
            {
                RemovedProductsSkus = new string[0];
                AffectedItemsProductSkus = new string[0];
            }
        }
    }
}