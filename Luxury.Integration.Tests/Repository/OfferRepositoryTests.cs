﻿//using System;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using FluentAssertions;
//using LuxuryApp.Auth.Api.Models;
//using LuxuryApp.Auth.DataAccess;
//using LuxuryApp.Contracts.Enums;
//using LuxuryApp.Contracts.Interfaces;
//using LuxuryApp.Contracts.Models;
//using LuxuryApp.Contracts.Models.Offer;
//using LuxuryApp.Contracts.Repository;
//using LuxuryApp.Contracts.Services;
//using LuxuryApp.Core.DataAccess;
//using LuxuryApp.Core.DataAccess.Repository;
//using LuxuryApp.Processors.Services;
//using NUnit.Framework.Internal;
//using NUnit.Framework;
//using LuxuryApp.Auth.DataAccess.Repositories;
//using LuxuryApp.Core.Infrastructure.Authorization;

//namespace Luxury.Integration.Tests.Repository
//{
//    [TestFixture]
//    public class OfferRepositoryTests
//    {
//        protected IOfferRepository OfferRepository { get; set; }
//        protected IProductService ProductService { get; set; }
//        protected IContextProvider _contextProvider;

//        protected string SellerCompanyName = "Polka Inferno";
//        protected string SellerUserName = "UlyssesPlulpigger@mejix.com";

//        protected int SellerCompanyId { get; set; }
//        protected int SellerUserId { get; set; }

//        [SetUp]
//        public void Setup()
//        {
//            var connectionStringProvider = new LuxuryMarketConnectionStringProvider();
//            var authConnectionString = new AuthConnectionStringProvider();

//            ProductService = new ProductService(_contextProvider);

//            OfferRepository = new OfferRepository(connectionStringProvider);

//            var userStore =
//                new UserStore<ApplicationUser, ApplicationRole>(new DapperIdentityDbContext<ApplicationUser, ApplicationRole>(authConnectionString));
//            var companyRepository = new CompanyRepository(connectionStringProvider);

//            int temp;
//            SellerCompanyId = companyRepository.SearchCompaniesByName(SellerCompanyName, out temp).Single().CompanyId;
//            SellerUserId = userStore.FindByNameAsync(SellerUserName).Result.Id;
//        }

//        [Test]
//        public void GetOffer_AfterSaveOffer_ShouldReturnExpected()
//        {
//            // Arrange 

//            var currency = new Currency
//            {
//                CurrencyId = 2,
//                Name = "USD",
//                Rate = 1,
//            };
//            var product = ProductService.GetProductBySku("1BB010ASKF0MW4");
//            var expected =
//                new Offer
//                {
//                    OfferNumber = 93039020,
//                    OfferName = "Test get save offer",
//                    EndDate = null,
//                    StartDate = DateTime.Now,
//                    OfferStatus = OfferStatus.Canceled,
//                    TakeAll = true,
//                    Currency = currency,
//                    SellerCompanyId = SellerCompanyId,
//                    OfferProducts = new OfferProduct[]
//                    {
//                        new OfferProduct
//                        {
//                            ProductId = product.ID,
//                            LineBuy = true,
//                            OfferCost = 2000,
//                            Currency = currency,
//                            Discount = 21,
//                            CustomerUnitPrice = (decimal)(2000 * Constants.CompanyFee),
//                            OfferProductSizes = new OfferProductSize[]
//                            {
//                                new OfferProductSize
//                                {
//                                    Quantity = 12,
//                                    Size = new Size {Name = "44", SizeType = new SizeType {Name = "T2"}},
//                                },
//                                new OfferProductSize
//                                {
//                                    Quantity = 13,
//                                    Size = new Size {Name = "48", SizeType = new SizeType {Name = "T2"}},
//                                },
//                                new OfferProductSize
//                                {
//                                    Quantity = 20,
//                                    Size = new Size {Name = "50", SizeType = new SizeType {Name = "T2"}},
//                                }
//                            }
//                        }
//                    }
//                };

//            int offerId = 0;

//            try
//            {

//                // Act
//                offerId = OfferRepository.SaveOffer(expected, SellerUserId);
//                var actual = OfferRepository.GetOfferById(offerId);
//                // Assert

//                actual.ShouldBeEquivalentTo(expected);
//            }
//            finally
//            {
//                OfferRepository.DeleteOffer(offerId);
//            }
//        }
//    }
//}

