﻿using LuxuryApp.Contracts.Models.Offer;

namespace Luxury.Integration.Tests
{
    public class SellerOffer
    {
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public Offer Offer { get; set; }
    }
}