﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using FluentAssertions;
//using LuxuryApp.Contracts.Models.Cart;
//using NUnit.Framework;

//namespace Luxury.Integration.Tests
//{
//    [TestFixture]
//    public class TestCartServiceAddScenarios:
//        TestCartService
//    {
//        [Test]
//        public void GetCartSummary_WhenProductOfferHasNotSetLineBuy_ShouldReturnExpected()
//        {
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                var expectedSummary = GetExpectedCartSummaryWhenProductOfferHasNotSetLineBuy(buyerCompanyId, true);
//                cartId = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 2)).Data.CartSummaryItem.CartId;
//                // Act

//                var result = cartService.GetCartSummaryForBuyer(buyerCompanyId);

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                var actualSummary = result.Data;
//                actualSummary.ShouldBeEquivalentTo(expectedSummary);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void AddOfferProductToCart_WhenProductOfferHasNotSetLineBuy_ShouldAddOnlyTheProductWithSizeSelectedToCart()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductNotSetLineBuy();
//                var offerProductSize = t.Item1;
//                var expectedCart = GetExpectedCartWhenProductOfferHasNotSetLineBuy(buyerCompanyId, true);

//                // Act

//                var result = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 2));
//                cartId = result.Data.CartSummaryItem.CartId;

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                var actualCart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                actualCart.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void AddOfferProductToCart_WhenProductOfferHasSetLineBuy_ShouldAddAllProductsWithAllSizesToCart()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferNoTakeAllWithProductSetLineBuy();
//                var offerProductSize = t.Item1;
//                var expectedCart = GetExpectedCartWhenProductOfferHasSetLineBuy(buyerCompanyId, true);

//                // Act

//                var result = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1));
//                cartId = result.Data.CartSummaryItem.CartId;

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                var actualCart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                actualCart.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void AddOfferProductToCart_WhenOfferHasSetTakeAll_ShouldAddAllOfferProductsWithAllSizesToCart()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var t = GetOfferTakeAll();
//                var offerProductSize = t.OfferProducts.First().OfferProductSizes.First();
//                var expectedCart = GetExpectedCartWhenOfferHasTakeAll(buyerCompanyId, true);

//                // Act

//                var result = cartService.AddOfferProductSizeToCart(new SaveOfferProductSizesToCartModel(buyerCompanyId, offerProductSize.Id, 1));
//                cartId = result.Data.CartSummaryItem.CartId;

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                var actualCart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                actualCart.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }

//        [Test]
//        public void AddEntireOfferToCart_ShouldAddAllProductsToCart()
//        {
//            // Arrange
//            var cartService = GetCartService();
//            var buyerCompanyId = GetBuyerCompanyId();
//            int cartId = 0;
//            try
//            {
//                var offer = GetOfferTakeAll();
//                var expectedCart = GetExpectedCartWhenOfferHasTakeAll(buyerCompanyId, true);

//                // Act

//                var result = cartService.AddEntireOfferToCart(new AddEntireOfferToCartModel(buyerCompanyId, offer.Id));
//                cartId = result.Data.CartSummaryItem.CartId;

//                // Assert
//                result.IsSuccesfull.Should().BeTrue();
//                var actualCart = cartService.GetCartForBuyer(buyerCompanyId).Data;
//                actualCart.ShouldBeEquivalentTo(expectedCart);
//            }
//            finally
//            {
//                CleanCart(cartId);
//            }
//        }
//    }
//}
