﻿declare @SellerCompanyName nvarchar(100);
declare @SellerUserName nvarchar(100);

select @SellerCompanyName = 'Polka Inferno';
select @SellerUserName = 'UlyssesPlulpigger@mejix.com';

declare @SellerCompanyId int;
declare @OfferIds table (iterId int not null identity(1, 1) primary key, offerId int);
declare @StartDate datetime;
declare @SellerUserId int;

select @SellerCompanyId = CompanyID from
	dbo.Companies where Name = @SellerCompanyName;

select @SellerUserId = Id
	from app.Users where username = @SellerUserName;

select @StartDate = GETDATE();

begin tran;

	
	begin

INSERT INTO [dbo].[Offers]
           ([OfferStatusID]
           ,[OfferNumber]
           ,[OfferName]
           ,[TakeAll]
           ,[StartDate]
           ,[EndDate]
           ,[CurrencyID]
           ,[SellerID]
           ,[CompanyID]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Active]
           ,[Deleted])
	output inserted.OfferId into @OfferIds
     VALUES
           (3
           ,99990093
           ,'Offer Not Take All'
           ,0
           ,@StartDate
           ,null
           ,2
           ,@SellerUserId
           ,@SellerCompanyId
           ,1
           ,@StartDate
           ,null
           ,null
           ,1
           ,0),
		   (3
           ,99990094
           ,'Offer Take All'
           ,1
           ,@StartDate
           ,null
           ,2
           ,@SellerUserId
           ,@SellerCompanyId
           ,1
           ,@StartDate
           ,null
           ,null
           ,1
           ,0);
		select * from @OfferIds

		declare @iterOffer int;
		declare @iterLowerBoundOffer int;
		declare @iterUpperBoundOffer int;

		select @iterLowerBoundOffer=min(iterId),
			@iterUpperBoundOffer = max(iterId) from @OfferIds;
		select @iterOffer = @iterLowerBoundOffer;
		while @iterOffer <= @iterUpperBoundOffer 
			begin;

			declare @OfferProductIds table
			(iterId int not null identity(1, 1) primary key,
			 offerProductId int);
			
			delete from @OfferProductIds; -- don't forget declare T-SQL is global not in the scope of begin/end block
		
			with op as 
			(
				select 
					   [ProductID] = 1407
					   ,[Discount] = 50.00
					   ,[TotalQuantity] = 70
					   ,[CurrencyID] = 2
					   ,[RetailPrice] = 4000.00
					   ,[WholesSaleCost] = 2000.00
					   ,[LineBuy] = 0
					   ,[CreatedBy] = 1
					   ,[CreatedDate] = @StartDate
					   ,[ModifiedBy] = null
					   ,[ModifiedDate] = null
					   ,[Active] = 1
					   ,[Deleted] = 0
				 union all
				 select 
					   [ProductID] = 1407
					   ,[Discount] = 40.00
					   ,[TotalQuantity] = 50
					   ,[CurrencyID] = 2
					   ,[RetailPrice] = 4000.00
					   ,[WholesSaleCost] = 2000.00
					   ,[LineBuy] = 1
					   ,[CreatedBy] = 1
					   ,[CreatedDate] = @StartDate
					   ,[ModifiedBy] = null
					   ,[ModifiedDate] = null
					   ,[Active] = 1
					   ,[Deleted] = 0
			)
			INSERT INTO [dbo].[OfferProducts]
					   ([OfferID]
					   ,[ProductID]
					   ,[Discount]
					   ,[TotalQuantity]
					   ,[CurrencyID]
					   ,[RetailPrice]
					   ,[WholesSaleCost]
					   ,[LineBuy]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[Active]
					   ,[Deleted])
					output inserted.OfferProductId into @OfferProductIds
				select 
					o.[OfferID]
					   ,op.[ProductID]
					   ,op.[Discount]
					   ,op.[TotalQuantity]
					   ,op.[CurrencyID]
					   ,op.[RetailPrice]
					   ,op.[WholesSaleCost]
					   ,op.[LineBuy]
					   ,op.[CreatedBy]
					   ,op.[CreatedDate]
					   ,op.[ModifiedBy]
					   ,op.[ModifiedDate]
					   ,op.[Active]
					   ,op.[Deleted]
					from @OfferIds o 
						cross join op
						where 
							o.IterId = @iterOffer;
				select * from @OfferProductIds;

				begin
					declare @iterOfferProduct int;
					declare @iterOfferProductLowerBound int;
					declare @iterOfferProductUpperBound int;

					select @iterOfferProductLowerBound = min(iterId),
							@iterOfferProductUpperBound = max(iterId)
							from @OfferProductIds;
					select @iterOfferProduct = @iterOfferProductLowerBound;
					while @iterOfferProduct <= @iterOfferProductUpperBound
						begin
							;with ops as 
							(
								select 
										opi.[OfferProductID]
										,[SizeID] = 1
										,[Quantity] = 10
										,[CreatedBy] = 1
										,[CreatedDate] = @StartDate
										,[ModifiedBy] = null
										,[ModifiedDate] = null
										,[Active] = 1
										,[Deleted] = 0
										from @OfferProductIds opi 
										where opi.iterid = @iterOfferProduct
								 union all 
								 select 
										opi.[OfferProductID]
										,[SizeID] = 2
										,[Quantity] = 20
										,[CreatedBy] = 1
										,[CreatedDate] = @StartDate
										,[ModifiedBy] = null
										,[ModifiedDate] = null
										,[Active] = 1
										,[Deleted] = 0
										from @OfferProductIds opi 
										where opi.iterid = @iterOfferProduct
								union all
								select 
										opi.[OfferProductID]
										,[SizeID] = 3
										,[Quantity] = 25
										,[CreatedBy] = 1
										,[CreatedDate] = @StartDate
										,[ModifiedBy] = null
										,[ModifiedDate] = null
										,[Active] = 1
										,[Deleted] = 0
										from @OfferProductIds opi 
										where opi.iterid = @iterOfferProduct
							)
							INSERT INTO [dbo].[OfferProductSizes]
										([OfferProductID]
										,[SizeID]
										,[Quantity]
										,[CreatedBy]
										,[CreatedDate]
										,[ModifiedBy]
										,[ModifiedDate]
										,[Active]
										,[Deleted])
									select 
										ops.[OfferProductID]
										,ops.[SizeID]
										,ops.[Quantity]
										,ops.[CreatedBy]
										,ops.[CreatedDate]
										,ops.[ModifiedBy]
										,ops.[ModifiedDate]
										,ops.[Active]
										,ops.[Deleted]
										from ops

							select @iterOfferProduct = @iterOfferProduct + 1;
						end
				end;

		   select @iterOffer = @iterOffer + 1;

			end; -- while

		end;


		select * from dbo.Offers;
		select * from dbo.OfferProducts;
		select * from dbo.OfferProductSizes;

rollback tran


