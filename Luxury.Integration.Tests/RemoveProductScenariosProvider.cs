﻿//using System;
//using System.Collections;
//using System.Linq;
//using LuxuryApp.Contracts.Enums;
//using LuxuryApp.Contracts.Models;
//using LuxuryApp.Contracts.Models.Cart;
//using LuxuryApp.Contracts.Models.Offer;
//using LuxuryApp.Contracts.Services;
//using LuxuryApp.Processors.Services;
//using NUnit.Framework;
//using LuxuryApp.Core.Infrastructure.Authorization;

//namespace Luxury.Integration.Tests
//{
//    public class RemoveProductScenariosProvider
//    {
//        private static IContextProvider _contextProvider;

//        public static Cart GetEmptyCart(Currency currency)
//        {
//            return new Cart() {Currency = currency};
//        }

//        public static CartSummary GetEmptyCartSummary(Currency currency)
//        {
//            return new CartSummary() { Currency = currency };
//        }

//        public static Currency DefaultCurrency =>
//            new Currency
//            {
//                CurrencyId = 2,
//                Name = "USD",
//                Rate = 1,
//            };

//        public static SellerOffer[] GetSampleSellerOffers()
//        {
//            var productService = GetProductService();

//            var currency = DefaultCurrency;
//            const string productNonApparelSku = "414581DHY0N1000";
//            const string productApparelSku = "12345DHY0N1000";
//            var productNonApparel = productService.GetProductBySku(productNonApparelSku);
//            ValidateThatProductExists(productNonApparel, productNonApparelSku);
//            var productApparel = productService.GetProductBySku(productApparelSku);
//            ValidateThatProductExists(productApparel, productApparelSku);
//            return
//                GetRemoveProductScenario_WhenProductIsFromTwoSellersBothWithTakeOffer(productApparel, productNonApparel,
//                    currency).SellerOffers;
//        }

//        private static void ValidateThatProductExists(Product product, string sku)
//        {
//            if (product?.ID == 0)
//            {
//                throw new ApplicationException($"Failed to retrieve product with SKU: {sku}");
//            }
//        }
//        #region Remove products
//        public static IEnumerable GetRemoveProductScenarions()
//        {
//            var productService = GetProductService();

//            var currency = DefaultCurrency;
//            var productNonApparel = productService.GetProductBySku("414581DHY0N1000");
//            var productApparel = productService.GetProductBySku("12345DHY0N1000");

//            yield return
//                new TestCaseData(GetRemoveProductScenario_WhenProductIsFromOneSeller(productApparel, productNonApparel, currency))
//                    .SetName("Remove product scenario when product is from one seller")
//                ;
//            yield return
//                new TestCaseData(GetRemoveProductScenario_WhenProductIsFromTwoSellers(productApparel, productNonApparel, currency))
//                    .SetName("Remove product scenario when product is from two sellers")
//                ;
//            yield return
//                new TestCaseData(GetRemoveProductScenario_WhenProductIsFromOneSellerTakeOffer(productApparel, productNonApparel, currency))
//                    .SetName(
//                        "Remove product scenario when product is from one seller, take offer");
//            yield return
//                new TestCaseData(GetRemoveProductScenario_WhenProductIsFromTwoSellersOneWithTakeOfferOneWithouLineBuy(productApparel, productNonApparel, currency))
//                    .SetName(
//                        "Remove product scenario when product is from two sellers, one with take offer, other without line buy");
//            yield return
//                new TestCaseData(GetRemoveProductScenario_WhenProductIsFromTwoSellersBothWithTakeOffer(productApparel, productNonApparel, currency))
//                    .SetName(
//                        "Remove product scenario when product is from two sellers, both with take offer");
//        }

//        private static IProductService GetProductService()
//        {
//            return new ProductService(_contextProvider);
//        }

//        private static RemoveProductScenario
//            GetRemoveProductScenario_WhenProductIsFromTwoSellersOneWithTakeOfferOneWithouLineBuy(
//                Product productApparel,
//                Product productNonApparel,
//                Currency currency)
//        {
//            return new RemoveProductScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                    new SellerOffer

//                    {
//                        CompanyName = "Bernadina Enrico",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Bernadina Enrico]",
//                            OfferNumber = 90823989,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = false,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 40,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 5,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 70,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 8,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        }
//                    }

//                },
//                RemoveProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    }
//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "OCI16",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 8,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (70*Constants.CompanyFee)),
//                                    DiscountPercentage = 45,
//                                    //TotalCustomerCost = (decimal) (70*Constants.CompanyFee)*2,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 8,
//                                            AvailableQuantity = 8,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        }
//                    }
//                }),
//                ExpectedRemovedProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    },
//                },
//            };
//        }

//        private static RemoveProductScenario
//            GetRemoveProductScenario_WhenProductIsFromTwoSellersBothWithTakeOffer(
//                Product productApparel,
//                Product productNonApparel,
//                Currency currency)
//        {
//            return new RemoveProductScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                    new SellerOffer

//                    {
//                        CompanyName = "Bernadina Enrico",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Bernadina Enrico]",
//                            OfferNumber = 90823989,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 40,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 5,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 70,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 8,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        }
//                    }

//                },
//                RemoveProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    }
//                },
//                ExpectedCart = RemoveProductScenariosProvider.GetEmptyCart(currency),
//                ExpectedRemovedProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    },
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productApparel.SKU,
//                    }
//                },
//            };

//        }

//        private static
//            RemoveProductScenario GetRemoveProductScenario_WhenProductIsFromOneSellerTakeOffer(
//                Product productApparel,
//                Product productNonApparel,
//                Currency currency)
//        {
//            return new RemoveProductScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                },
//                RemoveProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    }
//                },
//                ExpectedCart = RemoveProductScenariosProvider.GetEmptyCart(currency),
//                ExpectedRemovedProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    },
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productApparel.SKU,
//                    }
//                },
//            };
//        }

//        private static
//            RemoveProductScenario GetRemoveProductScenario_WhenProductIsFromTwoSellers(
//                Product productApparel,
//                Product productNonApparel,
//                Currency currency)
//        {
//            return new RemoveProductScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                    new SellerOffer

//                    {
//                        CompanyName = "Bernadina Enrico",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Bernadina Enrico]",
//                            OfferNumber = 90823989,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 40,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 5,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 70,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 8,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        }
//                    }

//                },
//                RemoveProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    }
//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {

//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "OCI16",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 8,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (70*Constants.CompanyFee)),
//                                    DiscountPercentage = 49,
//                                    //TotalCustomerCost = (decimal) (70*Constants.CompanyFee)*8,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 8,
//                                            AvailableQuantity = 8,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "ONR13",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 4,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (65*Constants.CompanyFee)),
//                                    DiscountPercentage = 45,
//                                    //TotalCustomerCost = (decimal) (65*Constants.CompanyFee)*4,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        }
//                    }
//                }),
//                ExpectedRemovedProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    },
//                },
//            };
//        }

//        private static
//            RemoveProductScenario GetRemoveProductScenario_WhenProductIsFromOneSeller(
//                Product productApparel,
//                Product productNonApparel,
//                Currency currency)
//        {
//            return new RemoveProductScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                },
//                RemoveProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    }
//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "ONR13",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 4,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (65*Constants.CompanyFee)),
//                                    DiscountPercentage = 49,
//                                    //TotalCustomerCost = Math.Ceiling((decimal) (65*Constants.CompanyFee)*4) + 3, //roundoff error
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        }
//                    }
//                }),
//                ExpectedRemovedProducts = new RemoveProductScenario.RemoveProductData[]
//                {
//                    new RemoveProductScenario.RemoveProductData
//                    {
//                        ProductSku = productNonApparel.SKU,
//                    },
//                },
//            };
//        }
//        #endregion

//        #region Remove Seller Unit

//        public static IEnumerable GetRemoveItemSellerUnitScenarios()
//        {
//            var productService = GetProductService();

//            var currency = DefaultCurrency;
//            const string productNonApparelSku = "414581DHY0N1000";
//            const string productApparelSku = "12345DHY0N1000";
//            var productNonApparel = productService.GetProductBySku(productNonApparelSku);
//            if (productNonApparel?.ID == 0)
//            {
//                throw new ApplicationException($"Failed to retrieve product with sku {productNonApparelSku}");
//            }
//            var productApparel = productService.GetProductBySku(productApparelSku);
//            if (productApparel?.ID == 0)
//            {
//                throw new ApplicationException($"Failed to retrieve product with sku {productApparelSku}");
//            }

//            yield return
//                new TestCaseData(GetRemoveItemSellerUnitScenario_WhenProductIsFromOneSeller(productApparel, productNonApparel, currency))
//                    .SetName("Remove item seller unit scenario when product is from one seller")
//                ;
//            yield return 
//                new TestCaseData(GetRemoveItemSellerUnitScenario_WhenProductIsFromOneSellerWithLineBuy(productApparel, productNonApparel, currency))
//                    .SetName("Remove item seller unit scenario when product is from one seller with line buy");
//            yield return
//                new TestCaseData(GetRemoveItemSellerUnitScenario_WhenProductIsFromTwoSellers(productApparel, productNonApparel, currency))
//                    .SetName("Remove item seller unit scenario when product is from two sellers")
//                ;
//            yield return
//                new TestCaseData(GetRemoveItemSellerUnitScenario_WhenProductIsFromOneSellerTakeOffer(productApparel, productNonApparel, currency))
//                    .SetName(
//                        "Remove item seller unit scenario when product is from one seller, take offer");
//            yield return
//                new TestCaseData(GetRemoveItemSellerUnitScenario_WhenProductIsFromTwoSellersBothWithTakeOffer(productApparel, productNonApparel, currency))
//                    .SetName(
//                        "Remove item seller unit scenario when product is from two sellers, both with take offer");
//        }


//        private static RemoveItemSellerUnitScenario GetRemoveItemSellerUnitScenario_WhenProductIsFromTwoSellersBothWithTakeOffer(Product productApparel, Product productNonApparel, Currency currency)
//        {
//            return new RemoveItemSellerUnitScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                    new SellerOffer

//                    {
//                        CompanyName = "Bernadina Enrico",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Bernadina Enrico]",
//                            OfferNumber = 90823989,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 40,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 5,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 70,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 8,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        }
//                    }

//                },
//                RemoveProduct = new RemoveItemSellerUnitScenario.RemoveProductData
//                {
//                    SellerCompanyName = "Polka Inferno",
//                    ProductSku = productNonApparel.SKU,

//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productNonApparel.Name,
//                            ProductMainImageName = productNonApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productNonApparel.BrandName,

//                            CategoryName = "CLUTCHES & EVENING BAGS",

//                            ProductSku = productNonApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productNonApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productNonApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productNonApparel.SeasonName,

//                            SellerUnits = new[]
//                            {
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "OCI16",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 8,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (40*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                    DiscountPercentage = 0,
//                                    //TotalCustomerCost = (decimal) (40*Constants.CompanyFee)*8,
//                                    IsTakeOffer = true,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 5,
//                                            AvailableQuantity = 5,
//                                            //public int SizeId = ,
//                                            SizeName = "48",
//                                        },
//                                    }
//                                },
//                            }
//                        },
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {

//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "OCI16",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 8,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (70*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageApparel,
//                                    DiscountPercentage = 0,
//                                    //TotalCustomerCost = (decimal) (70*Constants.CompanyFee)*8,
//                                    IsTakeOffer = true,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 8,
//                                            AvailableQuantity = 8,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        },
//                    }
//                }),
//                ExpectedRemoved = new RemoveItemSellerUnitScenario.ExpectedRemovedData
//                {
//                    AffectedItemsProductSkus = new [] {productNonApparel.SKU, productApparel.SKU,}
//                }
//            };
//        }

//        private static RemoveItemSellerUnitScenario GetRemoveItemSellerUnitScenario_WhenProductIsFromOneSellerTakeOffer(
//            Product productApparel, Product productNonApparel, Currency currency)
//        {
//            return new RemoveItemSellerUnitScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            TakeAll = true,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                },
//                RemoveProduct =
//                    new RemoveItemSellerUnitScenario.RemoveProductData
//                    {
//                        SellerCompanyName = "Polka Inferno",
//                        ProductSku = productNonApparel.SKU,
//                    },
//                ExpectedCart = RemoveProductScenariosProvider.GetEmptyCart(currency),
//                ExpectedRemoved = new RemoveItemSellerUnitScenario.ExpectedRemovedData
//                {
//                    RemovedProductsSkus = new [] {productNonApparel.SKU, productApparel.SKU},
//                }
//            };
//        }

//        private static RemoveItemSellerUnitScenario GetRemoveItemSellerUnitScenario_WhenProductIsFromTwoSellers(Product productApparel, Product productNonApparel, Currency currency)
//        {
//            return new RemoveItemSellerUnitScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                    new SellerOffer

//                    {
//                        CompanyName = "Bernadina Enrico",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Bernadina Enrico]",
//                            OfferNumber = 90823989,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 40,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 5,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 70,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 8,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        }
//                    }

//                },
//                RemoveProduct = new RemoveItemSellerUnitScenario.RemoveProductData()
//                {

//                    SellerCompanyName = "Polka Inferno",
//                    ProductSku = productNonApparel.SKU,
//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productNonApparel.Name,
//                            ProductMainImageName = productNonApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productNonApparel.BrandName,

//                            CategoryName = "CLUTCHES & EVENING BAGS",

//                            ProductSku = productNonApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productNonApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productNonApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productNonApparel.SeasonName,

//                            SellerUnits = new[]
//                            {
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "OCI16",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 8,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (40*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageNonApparel,
//                                    DiscountPercentage = 45,
//                                    //TotalCustomerCost = (decimal) (40*Constants.CompanyFee)*8,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 5,
//                                            AvailableQuantity = 5,
//                                            //public int SizeId = ,
//                                            SizeName = "48",
//                                        },
//                                    }
//                                },
//                            }
//                        },
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {

//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "OCI16",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 8,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (70*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageApparel,
//                                    DiscountPercentage = 49,
//                                    //TotalCustomerCost = (decimal) (70*Constants.CompanyFee)*8,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 8,
//                                            AvailableQuantity = 8,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "ONR13",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 4,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (65*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageApparel,
//                                    DiscountPercentage = 0,
//                                    //TotalCustomerCost = (decimal) (65*Constants.CompanyFee)*4,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        },
//                    }
//                }),
//                ExpectedRemoved = new RemoveItemSellerUnitScenario.ExpectedRemovedData
//                {
//                    AffectedItemsProductSkus = new [] {productNonApparel.SKU},
//                }
//            };
//        }

//        private static RemoveItemSellerUnitScenario GetRemoveItemSellerUnitScenario_WhenProductIsFromOneSellerWithLineBuy(Product productApparel, Product productNonApparel, Currency currency)
//        {
//            return new RemoveItemSellerUnitScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    LineBuy = true,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                },
//                RemoveProduct = new RemoveItemSellerUnitScenario.RemoveProductData
//                {
//                    SellerCompanyName = "Polka Inferno",
//                    ProductSku = productNonApparel.SKU,
//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "ONR13",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 4,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (65*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageApparel,
//                                    DiscountPercentage = 0,
//                                    //TotalCustomerCost = (decimal) (65*Constants.CompanyFee)*4,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        }
//                    }
//                }),
//                ExpectedRemoved = new RemoveItemSellerUnitScenario.ExpectedRemovedData
//                {
//                    RemovedProductsSkus = new [] {productNonApparel.SKU}
//                }
//            };
//        }

//        private static RemoveItemSellerUnitScenario GetRemoveItemSellerUnitScenario_WhenProductIsFromOneSeller(Product productApparel, Product productNonApparel, Currency currency)
//        {
//            return new RemoveItemSellerUnitScenario
//            {
//                SellerOffers = new SellerOffer[]
//                {
//                    new SellerOffer
//                    {
//                        CompanyName = "Polka Inferno",
//                        UserName = "UlyssesPlulpigger@mejix.com",
//                        Offer = new Offer
//                        {
//                            OfferName = "Offer_By_[Polka Inferno]",
//                            OfferNumber = 90823983,
//                            StartDate = DateTime.Today,
//                            OfferStatus = OfferStatus.Published,
//                            OfferProducts = new[]
//                            {
//                                new OfferProduct
//                                {
//                                    ProductId = productNonApparel.ID,
//                                    OfferCost = 60,
//                                    LineBuy = true,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 2,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 3,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "48",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        }
//                                    },
//                                    Currency = currency,
//                                },
//                                new OfferProduct
//                                {
//                                    ProductId = productApparel.ID,
//                                    OfferCost = 65,
//                                    OfferProductSizes = new OfferProductSize[]
//                                    {
//                                        new OfferProductSize
//                                        {
//                                            Quantity = 4,
//                                            Size =
//                                                new Size
//                                                {
//                                                    Name = "44",
//                                                    SizeType = new SizeType {Name = "T2"}
//                                                },
//                                        },
//                                    },
//                                    Currency = currency,
//                                }
//                            },
//                            Currency = currency,
//                        },


//                    },
//                },
//                RemoveProduct = new RemoveItemSellerUnitScenario.RemoveProductData
//                {
//                    SellerCompanyName = "Polka Inferno",
//                    ProductSku = productNonApparel.SKU,
//                },
//                ExpectedCart = TestHelpers.ComputedTotalsCart(new Cart
//                {
//                    Currency = currency,
//                    Items = new CartItem[]
//                    {
//                        new CartItem
//                        {
//                            //ProductId = ,
//                            ProductName = productApparel.Name,
//                            ProductMainImageName = productApparel.MainImageName,

//                            //BrandId = ,
//                            BrandName = productApparel.BrandName,

//                            CategoryName = "UNDERWEAR",

//                            ProductSku = productApparel.SKU,

//                            //MaterialId = ,
//                            MaterialCode = productApparel.MaterialName,

//                            //ColorId = ,
//                            ColorName = productApparel.ColorName,

//                            //SeasonId = ,
//                            SeasonName = productApparel.SeasonName,

//                            SellerUnits = new[]
//                            {
//                                new CartItemSellerUnit
//                                {
//                                    CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier(),
//                                    // in tests are ignored properties ending with Id
//                                    SellerAlias = "ONR13",
//                                    ShipsFromRegion = "Europe",
//                                    UnitsCount = 4,
//                                    CustomerUnitPrice = Math.Ceiling((decimal) (65*Constants.CompanyFee)),
//                                    UnitShippingTaxPercentage = Constants.ShippingPercentageApparel,
//                                    DiscountPercentage = 49,
//                                    //TotalCustomerCost = (decimal) (65*Constants.CompanyFee)*4,
//                                    IsTakeOffer = false,
//                                    IsLineBuy = false,
//                                    Active = true,
//                                    SellerUnitSizes = new CartItemSellerUnitSize[]
//                                    {
//                                        new CartItemSellerUnitSize
//                                        {
//                                            Quantity = 4,
//                                            AvailableQuantity = 4,
//                                            //public int SizeId = ,
//                                            SizeName = "44",
//                                        },
//                                    }
//                                },
//                            }
//                        }
//                    }
//                }),
//                ExpectedRemoved = new RemoveItemSellerUnitScenario.ExpectedRemovedData
//                {
//                    RemovedProductsSkus = new [] { productNonApparel.SKU}
//                }
//            };
//        }

//        #endregion
//    }
//}

