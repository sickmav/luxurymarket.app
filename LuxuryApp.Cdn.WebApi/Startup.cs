﻿using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using LuxuryApp.Cdn.WebApi;
using NLog;
using System.Configuration;
using NLog.Targets;
using System.Linq;

[assembly: OwinStartup(typeof(Startup))]
namespace LuxuryApp.Cdn.WebApi
{
    public class Startup
    {

        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            var dependencyResolver = new AutofacConfig().GetDependencyResolver();
            config.DependencyResolver = dependencyResolver;

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

            //Setup NLog
            GlobalDiagnosticsContext.Set("LogTable", ConfigurationManager.AppSettings["NLogTableName"]);
            GlobalDiagnosticsContext.Set("Environment", ConfigurationManager.AppSettings["Environment"]);
            var nlogConnectionString = ConfigurationManager.ConnectionStrings["NLogConnectionString"].ConnectionString;
            var databaseTargets = LogManager.Configuration.AllTargets.OfType<DatabaseTarget>();
            foreach (var databaseTarget in databaseTargets)
                databaseTarget.ConnectionString = nlogConnectionString;
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            //Token Consumption
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
        }
    }
}
