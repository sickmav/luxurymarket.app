﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace LuxuryApp.Core.Infrastructure.Authorization
{
    public class AppPrincipal
    {
       /// <summary>
        /// Checks if the principal is authenticated.
        /// </summary>
        public static bool IsAuthenticated
        {
            get { return System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        private static T GetClaim<T>(string claimType)
        {
            var claimsPrincipal = System.Threading.Thread.CurrentPrincipal as ClaimsPrincipal;
            if (claimsPrincipal == null)
                return default(T);

            var claim = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == claimType);
            return GetClaimValue<T>(claim);
        }

        private static T GetClaimValue<T>(Claim claim)
        {
            if (claim == null)
                return default(T);

            if (typeof(T) == typeof(Guid))
                return (T)Convert.ChangeType(new Guid(claim.Value), typeof(T));

            if (typeof(T).IsEnum)
                return (T)Enum.Parse(typeof(T), claim.Value);

            return (T)Convert.ChangeType(claim.Value, typeof(T));
        }

        private static bool TryGetClaimValue<T>(Claim claim, out T ret)
        {
            try
            {
                ret = (typeof(T) == typeof(Guid))
                    ? (T)Convert.ChangeType(new Guid(claim.Value), typeof(T))
                    : (T)Convert.ChangeType(claim.Value, typeof(T));
            }
            catch
            {
                ret = default(T);
                return false;
            }
            return true;
        }

        private static IEnumerable<T> GetClaims<T>(string claimType)
        {
            var claimValues = new List<T>();

            var claimsPrincipal = System.Threading.Thread.CurrentPrincipal as ClaimsPrincipal;
            if (claimsPrincipal == null) return claimValues;

            foreach (var claim in claimsPrincipal.Claims.Where(c => c.Type == claimType))
            {
                T ret;
                if (TryGetClaimValue(claim, out ret))
                {
                    claimValues.Add(ret);
                }
            }

            return claimValues;
        }
    }
}
