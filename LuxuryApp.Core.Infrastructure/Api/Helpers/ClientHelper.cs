﻿using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace LuxuryApp.Core.Infrastructure.Api.Helpers
{
    public static class ClientHelper
    {
        public static async Task<LoginTokenResult> GetTokenAsync(string authUrl, string username, string password)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(authUrl);
                    HttpResponseMessage response = await client.PostAsync("Token",
                        new StringContent(string.Format("grant_type=password&username={0}&password={1}",
                          HttpUtility.UrlEncode(username),
                          HttpUtility.UrlEncode(password)), Encoding.UTF8,
                          "application/x-www-form-urlencoded")).ConfigureAwait(false);

                    string resultJSON = response.Content.ReadAsStringAsync().Result;

                    LoginTokenResult result = JsonConvert.DeserializeObject<LoginTokenResult>(resultJSON);
                    return result;
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    return new LoginTokenResult() { Error = ex.Message };
                }
            }
        }

        public static async Task<T> PostContentAsync<T>(string token, Uri baseUri, string requestUri, object values)
        {
            T responseT = default(T);
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = baseUri;
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", token);  //with bearer
                    var response = await client.PostAsJsonAsync(requestUri, values).ConfigureAwait(false); ;
                    if (response.IsSuccessStatusCode)
                        responseT = await response.Content.ReadAsAsync<T>();
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                }
                return responseT;
            }
        }

    }
}