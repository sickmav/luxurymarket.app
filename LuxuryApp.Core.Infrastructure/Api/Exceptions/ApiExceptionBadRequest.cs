using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Runtime.Serialization;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class ApiExceptionBadRequest : ApiException
    {
        public ApiExceptionBadRequest()
        {
            Code = ApiResultCode.BadRequest;
        }

        public ApiExceptionBadRequest(string message)
            : base(message)
        {
        }

        public ApiExceptionBadRequest(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ApiExceptionBadRequest(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}