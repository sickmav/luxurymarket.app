using LuxuryApp.Core.Infrastructure.Api.Exceptions;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiResultNotFound<T> : ApiResult<T>
    {
        public ApiResultNotFound(T data):
            base(data, new []{ "Not found"}, new ApiExceptionNotFound())
        {
            
        }

        public ApiResultNotFound(T data, string message = "Access denied"):
            base(data, new []{message}, new ApiExceptionNotFound())
        {

        }
    }
}