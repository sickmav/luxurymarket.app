using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using System.Collections.Generic;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiResultUnprocessableEntity<T> : ApiResult<T>
    {
        public IEnumerable<string> ValidationErrorMessages { get; set; }

        public ApiResultUnprocessableEntity(T data, ApiExceptionUnprocessableEntity ex) : 
            base(data, ex.ValidationErrorMessages, ex)
        {
            ValidationErrorMessages = ex.ValidationErrorMessages;
        }

        public ApiResultUnprocessableEntity(T data, string validationErrorMessage)
            : base(
                data, new[] {validationErrorMessage},
                new ApiExceptionUnprocessableEntity(new[] {validationErrorMessage}))
        {
            ValidationErrorMessages = new[] {validationErrorMessage};
        }

        public ApiResultUnprocessableEntity(T data, string[] validationErrorMessages)
            : base(data, validationErrorMessages, new ApiExceptionUnprocessableEntity(validationErrorMessages))
        {
            ValidationErrorMessages = validationErrorMessages;
        }
    }
}