﻿namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ErrorResponse
    {
        public ApiResultCode ErrorCode { get; set; }

        public string Message { get; set; }

        public string MoreInfo { get; set; }
    }
}
