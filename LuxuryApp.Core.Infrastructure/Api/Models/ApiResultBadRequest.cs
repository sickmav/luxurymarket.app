using LuxuryApp.Core.Infrastructure.Api.Exceptions;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ApiResultBadRequest<T> : ApiResult<T>
    {
        public ApiResultBadRequest(T data, string message = "Invalid Request") :
            base(data, new[] { message }, new ApiExceptionForbidden())
        {

        }
    }
}