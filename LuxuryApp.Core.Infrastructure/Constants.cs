﻿
namespace LuxuryApp.Core.Infrastructure
{
    public class RequestHeaders
    {
        public const string UserID = "UserID";
        public const string CompanyID = "CompanyID";
    }

    public class Claims
    {
        public const string NS = "";
    }

    public class ClaimTypes
    {
        public const string UserId = "userid";
        public const string Permissions = "permissions";
    }
}
