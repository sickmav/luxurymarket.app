﻿using LuxuryApp.Core.Infrastructure.Api.Models;
using System.Web.Http.ExceptionHandling;

namespace LuxuryApp.Core.Infrastructure.Handlers
{
    public class LuxuryExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            context.Result = new ErrorMessageResult
            {
                Request = context.ExceptionContext.Request,
                Content = context.Exception.Message,
                Context = context
            };
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }
    }
}
