﻿using LuxuryApp.Core.Infrastructure.Authorization;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace LuxuryApp.Core.Infrastructure.Security
{
    public class CustomAuthorizationFilter : AuthorizeAttribute
    {
        private const string BasicAuthResponseHeader = "WWW-Authenticate";
        private const string BasicAuthResponseHeaderValue = "Basic";

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            return AppPrincipal.IsAuthenticated;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);
            var response = actionContext.Response;
            response.Headers.Add(BasicAuthResponseHeader, BasicAuthResponseHeaderValue);
        }
    }
}
