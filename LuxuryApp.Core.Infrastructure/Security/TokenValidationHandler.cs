﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.IdentityModel.Tokens;
//using System.Linq;
//using System.Net.Http;
//using System.Security.Claims;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using System.Web;

//namespace LuxuryApp.Core.Infrastructure.Security
//{
//    public class TokenValidationHandler : DelegatingHandler
//    {
//        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
//        {
//            string token;

//            if (!TryRetrieveToken(request, out token))
//            {
//                SetAnonymousPrincipal();
//                return base.SendAsync(request, cancellationToken);
//            }

//            try
//            {
//                //var validator = new JwtTokenValidator();
//                //var principal = validator.ValidateToken(token, CloudConfigurationManager.GetSetting("TokenIssuerUri"));

//                Thread.CurrentPrincipal = principal;
//                if (HttpContext.Current != null)
//                    HttpContext.Current.User = principal;

//                return base.SendAsync(request, cancellationToken);
//            }
//            catch (SecurityTokenValidationException ex)
//            {
//                Trace.TraceError("JWT security token validation error: " + ex);
//            }
//            catch (Exception ex)
//            {
//                Trace.TraceError("JWT validation error: " + ex);
//                throw;
//            }
//            SetAnonymousPrincipal();
//            return base.SendAsync(request, cancellationToken);
//        }

//        private static void SetAnonymousPrincipal()
//        {
//            var claimsIdentity = new ClaimsIdentity(authenticationType: null); // is not authenticated
//            var principal = new ClaimsPrincipal(claimsIdentity);

//            Thread.CurrentPrincipal = principal;

//            if (HttpContext.Current != null)
//                HttpContext.Current.User = principal;
//        }

//        private static bool TryRetrieveToken(HttpRequestMessage request, out string token)
//        {
//            token = null;
//            IEnumerable<string> authHeaders;
//            if (!request.Headers.TryGetValues("Authorization", out authHeaders) || authHeaders.Count() > 1)
//            {
//                return false;
//            }

//            string authToken = authHeaders.First();
//            if (authToken.StartsWith("Bearer "))
//            {
//                authToken = authToken.Replace("Bearer ", string.Empty);
//            }

//            if (authToken.StartsWith("Basic "))
//            {
//                authToken = authToken.Replace("Basic ", string.Empty);
//                try
//                {
//                    // In Basic authentication, the header is username:password, Base64 encoded
//                    authToken = Encoding.ASCII.GetString(Convert.FromBase64String(authToken));
//                }
//                catch { }

//                authToken = authToken.Split(':')[0];
//            }

//            token = authToken;

//            return true;
//        }
//    }
//}
