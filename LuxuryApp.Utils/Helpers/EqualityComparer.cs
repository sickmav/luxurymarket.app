﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Utils.Helpers
{
    public class EqualityComparer<T> : IEqualityComparer<T>
    {
        private Func<T, T, bool> Cmp { get; }

        public EqualityComparer(Func<T, T, bool> cmp)
        {
            Cmp = cmp;
        }
        public bool Equals(T x, T y)
        {
            var result = Cmp(x, y);
            return result;
        }

        public int GetHashCode(T obj)
        {
            return 0; // to force path throuh equals 
        }
    }
}
