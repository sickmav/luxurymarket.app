﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System;


namespace LuxuryApp.Utils.Helpers
{
    public static class ExcelParseHelper
    {
        public static ExcelWorksheet GetWorksheet(Stream stream, int wsNumber)
        {
            try
            {
                var package = new ExcelPackage();
                package.Load(stream);
                ExcelWorksheet workSheet = null;
                var wsCount = package.Workbook.Worksheets.Count();
                if (wsNumber < wsCount)
                {
                    workSheet = package.Workbook.Worksheets[wsNumber];
                    workSheet.ClearExcelWorksheet();
                }
                return workSheet;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void Dispose(this ExcelWorksheet workSheet)
        {
            if (workSheet != null)
                workSheet.Dispose();
        }


        public static ExcelWorksheet GetWorksheetByName(Stream stream, string name)
        {
            using (var package = new ExcelPackage())
            {
                package.Load(stream);
                var wsCount = package.Workbook.Worksheets.Count();

                var workSheet = package.Workbook.Worksheets[name];
                if (workSheet != null)
                {
                    workSheet.ClearExcelWorksheet();
                }
                return workSheet;
            }
        }

        public static string[] GetColumnHeaders(this ExcelWorksheet workSheet, int rowNum, int startColumnIndex, int lastColumnIndex)
        {
            var values = new string[lastColumnIndex - startColumnIndex + 1];
            for (var col = startColumnIndex; col <= lastColumnIndex; col++)
            {
                var cell = workSheet.Cells[rowNum, col, rowNum, col].Value?.ToString();
                var cellValue = string.IsNullOrWhiteSpace(cell) ? $"__column_{rowNum}_{col}" : cell.RemoveSpecialCharacters().ToLower();
                if (values.Contains(cellValue)) cellValue = $"{cellValue}(2)";
                values[col - 1] = cellValue;
            }
            return values;
        }

        public static void ClearExcelWorksheet(this ExcelWorksheet workSheet)
        {
            var lastColumnIndex = GetLastColumnIndex(workSheet);
            var lastRowIndex = GetLastRowIndex(workSheet);

            ClearExcelWorksheet(workSheet, lastRowIndex, lastColumnIndex);
        }

        public static void ClearExcelWorksheet(this ExcelWorksheet workSheet, int lastRowNumber, int lastColumnNumber)
        {
            workSheet.DeleteColumn(lastColumnNumber + 1, workSheet.Dimension.End.Column - lastColumnNumber);
            workSheet.DeleteRow(lastRowNumber + 1, workSheet.Dimension.End.Row - lastRowNumber);
        }

        public static DataTable ClearEmptyColumns(this DataTable dtExcel)
        {
            string[] columnNames = (from dc in dtExcel.Columns.Cast<DataColumn>()
                                    where dc.ColumnName.ToLower().StartsWith("__column_")
                                    select dc.ColumnName).ToArray();
            foreach (var columnName in columnNames)
            {
                if (dtExcel.Rows.OfType<DataRow>().All(r => r.IsNull(columnName)))
                    dtExcel.Columns.Remove(columnName);
            }
            return dtExcel;
        }

        public static DataTable ClearSpecialCharacters(this DataTable dtExcel)
        {
            string[] columnNames = dtExcel.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
            for (var rowIndex = 0; rowIndex < dtExcel.Rows.Count; rowIndex++)
            {
                foreach (var columnName in columnNames)
                {
                    if (!string.IsNullOrWhiteSpace(dtExcel.Rows[rowIndex][columnName]?.ToString()))
                        dtExcel.Rows[rowIndex][columnName] = dtExcel.Rows[rowIndex][columnName]?.ToString();
                }
            }
            return dtExcel;
        }

        public static bool HasNull(this DataTable table)
        {
            foreach (DataColumn column in table.Columns)
            {
                if (table.Rows.OfType<DataRow>().Any(r => r.IsNull(column)))
                    return true;
            }

            return false;
        }

        public static int GetLastColumnIndex(this ExcelWorksheet workSheet)
        {
            var lastIndex = 0;
            for (var col = 1; col <= workSheet.Dimension.End.Column; col++)
            {
                if (!workSheet.Cells[1, col, workSheet.Dimension.End.Row, col].IsEmptyRange())
                    lastIndex = col;
            }
            return lastIndex;
        }

        public static int GetLastRowIndex(this ExcelWorksheet workSheet)
        {
            var lastIndex = 0;
            for (var index = 1; index <= workSheet.Dimension.End.Row; index++)
            {
                if (!workSheet.Cells[index, 1, index, workSheet.Dimension.End.Column].IsEmptyRange())
                    lastIndex = index;
            }
            return lastIndex;
        }

        public static DataTable GetTable(this ExcelWorksheet workSheet, int startRow)
        {
            var startColumnIndex = workSheet.Dimension.Start.Column;
            var lastColumnIndex = workSheet.GetLastColumnIndex();
            var lastRowIndex = workSheet.GetLastRowIndex();

            DataTable dtExcel = null;

            for (int rowNum = startRow; rowNum <= lastRowIndex; rowNum++)
            {
                var wsRow = workSheet.Cells[rowNum, startColumnIndex, rowNum, lastColumnIndex];
                if (wsRow.IsEmptyRange())
                    continue;

                if (dtExcel == null || dtExcel?.Columns.Count == 0)
                {
                    dtExcel = new DataTable();
                    var headerColumns = GetColumnHeaders(workSheet, rowNum, startColumnIndex, lastColumnIndex);
                    headerColumns.ToList().ForEach(x => dtExcel.Columns.Add(x));
                }
                else
                {
                    var dtRow = dtExcel.NewRow();
                    for (var col = startColumnIndex; col <= lastColumnIndex; col++)
                    {
                        dtRow[col - 1] = workSheet.Cells[rowNum, col, rowNum, col].Value?.ToString().Trim();
                    }
                    dtExcel.Rows.Add(dtRow);
                }
            }
            if (dtExcel != null)
                dtExcel = dtExcel.ClearEmptyColumns().ClearSpecialCharacters();

            return dtExcel;
        }

        public static string GetCellValue(this ExcelWorksheet workSheet, int rowIndex, int columnIndex)
        {
            var cellValue = workSheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex].Value?.ToString().Trim();
            return cellValue;
        }
    }

    public static class ExcelExtensions
    {
        public static bool IsEmptyRange(this ExcelRange range)
        {
            return range == null || range.All(x => string.IsNullOrWhiteSpace(x.Value?.ToString()));
        }
    }
}
