﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace LuxuryApp.Utils.Helpers
{
    public static class ConverterExtension
    {
        public static Nullable<T> TryParse<T>(this String str) where T : struct
        {
            try
            {
                T parsedValue = (T)Convert.ChangeType(str, typeof(T));
                return parsedValue;
            }
            catch { return null; }
        }

        public static double? Round(double? value)
        {
            return value != null ? (double?)decimal.Round((decimal)value, 2, MidpointRounding.AwayFromZero) : null;
        }

        public static DataTable ConvertToDataTable<T>(this List<T> data)
        {
            PropertyDescriptorCollection props = null;
            DataTable table = new DataTable();
            if (data != null && data.Count > 0)
            {
                props = TypeDescriptor.GetProperties(data[0]);
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            if (props != null)
            {
                object[] values = new object[props.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            return table;
        }

    }
}
