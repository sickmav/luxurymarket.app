﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Reflection;

namespace LuxuryApp.Core.DataAccess
{
    public static class Extensions
    {
        public static DataTable ToIdList(this int[] ids)
        {
            var result = new DataTable() {Columns = { "Id"}};
            result.SetTypeName("dbo.IdList");
            foreach (var id in ids)
            {
                result.Rows.Add(id);
            }
            return result;
        }

        public static List<T> DataReaderMapToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }
    }
}
