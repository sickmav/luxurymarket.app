﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Orders;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static class OrderReader
    {
        public static List<SuborderDataForPDF> ToSuborderDetails(this DataReader reader)
        {
            var suborder = new List<SuborderDataForPDF>();

            if (reader == null) return suborder;

            while (reader.Read())
            {
                suborder.Add(new SuborderDataForPDF
                {
                    OrderId = reader.GetInt("OrderID"),
                    BuyerCompanyID = reader.GetInt("BuyerCompanyID"),
                    BuyerCompanyName = reader.GetString("BuyerCompanyName"),
                    OrderSellerId = reader.GetInt("OrderSellerID"),
                    OrderSellerPONumber = reader.GetString("OrderSellerPONumber"),
                    ProductID = reader.GetInt("ProductID"),
                    ProductName = reader.GetString("ProductName"),
                    BrandName = reader.GetString("BrandName"),
                    CategoryName = reader.GetString("CategoryName"),
                    ModelNumber = reader.GetString("ModelNumber"),
                    ColorCode = reader.GetString("ColorCode"),
                    StandardColorID = reader.GetInt("StandardColorID"),
                    StandardColorName = reader.GetString("StandardColorName"),
                    MaterialCode = reader.GetString("MaterialCode"),
                    SKU = reader.GetString("SKU"),
                    ProductRetailPrice = reader.GetDecimal("ProductRetailPrice"),
                    OrderSellerProductID = reader.GetInt("OrderSellerProductID"),
                    OfferCost = reader.GetDecimal("OfferCost"),
                    CustomerUnitPrice = reader.GetDecimal("CustomerUnitPrice"),
                    UnitShippingCost = reader.GetDecimal("UnitShippingCost"),
                    CurrencyName = reader.GetString("CurrencyName"),
                    UnitDiscountPercentage = reader.GetDecimal("UnitDiscountPercentage"),
                    UnitShippingTaxPercentage = reader.GetDecimal("UnitShippingTaxPercentage"),
                    ShippingCost = reader.GetDecimal("ShippingCost"),
                    Units = reader.GetInt("Units"),
                    UnitsConfirmed = reader.GetInt("ConfirmedUnits"),
                    SizeTypeName = reader.GetString("SizeTypeName"),
                    SizeName = reader.GetString("SizeName"),
                    CreatedDate = reader.GetDateTimeOffset("CreatedDate")
                });
            }

            if (reader.NextResult())
            {
                suborder.FirstOrDefault().Addresses = GetCompanyMainAddresses(reader);
            }

            reader.Close();

            return suborder;
        }

        public static List<Address> GetCompanyMainAddresses(DataReader reader)
        {
            var addresses = new List<Address>();
            if (reader == null) return addresses;
            while (reader.Read())
            {
                addresses.Add(new Address
                {
                    Id = reader.GetString("AddressID"),
                    AddressType = (AddressType)reader.GetInt("AddressTypeID"),
                    Street1 = reader.GetString("Street1"),
                    Street2 = reader.GetString("Street2"),
                    City = reader.GetString("City"),
                    State = new State
                    {
                        StateName = reader.GetString("StateName")
                    },
                    Country = new Country
                    {
                        CountryName = reader.GetString("CountryName")
                    },
                    ZipCode = reader.GetString("ZipCode")
                });
            }

            return addresses;
        }
    }
}
