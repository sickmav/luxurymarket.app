﻿using LuxuryApp.Contracts.Models.Addresses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static List<AddressInfo> ToAddressList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<AddressInfo>();

            while (reader.Read())
            {
                var item = new AddressInfo();
                item.AddressId = reader.GetInt("AddressID");
                item.AddressTypeId = reader.GetInt("AddressTypeID");
                item.CompanyId = reader.GetInt("CompanyID");
                item.Street1 = reader.GetString("Street1");
                item.Street2 = reader.GetString("Street2");
                item.City = reader.GetString("City");
                item.StateId = reader.GetInt("StateID");
                item.StateName = reader.GetString("StateName");
                item.ZipCode = reader.GetString("ZipCode");
                item.CountryId = reader.GetInt("CountryID");
                item.CountryName = reader.GetString("CountryName");
                item.CountryIso2Code = reader.GetString("CountryIso2Code");
                item.IsMain = reader.GetBool("SetMain");

                items.Add(item);
            }

            reader.Close();

            return items;
        }
    }
}
