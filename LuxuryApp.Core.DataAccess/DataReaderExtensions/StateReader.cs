﻿using LuxuryApp.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.DataReaderExtensions
{
    public static partial class DataReaderExtensions
    {
        public static List<State> ToStateModel(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<State>();

            while (reader.Read())
            {
                var item = new State();
                item.StateId = reader.GetInt("StateID");
                item.StateName = reader.GetString("StateName");
                item.CountryId = reader.GetInt("CountryID");

                items.Add(item);
            }

            return items;
        }

    }
}
