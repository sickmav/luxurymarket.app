﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.SizeMappings;


namespace LuxuryApp.Core.DataAccess.Models
{
    public class SizeTypeMappingDenormalized
    {
        public int SizeTypeMappingId { get; set; }

        public int SellerSizeMappingId { get; set; }

        public int SellerId { get; set; }

        public SizeType SizeType { get; set; }

        public SizeRegion SizeRegion { get; set; }

        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public bool? IsValid { get; set; }

        public string Size { get; set; }

        public string VendorSize { get; set; }
    }
}
