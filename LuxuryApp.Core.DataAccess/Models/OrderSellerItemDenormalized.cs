﻿using System;

namespace LuxuryApp.Core.DataAccess.Models
{
    public class OrderSellerItemDenormalized
    {
        public OrderSellerItemDenormalized()
        {
            ShowConfirmedUnits = true;
        }

        public int OrderId { get; set; }

        public int BuyerCompanyId { get; set; }

        public string BuyerAlias { get; set; }

        public string BuyerCompanyName { get; set; }

        public int SellerCompanyId { get; set; }

        public string SellerAlias { get; set; }

        public string SellerCompanyName { get; set; }

        public int OrderSellerId { get; set; }

        public int OrderSellerItemId { get; set; }

        public int OrderSellerProductId { get; set; }

        //public int OfferProductSizeId { get; set; }

        // public int OfferProductId { get; set; }

        public int OfferPublishedBy { get; set; }

        public string OrderPONumber { get; set; }

        public string OrderSellerPONumber { get; set; }

        public DateTimeOffset OrderCreatedDate { get; set; }

        public int OrderCreatedBy { get; set; }

        public int PaymentMethodId { get; set; }

        public string PaymentMethodName { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string ModelNumber { get; set; }

        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string MaterialCode { get; set; }

        public string ColorCode { get; set; }

        public string SeasonCode { get; set; }

        public string DivisionName { get; set; }

        public double RetailPrice { get; set; }

        public string MainImageName { get; set; }

        public double OfferCost { get; set; }

        public double CustomerUnitPrice { get; set; }

        public double UnitDiscountPercentage { get; set; }

        public int ShippingTaxId { get; set; }

        public double ApparelTaxUnitPercentage { get; set; }

        public double NonApparelTaxUnitPercentage { get; set; }

        public string ShippingName { get; set; }

        public string ShippingDays { get; set; }

        public double UnitShippingTaxPercentage { get; set; }

        public double UnitShippingCost { get; set; }

        public double LMUnitPrice { get; set; }

        public int Units { get; set; }

        public int? ConfirmedUnits { get; set; }

        public bool ShowConfirmedUnits { get; set; }

        public int DisplayUnits
        {
            get
            {
                return ShowConfirmedUnits && ConfirmedUnits.HasValue ? ConfirmedUnits.Value : Units;
            }
        }

        public string SizeName { get; set; }

        public string SizeTypeName { get; set; }

        public string StandardMaterial { get; set; }

        public string StandardColor { get; set; }

        public int StatusTypeId { get; set; }

        public string StatusTypeName { get; set; }

        public int CurrencyId { get; set; }

        public string CurrencyName { get; set; }

        public double CurrencyRate { get; set; }
    }
}
