﻿using System;

namespace LuxuryApp.Core.DataAccess.Models
{
    public class CartItemsDenormalized
    {
        public int CartItemId { get; set; }
        public int CartId { get; set; }
        public int OfferProductSizeId { get; set; }
        public int OfferProductId { get; set; }
        public int OfferId { get; set; }

        public bool TakeOffer { get; set; }
        public bool LineBuy { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductSku { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string CategoryName { get; set; }
        public int MaterialId { get; set; }
        public string MaterialCode { get; set; }
        public int ColorId { get; set; }
        public string ColorCode { get; set; }
        public int StandardColorId { get; set; }
        public string StandardColorName { get; set; }
        public int StandardMaterialId { get; set; }
        public string StandardMaterialName { get; set; }
        public int SeasonId { get; set; }
        public string SeasonCode { get; set; }
        public string MainImageName { get; set; }
        public int SellerCompanyId { get; set; }
        public string SellerAlias { get; set; }
        public int ShippingTaxId { get; set; }
        public string ShipsFromRegion { get; set; }
        public string SizeName { get; set; }
        public int SizeTypeId { get; set; }
        public string SizeTypeName { get; set; }
        public int Quantity { get; set; }
        public bool IsItemActive { get; set; }
        public decimal OfferCost { get; set; }
        public decimal RetailPrice { get; set; }
        public int AvailableQuantity { get; set; }
        public decimal LMUnitPrice { get; set; }
        public decimal CustomerUnitPrice { get; set; }
        public decimal UnitShippingCost { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal UnitShippingTaxPercentage { get; set; }

        public int ReservedTimeSeconds { get; set; }
        public int CartActiveMinutes { get; set; }
        public bool IsActiveOffer { get; set; }

        public int BuyerCompanyId { get; set; }
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public decimal CurrencyRate { get; set; }

        public int CartReservedTime { get; set; }
    }
}
