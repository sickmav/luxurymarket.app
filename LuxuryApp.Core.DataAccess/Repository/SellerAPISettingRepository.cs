﻿using Dapper;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;
using System.Linq;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class SellerAPISettingRepository : RepositoryBase, ISellerAPISettingRepository
    {
        public SellerAPISettingRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider) { }

        public async Task<APISettings> GetSellerApiSettings(SellerAPISettingType sellerAPIType)
        {
            var sellerApiId = (int)sellerAPIType;

            var sql = $"EXEC {StoredProcedureNames.Inventory_GetSellerAPISettings} @SellerAPIID";
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<APISettings>(sql, new { sellerApiId });
                conn.Close();
                return result.FirstOrDefault();
            }
        }
    }
}
