﻿using LuxuryApp.Contracts.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.DataAccess;
using Dapper;
using LuxuryApp.Contracts.Models.SizeMappings;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class SizeTypeMappingRepository : RepositoryBase, ISizeTypeMappingRepository
    {
        public SizeTypeMappingRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<IEnumerable<SizeRegion>> GetSizeRegions()
        {
            var sql = @"SELECT ID
		                      ,Name
		                      ,Description
		                      ,KeyWords
	                    FROM dbo.SizeRegions";
            sql = $"{sql} from {SqlFunctionName.fn_SizeRegionsGet}()";
            IEnumerable<SizeRegion> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<SizeRegion>(sql);
                conn.Close();
            }
            return result.ToList();
        }

        public async Task<IEnumerable<SizeRegion>> SearchSizeRegions(string name)
        {
            var sql = @"SELECT ID
		                      ,Name
		                      ,Description
		                      ,KeyWords
	                    FROM dbo.SizeRegions";
            sql = $"{sql} from {SqlFunctionName.fn_SizeRegionsSearch}(@Region)";
            IEnumerable<SizeRegion> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<SizeRegion>(sql, new { @Region = name });
                conn.Close();

            }
            return result.ToList();
        }
    }
}
