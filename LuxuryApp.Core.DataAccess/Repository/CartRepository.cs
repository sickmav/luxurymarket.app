﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;
using LuxuryApp.Core.DataAccess.Models;
using System.Linq.Expressions;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class CartRepository :
        RepositoryBase,
        ICartRepository
    {
        public CartRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public void PhysicalDeleteCart(int cartId)
        {
            const string spName = "dbo.usp_DeleteCart";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName, new { cartId }, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public int GetOrCreateCartId(int userId, int buyerCompanyId, int currencyId)
        {
            const string spName =
                "[dbo].[usp_GetOrCreateCartId]";

            int result;
            var p = new DynamicParameters();
            p.Add("userId", userId);
            p.Add("buyerCompanyId", buyerCompanyId);
            p.Add("currencyId", currencyId);
            p.Add("cartId", direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName, p,
                    commandType: CommandType.StoredProcedure);
                result = p.Get<int>("cartId");
                conn.Close();
            }
            return result;
        }

        public Cart GetCart(int cartId)
        {
            var cartSummaryGetter = Task<CartSummary>.Factory.StartNew(() => GetCartSummary(cartId));
            var cartItemsGetter = Task<CartItem[]>.Factory.StartNew(() => GetCartItems(cartId));
            var cart = MapCart(cartSummaryGetter.Result, cartItemsGetter.Result);
            return cart;
        }

        public CartSplitPreview GetCartSplitPreview(int cartId)
        {
            var cartItemsDenormalizeds = Task<CartItemsDenormalized[]>.Factory.StartNew(() => GetAllCartItems(cartId));
            var cartItems = cartItemsDenormalizeds.Result.ToArray();

            if (cartItems == null || !cartItems.Any()) return new CartSplitPreview();

            var cartValidItems = GetFilteredtCartItems(cartItems, true);
            var cartInvalidItems = GetFilteredtCartItems(cartItems, false);

            var splitPreview = new CartSplitPreview
            {
                ValidItems = cartValidItems,
                InvalidItems = cartInvalidItems,
                InvalidItemSummary = CalculateTotalItemSummary(cartId, cartValidItems),
                validItemSummary = CalculateTotalItemSummary(cartId, cartInvalidItems)
            };

            return splitPreview;
        }

        private CartItem[] GetFilteredtCartItems(CartItemsDenormalized[] items, bool valid)
        {
            var filteredItems = FilterCartItemsDenormalized(items, valid);
            return MapCartItems(filteredItems);
        }

        private CartItemsDenormalized[] FilterCartItemsDenormalized(CartItemsDenormalized[] items, bool valid)
        {
            return (valid) ? items.Where(x => x.Quantity > 0 && x.IsActiveOffer && x.AvailableQuantity >= x.Quantity).ToArray() :
                           items.Where(x => x.Quantity > 0 && (!x.IsActiveOffer || x.AvailableQuantity < x.Quantity)).ToArray();
        }

        private CartItemsDenormalized[] GetAllCartItems(int cartId)
        {
            var result = GetAllCartItemsDenormalized(cartId);
            return result;
        }

        private CartTotalSummary CalculateTotalItemSummary(int cartId, CartItem[] cartItems)
        {
            if (cartItems == null)
            {
                return null;
            }
            var cartSummary = new CartTotalSummary();
            cartSummary.CartId = cartId;
            cartSummary.ProductStyles = cartItems.Select(x => x.ProductId).Distinct().Count();
            cartSummary.ShippingCost = cartItems.Sum(x => x.TotalShippingCost);
            cartSummary.TotalLandedCost = cartItems.Sum(x => x.TotalLandedCost);
            cartSummary.ProductCosts = cartItems.Sum(x => x.TotalCustomerCost);
            cartSummary.UnitsCount = cartItems.Sum(x => x.SellerUnits.Sum(y => y.UnitsCount));

            return cartSummary;
        }

        private Cart MapCart(CartSummary cartSummary, CartItem[] cartItems)
        {
            if (cartSummary == null)
            {
                return null;
            }
            var cart = new Cart();
            cart.Id = cartSummary.CartId;
            cart.BuyerCompanyId = cartSummary.BuyerCompanyId;
            cart.Currency = cartSummary.Currency;
            cart.TotalCustomerCost = cartSummary.TotalCustomerCost + cartSummary.TotalShippingCost;
            cart.ItemsCount = cartSummary.ItemsCount;
            cart.UnitsCount = cartSummary.UnitsCount;
            cart.Items = cartItems;
            cart.CartReservedTime = cartSummary.CartReservedTime;
            return cart;
        }

        public CartItem[] GetCartItems(int cartId)
        {
            CartItemsDenormalized[] cartItemsDenormalizeds = GetAllCartItemsDenormalized(cartId);
            return GetCartItems(cartId, cartItemsDenormalizeds);
        }

        private CartItem[] GetCartItems(int cartId, CartItemsDenormalized[] cartItemsDenormalizeds)
        {
            var cartItems = MapCartItems(cartItemsDenormalizeds);
            return cartItems;
        }

        public CartSummary GetCartSummary(int cartId)
        {
            const string sql =
                @"SELECT
                  CartId
                  ,[BuyerCompanyId]
                  ,[PlacedByUserId]
                  ,[UnitsCount] 
                  ,[ItemsCount]
	              ,[TotalCustomerCost]
                  ,[TotalShippingCost]  
                  ,CartReservedTime
                  ,[CurrencyId] as Id
	              ,CurrencyName as Name
	              ,CurrencyRate as Rate 
            from dbo.fn_GetCartSummaryByCartId(@cartId)";
            CartSummary result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result =
                    conn.Query<CartSummary, Currency, CartSummary>(
                        sql,
                        (cartSummary, currency) =>
                        {
                            cartSummary.Currency = currency;
                            return cartSummary;
                        },
                        new { cartId })
                        .SingleOrDefault();
                conn.Close();
            }
            return result;
        }

        public void UpdateCartTotalsByCartId(int cartId)
        {
            const string spName = "dbo.usp_UpdateCartTotalsByCartId";

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName, new { cartId }, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        private CartItem[] MapCartItems(CartItemsDenormalized[] cartItemsDenormalizeds)
        {
            var result = (from cid in cartItemsDenormalizeds
                          group cid by cid.ProductId
                into g
                          select MapCartItem(g.Key, g.ToArray())).ToArray();
            return result;
        }

        private CartItem MapCartItem(int productId, CartItemsDenormalized[] cartItemsDenormalizeds)
        {
            var commonItem = cartItemsDenormalizeds.First();
            var cartItem = new CartItem
            {
                CartId = commonItem.CartId,
                ProductId = commonItem.ProductId,
                ProductName = commonItem.ProductName,
                //OfferId = commonItem.O ///todo
                OfferProductId = commonItem.OfferProductId,
                BrandId = commonItem.BrandId,
                BrandName = commonItem.BrandName,
                CategoryName = commonItem.CategoryName,
                ProductSku = commonItem.ProductSku,
                MaterialId = commonItem.MaterialId,
                MaterialCode = commonItem.MaterialCode,
                ColorId = commonItem.ColorId,
                ColorName = commonItem.ColorCode,
                StandardColorId = commonItem.StandardColorId,
                StandardColorName = commonItem.StandardColorName,
                StandardMaterialId = commonItem.StandardMaterialId,
                StandardMaterialName = commonItem.StandardMaterialName,
                SeasonId = commonItem.SeasonId,
                SeasonName = commonItem.SeasonCode,
                ProductMainImageName = commonItem.MainImageName
            };
            cartItem.SellerUnits =
            (from c in cartItemsDenormalizeds
             group c by new { c.OfferProductId }
                into g
             let common = g.First()
             select new CartItemSellerUnit
             {
                 CartItemSellerUnitIdentifier = new CartItemSellerUnitIdentifier
                 {
                     CartId = common.CartId,
                     OfferProductId = common.OfferProductId,
                 },
                 SellerAlias = common.SellerAlias,
                 ShippingTaxId = common.ShippingTaxId,
                 ShipsFromRegion = common.ShipsFromRegion,
                 UnitsCount = g.Sum(c => c.Quantity),
                 CustomerUnitPrice = common.CustomerUnitPrice,
                 LMUnitPrice = common.LMUnitPrice,
                 DiscountPercentage = common.DiscountPercentage,
                 TotalCustomerCost = common.CustomerUnitPrice * g.Sum(c => c.Quantity),
                 RetailPrice = common.RetailPrice,
                 UnitShippingTaxPercentage = common.UnitShippingTaxPercentage,
                 UnitShippingCost = common.UnitShippingCost,
                 TotalShippingCost = g.Sum(c => c.Quantity) * common.UnitShippingCost,
                 IsLineBuy = common.LineBuy,
                 IsTakeOffer = common.TakeOffer,
                 OfferId = common.OfferId,
                 Active = g.Any(x => x.IsItemActive),

                 ReservedTimeSeconds = g.Any(x => x.ReservedTimeSeconds > 0) ? g.Where(c => c.ReservedTimeSeconds > 0).Min(c => c.ReservedTimeSeconds) : 0,
                 IsActiveOffer = commonItem.IsActiveOffer,

                 SellerUnitSizes = (from cisu in g
                                    select
                                    new CartItemSellerUnitSize
                                    {
                                        Id = cisu.CartItemId,
                                        OfferProductSizeId = cisu.OfferProductSizeId,
                                        ReservedTimeSeconds = cisu.ReservedTimeSeconds,
                                        Quantity = cisu.Quantity,
                                        AvailableQuantity = cisu.AvailableQuantity,
                                        SizeName = cisu.SizeName,
                                        SizeTypeId = cisu.SizeTypeId,
                                        SizeTypeName = cisu.SizeTypeName
                                    }).OrderBy(i => i.SizeName).ToArray(),
             }).OrderBy(f => f.SellerAlias)
                .ThenBy(f => f.IsTakeOffer)
                .ThenBy(f => f.IsLineBuy)
                .ToArray();
            var activeSellerUnits = cartItem.SellerUnits.Where(x => x.Active).ToArray();
            var hasActiveSellerUnits = activeSellerUnits.Any();
            cartItem.TotalCustomerCost = hasActiveSellerUnits ? activeSellerUnits.Sum(x => x.TotalCustomerCost) : 0;
            cartItem.TotalShippingCost = hasActiveSellerUnits ? activeSellerUnits.Sum(x => x.TotalShippingCost) : 0;
            cartItem.TotalLandedCost = cartItem.TotalCustomerCost + cartItem.TotalShippingCost;
            cartItem.AverageUnitCost = hasActiveSellerUnits ? activeSellerUnits.Select(x => x.LMUnitPrice).Average() : 0;
            cartItem.AverageUnitDiscountPercentage = hasActiveSellerUnits ? activeSellerUnits.Select(x => x.DiscountPercentage).Average() : 0;
            return cartItem;
        }

        public void AddProductsToCart(int cartId, int[] productIds)
        {
            // todo: batch insert optimization
            const string spName = "dbo.usp_AddProductsToCart";

            var parameters = new DynamicParameters(
              new
              {
                  cartId = cartId,
                  productIds = ListToDataTable(productIds.ToList())
              });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                conn.Execute(spName, parameters, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public void AddEntireOfferToCart(int cartId, int offerId)
        {
            AddEntireOffersToCart(cartId, new[] { offerId });
        }

        public void AddEntireOffersToCart(int cartId, int[] offerIds)
        {
            // todo: batch insert optimization
            const string spName = "dbo.usp_AddEntireOfferToCart";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                foreach (var offerId in offerIds)
                {
                    conn.Execute(spName, new { cartId, offerId }, commandType: CommandType.StoredProcedure);
                }
                conn.Close();
            }
        }

        public void AddEntireOfferProductToCart(int cartId, int offerProductId)
        {
            AddEntireOfferProductsToCart(cartId, new[] { offerProductId });
        }

        public void AddEntireOfferProductsToCart(int cartId, int[] offerProductIds)
        {
            // todo: batch insert optimization
            const string spName =
                "dbo.usp_AddEntireOfferProductToCart";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                foreach (var offerProductId in offerProductIds)
                {
                    conn.Execute(spName, new { cartId, offerProductId }, commandType: CommandType.StoredProcedure);
                }
                conn.Close();
            }
        }

        public void AddOfferProductSizeToCart(int cartId, int offerProductSizeId, int quantity)
        {
            AddOfferProductSizesToCart(cartId, new[]
            {
                new SaveOfferProductSizesToCartModel.ItemQuantity
                {
                    OfferProductSizeId = offerProductSizeId,
                    Quantity = quantity
                }
            });
        }

        public void AddOfferProductSizesToCart(int cartId, SaveOfferProductSizesToCartModel.ItemQuantity[] itemQuantities)
        {
            // todo: batch insert optimization
            const string sql =
                "EXEC dbo.usp_AddOfferProductSizeToCart @cartId, @offerProductSizeId, @quantity";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                foreach (var itemQuantity in itemQuantities)
                {
                    conn.Execute(sql,
                        new
                        {
                            cartId,
                            offerProductSizeId = itemQuantity.OfferProductSizeId,
                            quantity = itemQuantity.Quantity
                        });
                }

                conn.Close();
            }
        }

        public int GetCartItemSellerUnitSizeIdByOfferProductSizeId(int cartId, int offerProductSizeId)
        {
            const string sql =
                "select CartItemSellerUnitSizeId " +
                "  from dbo.fn_GetCartItemSellerUnitSizeIdByOfferProductSizeId(@cartId, @offerProductSizeId)";

            int result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<int>(sql, new { cartId, offerProductSizeId }).SingleOrDefault();
                conn.Close();
            }
            return result;
        }

        public CartItemIdentifier[] GetCartItemIdentifiersByCartItemSellerUnitIdentifier(CartItemSellerUnitIdentifier cartItemSellerUnitIdentifier)
        {
            const string sql =
                "select " +
                "CartId," +
                "BuyerCompanyId," +
                "CartItemSellerUnitSizeId," +
                "OfferProductSizeId," +
                "OfferProductId," +
                "OfferId " +
                "   from dbo.fn_GetCartItemIdentifiersByCartItemSellerUnitIdentifier" +
                        "(@cartId, @offerProductId)";
            CartItemIdentifier[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<CartItemIdentifier>(sql,
                    new
                    {
                        cartId = cartItemSellerUnitIdentifier.CartId,
                        offerProductId = cartItemSellerUnitIdentifier.OfferProductId
                    }).ToArray();
                conn.Close();
            }
            return result;
        }

        public CartItem[] GetCartItemByCartItemSellerUnitIdentifier(CartItemSellerUnitIdentifier cartItemSellerUnitIdentifier)
        {
            throw new NotImplementedException();
        }

        public void SetActiveFlagForEntireOffersUnderCart(int cartId, int[] offerIds, bool activeFlag)
        {
            // todo: change to vectorial
            foreach (var offerId in offerIds)
            {
                SetActiveFlagForEntireOfferUnderCart(cartId, offerId, activeFlag);
            }
        }

        public void SetActiveFlagForEntireOfferProductsUnderCart(int cartId, int[] offerProductIds, bool activeFlag)
        {
            // todo: change to vectorial
            foreach (var offerProductId in offerProductIds)
            {
                SetActiveFlagForEntireOfferProductUnderCart(cartId, offerProductId, activeFlag);
            }
        }

        public void SetActiveFlagForOfferProductSizesUnderCart(int cartId, int[] offerProductSizeIds, bool activeFlag)
        {
            // todo: change to vectorial
            foreach (var offerProductSizeId in offerProductSizeIds)
            {
                SetActiveFlagForOfferProductSizeUnderCart(cartId, offerProductSizeId, activeFlag);
            }
        }

        public void RemoveCartItemsWithShippingTaxId(int cartId, int shippingTaxId)
        {
            const string spName = "dbo.usp_DeleteCartItemsByShippingTaxIdFromCart";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName,
                    new
                    {
                        cartId,
                        shippingTaxId
                    },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public void RemoveCartItems(int cartId, int[] itemsIds)
        {
            const string spName = "dbo.usp_DeleteCartItems";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName,
                    new { cartId, cartItemsIds = itemsIds.ToIdList() },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public void EmptyCart(int cartId)
        {
            const string spName = "dbo.usp_EmptyCart";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName,
                    new { cartId },
                    commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }

        public void SetActiveFlagForEntireOfferUnderCart(int cartId, int offerId, bool activeFlag)
        {
            const string spName =
                "dbo.usp_SetActiveFlagForEntireOfferUnderCart";
            using (var conn = GetReadWriteConnection())
            {
                conn.Execute(spName, new { cartId, offerId, activeFlag }, commandType: CommandType.StoredProcedure);
            }
        }

        public void SetActiveFlagForEntireOfferProductUnderCart(int cartId, int offerProductId, bool activeFlag)
        {
            const string spName =
                "dbo.usp_SetActiveFlagForEntireOfferProductUnderCart";

            using (var conn = GetReadWriteConnection())
            {
                conn.Execute(spName, new { cartId, offerProductId, activeFlag }, commandType: CommandType.StoredProcedure);
            }
        }

        public void SetActiveFlagForOfferProductSizeUnderCart(int cartId, int offerProductSizeId,
            bool activeFlag)
        {
            const string spName =
                "dbo.usp_SetActiveFlagForOfferProductSizeUnderCart";

            using (var conn = GetReadWriteConnection())
            {
                conn.Execute(spName, new { cartId, offerProductSizeId, activeFlag }, commandType: CommandType.StoredProcedure);
            }
        }

        public CartTotalSummary GetTotalSummary(int cartId, bool onlyValidItems, int userId)
        {
            var sql = @"SELECT CartId
                             ,ProductCosts
                             ,ShippingCost
                             ,TotalStyles
                             ,UnitsCount
                             ,TotalLandedCost";
            sql = $"{sql} from {SqlFunctionName.fn_CartGetTotalSummary}(@cartId,@userId,@OnlyValidItems)";

            CartTotalSummary result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<CartTotalSummary>(sql, new { cartId, userId, onlyValidItems }).FirstOrDefault();
                conn.Close();
            }
            return result;
        }

        public bool IsUserAssociatedToCart(int cartId, int userId)
        {
            var sql = $"SELECT {SqlFunctionName.fn_IsUserAssociatedToCart}(@CartId, @UserId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { cartId, userId });
                conn.Close();
            }
            return result;
        }

        public OfferHasReservedItem HasReservedItemsByOtherUsers(int? offerId, int? offerProductId, int? cartId)
        {
            var query = $"Select OfferID,OfferProductID,IsActiveOffer,HasReservedItems from {SqlFunctionName.fn_OfferHasReservedItems}(@OfferID,@OfferProductID,@CartID)";
            var result = new OfferHasReservedItem();
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<OfferHasReservedItem>(query, new { offerId, offerProductId, cartId }).FirstOrDefault();
                conn.Close();
            }

            return result;
        }

        public OfferHasAvailableQuantity OfferProductSizeHasAvailableQuantity(int offerProductSizeId, int quantity, int? cartId)
        {
            var query = $"Select OfferProductSizeID,IsActiveOffer,ReservedQuantity,AvailableQuantity, HasAvailableQuantity from {SqlFunctionName.fn_OfferProductSizeHasAvailableQuantity}(@OfferProductSizeID,@Quantity,@CartID)";
            OfferHasAvailableQuantity result = new OfferHasAvailableQuantity();
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<OfferHasAvailableQuantity>(query, new { offerProductSizeId, quantity, cartId }).FirstOrDefault();
                conn.Close();

            }
            return result;
        }

        public CartItemValidatelCollection GetInvalidItems(int cartId)
        {
            var sql = @"select CartItemId,
		                          OfferProductSizeId,
		                          IsReserved,
		                          IsActiveOffer,IsOfferQuantityAvailable";
            sql = $"{sql} from {SqlFunctionName.fn_CartItems_GetInvalidItems}(@cartId)";

            List<CartItemValidate> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<CartItemValidate>(sql, new { cartId }).ToList();
                conn.Close();
            }
            return new CartItemValidatelCollection() { Items = result };
        }

        public List<CartItemUpdatedQuantity> RelockCartProduct(int cartId, int offerProductId)
        {
            const string sql = "EXEC dbo.usp_Cart_RelockOfferProduct @CartID, @OfferProductID";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                var result = conn.Query<CartItemUpdatedQuantity>(sql, new { cartId, offerProductId });

                conn.Close();

                return result.ToList();
            }

        }

        public List<CartItemReserved> GetCartTimes(int cartId)
        {
            var sql = @"select CartItemID,
			            OfferProductSizeID,
                        OfferProductID,
			            Quantity,
			            IsReserved,
			            ReservedTimeSeconds,
			            QuantityReservedByOthers,
			            OfferTotalQuantity ,
			            OfferAvailableQuantity from fn_Cart_GetItemsTime(@cartId)";

            List<CartItemReserved> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<CartItemReserved>(sql, new { cartId }).ToList();
                conn.Close();
            }
            return result;
        }

        private CartItemsDenormalized[] GetAllCartItemsDenormalized(int cartId)
        {
            const string sql =
                @"select   CartId
	                      ,CartItemId
	                      ,OfferProductSizeId
	                      ,OfferProductID
	                      ,ProductID
	                      ,OfferId
	                      ,TakeOffer
	                      ,LineBuy
	                      ,ProductName
	                      ,ProductSku
	                      ,BrandID
	                      ,BrandName
	                      ,CategoryName
	                      ,MaterialId
	                      ,MaterialCode
	                      ,ColorId
	                      ,ColorCode
	                      ,SeasonId
	                      ,SeasonCode
	                      ,StandardColorId
	                      ,StandardColorName
                          ,StandardMaterialId
                          ,StandardMaterialName
	                      ,MainImageName
	                      ,SellerCompanyId
	                      ,OfferPublishedBy
	                      ,CurrencyID	  
	                      ,CurrencyName
	                      ,CurrencyRate  
	                      ,ShippingTaxId
	                      ,UnitShippingTaxPercentage
	                      ,Quantity
	                      ,ShipsFromRegion
                          ,ShippingDays
	                      ,ApparelTax
	                      ,NonApparelTax
                          ,DivisionId
	                      ,DivisionName
	                      ,AvailableQuantity
	                      ,RetailPrice
	                      ,OfferCost
	                      ,CustomerUnitPrice
                          ,UnitShippingCost
                          ,LMUnitPrice
	                      ,DiscountPercentage
	                      ,SellerAlias	 
	                      ,PlacedByUserId
	                      ,SizeName
                          ,SizeTypeId
						  ,SizeTypeName
                          ,BuyerCompanyId
	                      ,IsItemActive
	                      ,IsReserved
	                      ,ReservedTimeSeconds
	                      ,IsActiveOffer	
	                      from dbo.fn_CartItems_GetForCartId(@CartId)";
            CartItemsDenormalized[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<CartItemsDenormalized>(sql, new { cartId }).ToArray();
                conn.Close();
            }
            return result;
        }
        
        private DataTable ListToDataTable(List<int> ids)
        {
            DataTable idsDataTable = null;
            if (ids != null)
            {
                idsDataTable = new DataTable();
                idsDataTable.Columns.Add("ID", typeof(int));
                foreach (var prodId in ids)
                {
                    idsDataTable.Rows.Add(prodId);
                }
            }

            return idsDataTable;
        }
    }
}
