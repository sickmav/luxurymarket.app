﻿using System.Linq;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.DataAccess;
using System.Data;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class ProductRepository :
        RepositoryBase,
        IProductRepository
    {
        public ProductRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        //public Product GetProductBySku(string sku)
        //{
        //    using (var conn = GetReadOnlyConnection())
        //    {
        //        conn.Open();
        //        var reader = conn.ExecuteReader(StoredProcedureNames.Products_GetBySKU,
        //                                        new { sku },
        //                                         commandType: CommandType.StoredProcedure);
        //        var dataReader = new DataReader { Connection = conn, Reader = reader };

        //        Product returnItem = null;
        //        if (dataReader != null)
        //            returnItem = dataReader.ToProductDetails();

        //        conn.Close();

        //        return returnItem;
        //    }
        //}

        public ProductDivision[] GetProductsDivisions(int[] productIds)
        {
            const string sql =
                "select " +
                "ProductId," +
                "DivisionId," +
                "DivisionName " +
                "  from dbo.fn_GetProductsDivisions(@pIds)";

            ProductDivision[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<ProductDivision>(sql, new
                {
                    pIds = productIds.ToIdList(),
                }).ToArray();
                conn.Close();
            }
            return result;
        }

        public bool IsvalidProductSKU(string sku)
        {
            var sql = $"select {SqlFunctionName.fn_IsvalidProductSKU}(@sku)";

            bool result = false;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<bool>(sql, new { sku }).FirstOrDefault();
                conn.Close();
            }
            return result;
        }
    }
}
