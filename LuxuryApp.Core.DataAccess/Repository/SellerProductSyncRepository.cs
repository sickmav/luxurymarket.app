﻿using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.SellerProductSync;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.DataAccess;
using LuxuryApp.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class SellerProductSyncRepository : RepositoryBase, ISellerProductSyncRepository
    {
        public SellerProductSyncRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<int> InsertProductsAsync(DataTable productDt, DataTable imageDt, Guid mainBatchID, int sellerID,int commandTimeoutSeconds)
        {
            const string spName = "dbo.usp_ProductSync_Insert";
            var parameters = new DynamicParameters(new
            {
                MainBatchID = mainBatchID,
                SellerID = sellerID,
                Products = productDt,
                Images = imageDt
            });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
               var result= await conn.ExecuteAsync(spName, parameters, commandType: CommandType.StoredProcedure,commandTimeout: commandTimeoutSeconds);
                conn.Close();
                return result;
            }
        }

        public async Task<DateTimeOffset?> GetLastSyncDateAsync(int sellerId)
        {
            var sql = $"EXEC {StoredProcedureNames.usp_ProductSyncBatchGetLastSync} @SellerID";
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<DateTimeOffset?>(sql, new { sellerId });
                conn.Close();
                return result.FirstOrDefault();
            }
        }

        public async Task<int> SaveInventoryAsync(DataTable inventory, int sellerID,int commandTimeoutSeconds)
        {
            const string spName = "dbo.InventoryUpdate_SaveInventory";
            var parameters = new DynamicParameters(new
            {
                SellerID = sellerID,
                InventoryList = inventory
            });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                var result = await conn.ExecuteAsync(spName, parameters, commandTimeout: commandTimeoutSeconds,commandType: CommandType.StoredProcedure);
                conn.Close();

                return result;
            }
        }
    }
}
