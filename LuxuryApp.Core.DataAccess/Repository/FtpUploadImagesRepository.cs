﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Contracts.Repository;
using System.Collections.Generic;
using LuxuryApp.Utils.Helpers;
using LuxuryApp.DataAccess;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class FtpUploadImagesRepository : RepositoryBase,
        IFtpUploadImagesRepository
    {
        public FtpUploadImagesRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<int> FtpUploadImagesAsync(List<FtpProductImageItem> items, int createdBy)
        {
            var parameters = new DynamicParameters(
            new
            {
                Items = items.Select(x => new
                {
                    Sku = x.Sku,
                    FileName = x.FileName,
                    OriginalFileName = x.OriginalFileName,
                    FileSizeBytes = x.FileSizeBytes,
                    IsUploaded = x.IsUploaded,
                    Error = x.Error
                }).ToList().ConvertToDataTable(),
                CreatedBy = createdBy,
                CreatedDate = DateTimeOffset.UtcNow
            });
            var countInserted = 0;
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                try
                {
                    countInserted = await conn.ExecuteAsync(StoredProcedureNames.FtpProductImagesInsert,
                                                       param: parameters,
                                                       commandType: CommandType.StoredProcedure
                                                       );
                }
                catch (Exception ex)
                {
                    throw;
                }
                conn.Close();

            }
            return countInserted;
        }

        public async Task<int> InsertScheduleAsync(FtpProductImagesSchedule item)
        {
            var returnIdentity = 0;
            var parameters = new DynamicParameters(
              new
              {
                  StartDateTime = item.StartDateTime,
                  ExecutionTime = item.ExecutionTime,
                  NumberUploadedImages = item.NumberUploadedImages,
                  NumberErrors = item.NumberErrors,
                  NextRun = item.NextRun
              });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                try
                {
                    returnIdentity = await conn.ExecuteAsync(StoredProcedureNames.FtpProductImagesSchedulesInsert,
                                                       param: parameters,
                                                       commandType: CommandType.StoredProcedure
                                                       );
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }

            }

            return returnIdentity;
        }
    }
}
