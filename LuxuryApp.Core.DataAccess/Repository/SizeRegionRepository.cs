﻿using LuxuryApp.Contracts.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.DataAccess;
using Dapper;
using LuxuryApp.Contracts.Models.SizeMappings;
using System;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class SizeRegionRepository : RepositoryBase, ISizeRegionRepository
    {
        public SizeRegionRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<IEnumerable<SizeRegion>> GetSizeRegions()
        {
            var sql = @"SELECT ID
		                      ,Name
		                      ,Description
		                      ,KeyWords";
            sql = $"{sql} from {SqlFunctionName.fn_SizeRegionsGet}()";
            IEnumerable<SizeRegion> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<SizeRegion>(sql);
                conn.Close();
            }
            return result.ToList();
        }

        public IEnumerable<SizeRegion> SearchSizeRegions(string name)
        {
            var sql = @"SELECT ID
		                      ,Name
		                      ,Description
		                      ,KeyWords";
            sql = $"{sql} from {SqlFunctionName.fn_SizeRegionsSearch}(@Name)";
            IEnumerable<SizeRegion> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<SizeRegion>(sql, new { name });
                conn.Close();

                return result.ToList();
            }

        }
    }
}
