﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class CurrencyRepository: 
        RepositoryBase,
        ICurrencyRepository
    {
        public CurrencyRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public Currency[] GetAllCurrencies()
        {
            const string sql =
                "SELECT [CurrencyID]" +
                ",[Name]" +
                ",[Rate]" +
                "   from dbo.GetAllCurrencies()";
            Currency[] result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<Currency>(sql).ToArray();
                conn.Close();
            }
            return result;
        }

        public void UpdateCurrencyRates(Currency[] currencies)
        {
            foreach (var currency in currencies)
            {
                UpdateCurrencyRate(currency.CurrencyId, currency.Rate);
            }
        }

        private void UpdateCurrencyRate(int currencyId, decimal rate)
        {
            const string spName = "dbo.UpdateCurrencyRate";
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                conn.Execute(spName, new
                {
                    currencyId,
                    rate
                }, commandType: CommandType.StoredProcedure);
                conn.Close();
            }
        }
    }
}
