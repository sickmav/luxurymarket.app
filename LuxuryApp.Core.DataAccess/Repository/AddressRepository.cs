﻿using LuxuryApp.Contracts.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.DataAccess;
using Dapper;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;
using LuxuryApp.Contracts.Models.Addresses;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class AddressRepository : RepositoryBase, IAddressRepository
    {
        public AddressRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<IEnumerable<AddressInfo>> GetCompanyMainAddresses(int companyId)
        {
            var sql = @"SELECT distinct
			                 CompanyID
			                ,AddressID
			                ,AddressTypeID
			                ,AddressTypeName
			                ,Street1
			                ,Street2
			                ,City
			                ,StateID
			                ,StateName
			                ,ZipCode
			                ,CountryID
			                ,CountryName";
            sql = $"{sql} from {SqlFunctionName.fn_CompanyAddresses_GetAllMain}(@CompanyId)";

            //using (var conn = GetReadOnlyConnection())
            //{
            //    conn.Open();
            //    var reader = await conn.ExecuteReaderAsync(sql, new { @CompanyId = companyId });
            //    var dataReader = new DataReader { Connection = conn, Reader = reader };

            //    if (dataReader != null)
            //        result = dataReader.ToAddressList();
            //    conn.Close();

            //}

            IEnumerable<AddressInfo> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<AddressInfo>(sql, new { @CompanyId = companyId });
                conn.Close();

            }
            return result.ToList();
        }
    }
}
