﻿using LuxuryApp.Contracts.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Interfaces;
using Dapper;
using LuxuryApp.Contracts.Models.Orders;
using LuxuryApp.Contracts.Models.Shipping;
using LuxuryApp.DataAccess;
using System.Data;
using System;
using LuxuryApp.Core.DataAccess.Models;
using LuxuryApp.Utils.Helpers;
using System.Data.SqlClient;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class OrderRepository : RepositoryBase, IOrderRepository
    {
        public OrderRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        #region Public Methods

        public async Task<Order> GetOrderDetails(int orderId)
        {
            var orderDenormalized = await GetOrderDenormalizedDB(orderId, null);
            var order = ComputeOrder(orderDenormalized).FirstOrDefault();
            return order;
        }

        public async Task<int> SubmitOrder(int cartId, int paymentMethodId, int userId, DateTimeOffset date)
        {
            var p = new DynamicParameters();
            p.Add("@CartId", cartId);
            p.Add("@PaymentMethodId", paymentMethodId);
            p.Add("@CreatedBy", userId);
            p.Add("OrderID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
            p.Add("CreatedDate", date);
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                await conn.ExecuteAsync(StoredProcedureNames.OrderSubmit, p, commandType: CommandType.StoredProcedure);

                conn.Close();
            }
            int id = p.Get<int>("OrderID");
            return id;
        }

        public async Task<OrderPoNumberForEmail> GetPoNumbersForEmails(int orderId)
        {
            var sql = @"SELECT OrderID,
                               OrderSellerID,
                               PoNumber,
                               Email";
            sql = $"{sql} from  {SqlFunctionName.fn_OrderGetPoNumbersAndEmails}(@OrderID)";

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<OrderPoNumberForEmailDenormalized>(sql, new { orderId });
                conn.Close();

                var resultEmailList = MapOrderPoNumberForEmail(result.ToList());

                return resultEmailList;
            }
        }

        private OrderPoNumberForEmail MapOrderPoNumberForEmail(List<OrderPoNumberForEmailDenormalized> items)
        {
            var buyerData = items.Where(x => x.OrderSellerId == null).FirstOrDefault();

            var model = new OrderPoNumberForEmail
            {
                BuyerEmailAddress = buyerData.Email,
                OrderPoNumber = buyerData.PoNumber,
                OrderId = buyerData.OrderId,
                SellerEmailAddressList = items.Where(x => x.OrderSellerId != null).GroupBy(x => x.OrderSellerId)
                                        .Select(x => new SellerEmailAddress
                                        {
                                            OrderSellerId = x.Key.Value,
                                            PoNumber = x.Select(y => y.PoNumber).FirstOrDefault(),
                                            EmailList = x.Select(y => y.Email).ToList()
                                        }).Distinct().ToList()
            };

            return model;
        }

        public async Task<Order> GetOrderDetailsForBuyer(int orderId)
        {
            var orderDenormalized = await GetOrderDenormalizedDB(orderId, null);
            var order = ComputeOrder(orderDenormalized).FirstOrDefault();
            return order;
        }

        public async Task<OrderSellerView> GetOrderItemForSellerById(int orderSellerId)
        {
            var orderDenormalized = await GetOrderDenormalizedDB(null, orderSellerId);
            var order = GetOrderSellerItemsForSeller(orderDenormalized).FirstOrDefault();
            return order;
        }

        public async Task<OrderSellerManageList> FilterSellerOrders(RequestFilterOrders model, int userId)
        {
            var listItems = await FilterSellerOrdersDB(model, userId);
            var result = new OrderSellerManageList();
            result.Items = ComputeSellerManageList(listItems);
            return result;
        }

        public async Task<OrderBuyerManageList> FilterBuyerOrders(RequestFilterOrders model, int userId)
        {
            var listItems = await FilterBuyerOrdersDB(model, userId);
            var result = new OrderBuyerManageList();
            result.Items = ComputeBuyerManageList(listItems);
            return result;
        }

        public async Task<IEnumerable<PaymentMethod>> GetPaymentMethod()
        {
            var sql = $"SELECT PaymentMethodId,PaymentMethodName from  {SqlFunctionName.fn_PaymentMethods_GetAll}()";

            IEnumerable<PaymentMethod> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<PaymentMethod>(sql);
                conn.Close();
            }
            return result.ToList();
        }

        public async Task<IEnumerable<OrderStatusType>> GetSuborderStatusTypes()
        {
            var sql = $"SELECT Id,Name from  {SqlFunctionName.fn_OrderSellerGetAllStatusTypes}()";

            IEnumerable<OrderStatusType> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OrderStatusType>(sql);
                conn.Close();
            }
            return result.ToList();
        }

        public int UpdateStatus(OrderStatusUpdateView model, double orderConfirmationPercentage, int userId, DateTimeOffset date, out string errorMessage)
        {
            errorMessage = string.Empty;

            var p = new DynamicParameters();
            p.Add("@OrderSellerID", model.OrderSellerId);
            p.Add("@StatusTypeID", model.StatusTypeId);
            p.Add("@OrderCustomerTypeID", model.OrderCustomerType);
            p.Add("@CreatedBy", userId);
            p.Add("@CreatedDate", date);
            p.Add("@CompletedPercentage", orderConfirmationPercentage);
            p.Add("ReturnStatusID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            var returnStatusId = 0;
            using (var conn = GetReadWriteConnection())
            {
                try
                {
                    conn.Open();

                    conn.Execute(StoredProcedureNames.OrderStatusUpdate, p, commandType: CommandType.StoredProcedure);

                    returnStatusId = p.Get<int>("ReturnStatusID");
                }
                catch (Exception ex)
                {
                    errorMessage = string.IsNullOrWhiteSpace(ex.Message) ? "Internal error" : ex.Message;
                    if (errorMessage.Contains("ForbiddenStatus"))
                        errorMessage = "ForbiddenStatus";
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnStatusId;
        }

        public async Task<string> ConfirmUnits(OrderConfirmUnitView model, int userId, DateTimeOffset date)
        {
            var errorMessage = string.Empty;
            var dt = GetOrderConfirmUnitsDataTable(model.ConfirmUnits);

            var p = new DynamicParameters();
            p.Add("@OrderSellerProductID", model.OrderSellerProductId);
            p.Add("@Units", dt);
            p.Add("@ModifiedBy", userId);
            p.Add("@ModifiedDate", date);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                try
                {
                    await conn.ExecuteAsync(StoredProcedureNames.OrderSellerConfirmUnits, p, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    errorMessage = string.IsNullOrWhiteSpace(ex.Message) ? "Internal error" : ex.Message;
                    if (errorMessage.Contains("ConfirmedUnitsGreaterThanUnits"))
                        errorMessage = "ConfirmedUnitsGreaterThanUnits";
                }
                finally
                {
                    conn.Close();
                }
            }
            return errorMessage;
        }

        public async Task<OrderProductTotalForSeller> GetOrderProductTotal(int orderSellerProductId)
        {
            var sql = @"SELECT OrderSellerProductID,
                               OrderSellerID,
                               TotalUnits,
                               TotalCost";
            sql = $"{sql} from  {SqlFunctionName.fn_OrderProductGetTotalsForSeller}(@OrderSellerProductID)";

            IEnumerable<OrderProductTotalForSeller> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OrderProductTotalForSeller>(sql, new { orderSellerProductId });
                conn.Close();
            }
            var item = result.FirstOrDefault();
            if (item != null && item.OrderSellerId > 0)
                item.SuborderSummary = await GetOrderTotal(item.OrderSellerId);

            return item;
        }

        public async Task<SuborderTotalForSeller> GetOrderTotal(int orderSellerId)
        {
            var sql = @"SELECT OrderSellerID,
                               TotalStyles,
                               TotalUnits,
                               TotalCost";
            sql = $"{sql} from  {SqlFunctionName.fn_OrderGetTotalsForSeller}(@OrderSellerID)";

            IEnumerable<SuborderTotalForSeller> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<SuborderTotalForSeller>(sql, new { orderSellerId });
                conn.Close();
            }
            return result.FirstOrDefault();
        }

        public string CanConfirmUnits(int orderProductId, int userId)
        {
            var sql = $"SELECT {SqlFunctionName.fn_OrderProductCanConfirmItems}(@OrderProductID, @UserID)";
            var result = string.Empty;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<string>(sql, new { orderProductId, userId });
                conn.Close();
            }
            return result;
        }

        public BuyerEmailDataAfterSellerConfirmation GetBuyerDataAfterSellerConfirmation(int orderSellerId)
        {
            var sql = @"SELECT OrderSellerID,
                               OrderID,
                               BuyerFullName,
                               BuyerEmail,
                               OrderSellerPoNumber,
                               SellerAlias,
                               StatusTypeID ,
                               StatusTypeName,
                               SellerEmail";
            sql = $"{sql} from  {SqlFunctionName.fn_OrderGetBuyerDataForEmail}(@OrderSellerID)";

            IEnumerable<BuyerEmailDataAfterSellerConfirmation> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.Query<BuyerEmailDataAfterSellerConfirmation>(sql, new { orderSellerId });
                conn.Close();
            }

            return result.FirstOrDefault();
        }

        public async Task<OrderFileList> GetFilesAsync(int? mainOrderId, int? orderSellerId)
        {
            var sql = @"SELECT ID
			                  ,OrderSellerID
                              ,MainOrderID
			                  ,FileName
			                  ,OriginalName
			                  ,CreatedBy
		                      ,CreatedDate
			                  ,ModifiedBy
			                  ,ModifiedDate";
            sql = $"{sql} from  {SqlFunctionName.fn_OrderFilesGetByOrderId}(@MainOrderID,@OrderSellerID)";

            IEnumerable<OrderFile> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OrderFile>(sql, new { mainOrderId, orderSellerId });
                conn.Close();
            }
            return new OrderFileList { Items = result.ToList() };
        }

        public async Task<int> UpdateFileAsync(OrderFile file, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
           new
           {
               OrderFileID = file.Id,
               OriginalName = file.OriginalName,
               Deleted = file.Deleted,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OrderFiles_Update,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public int InsertFile(OrderFile model, int userId)
        {
            var returnStatus = -1;
            var p = new DynamicParameters();
            p.Add("MainOrderID", model.MainOrderId);
            p.Add("OrderSellerID", model.OrderSellerId);
            p.Add("OriginalName", model.OriginalName);
            p.Add("FileName", model.FileName);
            p.Add("CreatedBy", userId);
            p.Add("CreatedDate", DateTimeOffset.Now);
            p.Add("ID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = conn.Execute(StoredProcedureNames.OrderFiles_Insert,
                                                    param: p,
                                                    commandType: CommandType.StoredProcedure
                                                    );

                returnStatus = p.Get<int>("ID");
                conn.Close();
            }
            return returnStatus;
        }

        public List<SuborderDataForPDF> GetSuborderData(int orderSellerId)
        {
            var p = new DynamicParameters();
            p.Add("@OrderSellerID", orderSellerId);

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = conn.ExecuteReader(StoredProcedureNames.Order_GetSuborders,
                                                   param: p,
                                                   commandType: CommandType.StoredProcedure);
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader == null) return null;
                var result = dataReader.ToSuborderDetails();

                conn.Close();

                return result;
            }

        }

        public async Task<OrderSellerPackageCollection> GetOrderSellerPackages(int ordersellerId)
        {
            var sql = @"SELECT OrderSellerID
                              ,BoxSizeLength
                              ,BoxSizeWidth
                              ,BoxSizeDepth
                              ,Quantity
                              ,Weight";
            sql = $"{sql} from  {SqlFunctionName.fn_OrderSellerPackagesGet}(@OrderSellerID)";

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<OrderSellerPackage>(sql, new { ordersellerId });
                conn.Close();
                return new OrderSellerPackageCollection() { Items = result };
            }
        }

        public async Task<int> InsertOrderSellerPackingInformation(OrderSellerPackingInformationInsert model, int userId)
        {
            var dt = GetOrderSellerPackagesDataTable(model.PackageDimensionsList);

            var returnStatus = -1;
            var p = new DynamicParameters();
            p.Add("OrderSellerID", model.OrderSellerId);
            p.Add("ContactName", model.ContactName);
            p.Add("ContactPhone", model.ContactPhone);
            p.Add("ContactEmail", model.ContactEmail);
            p.Add("FromPickupHour", model.FromPickupHour);
            p.Add("ToPickupHour", model.ToPickupHour);
            p.Add("StartPickupDate", model.StartPickupDate);
            p.Add("EndPickupDate", model.EndPickupDate);
            p.Add("Notes", model.Notes);
            p.Add("Values", dt);
            p.Add("CurrentUser", userId);
            p.Add("CurrentDate", DateTimeOffset.Now);
            p.Add("ID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OrderSellerPackingInformationInsert,
                                                    param: p,
                                                    commandType: CommandType.StoredProcedure
                                                    );

                returnStatus = p.Get<int>("ID");
                conn.Close();
            }
            return returnStatus;
        }

        public async Task<bool> IsSubmittedPackageInformation(int ordersellerId)
        {
            var returnStatus = -1;
            var p = new DynamicParameters();
            p.Add("OrderSellerID", ordersellerId);
            p.Add("ExistsPackage", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.IsSubmittedPackageInformation,
                                                    param: p,
                                                    commandType: CommandType.StoredProcedure
                                                    );

                returnStatus = p.Get<int>("ExistsPackage");
                conn.Close();
            }
            return returnStatus == 1 ? true : false;
        }

        public async Task<OrderSellerPackingInformationInsert> GetOrderSellerPackingInformation(int orderSellerId)
        {
            var sql =
             @"SELECT  ID
                      ,OrderSellerID
                      ,ContactName
                      ,ContactPhone
                      ,ContactEmail
                      ,FromPickupHour
                      ,ToPickupHour
                      ,StartPickupDate
                      ,EndPickupDate
                      ,Notes
                      ,OrderSellerPackingInformationID
	                  ,Length as BoxSizeLength
	                  ,Width as BoxSizeWidth
	                  ,Depth as BoxSizeDepth
	                  ,Quantity
	                  ,Weight";
            sql = $"{ sql} from {SqlFunctionName.fn_OrderSeller_GetPackingInformation}(@OrderSellerId)";

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<OrderSellerPackingInformationDenormalized>(sql, new { orderSellerId });
                conn.Close();
                var item = MapOrderSellerPackingInformation(result.ToList());
                return item;
            }
        }

        public bool IsUserAssociatedToMainOrder(int orderId, int userId, int? companyId)
        {
            var sql = $"SELECT {SqlFunctionName.fn_IsUserAssociatedToMainOrder}(@OrderId, @UserId,@CompanyId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { orderId, userId, companyId });
                conn.Close();
            }
            return result;
        }

        public bool IsUserAssociatedToOrderSeller(int orderSellerId, int userId, int? companyId)
        {
            var sql = $"SELECT {SqlFunctionName.fn_IsUserAssociatedToOrderSeller}(@OrderSellerId, @UserId,@CompanyId)";
            bool result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = conn.ExecuteScalar<bool>(sql, new { orderSellerId, userId, companyId });
                conn.Close();
            }
            return result;
        }


        #endregion

        #region Private Methods

        private OrderSellerPackingInformationInsert MapOrderSellerPackingInformation(List<OrderSellerPackingInformationDenormalized> items)
        {
            var result = items.Select(x => new OrderSellerPackingInformationInsert
            {
                ContactEmail = x.ContactEmail,
                ContactName = x.ContactName,
                ContactPhone = x.ContactPhone,
                EndPickupDate = x.EndPickupDate,
                FromPickupHour = x.FromPickupHour,
                ToPickupHour = x.ToPickupHour,
                StartPickupDate = x.StartPickupDate,
                Notes = x.Notes,
                OrderSellerId = x.OrderSellerId
            }).FirstOrDefault() ?? new OrderSellerPackingInformationInsert();
            var packagesList = new List<OrderSellerPackageInsert>();
            items?.ForEach(x =>
            {
                packagesList.Add(new OrderSellerPackageInsert
                {
                    BoxSizeDepth = x.BoxSizeDepth,
                    BoxSizeLength = x.BoxSizeLength,
                    BoxSizeWidth = x.BoxSizeWidth,
                    Quantity = x.Quantity,
                    Weight = x.Weight,
                });
            });
            result.PackageDimensionsList = packagesList;
            return result;
        }
        private async Task<List<OrderSellerItemDenormalized>> FilterSellerOrdersDB(RequestFilterOrders model, int userId)
        {
            var sql =
              @"SELECT OrderID
                      ,BuyerCompanyID
                      ,BuyerAlias
                      ,OrderPONumber
                      ,OrderCreatedDate
                      ,OrderSellerID
                      ,OrderSellerPONumber
                      ,OrderSellerItemID
                      ,ProductID
                      ,RetailPrice
                      ,OfferCost
                      ,Units
                      ,ConfirmedUnits
	                  ,OfferProductID
                      ,OfferPublishedBy
	                 ,StatusTypeID 
	                 ,StatusTypeName";
            sql = $"{ sql} from {SqlFunctionName.fn_OrdersFilterListForSeller}(@CompanyID,@UserID,@StatusTypeID)";

            IEnumerable<OrderSellerItemDenormalized> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OrderSellerItemDenormalized>(sql, new { model.CompanyId, userId, model.StatusTypeId });
                conn.Close();
            }
            return result.ToList();
        }

        private IEnumerable<OrderSellerManageView> ComputeSellerManageList(List<OrderSellerItemDenormalized> orderDenormalized)
        {
            orderDenormalized.ForEach(x => x.ShowConfirmedUnits = true);
            var result = orderDenormalized.GroupBy(osi => osi.OrderSellerId).Select(osi => new OrderSellerManageView
            {
                OrderSellerId = osi.Key,
                PONumber = osi.Select(y => y.OrderSellerPONumber).FirstOrDefault(),
                BuyerAlias = osi.Select(y => y.BuyerAlias).FirstOrDefault(),
                BuyerCompanyId = osi.Select(y => y.BuyerCompanyId).FirstOrDefault(),
                CreatedDate = osi.Select(y => y.OrderCreatedDate).FirstOrDefault(),
                TotalUnits = osi.Sum(y => y.DisplayUnits),
                TotalStyles = osi.Where(y => y.DisplayUnits > 0).Select(y => y.ProductId).Distinct().Count(),
                TotalCost = osi.Sum(y => y.DisplayUnits * y.OfferCost),
                StatusTypeId = osi.Select(y => y.StatusTypeId).FirstOrDefault(),
                StatusTypeName = osi.Select(y => y.StatusTypeName).FirstOrDefault(),
            }).ToList().OrderByDescending(o => o.CreatedDate);
            return result;
        }

        private async Task<List<OrderSellerItemDenormalized>> FilterBuyerOrdersDB(RequestFilterOrders model, int userId)
        {
            var sql = @"SELECT 
	                       OrderID
                          ,OrderPONumber
                          ,OrderCreatedDate
                          ,PaymentMethodID
                          ,PaymentMethodName
                          ,OrderSellerID
                          ,OrderSellerPONumber
                          ,SellerCompanyID
                          ,SellerAlias
                          ,UnitShippingTaxPercentage
                          ,ProductID
	                      ,OfferProductID
                          ,OfferPublishedBy
                          ,CustomerUnitPrice
                          ,UnitDiscountPercentage
                          ,LMUnitPrice
                          ,UnitShippingCost
                          ,Units
                          ,ConfirmedUnits
	                     ,StatusTypeID 
	                     ,StatusTypeName
                         ,ShowConfirmedUnits";
            sql = $"{ sql} from {SqlFunctionName.fn_OrdersFilterListForBuyer}(@CompanyID,@UserID,@StatusTypeID)";

            IEnumerable<OrderSellerItemDenormalized> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OrderSellerItemDenormalized>(sql, new { model.CompanyId, userId, model.StatusTypeId });
                conn.Close();
            }
            return result.ToList();
        }

        private IEnumerable<OrderBuyerManageView> ComputeBuyerManageList(List<OrderSellerItemDenormalized> orderDenormalized)
        {
            var result = orderDenormalized.GroupBy(o => o.OrderId)
                                            .Select(o => new OrderBuyerManageView
                                            {
                                                OrderId = o.Key,
                                                PONumber = o.Select(y => y.OrderPONumber).FirstOrDefault(),
                                                TotalStyles = o.Where(y => y.DisplayUnits > 0).Select(y => y.ProductId).Distinct().Count(),
                                                TotalUnits = o.Sum(y => y.DisplayUnits),
                                                TotalCost = o.Sum(y => y.DisplayUnits * y.LMUnitPrice),
                                                CreatedDate = o.Select(y => y.OrderCreatedDate).FirstOrDefault(),
                                                SubOrders = o.GroupBy(os => os.OrderSellerId).Select(
                                                                        os => new OrderBuyerSubOrderManageView
                                                                        {
                                                                            OrderSellerId = os.Key,
                                                                            OrderSellerPONumber = os.Select(y => y.OrderSellerPONumber).FirstOrDefault(),
                                                                            SellerAlias = os.Select(y => y.SellerAlias).FirstOrDefault(),
                                                                            SellerCompanyId = os.Select(y => y.SellerCompanyId).FirstOrDefault(),
                                                                            StatusId = os.Select(y => y.StatusTypeId).FirstOrDefault(),
                                                                            StatusTypeName = os.Select(y => y.StatusTypeName).FirstOrDefault()
                                                                        }).ToList()
                                            }).ToList().OrderByDescending(o => o.CreatedDate);
            return result;
        }

        private IEnumerable<OrderSellerView> GetOrderSellerItemsForSeller(List<OrderSellerItemDenormalized> orderDenormalized)
        {
            orderDenormalized.ForEach(x => x.ShowConfirmedUnits = true);
            var result = orderDenormalized.GroupBy(osi => osi.OrderSellerId).
                                                        Select(osi => new OrderSellerView
                                                        {
                                                            OrderSellerId = osi.Key,
                                                            OrderId = osi.Select(y => y.OrderId).FirstOrDefault(),
                                                            PONumber = osi.Select(y => y.OrderSellerPONumber).FirstOrDefault(),
                                                            SellerCompanyId = osi.Select(y => y.SellerCompanyId).FirstOrDefault(),
                                                            SellerAlias = osi.Select(y => y.SellerAlias).FirstOrDefault(),
                                                            SellerCompanyName = osi.Select(y => y.SellerCompanyName).FirstOrDefault(),
                                                            BuyerAlias = osi.Select(y => y.BuyerAlias).FirstOrDefault(),
                                                            BuyerCompanyId = osi.Select(y => y.BuyerCompanyId).FirstOrDefault(),
                                                            CreatedDate = osi.Select(y => y.OrderCreatedDate).FirstOrDefault(),
                                                            CreatedBy = osi.Select(y => y.OrderCreatedBy).FirstOrDefault(),
                                                            StatusTypeId = osi.Select(y => y.StatusTypeId).FirstOrDefault(),
                                                            StatusTypeName = osi.Select(y => y.StatusTypeName).FirstOrDefault(),
                                                            ProductItems = osi.GroupBy(op => op.OrderSellerProductId).
                                                            Select(op => new OrderProductItemSellerView
                                                            {
                                                                OrderSellerProductId = op.Key,
                                                                OfferCost = op.Select(y => y.OfferCost).FirstOrDefault(),
                                                                CurrencyId = op.Select(y => y.CurrencyId).FirstOrDefault(),
                                                                CurrencyName = op.Select(y => y.CurrencyName).FirstOrDefault(),
                                                                CurrencyRate = op.Select(y => y.CurrencyRate).FirstOrDefault(),
                                                                ProductDetails = new OrderProductDataView
                                                                {
                                                                    BrandName = op.Select(y => y.BrandName).FirstOrDefault(),
                                                                    CategoryName = op.Select(y => y.CategoryName).FirstOrDefault(),
                                                                    MainImageName = op.Select(y => y.MainImageName).FirstOrDefault(),
                                                                    DivisionName = op.Select(y => y.DivisionName).FirstOrDefault(),
                                                                    ProductId = op.Select(y => y.ProductId).FirstOrDefault(),
                                                                    ColorCode = op.Select(y => y.ColorCode).FirstOrDefault(),
                                                                    MaterialCode = op.Select(y => y.MaterialCode).FirstOrDefault(),
                                                                    ModelNumber = op.Select(y => y.ModelNumber).FirstOrDefault(),
                                                                    ProductName = op.Select(y => y.ProductName).FirstOrDefault(),
                                                                    RetailPrice = op.Select(y => y.RetailPrice).FirstOrDefault(),
                                                                    SeasonCode = op.Select(y => y.SeasonCode).FirstOrDefault(),
                                                                    SKU = op.Select(y => y.Sku).FirstOrDefault(),
                                                                    SizeTypeName = op.Select(y => y.SizeTypeName).FirstOrDefault()
                                                                },
                                                                SizeItems = op.Select(y => new OrderProductSizeItem
                                                                {
                                                                    OrderSellerItemId = y.OrderSellerItemId,
                                                                    ConfirmedUnits = y.ConfirmedUnits,
                                                                    OrderSellerProductId = y.OrderSellerProductId,
                                                                    Units = y.Units,
                                                                    SizeName = y.SizeName,
                                                                    DisplayUnits = y.DisplayUnits
                                                                }).ToList()
                                                            }).ToList()
                                                        }).ToList();
            return result;
        }

        private IEnumerable<OrderSellerBuyerView> GetOrderSellerItemsForBuyer(List<OrderSellerItemDenormalized> orderDenormalized)
        {
            var result = orderDenormalized.GroupBy(osi => osi.OrderSellerId).Select(osi => new OrderSellerBuyerView
            {
                OrderSellerId = osi.Key,
                OrderId = osi.Select(y => y.OrderId).FirstOrDefault(),
                PONumber = osi.Select(y => y.OrderSellerPONumber).FirstOrDefault(),
                SellerCompanyId = osi.Select(y => y.SellerCompanyId).FirstOrDefault(),
                SellerAlias = osi.Select(y => y.SellerAlias).FirstOrDefault(),
                StatusTypeId = osi.Select(y => y.StatusTypeId).FirstOrDefault(),
                StatusTypeName = osi.Select(y => y.StatusTypeName).FirstOrDefault(),
                ShippingTax = new ShippingTax
                {
                    Id = osi.Select(y => y.ShippingTaxId).FirstOrDefault(),
                    Days = osi.Select(y => y.ShippingDays).FirstOrDefault(),
                    DisplayName = osi.Select(y => y.ShippingName).FirstOrDefault()
                },
                CreatedDate = osi.Select(y => y.OrderCreatedDate).FirstOrDefault(),
                ProductItems = osi.GroupBy(op => op.OrderSellerProductId).Select(op => new OrderProductItemBuyerView
                {
                    OrderSellerProductId = op.Key,
                    CustomerUnitPrice = op.Select(y => y.CustomerUnitPrice).FirstOrDefault(),
                    UnitShippingTaxPercentage = op.Select(y => y.UnitShippingTaxPercentage).FirstOrDefault(),
                    UnitDiscountPercentage = op.Select(y => y.UnitDiscountPercentage).FirstOrDefault(),
                    UnitShippingCost = op.Select(y => y.UnitShippingCost).FirstOrDefault(),
                    LMUnitPrice = op.Select(y => y.LMUnitPrice).FirstOrDefault(),
                    ProductDetails = new OrderProductDataView
                    {
                        ProductId = op.Select(y => y.ProductId).FirstOrDefault(),
                        SizeTypeName = op.Select(y => y.SizeTypeName).FirstOrDefault(),
                        BrandName = op.Select(y => y.BrandName).FirstOrDefault(),
                        CategoryName = op.Select(y => y.CategoryName).FirstOrDefault(),
                        MainImageName = op.Select(y => y.MainImageName).FirstOrDefault(),
                        DivisionName = op.Select(y => y.DivisionName).FirstOrDefault(),
                        ColorCode = op.Select(y => y.ColorCode).FirstOrDefault(),
                        MaterialCode = op.Select(y => y.MaterialCode).FirstOrDefault(),
                        ProductName = op.Select(y => y.ProductName).FirstOrDefault(),
                        RetailPrice = op.Select(y => y.RetailPrice).FirstOrDefault(),
                        SeasonCode = op.Select(y => y.SeasonCode).FirstOrDefault(),
                        SKU = op.Select(y => y.Sku).FirstOrDefault(),
                        ModelNumber = op.Select(y => y.ModelNumber).FirstOrDefault(),
                        StandardMaterial = op.Select(y => y.StandardMaterial).FirstOrDefault(),
                        StandardColor = op.Select(y => y.StandardColor).FirstOrDefault()
                    },
                    SizeItems = op.Select(y => new OrderProductSizeItem
                    {
                        OrderSellerItemId = y.OrderSellerItemId,
                        ConfirmedUnits = y.ConfirmedUnits,
                        OrderSellerProductId = y.OrderSellerProductId,
                        Units = y.Units,
                        SizeName = y.SizeName,
                        DisplayUnits = y.DisplayUnits
                    }).ToList()
                }).ToList()
            }).ToList();
            return result;
        }

        private IEnumerable<Order> ComputeOrder(List<OrderSellerItemDenormalized> orderDenormalized)
        {
            var result = orderDenormalized.GroupBy(x => x.OrderId).Select(o => new Order
            {
                Id = o.Key,
                PONumber = o.Select(y => y.OrderPONumber).FirstOrDefault(),
                BuyerAlias = o.Select(y => y.BuyerAlias).FirstOrDefault(),
                BuyerCompanyName = o.Select(y => y.BuyerCompanyName).FirstOrDefault(),
                BuyerCompanyId = o.Select(y => y.BuyerCompanyId).FirstOrDefault(),
                CreatedDate = o.Select(y => y.OrderCreatedDate).FirstOrDefault(),
                PaymentMethodId = o.Select(y => y.PaymentMethodId).FirstOrDefault(),
                PaymentMethodName = o.Select(y => y.PaymentMethodName).FirstOrDefault(),
                CreatedBy = o.Select(y => y.OrderCreatedBy).FirstOrDefault(),
                CurrencyName = o.Select(y => y.CurrencyName).FirstOrDefault()
            }).ToList();
            result.ForEach(x => x.OrderSellerList = GetOrderSellerItemsForBuyer(orderDenormalized));
            return result;
        }

        private async Task<List<OrderSellerItemDenormalized>> GetOrderDenormalizedDB(int? orderId, int? orderSellerID)
        {
            var sql =
               @"SELECT OrderID
                  ,BuyerCompanyID
                  ,BuyerAlias
                  ,BuyerCompanyName
                  ,OrderPONumber
                  ,OrderCreatedDate
                  ,OrderCreatedBy
                  ,PaymentMethodID
                  ,PaymentMethodName
                  ,OrderSellerID
                  ,OrderSellerPONumber
                  ,SellerCompanyID
                  ,SellerAlias
                  ,SellerCompanyName
                  ,ShippingTaxID
                  ,ShippingName
                  ,ShippingDays
                  ,ApparelTaxUnitPercentage
                  ,NonApparelTaxUnitPercentage
                  ,CurrencyID
                  ,CurrencyName
                  ,CurrencyRate
                  ,UnitShippingTaxPercentage
                  ,OfferCost
                  ,CustomerUnitPrice
                  ,UnitDiscountPercentage
                  ,OrderSellerItemID
                  ,OrderSellerProductID
                  ,OfferProductSizeID
                  ,Units
                  ,ConfirmedUnits
                  ,SizeName
                  ,ProductID
                  ,RetailPrice
                  ,ProductName
                  ,ModelNumber
                  ,ColorCode
                  ,MaterialCode
                  ,SKU
                  ,MainImageName
                  ,CategoryName
                  ,DivisionName
                  ,BrandName
                  ,SeasonCode
                  ,SizeTypeName
                  ,StandardMaterial
                  ,StandardColor
	              ,OfferProductID
                  ,OfferPublishedBy
	             ,StatusTypeID 
	             ,StatusTypeName
	             ,StatusCreatedDate
                 ,ShowConfirmedUnits
                 ,UnitShippingCost
                 ,LMUnitPrice";
            sql = $"{sql} from {SqlFunctionName.fn_OrderSellerGetItems}(@OrderID,@OrderSellerID)";

            IEnumerable<OrderSellerItemDenormalized> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OrderSellerItemDenormalized>(sql, new { orderId, orderSellerID });
                conn.Close();
            }
            return result.ToList();
        }

        public Task<IEnumerable<bool>> VerifyCartItemsBeforeSubmit()
        {
            throw new NotImplementedException();
        }

        private DataTable GetOrderConfirmUnitsDataTable(IEnumerable<ConfirmUnit> list)
        {
            var dt = list.AsEnumerable().Select((dr, index) => new
            {
                Id = dr.Id,
                Units = dr.Unit
            }).ToList().ConvertToDataTable();

            dt.SetTypeName("dbo.OrderConfirmUnits");

            return dt;
        }

        private DataTable GetOrderSellerPackagesDataTable(IEnumerable<OrderSellerPackageInsert> list)
        {
            var dt = list.AsEnumerable().Select((dr, index) => new
            {
                Length = dr.BoxSizeLength,
                Width = dr.BoxSizeWidth,
                Depth = dr.BoxSizeDepth,
                Quantity = dr.Quantity,
                Weight = dr.Weight
            }).ToList().ConvertToDataTable();

            dt.SetTypeName("dbo.OrderSellerPackages");

            return dt;
        }

        #endregion
    }
}
