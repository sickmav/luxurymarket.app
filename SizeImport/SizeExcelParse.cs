﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System;

namespace SizeImport
{
    public static class SizeExcelParse
    {
        public static DataSet ParseWorkSheet(Stream stream)
        {
            using (var package = new ExcelPackage())
            {
                package.Load(stream);

                var workSheet = package.Workbook.Worksheets.First();

                if (workSheet == null) return null;

                var dt = GetDataSet(workSheet);

                return dt;
            }
        }

        /// <summary>
        /// Get all tables from worksheet
        /// All tables are separated by an empty row
        /// </summary>
        /// <param name="workSheet"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(ExcelWorksheet workSheet)
        {
            var lastColumnIndex = GetLastColumnIndex(workSheet);
            var lastRowIndex = GetLastRowIndex(workSheet);

            ClearExcelWorksheet(workSheet, lastRowIndex, lastColumnIndex);

            var startColumnIndex = workSheet.Dimension.Start.Column;
            var startRowIndex = workSheet.Dimension.Start.Row;

            var dataset = new DataSet();
            DataTable dtExcel = null;

            for (int rowNum = startRowIndex; rowNum <= lastRowIndex; rowNum++)
            {
                var wsRow = workSheet.Cells[rowNum, startColumnIndex, rowNum, lastColumnIndex];

                if (wsRow.IsEmptyRange())
                {
                    if (dtExcel?.Rows.Count > 0)
                    {
                        dtExcel = ClearEmptyColumns(dtExcel);
                        if (dtExcel?.Rows.Count > 0)
                            dataset.Tables.Add(dtExcel);
                        dtExcel = null;
                    }
                    continue;
                }
                if (dtExcel == null)
                {
                    //start new datatable - set column names
                    dtExcel = new DataTable();
                    var headerColumns = GetColumnHeaders(workSheet, rowNum, startColumnIndex, lastColumnIndex);
                    headerColumns.ToList().ForEach(x => dtExcel.Columns.Add(x));
                }
                else
                {
                    var dtRow = dtExcel.NewRow();
                    for (var col = startColumnIndex; col <= lastColumnIndex; col++)
                    {
                        //if (col < dtExcel.Columns.Count)  //this should not be neccesary
                        dtRow[col - 1] = workSheet.Cells[rowNum, col, rowNum, col].Value?.ToString().Trim();
                    }
                    dtExcel.Rows.Add(dtRow);
                }
            }

            if (dtExcel != null)
            {
                dataset.Tables.Add(dtExcel);
            }

            return dataset;
        }

        private static string[] GetColumnHeaders(ExcelWorksheet workSheet, int rowNum, int startColumnIndex, int lastColumnIndex)
        {
            var values = new string[lastColumnIndex - startColumnIndex + 1];
            for (var col = startColumnIndex; col <= lastColumnIndex; col++)
            {
                var cell = workSheet.Cells[rowNum, col, rowNum, col].Value?.ToString();
                values[col - 1] = string.IsNullOrWhiteSpace(cell) ? $"__column_{rowNum}_{col}" : cell.Trim().ToLower();
            }
            return values;
        }

        private static void ClearExcelWorksheet(ExcelWorksheet workSheet, int lastRowNumber, int lastColumnNumber)
        {
            workSheet.DeleteColumn(lastColumnNumber + 1, workSheet.Dimension.End.Column - lastColumnNumber);
            workSheet.DeleteRow(lastRowNumber + 1, workSheet.Dimension.End.Row - lastRowNumber);
        }

        private static DataTable ClearEmptyColumns(DataTable dtExcel)
        {
            if (dtExcel == null) return dtExcel;
            string[] columnNames = (from dc in dtExcel.Columns.Cast<DataColumn>()
                                    where dc.ColumnName.ToLower().StartsWith("__column_")
                                    select dc.ColumnName).ToArray();
            foreach (var columnName in columnNames)
            {
                if (dtExcel.Rows.OfType<DataRow>().All(r => r.IsNull(columnName)))
                    dtExcel.Columns.Remove(columnName);
            }
            return dtExcel;
        }

        private static bool HasNull(this DataTable table)
        {
            foreach (DataColumn column in table.Columns)
            {
                if (table.Rows.OfType<DataRow>().Any(r => r.IsNull(column)))
                    return true;
            }

            return false;
        }

        private static int GetLastColumnIndex(ExcelWorksheet workSheet)
        {
            var lastIndex = 0;
            for (var col = 1; col <= workSheet.Dimension.End.Column; col++)
            {
                if (!workSheet.Cells[1, col, workSheet.Dimension.End.Row, col].IsEmptyRange())
                    lastIndex = col;
            }
            return lastIndex;
        }

        private static int GetLastRowIndex(ExcelWorksheet workSheet)
        {
            var lastIndex = 0;
            for (var index = 1; index <= workSheet.Dimension.End.Row; index++)
            {
                if (!workSheet.Cells[index, 1, index, workSheet.Dimension.End.Column].IsEmptyRange())
                    lastIndex = index;
            }
            return lastIndex;
        }
    }

    public static class ExcelExtensions
    {
        public static bool IsEmptyRange(this ExcelRange range)
        {
            return range == null || range.All(x => string.IsNullOrWhiteSpace(x.Value?.ToString()));
        }
    }
}
