# valid versions are [2.0, 3.5, 4.0, 12.0, 14.0]
$dotNetVersion = "14.0"
$regKey = "HKLM:\software\Microsoft\MSBuild\ToolsVersions\$dotNetVersion"
$regProperty = "MSBuildToolsPath"
$Source = $PSScriptRoot+"\build\apps" 
$ZipName = "build"+(Get-Date).ToString('yyyyMMdd_HHmm')+"-beta.zip"
$Target = $PSScriptRoot+"\build\" + $ZipName

rm -r $Source

$msbuildExe = join-path -path (Get-ItemProperty $regKey).$regProperty -childpath "msbuild.exe"

&$msbuildExe LuxuryMarketApp.sln /p:DeployOnBuild=true /p:PublishProfile=Beta /m

if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"  

Write-Host "Zipping"

sz a -mx=9 $Target $Source

Write-Host "FTP uploading " $ZipName

# create the FtpWebRequest and configure it
$ftp = [System.Net.FtpWebRequest]::Create("ftp://w030.mejix.net/builds/"+$ZipName)
$ftp = [System.Net.FtpWebRequest]$ftp
$ftp.Method = [System.Net.WebRequestMethods+Ftp]::UploadFile
$ftp.Credentials = new-object System.Net.NetworkCredential("LuxuryMarket","LM`$Wb40")
$ftp.UseBinary = $true
$ftp.UsePassive = $false
# read in the file to upload as a byte array
$content = [System.IO.File]::ReadAllBytes($Target)
$ftp.ContentLength = $content.Length
# get the request stream, and write the bytes into it
$rs = $ftp.GetRequestStream()
$rs.Write($content, 0, $content.Length)
# be sure to clean up after ourselves
$rs.Close()
$rs.Dispose()