﻿using System.Threading;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;

namespace Luxury.Ftp.Implementations
{
    class FtpUploadWorker:IWorker
    {
        private readonly IWorkerConfig _workerConfig;
        private readonly IFtpUploadAgent _ftpUploadAgent;

        private Task _workerTask = null;

        public FtpUploadWorker(IWorkerConfig workerConfig, IFtpUploadAgent ftpUploadAgent)
        {
            _workerConfig = workerConfig;
            _ftpUploadAgent = ftpUploadAgent;
        }

        public void Start(CancellationToken cancellationToken)
        {
            if (_workerTask != null && _workerTask.Status == TaskStatus.Running)
            {
                return;
            }
            _workerTask = Task.Run(() => Worker(cancellationToken));
        }

        private void Worker(CancellationToken cancellationToken)
        {
            for (;;)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                _ftpUploadAgent.UploadFromQueue();
                Task.Delay(_workerConfig.DelaySeconds * 1000, cancellationToken).Wait();
            }
        }
    }
}
