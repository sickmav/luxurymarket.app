﻿using System.ServiceProcess;
using Autofac;
using Gs1.Vics.Contracts.Factories;
using Gs1.Vics.Implementations.Factories;
using Luxury.Ftp.Implementations;
using LuxuryApp.Contracts.Access;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Processors.Access;
using LuxuryApp.Processors.Agents;
using LuxuryApp.Processors.Services;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.NotificationService;

namespace Luxury.Ftp
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();

            //builder.RegisterInstance(configuration).AsSelf(); // needed for help area configuration


            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>()
                .As<IConnectionStringProvider>();

            builder.RegisterType<NullContextProvider>()
                .As<IContextProvider>().InstancePerDependency();
            builder.RegisterType<OfferRepository>()
                .As<IOfferRepository>();
            builder.RegisterType<CurrencyService>()
                .As<ICurrencyService>();
            builder.RegisterType<CartRepository>()
                .As<ICartRepository>();
            builder.RegisterType<TrivialCompanyAccess>()
                .As<ICompanyAccess>();
            builder.RegisterType<CartService>()
                .As<ICartService>();

            builder.RegisterType<ProductService>()
                .As<IProductService>();
            builder.RegisterType<ProductRepository>()
                .As<IProductRepository>();
            builder.RegisterType<ShippingRepository>()
                .As<IShippingRepository>();
            builder.RegisterType<ShippingService>()
                .As<IShippingService>();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();

            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            builder.RegisterType<CustomerService>().As<ICustomerService>();

            builder.RegisterType<OfferImportService>().As<IOfferImportService>();
            builder.RegisterType<OfferImportRepository>().As<IOfferImportRepository>();

            builder.RegisterType<CompanyService>().As<ICompanyService>();
            builder.RegisterType<CompanyRepository>().As<ICompanyRepository>();

            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<OrderRepository>().As<IOrderRepository>();

            builder.RegisterType<AddressService>().As<IAddressService>();
            builder.RegisterType<AddressRepository>().As<IAddressRepository>();

            builder.RegisterType<RegistrationRequestService>().As<IRegistrationRequestService>();

            builder.RegisterType<X2EdiSerializeServiceFactory>().As<IEdiSerializeServiceFactory>();
            builder.RegisterType<FtpRepository>().As<IFtpRepository>();

            builder.RegisterType<OrderTrackingService>().As<IOrderTrackingService>();
            builder.RegisterType<OrderBookingService>().As<IOrderBookingService>();

            builder.RegisterType<FtpUploadAgent>().As<IFtpUploadAgent>();

            builder.RegisterType<EmailService<EmailModel>>().As<INotificationHandler<EmailModel>>();


            builder.RegisterType<WorkerConfigAppSettings>().As<IWorkerConfig>();
            builder.RegisterType<FtpUploadWorker>().As<IWorker>();

            //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //// Register your MVC controllers.
            //builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //// OPTIONAL: Register model binders that require DI.
            //builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //builder.RegisterModelBinderProvider();

            //// OPTIONAL: Register web abstractions like HttpContextBase.
            //builder.RegisterModule<AutofacWebTypesModule>();

            //// OPTIONAL: Enable property injection in view pages.
            //builder.RegisterSource(new ViewRegistrationSource());

            //// OPTIONAL: Enable property injection into action filters.
            //builder.RegisterFilterProvider();

            builder.RegisterType<ServiceFtp>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
